(defproject nnnamespace "MASTER"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [bidi "2.1.2"]
                 [buddy "2.0.0" :exclude [buddy/buddy-auth]]
                 [buddy/buddy-auth "2.1.0"]
                 ;; [cc.qbits/spandex "0.6.2"]               ;; use algolia service most of the time
                 [ch.qos.logback/logback-classic "1.1.7" :exclusions [org.slf4j/slf4j-api]]
                 [cheshire "5.8.0" :exclusions [com.fasterxml.jackson.core/jackson-core]]
                 [cljs-css-modules "0.2.1"]
                 [cljs-http "0.1.44"]
                 [cljs-web3 "0.19.0-0-11"]
                 [cljsjs/firebase "4.0.0-0"]
                 [cljsjs/react "16.2.0-3"]
                 [cljsjs/react-dom "16.2.0-3"]
                 [cljsjs/react-dom-server "16.2.0-3"]
                 [com.cognitect/transit-clj "0.8.313"]
                 [com.datomic/datomic-free "0.9.5697"]
                 [com.draines/postal "2.0.2"]
                 [com.fasterxml.jackson.dataformat/jackson-dataformat-cbor "2.9.3"]
                 [com.fasterxml.jackson.dataformat/jackson-dataformat-smile "2.9.3"]
                 [com.google.guava/guava "22.0"]
                 [com.stuartsierra/component "0.3.2"]
                 [com.taoensso/timbre "4.10.0"]
                 [com.walmartlabs/lacinia "0.25.0" :exclusions [clojure-future-spec]]
                 [com.walmartlabs/lacinia-pedestal "0.7.0" :ebclusions [ring/ring-core]]
                 [commons-codec "1.11"]
                 [cprop "0.1.11"]
                 [css "MASTER"]
                 [firebase-cljs "4.0.0-0"]
                 [tick "0.3.5"]
                 [funcool/promesa "1.9.0"]
                 [funcool/httpurr "1.0.0"]

                 [hiccup "1.0.5"]
                 [hiccups "0.3.0"]
                 [io.pedestal/pedestal.jetty "0.5.3"]
                 [io.pedestal/pedestal.service "0.5.3" :exclusions [ring/ring-core]]
                 [io.pedestal/pedestal.service "0.5.3"]
                 [io.pedestal/pedestal.service-tools "0.5.3" :exclusions [ring/ring-core]]
                 [com.cognitect/pedestal.vase "0.9.3"]
                 [kibu/pushy "0.3.8"]
                 [org.clojure/clojure "1.10.0" :exclusions [org.clojure/spec.alpha]]
                 [org.clojure/clojurescript "1.10.339" :exclusions [com.google.code.findbugs/jsr305]]
                 [org.clojure/spec.alpha "0.1.143"]
                 [org.clojure/core.async "0.4.474" :exclusions [org.clojure/tools.reader]]
                 [org.clojure/test.check "0.10.0-alpha2"]
                 [org.clojure/tools.reader "1.3.0"]
                 [org.eclipse.jetty/jetty-servlets "9.4.0.v20161208"]
                 [org.roman01la/citrus "3.0.1" :exclusions [sablono]]
                 [org.slf4j/jcl-over-slf4j "1.7.22"]
                 [org.slf4j/jul-to-slf4j "1.7.22"]
                 [org.slf4j/log4j-over-slf4j "1.7.22"]
                 [pear "MASTER"]
                 [ring/ring-core "1.6.3" :exclusions [org.clojure/clojure org.clojure/tools.reader crypto-random crypto-equality]]
                 [rum "0.11.2" :exclusions [cljsjs/react sablono cljsjs/react-dom]]
                 [sablono "0.8.3"]
                 [clj-algolia "MASTER"]
                 [commons-io "2.6" :exclusions [org.clojure/clojure]]
                 ]
  :pedantic? :abort
  :min-lein-version "2.0.0"
  :source-paths ["src/clj" "src/cljc" "src/cljs"]
  :resource-paths ["config", "resources"]
  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  :cljfmt {:indents ~(clojure.edn/read-string (slurp ".cljfmt.edn"))}
  :java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.3"]]
  :buster {:files #{"resources/public/js/site.js"
                    "resources/public/css/site.css"}
           :manifest "resources/manifest.json"}
  :garden {:builds [{:id           "screen"
                     :source-paths ["src/clj"]
                     :stylesheet   styles.screen/screen
                     :compiler     {:output-to     "resources/public/css/site.css"
                                    :vendors       [:moz :webkit]
                                    :auto-prefix   #{:transform
                                                     :appearance
                                                     :animation
                                                     :filter
                                                     :background-size
                                                     :user-select
                                                     :font-feature-settings}
                                    :pretty-print? true}}
                    {:id           "production"
                     :source-paths ["src/clj"]
                     :stylesheet   styles.screen/screen
                     :compiler     {:output-to     "resources/public/css/site.css"
                                    :vendors       [:moz :webkit]
                                    :auto-prefix   #{:transform
                                                     :appearance
                                                     :animation
                                                     :filter
                                                     :background-size
                                                     :user-select
                                                     :font-feature-settings}
                                    :pretty-print? false}}]}
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "nnnamespace.main/run-dev"]
                             "run-test" ["trampoline" "run" "-m" "nnnamespace.eftest-runner/main"]}
                   :source-paths ["dev"
                                  "test/clj" "test/cljc" "test/cljs"
                                  "src/cljc" "src/cljs" "src/clj"]
                   :plugins [[lein-cljsbuild "1.1.7"]
                             [lein-cljfmt "0.6.1" :exclusions [org.clojure/clojure org.clojure/tools.reader org.clojure/tools.cli]]
                             [lein-figwheel "0.5.14" :exclusions [org.clojure/clojure]]
                             [lein-garden "0.3.0" :exclusions [org.clojure/clojure
                                                               org.apache.commons/commons-compress]]
                             [lein-buster "0.2.0"]]
                   :dependencies [[com.stuartsierra/component.repl "0.2.0"]
                                  [eftest "0.4.1"]
                                  [io.pedestal/pedestal.service-tools "0.5.3"]
                                  [org.clojure/tools.namespace "0.2.11"]
                                  [devcards "0.2.4"]]
                   :repl-options {:init-ns user}}

             :uberjar {:aot [nnnamespace.main]
                       :uberjar-name "app.jar"}}

  :cljsbuild {:builds [{:id           "dev"
                        :source-paths ["src/cljs" "src/cljc" "test"]
                        :compiler     {:main                 "nnnamespace.core"
                                       :closure-defines    {"goog.DEBUG" true}
                                       :asset-path           "cljs/dev_out"
                                       :parallel-build       true
                                       :output-to            "resources/public/cljs/site_dev.js"
                                       :output-dir           "resources/public/cljs/dev_out"
                                       :source-map-timestamp true}}
                       {:id           "devcards"
                        :source-paths ["src/cljs" "src/cljc" "test"]
                        :figwheel     {:devcards true}
                        :compiler     {:main                 "nnnamespace.ui.main-test"
                                       :closure-defines    {"goog.DEBUG" true}
                                       :asset-path           "cljs/devcards_out"
                                       :parallel-build       true
                                       :output-to            "resources/public/cljs/site_devcards.js"
                                       :output-dir           "resources/public/cljs/devcards_out"
                                       :source-map-timestamp true}}
                       {:id           "browser-test"
                        :source-paths ["test/cljs" "test/cljc" "src/cljs" "src/cljc"]
                        :compiler     {:main                 nnnamespace.browser-run-tests
                                       :optimizations        :none
                                       :recompile-dependents true
                                       :parallel-build       true
                                       :compiler-stats       true
                                       :asset-path           "cljs/test/karma/out"
                                       :output-to            "resources/public/cljs/test/karma/test.js"
                                       :output-dir           "resources/public/cljs/test/karma/out"}}
                       {:id           "production"
                        :source-paths ["src/cljs" "src/cljc"]
                        :compiler     {:main               nnnamespace.core
                                       :closure-defines    {"goog.DEBUG" false}
                                       :parallel-build     true
                                       :verbose            true
                                       :asset-path         "js/production/out"
                                       :output-to          "resources/public/js/site.js"
                                       :output-dir         "resources/public/js/production/out"
                                       :source-map "resources/public/js/site.js.map"
                                       :optimizations      :advanced
                                       :pseudo-names       false
                                       :pretty-print       true}}
                       ]}

  :figwheel {:css-dirs ["resources/public/css"]}

  :main ^{:skip-aot true} nnnamespace.main)
