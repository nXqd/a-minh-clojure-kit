(ns dev
  "Tools for interactive development with the REPL. This file should
  not be included in a production build of the application.
  The system under development is :refer'd from
  `com.stuartsierra.component.repl/system`.
  Call `(reset)` to reload modified code and (re)start the system.
  See also https://github.com/stuartsierra/component.repl"
  (:require
   [clojure.java.io :as io]
   [clojure.java.javadoc :refer [javadoc]]
   [clojure.pprint :refer [pprint]]
   [clojure.reflect :refer [reflect]]
   [clojure.repl :refer [apropos dir doc find-doc pst source]]
   [clojure.set :as set]
   [clojure.string :as string]
   [clojure.test :as test]
   [clojure.tools.namespace.repl :refer [refresh refresh-all clear]]
   [com.stuartsierra.component :as component]
   [com.stuartsierra.component.repl :refer [reset set-init start stop system]]
   [nnnamespace.infra.system :as isystem]
   [nnnamespace.interfaces.web.index :as web]
   [nnnamespace.infra.pedestal.interceptor.rum :as ir]))

;; Do not try to load source code from 'resources' directory
(clojure.tools.namespace.repl/set-refresh-dirs "dev" "src" "test")

(set-init isystem/new-system)

(defn- gen-index
  [env]
  (let [file-name (condp = env
                    :devcards "resources/public/cards.html"
                    "resources/public/index.html")
        html (web/index nil nil {:scripts (ir/scripts)
                                 :env env})]
    (io/make-parents file-name)
    (spit file-name html)))

(defn generate-index
  "An entry to generate index.html / cards.html file based on production index.clj"
  []
  (doall
   (->> #{:devcards :dev} (map gen-index))))
