(ns nnnamespace.infra.system
  (:require
   [com.stuartsierra.component :as component]
   [io.pedestal.http :as http]
   [nnnamespace.infra.config :as iconfig]
   [nnnamespace.infra.datomic :as idatomic]
   [nnnamespace.infra.jvm :as ijvm]
   [nnnamespace.infra.log :as ilog]
   [nnnamespace.infra.pedestal :as ipedestal]
   [nnnamespace.infra.pedestal.vase :as ivase]))

(def vase-spec (ivase/vase-spec))

(defn new-system
  [_]
  (component/system-map
   :log (ilog/new-log {})

   :datomic
   (component/using
    (idatomic/new-datomic {:uri (:datomic-uri vase-spec)})
    [:log])

   :exception-catcher
   (component/using
    (ijvm/new-jvm)
    [:log])

   :config
   (component/using
    iconfig/config
    [:log])

   :pedestal
   (component/using
    (ipedestal/new-pedestal {:vase-spec vase-spec})
    [:config :datomic])))

