(ns nnnamespace.infra.pedestal
  (:require
   [clojure.set :as cset]
   [com.cognitect.vase :as vase]
   [com.stuartsierra.component :as component]
   [io.pedestal.http :as http]
   [io.pedestal.http.body-params :as body-params]
   [io.pedestal.http.jetty.util :as jetty-util]
   [io.pedestal.http.route :as route]
   [io.pedestal.interceptor.helpers :as interceptor]
   [nnnamespace.infra.pedestal.interceptor :as iinterceptor]
   [nnnamespace.infra.pedestal.interceptor.rum :as rum]
   [nnnamespace.infra.pedestal.vase :as ivase]
   [nnnamespace.interfaces.api.core :as api]
   [nnnamespace.interfaces.graphql.core :as gqlcore]
   [taoensso.timbre :as log])
  (:import
   (org.eclipse.jetty.servlets DoSFilter)
   (org.eclipse.jetty.server.handler.gzip GzipHandler)))

(def ^:dynamic *url-for* (atom nil))

(defn shared-interceptors
  [& {:keys [config]}]
  [iinterceptor/log-request
   (iinterceptor/inject-config config)])

(def web-interceptors
  [http/html-body
   rum/rum-interceptor])

(defn api-interceptors [datomic-conn]
  [http/json-body
   (body-params/body-params)
   (iinterceptor/inject-datomic datomic-conn)])

(defn frontend-routes
  [datomic-conn]
  #{["/" :get web-interceptors :route-name :route/home-page]
    ["/api/login" :post (conj (api-interceptors datomic-conn) `api/login-post)]})

(defn routes
  [vase-spec datomic-conn]
  ^{:doc "Frontend routes and graphql routes"}
  (let [gql-routes (gqlcore/routes datomic-conn)
        fe-routes (frontend-routes datomic-conn)]
    (concat
     (route/expand-routes (cset/union fe-routes gql-routes))
     (ivase/vase-routes vase-spec))))

(defn service
  [port routes]
  {::http/routes routes
   ;; custom secure-headers
   ::http/secure-headers {:content-security-policy-settings {:object-src "none"}}
   ;; use "graphiql" to activate graphiql
   ::http/resource-path "/public"
   ::http/enable-csrf nil
   ::http/type :jetty
   ::http/join? false
   ;;::http/host "localhost"
   ::http/port port
   ::http/allowed-origins {:creds true :allowed-origins (constantly true)}
   ;; Options to pass to the container (Jetty)
   ::http/container-options {:h2c? true
                             :h2? false
                             ;:keystore "test/hp/keystore.jks"
                             ;:key-password "password"
                             ;:ssl-port 8443
                             :ssl? false
                             :context-configurator (fn [c]
                                                     (let [gzip-handler (GzipHandler.)]
                                                       (.setGzipHandler c gzip-handler)
                                                       ;; You can also add Servlet Filters...
                                                       (jetty-util/add-servlet-filter c {:filter DoSFilter})
                                                       c))}})

(defn- add-interceptors
  [service & {:keys [config]}]
  (let [interceptors (-> (::http/interceptors service)
                         (concat (shared-interceptors :config config))
                         vec)]
    (assoc service ::http/interceptors interceptors)))

(defn- dev-service
  [service & {:keys [config]}]
  (-> service
      (merge {::http/enable-csrf nil})
      http/default-interceptors
      http/dev-interceptors
      (add-interceptors :config config)))

(defn- prod-service
  [service & {:keys [config]}]
  (-> service
      (merge {::http/join? true})
      http/default-interceptors
      (add-interceptors :config config)))

(defrecord Pedestal
  [config datomic vase-spec]
  component/Lifecycle
  (start [component]
         (let [{:keys [env webserver-port]} config
               routes (routes vase-spec (:connection datomic))
               service (as-> (service webserver-port routes) $
                         (if (= env :prod)
                           (prod-service $ :config config)
                           (dev-service $ :config config)))
               container (-> service
                             http/create-server
                             http/start)]
           (log/info "Webserver starting at port:" webserver-port)
           (reset! *url-for* (route/url-for-routes routes))
           (vase/ensure-schema vase-spec)
           (assoc component :service prod-service
                  :container container)))
  (stop [component]
        (do (http/stop (:container component))
          (log/info "Webserver is stopped successfully.")
          (dissoc component :container))))

(defn new-pedestal
  [opts]
  (map->Pedestal opts))

