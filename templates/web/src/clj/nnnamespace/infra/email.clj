(ns nnnamespace.infra.email
  (:require
   [nnnamespace.infra.config :as iconf]
   [postal.core :as postal]))

(def config (merge (:amazon-simple-mail iconf/config)
                   {:tls true
                    :port 587}))

;; create customize verification email
(defn send
  [{:keys [from to subject body]}]
  (postal/send-message config
                       {:from from
                        :to to
                        :subject subject
                        :body body}))

(comment
  config
  (send {:from "nxqd.inbox@gmail.com"
         :to "success@simulator.amazonses.com"
         :subject "hallo"
         :body "Body"}))

