(ns nnnamespace.infra.elasticsearch
  (:require
   [com.stuartsierra.component :as component]
   [nnnamespace.infra.config :as iconf]
   [qbits.spandex :as s]
   [taoensso.timbre :as log]))

(defn es-host
  []
  (get-in iconf/config [:elasticsearch :host]))

(defn es-client
  []
  (s/client {:hosts [(es-host)]}))

(defrecord Elasticsearch [log]
  component/Lifecycle
  (start [this]
         (log/info (str "Elasticsearch host: " (es-host)))
         (assoc this :client (es-client)))
  (stop [this]
        this))

(defn new-es
  [& opts]
  (map->Elasticsearch opts))

(comment
  (def c (es-client))

  (s/request c {:url [:entries :entry :_search]
                :method :get
                :body {:query {:match_all {}}}}))