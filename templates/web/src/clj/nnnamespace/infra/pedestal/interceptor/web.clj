(ns nnnamespace.infra.pedestal.interceptor.web
  (:require
   [nnnamespace.interfaces.web.index :as web]
   [nnnamespace.infra.routes-map :as rm]
   [ring.util.response :as ring-resp]))

(defn server-meta-props
  [{:keys [route request]}]
  (let [{:keys [route-name]} route]
    {:route-name route-name}))

(def web-interceptor
  "This interceptor returns a server rendered html if a request is not an API call."
  {:name ::web-interceptor
   :leave (fn [{:keys [response route] :as context}]
            (let [{:keys [route-name]} route
                  {:keys [edn-data]} response
                  component (rm/get-page route-name)
                  styles (rm/get-styles route-name)
                  edn-data (assoc edn-data :styles styles)
                  response (web/index component edn-data (server-meta-props context))]
              (assoc context :response (ring-resp/response response))))})
