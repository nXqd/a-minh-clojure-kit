(ns nnnamespace.infra.pedestal.interceptor.rum
  (:require
   [citrus.core :as citrus]
   [clojure.spec.alpha :as s]
   [nnnamespace.infra.analytics :as ana]
   [nnnamespace.infra.routes-map :as rm]
   [nnnamespace.interfaces.web.index :as web]
   [nnnamespace.ui.app :as app]
   [ring.util.response :as ring-resp]
   [rum.core :as rum]))

(defn server-meta-props
  [{:keys [route request]}]
  (let [{:keys [route-name]} route]
    {:route-name route-name}))

(s/fdef scripts
  :ret string?)
(defn scripts
  []
  (ana/analytics-scripts))

(s/fdef make-reconciler
  :args (s/cat :route-name keyword?))
(defn- make-reconciler
  [route-name]
  (citrus/reconciler
   {:state (atom {})
    :resolvers {:router (constantly {:handler route-name})
                :route/home-page (constantly nil)}}))

(defn create-response
  "Create html response with rum reconciler, state and opts."
  [reconciler state opts]
  (-> reconciler
      (app/app-ui)
      (rum/render-html)
      (web/index state opts)
      ring-resp/response))

(def rum-interceptor
  "This interceptor returns a server rendered html if a request is not an API call."
  {:name ::rum-interceptor
   :enter (fn [{:keys [route] :as context}]
            (let [{:keys [route-name]} route
                  styles (rm/get-styles route-name)
                  reconciler (make-reconciler route-name)
                  response (create-response reconciler {} {:styles styles
                                                           :scripts (scripts)})]
              (assoc context :response response)))})

