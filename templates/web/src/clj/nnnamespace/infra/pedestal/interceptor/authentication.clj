(ns nnnamespace.infra.pedestal.interceptor.authentication
  (:require
   [buddy.auth.backends.httpbasic :refer [http-basic-backend]]
   [buddy.auth.backends.token :refer [jws-backend]]
   [buddy.auth.middleware :as middleware]
   [buddy.hashers :as hashers]
   [buddy.sign.jwt :as jwt]
   [cheshire.core :as json]
   [clj-time.core :as time :refer [now plus seconds months]]
   [clojure.spec.alpha :as s]
   [io.pedestal.interceptor :as i]
   [nnnamespace.domain.user.core :as user]
   [nnnamespace.infra.config :as iconfig]
   [nnnamespace.specs.user :as users]
   [pear.http.response :as response]
   [pear.spec.core :as us]
   [taoensso.timbre :as log]))

(def unauthenticated
  {:status 400
   :headers {}
   :body
   {:errors
    [{:error :unauthenticated
      :description "You must be authenticated to make this request."}]}})

(def unauthenticated2
  {:status 400
   :headers {}
   :body
   {:errors
    [{:error :invalid
      :description "User input is invalid"}]}})

(defonce jwt-expires-in (get-in iconfig/config [:jwt :expires-in]))
(defonce jwt-secret (get-in iconfig/config [:jwt :secret]))

(defn get-identity-middleware
  "Identity middleware from buddy"
  [request jwt-secret]
  (middleware/authenticate-request
   request
   [(jws-backend {:secret jwt-secret})]))

(s/fdef generate-token
  :args (s/cat :claim string? :jwt-expires-in (s/int-in 0 100) :jwt-secret string?)
  :ret string?)
(defn- generate-token
  "Generate jwt token with claim jwt-expires-in and jwt-secret"
  [claim jwt-secret jwt-expires-in]
  (let [claims {:user claim
                :exp (time/plus (time/now) (time/hours jwt-expires-in))}]
    (jwt/sign claims jwt-secret {:alg :hs512})))

(s/fdef ok-response
  :args (s/cat :token string?)
  :ret nil)
(defn ok-response
  "Generates ok response with token"
  [token]
  (response/ok {:token token}))

(defn login
  [{:keys [data conn] :as req}]
  (let [{:keys [email password]} data]
    (if (true? (user/authenticated? conn email password))
      (let [token (generate-token email jwt-secret jwt-expires-in)]
        (ok-response token))
      unauthenticated)))

(s/def :signup/email ::us/email-type)
(s/def :signup/password ::users/password)
(s/def :signup/input (s/keys :req-un [:signup/email :signup/password]))

(comment
  (s/valid? :signup/input {:email "new@email.com"
                           :password "12345678abc"}))

(defn- erase-password
  "Erases user's password while logging"
  [input]
  (assoc input :password "XXXXXXXX"))

(defn signup-valid-handler
  "Returns response with token if user is created
   otherwise return unauthenticated response."
  [conn {:keys [email password]} jwt-secret jwt-expires-in]
  (let [hashed-password (hashers/derive password)]
    (if (user/create conn [{:user/email email
                            :user/hashed-password hashed-password}])
      (let [token (generate-token email jwt-secret jwt-expires-in)]
        (ok-response token))
      unauthenticated2)))

(defn valid-user-creation?
  [conn {:keys [email password] :as data}]
  (if (not (true? (s/valid? :signup/input data)))
    (do (log/info "User input is not correct")
      false)
    (if (not (nil? (user/by-email conn email '[:user/email])))
      (do (log/info (str "User with the email " email " is already existed."))
        false)
      true)))

(defn signup
  [{:keys [data conn] :as req}]
  (let [{:keys [email password]} data]
    (if (not (valid-user-creation? conn data))
      unauthenticated2
      (signup-valid-handler conn data jwt-secret jwt-expires-in))))

;; Add identity to request
;; Identity can be nil in the case of anonymous user
(def authentication-interceptor
  (i/interceptor
   {:name ::authentication-interceptor
    :enter
    (fn [{:keys [request] :as context}]
      (let [identity (get-identity-middleware request jwt-secret)]
        (assoc-in context [:request :identity] identity)))}))
