(ns nnnamespace.infra.pedestal.interceptor.authorization
  (:require
   [buddy.auth :as bauth]
   [buddy.auth.accessrules :as baccess-rules]
   [clojure.spec.alpha :as s]
   [io.pedestal.interceptor :refer [interceptor]]
   [io.pedestal.interceptor.chain :as bchain]
   [ring.util.response :as ring]))

(def unauthorized
  {:status 401
   :headers {}
   :body
   {:errors
    [{:error :unauthorized
      :description "You must be authenticated to make this request."}]}})

(def forbidden
  {:status 403
   :headers {}
   :body
   {:errors
    [{:error :forbidden
      :description "You are not allowed to make this request."}]}})

(s/def ::identity any?)
(s/fdef request-unauthorized?
  :args (s/cat :request (s/keys :req-un [::identity]))
  :ret boolean?
  :fn (s/or :unauthorized? #(and (= true (:ret %)) (nil? (-> % :args :request :identity)))
            :authorized? #(and (= false (:ret %)) (not (nil? (-> % :args :request :identity))))))
(defn request-unauthorized?
  [request]
  (nil? (:identity request)))

(defn on-error
  "Authorization error handler."
  [request value]
  (cond
    (ring/response? value)
    value
    (request-unauthorized? request)
    unauthorized
    :else forbidden))

(defn authenticated-handler
  "Check that the request is authenticated.
   It checks [:request :identity] in context for identity. Check authentication interceptor for more information."
  [request]
  (if (bauth/authenticated? request)
    true
    (baccess-rules/error unauthorized)))

(def access-rules
  "The access rules for the API."
  [{:uri "/api/v1/auth"
    :handler {:and [authenticated-handler]}
    :request-method :post}])

(defn- authorize-request
  "Return `[match result]`."
  [access-rules request]
  (if-let [match (#'baccess-rules/match-access-rules access-rules request)]
    (let [res (#'baccess-rules/apply-matched-access-rule match request)]
      [match res])
    [nil true]))

(defn unmatch-handler
  "Handles unmatched request."
  [{:keys [request] :as context} {:keys [policy] :as opts}]
  (case policy
    :allow context
    :reject (-> (bchain/terminate context)
                (update :response #(baccess-rules/handle-error
                                    (baccess-rules/error nil) request opts)))))

(defn match-handler
  "Handle matched request."
  [{:keys [request] :as context} opts [match res]]
  (if (baccess-rules/success? res)
    context
    (-> (bchain/terminate context)
        (update :response #(baccess-rules/handle-error
                            % request (merge opts match))))))

(defn authorization-interceptor
  [{:keys [policy rules] :or {policy :allow} :as opts}]
  (when (nil? rules)
    (throw (IllegalArgumentException. "rules should not be empty.")))
  (let [access-rules (baccess-rules/compile-access-rules rules)]
    (interceptor
     {:name ::authorization-interceptor
      :enter
      (fn [{:keys [request] :as context}]
        (let [[match res] (authorize-request access-rules request)]
          (if match
            (match-handler context opts [match res])
            (unmatch-handler context opts))))})))

