(ns nnnamespace.infra.pedestal.interceptor
  (:require
   [clojure.string :as str]
   [cognitect.transit :as transit]
   [com.stuartsierra.component :as component]
   [io.pedestal.http :as http]
   [io.pedestal.http.body-params :as body-params]
   [io.pedestal.interceptor :refer [interceptor interceptor-name]]
   [io.pedestal.interceptor.error :refer [error-dispatch]]
   [io.pedestal.interceptor.helpers :as interceptor-helper]
   [io.pedestal.log :as log]
   [ring.util.response :as ring-resp]
   [ring.util.response :as ring-response])
  (:import
   [java.io OutputStream]
   [com.cognitect.transit ReadHandler]))

(def exception-interceptor
  "The exception interceptor."
  (error-dispatch
   [context exception]
   [{:exception-type :clojure.lang.ExceptionInfo}]
   (cond
     (= (:type (ex-data exception)) :schema.core/error)
     (-> (assoc-in context [:response :status] 422)
         (assoc-in [:response :body] (:error (ex-data exception))))

     (= (:type (ex-data exception)) :validation-error)
     (-> (assoc-in context [:response :status] 422)
         (assoc-in [:response :body]
                   (select-keys (ex-data exception) [:data :errors :type])))

     (= (:type (ex-data exception)) :invalid-query-parameters)
     (-> (assoc-in context [:response :status] 422)
         (assoc-in [:response :body]
                   (select-keys (ex-data exception) [:data :errors :type])))
     :else
     (assoc context :io.pedestal.interceptor.chain/error exception))
   :else
   (assoc context :io.pedestal.interceptor.chain/error exception)))

(def not-found
  "An interceptor that returns a 404 when routing failed to resolve a route."
  (interceptor-helper/after
   ::not-found
   (fn [context]
     (if-not (http/response? (:response context))
       (do (log/meter ::not-found)
         (assoc context :response (ring-response/redirect "/")))
       context))))

(defn inject-config
  "Inject datomic connection into request map at :datomic-conn"
  [config]
  {:name ::inject-config
   :enter
   (fn [context]
     (assoc-in context [:request :config] config))})

(defn inject-datomic
  "Inject datomic connection into request map at :datomic-conn"
  [datomic]
  {:name ::inject-datomic
   :enter
   (fn [context]
     (assoc-in context [:request :datomic-conn] (:connection datomic)))})

(defn remove-interceptors-by-names
  "Remove all `interceptors` matching `names`."
  [interceptors names]
  (let [names (set names)]
    (remove #(names (:name %)) interceptors)))

(defn dependencies
  "Interceptor that adds the dependencies of a `component` into the
  Pedestal context."
  [component]
  (let [dependencies (vals (component/dependencies component))
        components (select-keys component dependencies)]
    (interceptor
     {:name ::dependencies
      :enter #(update % :request merge components)})))

;; Parameter merging

(defn merge-request-keys
  "Return an interceptor that merges the `source-keys` of the request
  into `target-key`."
  [source-keys target-key]
  (interceptor
   {:name ::merge-request-keys
    :enter
    (fn [{:keys [request] :as context}]
      (let [params (map request source-keys)]
        (if (every? #(or (nil? %) (map? %)) params)
          (->> (apply merge params)
               (assoc-in context [:request target-key]))
          (->> (first (remove empty? params))
               (assoc-in context [:request target-key])))))}))

(def merge-params-into-data
  "Interceptor that merges :transit-params, :edn-params
  and :json-params into :data."
  (merge-request-keys [:transit-params :edn-params :json-params] :data))

;; Request logging
(defn- duration-msecs
  "Return the duration between `start` and `end` in milliseconds."
  [context]
  (/ (double (- (System/nanoTime) (::request-started-at context))) 1000000.0))

(defn- request-method-name
  "Return the duration between `start` and `end` in milliseconds."
  [context]
  (some-> context :request :request-method name str/upper-case))

(defn- request-duration-msg [context duration]
  (format "%s %s, %s msecs"
          (request-method-name context)
          (-> context :request :uri)
          duration))

(defn- log-request-duration
  "Log the request's method, uri and duration in ms."
  ([context]
   (let [duration (duration-msecs context)]
     (log/info :msg (request-duration-msg context duration) :duration duration)
     context))
  ([context error]
   (let [duration (duration-msecs context)]
     (log/error :msg (request-duration-msg context duration)
                :exception error
                :duration duration)
     context)))

(def log-request
  "Interceptor that logs the request's method, uri and duration in ms."
  (interceptor
   {:name ::log-request
    :enter #(assoc % ::request-started-at (System/nanoTime))
    :leave log-request-duration
    :error log-request-duration}))

(def proxy-headers
  "Interceptor for handling headers set by HTTP proxies."
  (interceptor
   {:name ::proxy-headers
    :enter
    (fn [context]
      (let [path [:request :headers "x-forwarded-for"]]
        (if-let [forwarded-for (get-in context path)]
          (let [remote-addr (str/trim (re-find #"[^,]*$" forwarded-for))]
            (assoc-in context [:request :remote-addr] remote-addr))
          context)))}))
