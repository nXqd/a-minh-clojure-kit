(ns nnnamespace.infra.pedestal.vase
  (:require
   [com.cognitect.vase :as vase]
   [clojure.java.io :as io]
   [io.pedestal.http.route.definition.table :as table]))

(def spec-path "resources/vase/petstore.edn")

(defn vase-spec
  []
  (vase/load-edn-file spec-path))

(defn vase-routes
  [spec]
  (table/table-routes
   {}
   (vase/routes "/api" spec)))

(comment

  (def t (vase-spec))

  t
  (prn t))
