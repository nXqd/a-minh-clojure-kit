(ns nnnamespace.infra.jvm
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as log]))

(defn default-handler
  [thread ex]
  (log/error ex "Uncaught exception on" (.getName thread)))

(defn set-default-exception-handler
  [handler-fn]
  (Thread/setDefaultUncaughtExceptionHandler
   (reify Thread$UncaughtExceptionHandler
     (uncaughtException [_ thread ex]
       (when handler-fn (handler-fn thread ex))))))

(defn init
  []
  (set-default-exception-handler default-handler))

(defrecord Jvm [dev]
  component/Lifecycle
  (start [this]
         (init)
         this)
  (stop [this]
        this))

(defn new-jvm
  [& opts]
  (map->Jvm opts))