(ns nnnamespace.infra.log
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as log]))

(defn- sentry-log
  "setup sentry log"
  [])

(defrecord Log [dev]
  component/Lifecycle
  (start [this]
         (when-not (:dev? dev)
           (sentry-log))
         this)
  (stop [this]
        this))

(defn new-log
  [opts]
  (map->Log opts))
