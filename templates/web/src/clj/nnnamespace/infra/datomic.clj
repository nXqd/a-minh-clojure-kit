(ns nnnamespace.infra.datomic
  (:require
   [com.stuartsierra.component :as component :refer [Lifecycle]]
   [datomic.api :as d]
   [taoensso.timbre :as log]))

(defn- mem-database?
  "Test database uri should be the in memory one"
  [uri]
  (re-find #"datomic:mem://*" uri))

(defn create-mem-database
  [uri]
  (when (mem-database? uri)
    (d/create-database uri)
    (log/info "In-memory datomic is created at " uri)))

(defrecord Datomic [uri]
  Lifecycle
  (start [component]
         (create-mem-database uri)
         (let [conn (d/connect uri)]
           (do (log/info "Starting datomic service at" uri)
             (assoc component :connection conn
                    :uri uri))))
  (stop [component]
        (do (log/info "Stopping datomic service ...")
          (assoc component :connection nil))))

(defn new-datomic
  [opts]
  (map->Datomic opts))

