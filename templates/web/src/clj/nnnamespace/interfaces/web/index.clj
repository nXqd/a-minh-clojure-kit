(ns nnnamespace.interfaces.web.index
  (:refer-clojure :exclude [meta])
  (:require
   [cheshire.core :as json]
   [clojure.java.io :as io]
   [clojure.spec.alpha :as s]
   [clojure.spec.alpha :as s]
   [cognitect.transit :as t]
   [hiccup.page :as hiccup]
   [nnnamespace.infra.analytics :as analytics]
   [nnnamespace.ui.app :as app]
   [rum.core :as rum]
   [taoensso.timbre :as log])
  (:import (java.io ByteArrayOutputStream)))

(defn revision-manifest
  [path]
  (when-let [manifest (io/resource path)]
    (-> manifest slurp read-string)))

(def revision-manifest-memo
  (memoize revision-manifest))

(defn asset-version
  [path]
  (let [manifest (revision-manifest-memo "manifest.json")]
    (get manifest path)))

(def ^:private asset-version-memo
  (memoize asset-version))

(def preconnect
  ^{:doc "The preconnect link relation type is used to indicate an origin that will be used to fetch required resources. Initiating an early connection, which includes the DNS lookup, TCP handshake, and optional TLS negotiation, allows the user agent to mask the high latency costs of establishing a connection."}
  (->> ["//platform.twitter.com"
        "//www.facebook.com"
        "//staticxx.facebook.com"
        "//www.gstatic.com"
        "//ssl.gstatic.com"
        "//fonts.gstatic.com"
        "//accounts.google.com"
        "//apis.google.com"
        "//tpc.googlesyndication.com"]
       (map (fn [x]
              [:link {:rel "preconnect" :href x}]))))

(def meta-static
  '([:meta {:charset "utf-8"}]
    [:meta {:content "IE=edge", :http-equiv "X-UA-Compatible"}]
    [:meta {:content "width=device-width, initial-scale=1", :name "viewport"}]))

(defn meta-og
  ^{:doc "http://ogp.me/"}
  [{:keys [title]}]
  '([:meta {:property "og:title" :content title}]
    [:meta {:property "og:type" :content ""}]
    [:meta {:property "og:url" :content ""}]
    [:meta {:property "og:image" :content ""}]))

(defn meta
  ^{:doc "Coordinator"}
  [{:keys [title]}]
  (list
   meta-static
   (meta-og {:title title})
   [:title title]))

(defn start-react-app
  [env state]
  (when-not (= env :devcards)
    [:script
     (str "window.addEventListener(\"load\", function() {
         nnnamespace.core.setup_BANG_(" state ");
       });")]))

(defn server-ender
  "React server render"
  [component props]
  (when component
    (rum/render-html (app/app-ui {:component component
                                  :component-props props}))))

(defn server-props
  [props id]
  (when props
    [:script
     {:id id
      :type "application/edn"} props]))

(defn main-css
  [env]
  (condp = env
    :dev "/css/site.css"
    :devcards "/css/site.css"
    (asset-version-memo "/css/site.css")))

(s/fdef app-script
  :args (s/cat :env #{:dev :devcards :prod}))
(defn app-script
  [env]
  (let [js-file (condp = env
                  :dev "/cljs/site_dev.js"
                  :devcards "/cljs/site_devcards.js"
                  (asset-version-memo "/js/site.js"))]
    (hiccup/include-js js-file)))

(defn state->str
  "Convert clojure datastructure to transit format."
  [state]
  (let [out (ByteArrayOutputStream.)]
    (t/write (t/writer out :json) state)
    (json/generate-string (.toString out))))

(defn- devcards-styles
  [env]
  (when (= env :devcards)
    (str "<link id=\"com-rigsomelight-devcards-css\" href=\"/css/site.css\" rel=\"stylesheet\" type=\"text/css\">
          <style id=\"com-rigsomelight-devcards-addons-css\"></style>
          <style id=\"com-rigsomelight-code-highlight-css\"></style>
          <style id=\"com-rigsomelight-edn-css\"></style>
          <style id=\"com-rigsomelight-code-highlighting\"></style>")))

(s/fdef index
  :args (s/cat :app-html string? :styles string?))
(defn index
  "If you have an error regarding to-uri. It's because the css file is missing and not compiled."
  [app-html state {:keys [styles html-meta scripts env] :as props}]
  (try
    (hiccup/html5
     [:head
      preconnect
      (meta html-meta)
      (hiccup/include-css
       (main-css env)
       "//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css")

      (when styles [:style styles])
      (analytics/include-ga)
      (devcards-styles env)]

     [:body
      [:noscript "Your browser does not support JavaScript!"]
      [:div#app-root app-html]
      (when scripts scripts)
      (app-script env)
      (start-react-app env state)])
    (catch Exception e
      (log/info "You might forget to compile css file ( make dev ).")
      (log/error "Exception in index " e))))
