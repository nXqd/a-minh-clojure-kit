(ns nnnamespace.interfaces.graphql.schema
  (:require
   [clojure.walk :as walk]
   [clojure.java.io :as io]
   [clojure.edn :as edn]
   [com.walmartlabs.lacinia :as lacinia]
   [com.walmartlabs.lacinia.util :as util]
   [com.walmartlabs.lacinia.schema :as schema]
   [nnnamespace.interfaces.graphql.resolvers :as resolvers])
  (:import (clojure.lang IPersistentMap)))

(defn resolver-map
  [datomic-conn]
  {:hero    resolvers/resolve-hero
   :human   resolvers/resolve-human
   :droid   resolvers/resolve-droid
   :friends resolvers/resolve-friends})

(defn schema
  [datomic-conn]
  (-> (io/resource "graphql/star-wars-schema.edn")
      slurp
      edn/read-string
      (util/attach-resolvers (resolver-map datomic-conn))
      schema/compile))

(comment

 ;; Tool to test current schema
  (defn simplify
    "Converts all ordered maps nested within the map into standard hash maps, and
    sequences into vectors, which makes for easier constants in the tests, and eliminates ordering problems."
    [m]
    (walk/postwalk
     (fn [node]
       (cond
         (instance? IPersistentMap node)
         (into {} node)

         (seq? node)
         (vec node)

         :else
         node))
     m))

  (require '[datomic.api :as d])

  (require '[com.cognitect.vase :as vase])

  (def uri "datomic:mem://nnnamspace")
  (d/create-database uri)
  (def conn (d/connect uri)) (defn q
                               [datomic-conn query-string]
                               (-> (lacinia/execute (schema) query-string nil nil)
                                   simplify))

 ;; curl localhost:8080/graphql -X POST -H "content-type: application/graphql" -d "{ human(id: \"1000\") { id name }}"
  (q datomic-conn "{ human(id: \"1000\") { id name }}"))
