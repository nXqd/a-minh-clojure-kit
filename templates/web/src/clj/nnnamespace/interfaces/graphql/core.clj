(ns nnnamespace.interfaces.graphql.core
  (:require
   [com.walmartlabs.lacinia.pedestal :as lp]
   [nnnamespace.interfaces.graphql.schema :as schema]))

(defn routes
  [datomic-conn]
  (lp/graphql-routes (schema/schema datomic-conn) {:async true}))

