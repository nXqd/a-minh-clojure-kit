(ns nnnamespace.interfaces.api.core
  [:require
   [buddy.sign.jwt :as jwt]
   [clj-time.core :as time :refer [now plus seconds months]]
   [nnnamespace.domain.user.core :as user]
   [ring.util.response :as ring-resp]])

(defonce jwt-secret "mysupersecret")

(def unauthenticated
  {:status 400
   :headers {}
   :body
   {:errors
    [{:error :unauthenticated
      :description "You must be authenticated to make this request."}]}})

(defn login-post
  [{:keys [datomic-conn] :as request}]
  (let [user-post (get-in request [:json-params :user])
        {:keys [username password]} user-post]
    (if (true? (user/authenticated? datomic-conn username password))
      (let [claims {:user username
                    :exp (time/plus (time/now) (time/seconds 60))}
            token (jwt/sign claims jwt-secret {:alg :hs512})
            response {:token token
                      :username username}]
        (ring-resp/response response))
      unauthenticated)))
