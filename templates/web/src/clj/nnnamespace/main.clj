(ns nnnamespace.main
  (:gen-class)
  (:require
   [nnnamespace.infra.system :as system]
   [com.stuartsierra.component :as component]))

(defonce system (atom nil))

(defn start
  []
  (let [sys  (system/new-system {})
        sys' (component/start sys)]
    (reset! system sys')
    sys'))

(defn dev-stop []
  (try (component/stop @system)
    (catch Exception e nil)))

(defn run-dev
  []
  (println "\nCreating your [DEV] server...")
  (start))

(defn -main
  "The entry-point for 'lein run'"
  [& args]
  (println "\nCreating your server...")
  (start))
