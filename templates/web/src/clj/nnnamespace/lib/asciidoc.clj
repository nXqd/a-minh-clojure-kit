(ns nnnamespace.lib.asciidoc
  (:require
   [clojure.java.io :as io])
  (:import
   (org.asciidoctor Asciidoctor$Factory)))

(def container
  (Asciidoctor$Factory/create ""))

;; "ascii/test.asciidoc"
(defn- file-content
  [path]
  (->
    (io/resource path)
    slurp))

(defn asciidoctor-to-html
  [file-path]
  (let [file-content (file-content file-path)]
    (.convert container file-content {})))
