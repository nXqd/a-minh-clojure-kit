(ns nnnamespace.domain.user.core
  (:require
   [clojure.spec.alpha :as s]
   [clj-algolia.core :as al]
   [nnnamespace.infra.config :as iconf]))

(def creds {:api-key (get-in iconf/config [:algolia :search-api-key])
            :app-id (get-in iconf/config [:algolia :app-id])})
(def user-index "user-algolia-index")

(s/fdef authenticated?
  :ret boolean?)
(defn authenticated?
  "todo: check if user is authenticated"
  [conn email pasword]
  true)

(s/fdef create
  :ret boolean?)
(defn create
  "todo: create new user"
  [conn user]
  true)

(s/fdef create
  :ret map?)
(defn by-email
  "todo: gets user by email with selector"
  [conn email selector])

;; search params
(defn search
  [query]
  (al/search creds user-index {:query query
                               :getRankingInfo 1}))

(comment
  (search "FPT"))
