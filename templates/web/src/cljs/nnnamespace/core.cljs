(ns nnnamespace.core
  (:require
   [citrus.core :as citrus]
   [com.stuartsierra.component :as component]
   [nnnamespace.infra.system :refer [new-system]]
   [rum.core :as rum]
   [nnnamespace.infra.reconciler :as ireconciler]
   [nnnamespace.infra.router :as irouter]))

(defn ^:export setup!
  []
  (component/start (new-system {}))
  (irouter/start! ireconciler/reconciler)
  (irouter/watch-router! ireconciler/reconciler))
