(ns nnnamespace.infra.router
  (:require
   [bidi.bidi :as bidi]
   [citrus.core :as citrus]
   [nnnamespace.infra.routes :as iroutes]
   [pushy.core :as pushy]
   [taoensso.timbre :as log]))

(defn watch-router!
  [reconciler]
  (add-watch
   (citrus/subscription reconciler [:router])
   :router
   (fn [_ _ _ {:keys [handler route-params]}]
     (log/debug route-params)
     (when handler
       (citrus/dispatch! reconciler handler :load route-params)))))

(defn on-set-page
  [r x]
  (citrus/dispatch! r :router :push x))

(defn history
  [r]
  (pushy/pushy #(on-set-page r %) (partial bidi/match-route iroutes/routes)))

(defn start!
  [r]
  (pushy/start! (history r)))

(defn stop!
  [r]
  (pushy/stop! (history r)))

(defn set-token!
  [r token]
  (let [handler-fn (partial bidi/match-route iroutes/routes)]
    (pushy/set-token! (history r) token)
    (citrus/dispatch! r :router :push (handler-fn token))))

