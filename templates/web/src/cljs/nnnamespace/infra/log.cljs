(ns nnnamespace.infra.log
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as log]))

(defn- disable-log
  []
  (log/swap-config! (fn [config] (assoc-in config [:appenders :console] nil))))

(defrecord Log [dev]
  component/Lifecycle
  (start [this]
         (when-not (:dev? dev)
           (disable-log))
         (log/info "Init log successfully.")
         this)
  (stop [this]
        this))

(defn new-log
  [& [opts]]
  (component/using (map->Log opts)
                   [:dev]))
