(ns nnnamespace.infra.firebase
  (:require
   [firebase-cljs.core :as fb]))

(defonce *app* (atom {}))
(defonce *db* (atom nil))

(defn new-firebase
  []
  (fb/init-app
   {:apiKey ""
    :authDomain ""
    :databaseURL ""
    :projectId ""
    :storageBucket ""
    :messagingSenderId ""})

  (reset! *app* (fb/get-app))
  (reset! *db* (fb/get-db)))
