(ns nnnamespace.infra.effects.firebase
  (:require
   [citrus.core :as citrus]
   [firebase-cljs.core :as fb]
   [nnnamespace.infra.firebase :as ifb]
   [taoensso.timbre :as log]))

(defmulti control (fn [r c {:keys [method]}] method))

(defmethod control :get-startups
  [r c {:keys [on-success]}]
  (let [startups (.ref @ifb/*db* "/data/startups")]
    (.once startups "value" (fn [snapshot]
                              (let [val {:startups (fb/->cljs (.val snapshot))}]
                                (citrus/dispatch! r c on-success val))))))

(defmethod control :default
  [_ _ {:keys [method]}]
  (log/info (str "Missing control for " (js/console.log method))))

