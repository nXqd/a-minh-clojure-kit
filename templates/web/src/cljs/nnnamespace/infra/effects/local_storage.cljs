(ns nnnamespace.infra.effects.local-storage
  (:require
   [citrus.core :as citrus]))

(defn local-storage [r controller-name effect]
  (let [{:keys [method data key on-success on-error]} effect]
    (case method
      :set (js/localStorage.setItem (name key) data)
      :get (if-let [token (js/localStorage.getItem (name key))]
             (citrus/dispatch! r controller-name on-success token)
             (citrus/dispatch! r controller-name on-error))
      nil)))