(ns nnnamespace.infra.effects.http
  (:require [citrus.core :as citrus]
            [httpurr.client.xhr :as xhr]
            [cljs-http.client :as http]
            [cljs.core.async :as a])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(defmulti ->endpoint (fn [id] id))

(defmethod ->endpoint :user [_ _]
  "/user")

(defmethod ->endpoint :login [_ _]
  "/login")

(defn- ->uri [path]
  (str "http://localhost:8080/api" path))

(defn- parse-body [res]
  (-> res
      js/JSON.parse
      (js->clj :keywordize-keys true)))

(defn- ->json [params]
  (.stringify js/JSON (clj->js params)))

(defn- method->http-fn
  [method]
  (case method
    :post http/post
    :put http/put
    :patch http/patch
    :delete http/delete
    xhr/get))

(defn- ->http
  [uri http-fn http-params]
  (http-fn uri http-params))

(defn- params-type-kw
  [type]
  (case type
    :edn :edn-params
    :transit :transit-params
    :form :form-params
    :json-params))

(defn- token->header
  [token]
  (if token
    {"Authorization" (str token)}
    {}))

(defn http [r c {:keys [endpoint params on-load on-error method type headers token]}]
  (go (let [body-params {(params-type-kw type) params}
            headers-params {:headers (merge headers (token->header token))}
            http-params (merge headers-params body-params)
            http-fn (method->http-fn method)
            response (a/<!
                      (-> endpoint
                          ->endpoint
                          ->uri
                          (->http http-fn http-params)))
            body (:body response)]
        (if (= (:status response) 200)
          (citrus/dispatch! r c on-load body)
          (citrus/dispatch! r c on-error body)))))

