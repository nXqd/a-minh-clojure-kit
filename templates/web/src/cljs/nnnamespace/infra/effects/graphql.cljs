(ns nnnamespace.infra.effects.graphql
  (:require [citrus.core :as citrus]
            [cljs-http.client :as http]
            [cljs.core.async :as a])
  (:require-macros [cljs.core.async.macros :refer [go]]))

;; source: https://github.com/oliyh/re-graph/blob/master/src/re_graph/internals.cljs

(defn- valid-graphql-errors?
  "Validates that response has a valid GraphQL errors map"
  [response]
  (and (map? response)
       (vector? (:errors response))
       (seq (:errors response))
       (every? map? (:errors response))))

(defn- insert-http-status
  "Inserts the HTTP status into the response for 3 conditions:
   1. Response contains a valid GraphQL errors map: update the map with HTTP status
   2. Response is a map but does not contain a valid errors map: merge in default errors
   3. Response is anything else: return default errors map"
  [response status]
  (let [f (fn [errors] (mapv #(assoc-in % [:extensions :status] status) errors))
        default-errors {:errors [{:message "The HTTP call failed."
                                  :extensions {:status status}}]}]
    (cond
      (valid-graphql-errors? response) (update response :errors f)
      (map? response) (merge response default-errors)
      :else default-errors)))

(defn graphql [r c {:keys [url request on-error query on-success data-cursor]}]
  "This function check and send jwt token if available"
  (let [http-url (or url "http://localhost:8080/graphql")
        payload {:query query}
        on-error-fn (or on-error #(prn %))
        data-refer (if data-cursor
                     [:body :data data-cursor]
                     [:body :data])
        token (s/get "jwt-token")]
    (go (let [response (a/<! (http/post http-url (assoc request
                                                        :json-params payload
                                                        :headers {"Authorization" (str "Bearer " token)})))
              {:keys [status error-code]} response]
          (if (= :no-error error-code)
            (citrus/dispatch! r c on-success (get-in  response data-refer)) ;;TODO: should we get in to data here?
            (on-error-fn (insert-http-status (:body response) status)))))))

