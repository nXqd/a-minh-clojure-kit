(ns nnnamespace.infra.effects.web3
  (:require
   [nnnamespace.infra.web3.wallet :as web3-wallet]
   [cljs-web3.async.eth :as web3-eth-async]
   [cljs-web3.core :as web3]
   [cljs.core.async :as a]
   [citrus.core :as citrus]))

(defmulti control (fn [_ _ params] (:action params)))

(defmethod control :get-transactions
  [r c {:keys [on-success tx-count]}]
  (let [setting (citrus/subscription r [:settings])
        {:keys [contract-instance-network]} @setting
        owners (web3-wallet/get-Owners-with-random-name contract-instance-network)]
    (citrus/dispatch! r c :load-owners owners)
    (web3-wallet/get-all-transactions-async
     #(citrus/dispatch! r c on-success %) contract-instance-network)))

(defmethod control :get-owners
  [r c {:keys [on-success]}]
  (let [setting (citrus/subscription r [:settings])
        {:keys [contract-instance-network]} @setting
        owners (web3-wallet/get-Owners-with-random-name contract-instance-network)]
    (citrus/dispatch! r c on-success owners)))

(defmethod control :submit-transaction
  [r c {:keys [on-success destination value data]}]
  (let [setting (citrus/subscription r [:settings])
        {:keys [contract-instance-metamask]} @setting]
    (web3-wallet/submit-transaction #(prn %)
                                    destination
                                    (-> value (web3/to-wei :ether) (js/parseInt))
                                    data
                                    contract-instance-metamask)))

(defmethod control :confirm-transaction
  [r c {:keys [tx-id]}]
  (let [setting (citrus/subscription r [:settings])
        {:keys [contract-instance-metamask]} @setting]
    (web3-wallet/confirm-transaction #(prn %) (js/parseInt tx-id) contract-instance-metamask)))

(defmethod control :load-setting
  [r c {:keys [on-load]}]
  (let [settings (citrus/subscription r [:settings])
        {:keys [contract-address network web3-provide]} @settings
        state {:contract-address contract-address
               :network network
               :web3-provide web3-provide}]
    (citrus/dispatch! r c on-load state)))
