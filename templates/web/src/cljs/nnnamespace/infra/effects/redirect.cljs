(ns nnnamespace.infra.effects.redirect
  (:require
   [nnnamespace.infra.router :as ir]))

(defn redirect [r _ path]
  (ir/set-token! r path))
