(ns nnnamespace.infra.dev
  (:require
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as log]))

(def dev? ^boolean js/goog.DEBUG)

(defn init
  []
  (when dev?
    #_(devtools/install!)
    (enable-console-print!))
  dev?)

(defrecord Dev []
  component/Lifecycle
  (start [this]
         (let [dev? (init)]
           (assoc this :dev? dev?)))
  (stop [this]
        this))

(defn new-dev
  [& [opts]]
  (map->Dev opts))
