(ns nnnamespace.infra.web3.wallet
  (:require
   [cljs-web3.async.eth :as web3-eth-async]
   [cljs-web3.core :as web3]
   [cljs-web3.eth :as web3-eth]
   [cljs.core.async :as a]
   [cljs.reader :as reader]
   [nnnamespace.infra.web3.config :as web3-config]
   [rum.core :as rum])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(def  provides-web3? (or (aget js/window "web3") goog.DEBUG))

(def web3 (or (aget js/window "web3")
              (if goog.DEBUG
                (web3/create-web3 web3-config/local-address)
                (web3/create-web3 "https://morden.infura.io/metamask"))))

(defn get-version
  [web3]
  (web3/version-api web3))

(defn get-active-account
  []
  (when provides-web3?
    (web3-eth/accounts web3)))

(defn get-Owners
  [ci]
  (->> (web3-eth/contract-call ci :getOwners)))

(defn get-Owners-with-random-name
  [ci]
  (->> (get-Owners ci)
       (map-indexed (fn [idx owner]
                      {:name (str "Owner " idx)
                       :address owner}))
       (into [])))

(defn create-contract-instance
  [web3 abi contract-address]
  (web3-eth/contract-at web3 web3-config/abi contract-address))

(defn call->results
  [data]
  (->> data js/JSON.stringify reader/read-string))

(defn get-balance
  [address web3]
  (->> (web3-eth/get-balance web3 address) call->results))

(defn get-Required
  [ci]
  (->> (web3-eth/contract-call ci :required) call->results))

(defn get-TransactionCount
  [ci]
  (->> (web3-eth/contract-call ci :transactionCount)  call->results))

(defn send-transaction
  [to-address]
  (web3-eth/send-transaction! web3 {:to to-address
                                    :from (first (get-active-account))
                                    :value (web3/to-wei 1 :ether)}
                              (fn [err res] (when-not err (println "send-transaction" res)))))

(defn submit-transaction
  [f destination value data ci]
  (go
    (a/<! (web3-eth-async/contract-call
           (a/chan 1
                   (comp (map second)
                         (map f)))
           ci :submitTransaction destination value data))))

(defn confirm-transaction
  [f tx-id ci]
  (go
    (a/<! (web3-eth-async/contract-call
           (a/chan 1
                   (comp (map second)
                         (map f)))
           ci :confirmTransaction tx-id))))

(defn get-confirmations
  [tx-id contract-instance]
  (->> (web3-eth/contract-call contract-instance :getConfirmations tx-id)))

(defn fetch-data
  [id results contract-instance]
  (fn [x]
    (let [[destination value data executed] x
          confirmations (get-confirmations id contract-instance)
          tx {:id          id
              :destination destination
              :value (web3/from-wei value :ether)
              :data        (web3/to-ascii data)
              :confirmations confirmations
              :executed   executed}]
      (swap! results conj tx))))

(defn get-all-transactions-async
  [f contract-instance]
  (let [results (atom [])
        c-result (a/chan)
        tx-count  (get-TransactionCount contract-instance)]
    (if (> tx-count 0)
      (go-loop [id 0]
        (a/<! (web3-eth-async/contract-call
               (a/chan 1
                       (comp (map second)
                             (map (fetch-data id results contract-instance))))
               contract-instance :transactions id))
        (if (= id (- tx-count 1))
          (f @results)
          (recur (inc id))))
      [])))

#_(defn get-all-transactions-async
    [*atom* f]
    (let [results (atom [])
          c-result (a/chan)
          tx-count  (get-TransactionCount ci)]
      (go-loop [id 0]
        (a/<! (web3-eth-async/contract-call
               (a/chan 1
                       (comp (map second)
                             (map (fetch-data id results))))
               ci :transactions id))
        (if (= id (- tx-count 1))
          (swap! *atom* (fn [atom-res] (f atom-res @results)))
          (recur (inc id))))))
