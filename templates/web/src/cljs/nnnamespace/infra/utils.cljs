(ns nnnamespace.infra.utils
  (:require
   [cljs.core.async :as async]
   [goog.string :as gstring]
   [goog.html.legacyconversions :as conv]
   [goog.net.jsloader :as jsloader]))

(defn handle-change
  "Handle input change with regex support."
  ([e state-atom kork]
   (handle-change e state-atom kork nil))
  ([e state-atom kork re]
   (let [value (.. e -target -value)
         kork (if (vector? kork) kork [kork])]
     (if (not (nil? re))
       (when (re-matches re value)
         (swap! state-atom assoc-in kork value))
       (swap! state-atom assoc-in kork value)))))

(defn load-script-async
  [uri]
  (let [ch (async/promise-chan)
        trusted-uri (->> uri
                         str
                         conv/trustedResourceUrlFromString)]
    (.addCallback (jsloader/safeLoad trusted-uri)
                  #(async/put! ch :lib-loaded)) ch))

(defn fiat-round [x]
  (gstring/format "%.2f" x))

(defn currency-round [x]
  (gstring/format "%.6f" x))
