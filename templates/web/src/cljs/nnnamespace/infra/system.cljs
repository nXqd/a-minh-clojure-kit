(ns nnnamespace.infra.system
  (:require
   [bidi.bidi :as bidi]
   [citrus.core :as citrus]
   [cljs.core.async :as async]
   [clojure.string :as cstr]
   [com.stuartsierra.component :as component]
   [nnnamespace.infra.app-state :as iapp-state]
   [nnnamespace.infra.dev :as idev]
   [nnnamespace.infra.firebase :as ifb]
   [nnnamespace.infra.log :as ilog]
   [nnnamespace.infra.router :as irouter]
   [nnnamespace.infra.routes :as iroutes]
   [nnnamespace.infra.reconciler :as ireconciler]
   [nnnamespace.ui.app :as app]
   [rum.core :as rum]
   [taoensso.timbre :as log])
  (:require-macros
   [cljs.core.async.macros :refer [go alt!]]))

(enable-console-print!)

(defn app-root
  []
  (js/document.getElementById "app-root"))

(defrecord Application [state reconciler]
  component/Lifecycle
  (start [this]
         (log/info "Application started.")
         (rum/mount (app/app-ui reconciler) (app-root))
         this)
  (stop [this]
        (rum/unmount (app-root))
        (log/info "Application stopped.")
        this))

(defn new-app
  [opts]
  (map->Application opts))

(defn new-system
  [config]
  (component/system-map
   :log (ilog/new-log)
   :dev (idev/new-dev)
   :router
   (irouter/new-router
    {:on-set-page #(citrus/dispatch! ireconciler/reconciler :router :push %)
     :routes iroutes/routes}) :app-state
   (component/using
    (iapp-state/new-app-state {:reconciler ireconciler/reconciler})
    [:log])

   :app
   (component/using
    (new-app {:reconciler ireconciler/reconciler})
    [:log :dev :app-state :router])))
