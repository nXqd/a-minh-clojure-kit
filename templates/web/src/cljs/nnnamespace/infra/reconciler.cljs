(ns nnnamespace.infra.reconciler
  (:require
   [citrus.core :as citrus]
   [nnnamespace.controller.about-page :as cap]
   [nnnamespace.controller.home-page :as chp]
   [nnnamespace.controller.router :as crouter]
   [nnnamespace.controller.session :as csess]
   [nnnamespace.infra.effects :as ieffects]
   [nnnamespace.infra.effects.graphql :as graphql]
   [nnnamespace.infra.effects.redirect :as redirect]
   [nnnamespace.infra.effects.local-storage :as local-storage]))

(def reconciler
  (citrus/reconciler
   {:state (atom {})
    :controllers
    {:session csess/control
     :router crouter/control
     :route/home-page chp/control
     :route/about-page cap/control}
    :effect-handlers
    {:local-storage local-storage/local-storage
     :graphql graphql/graphql
     :redirect redirect/redirect}}))
