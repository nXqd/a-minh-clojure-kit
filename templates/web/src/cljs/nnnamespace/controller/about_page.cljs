(ns nnnamespace.controller.about-page)

(def init-state
  {})

(defmulti control (fn [action] action))

;; action [args] state
(defmethod control :init [_ [args] state]
  (if (nil? state)
    {:state init-state}
    {:state state}))

(defmethod control :load [_ [args] state]
  ;; Do server API call here
  {:state state})
