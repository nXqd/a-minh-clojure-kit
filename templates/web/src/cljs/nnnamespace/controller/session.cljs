(ns nnnamespace.controller.session)

(def init-state
  {:user {}})

(defmulti control (fn [action] action))

(defmethod control :init [_ [args] state]
  {:state init-state})

