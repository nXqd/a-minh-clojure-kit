(ns nnnamespace.controller.shared)

(defn success-state
  [state new-state]
  (-> state
      (merge new-state)
      (assoc :error nil)
      (assoc :loading? false)))
