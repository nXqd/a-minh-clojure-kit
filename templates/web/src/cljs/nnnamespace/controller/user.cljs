(ns nnnamespace.controller.user)

(def initial-state
  {:current-user nil
   :loading?     false
   :token        nil
   :errors       nil})

(defmulti control (fn [event] event))

(defmethod control :default [_ _ state]
  {:state state})

(defmethod control :init []
  {:state initial-state})

(defmethod control :login [_ [{:keys [username password]}] _]
  {:state (assoc initial-state :loading? true)
   :http  {:endpoint :login
           :params   {:user {:username username :password password}}
           :method   :post
           :type     :json
           :on-load  :login-success
           :on-error :form-submit-error}})

(defmethod control :login-success [_ [{:keys [token username]}] state]
  {:state         (assoc state :token        token
                         :current-user username
                         :errors       nil
                         :loading?     false)
   :local-storage {:method :set
                   :key "jwt-token"
                   :data token}
   :redirect      ""})

(defmethod control :register [_ [{:keys [username email password]}] _]
  {:state (assoc initial-state assoc :loading? true)
   :http  {:endpoint :users
           :params   {:user {:username username :email email :password password}}
           :method   :post
           :type     :json
           :on-load  :register-success
           :on-error :form-submit-error}})

(defmethod control :register-success [_ [{user :user {token :token} :user}] state]
  {:state         (assoc state
                         :token        token
                         :current-user user
                         :errors       nil
                         :loading?     false)
   :local-storage {:action :set
                   :id     "jwt-token"
                   :value  token}
   :redirect      ""})

(defmethod control :form-submit-error [_ [{errors :errors}] state]
  {:state (assoc state
                 :errors   errors
                 :loading? false)})
(defmethod control :clear-errors [_ _ state]
  {:state (assoc state :errors nil)})

(defmethod control :check-auth [_ _ state]
  {:state         state
   :local-storage {:method :get
                   :key "jwt-token"
                   :on-success :load-user}})

(defmethod control :load-user [_ [token] state]
  (if token
    (let [query (str "mutation { check_token (token: \"" token "\") {username}}")]
      {:state   (assoc state :token token :loading? true)
       :graphql {:query      query
                 :on-success :load-user-success
                 :data-cursor :check_token}})))

(defmethod control :load-user-success [_ [{:keys [username] :as x}] state]
  {:state (assoc state :current-user username :loading? false)})
