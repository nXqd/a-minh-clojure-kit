(ns nnnamespace.controller.web3.wallet
  (:require
   [cljs-web3.async.eth :as web3-eth-async]
   [cljs-web3.core :as web3]
   [cljs.core.async :as a])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(def init-state
  {:transactions []
   :owners []
   :transactions-count ""
   :loading? false})

(defmulti control (fn [action] action))

(defmethod control :init [_ [args] state]
  (if (nil? state)
    {:state init-state}
    {:state state}))

(defmethod control :load [_ _ state]
  {:state state})

(defmethod control :load-transactions
  [_ [results] state]
  {:state (-> state
              (assoc :transactions results))})

(defmethod control :get-transactions
  [_ _ state]
  {:state state
   :web3  {:action :get-transactions
           :on-success :load-transactions}})

(defmethod control :load-owners
  [_ [results] state]
  {:state (-> state
              (assoc :owners results))})

(defmethod control :get-owners
  [_ _ state]
  {:state state
   :web3 {:action :get-owners
          :on-success :load-owners}})

(defmethod control :submit-transaction
  [_ [{:keys [destination value transaction-data]}]  state]
  {:state state
   :web3 {:action :submit-transaction
          :destination destination
          :value value
          :data transaction-data
          :on-success :load-transaction}})

(defmethod control :confirm-transaction
  [_ [{:keys [tx-id]}]  state]
  {:state state
   :web3 {:action :confirm-transaction
          :tx-id tx-id
          :on-success :load-transaction}})
