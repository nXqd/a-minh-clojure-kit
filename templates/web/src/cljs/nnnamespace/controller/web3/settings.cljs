(ns nnnamespace.controller.web3.settings
  (:require
   [cljs-web3.async.eth :as web3-eth-async]
   [cljs-web3.core :as web3]
   [cljs-web3.eth :as web3-eth]
   [cljs.core.async :as a]
   [cljs.reader :as reader]
   [nnnamespace.infra.web3.config :as web3-config]
   [nnnamespace.infra.web3.wallet :as web3-wallet])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(def init-state
  {:network :local
   :contract-address ""
   :web3-provide :metamask})

(defn get-web3-metamask
  [network-address]
  (or (aget js/window "web3")
      (if goog.DEBUG
        (web3/create-web3 network-address)
        (web3/create-web3 "https://morden.infura.io/metamask"))))

(defn create-full-setting
  [{:keys [network contract-address web3-provide]}]
  (let [network-address (get web3-config/networks-list network)
        web3-metamask (get-web3-metamask network-address)
        web3-network (web3/create-web3 network-address)
        contract-instance-metamask (web3-wallet/create-contract-instance web3-metamask web3-config/abi contract-address)
        contract-instance-network (web3-wallet/create-contract-instance web3-network web3-config/abi contract-address)]
    {:network network
     :contract-address contract-address
     :web3-provide :metamask
     :contract-instance-metamask contract-instance-metamask
     :contract-instance-network contract-instance-network
     :web3-metamask web3-metamask
     :web3-network web3-network}))

(defmulti control (fn [action] action))

(defmethod control :init
  []
  {:local-storage
   {:method :get
    :key :settings
    :on-read :init-ready}})

(defmethod control :init-ready
  [_ [results] state]
  (if (nil? results)
    {:state (create-full-setting init-state)}
    {:state (create-full-setting (reader/read-string results))}))

(defmethod control :load
  [_ _ state]
  {:state state})

(defmethod control :update-settings
  [_ [{:keys [web3-provide contract-address network]}] state]
  {:state (-> state
              (assoc :contract-address contract-address
                     :network network
                     :web3-provide web3-provide)
              (create-full-setting))
   :local-storage {:method :set
                   :data {:contract-address contract-address
                          :web3-provide web3-provide
                          :network network}
                   :key :settings}})
