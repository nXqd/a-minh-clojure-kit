(ns nnnamespace.controller.home-page)

(def init-state
  {:ui {:error nil
        :Loading? false}
   :data {:project {}}})

(defmulti control (fn [action] action))

(defmethod control :init [_ [args] state]
  (if (nil? state)
    {:state init-state}
    {:state state}))

(defmethod control :load [_ [args] state]
  ;; Do server API call here
  {:state state})
