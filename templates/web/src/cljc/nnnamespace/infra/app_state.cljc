(ns nnnamespace.infra.app-state
  (:require
   [citrus.core :as citrus]
   [com.stuartsierra.component :as component]
   [taoensso.timbre :as log]))

(defonce ^:const init-state
  {:data {:current-user {}}
   :routes {:current nil
            :uri ""
            :route-params {}
            :query-params {}}
   :ui {:current-lang :vi}
   :page {:project-details {}
          :project-list {:related-projects []}}})

(defonce ^:dynamic *state* (atom init-state))

(defrecord ApplicationState
  [state reconciler]
  component/Lifecycle
  (start [this]
         (citrus/broadcast-sync! reconciler :init)               ;; init all controllers
         (log/info "Application state is setup successfully.")
         this)
  (stop [this]
        this))

(defn new-app-state
  [opts]
  (map->ApplicationState opts))
