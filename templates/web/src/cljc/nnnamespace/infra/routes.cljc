(ns nnnamespace.infra.routes
  (:require
   [bidi.bidi :as bidi]
   #?@(:clj [[clojure.spec.alpha :as s]])
   #?@(:cljs [[cljs.spec.alpha :as s]])))

(def routes
  ["/" [["" :route/home-page]
        ["about" :route/about-page]
        [["projects/" :project-id] {"" :route/project-details}]]])

(s/fdef url-for
  :args (s/cat :route-name keyword?
               :params vector?))
(defn url-for
  "Example: (url-for :route/home-page [:id 1])"
  [route-name params]
  (apply bidi/path-for routes
         (cons route-name params)))

