(ns nnnamespace.infra.analytics.ga)

(defn include-script
  [tracking-id]
  (str "<script async src=\"https://www.googletagmanager.com/gtag/js?id=" tracking-id "\"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
                  gtag('js', new Date());
                  gtag('config', '" tracking-id "');</script>"))
