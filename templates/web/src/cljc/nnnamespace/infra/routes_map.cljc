(ns nnnamespace.infra.routes-map
  (:require
   [nnnamespace.ui.pages.home-page :as hp]
   [nnnamespace.ui.pages.about-page :as ap]
   #?@(:clj [[clojure.spec.alpha :as s]])
   #?@(:cljs [[cljs.spec.alpha :as s]])))

(def routes-styles
  {:route/home-page hp/styles
   :route/about-page ap/styles})

(s/fdef get-styles
  :args (s/cat :route-name string?)
  :ret (s/nilable string?))
(defn get-styles
  [route-name]
  (when-let [style-mapping (get routes-styles route-name)]
    (:css style-mapping)))

(def routes-pages
  {:route/home-page hp/page-ui
   :route/about-page ap/page-ui})

(s/fdef get-page
  :args (s/cat :route-name keyword?))
(defn get-page
  [route-name]
  (get routes-pages route-name))

