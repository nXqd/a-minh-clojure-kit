(ns nnnamespace.infra.config
  (:require
   #?@(:clj [[cprop.core :refer [load-config]]])))

#?(:clj (def config (load-config)))

#?(:cljs (def config {:segmentio {:write-key ""}
                      :google-analytics {:tracking-id ""}
                      :web3 {:networks
                             {:mainnet "https://mainnet.infura.io/UflHQ6jiuI2OGgLCXOz7"
                              :ropsten "https://ropsten.infura.io/UflHQ6jiuI2OGgLCXOz7"
                              :kovan "https://kovan.infura.io/UflHQ6jiuI2OGgLCXOz7"
                              :rinkeby "https://rinkeby.infura.io/UflHQ6jiuI2OGgLCXOz7"
                              :local "http://localhost:8545/"}}}))
