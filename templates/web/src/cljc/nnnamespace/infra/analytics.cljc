(ns nnnamespace.infra.analytics
  (:require
   [nnnamespace.infra.analytics.ga :as ga]
   [nnnamespace.libs.segmentio :as seg]
   [nnnamespace.infra.config :as conf]
   #?@(:clj [[clojure.spec.alpha :as s]])
   #?@(:cljs [[cljs.spec.alpha :as s]])))

(s/fdef analytics-scripts
  :ret string?)
(defn analytics-scripts
  []
  (seg/script-snippet (get-in conf/config [:segmentio :write-key])))

(defn include-ga
  []
  (ga/include-script (get-in conf/config [:google-analytics :tracking-id])))

