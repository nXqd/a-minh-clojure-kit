(ns nnnamespace.ui.app
  (:require
   [rum.core :as rum]
   [nnnamespace.ui.pages.home-page :as hp]
   [nnnamespace.ui.pages.about-page :as ap]
   [nnnamespace.infra.routes-map :as rm]
   [citrus.core :as citrus]))

(defn current-page
  [handler reconciler]
  (when-let [page (rm/get-page handler)]
    (page reconciler)))

(rum/defc app-ui < rum/reactive
  [r]
  (let [{:keys [handler route-params]} (rum/react (citrus/subscription r [:router]))]
    [:div
     [:p "App ui"]
     [:a {:href "/"} "Home page"]
     [:a {:href "/about"} "About page"]
     (current-page handler r)]))
