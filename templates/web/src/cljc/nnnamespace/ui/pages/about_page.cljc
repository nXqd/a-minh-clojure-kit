(ns nnnamespace.ui.pages.about-page
  (:require
   [cljs-css-modules.macro #?(:cljs :refer-macros :clj :refer) [defstyle]]
   [rum.core :as rum]))

(defstyle styles
  [:.page-block {:padding "3rem 1rem"}]
  [:.product-item--related {:padding "0.75rem 1rem !important"}]
  [:.person-ui {:margin-right "1rem"}
   [:&:last-child {:margin-right "0"}]])

(rum/defc page-ui < rum/static rum/reactive
  [r]
  [:.about-page.page
   "About page"])
