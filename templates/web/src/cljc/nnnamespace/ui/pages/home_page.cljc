(ns nnnamespace.ui.pages.home-page
  (:require
   [nnnamespace.libs.segmentio :as seg]
   [cljs-css-modules.macro #?(:cljs :refer-macros :clj :refer) [defstyle]]
   [rum.core :as rum]))

(defstyle styles
  [:.page-block {:padding "3rem 1rem"}])

(rum/defc page-ui < rum/static rum/reactive
  [r]
  [:.home-page.page
   "Home page"])

