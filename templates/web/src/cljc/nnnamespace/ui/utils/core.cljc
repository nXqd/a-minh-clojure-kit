(ns nnnamespace.ui.utils.core)

(defn style-class
  "Get the correct style class for inline styling both on clojure and clojurescript."
  [styles kork]
  #?(:clj (if (vector? kork)
            (get-in styles (cons :map kork))
            (get-in styles [:map kork]))
     :cljs (if (vector? kork)
             (get-in styles kork)
             (get styles kork))))
