(ns nnnamespace.ui.utils.ag-grid
  (:require
   [taoensso.timbre :as log]
   [rum.core :as rum]
   [citrus.core :as citrus]
   #?@(:cljs [[goog.dom :as gdom]
              [nnnamespace.infra.utils :as utils]
              [hiccups.runtime :as hiccupsrt]
              [cljs.core.async :as async]]))
  #?(:cljs
     (:require-macros
      [cljs.core.async.macros :refer [go]]
      [hiccups.core :as hiccups])))

(def ^:const lib-uri "//cdnjs.cloudflare.com/ajax/libs/ag-grid/18.1.1/ag-grid.min.js")

(defn lib-loaded?
  []
  #?(:cljs js/window.agGrid))

(defn load-lib
  []
  #?(:cljs (utils/load-script-async lib-uri)))

(defn cell-renderer
  [params]
  #?(:cljs (let [{:keys [value]} (js->clj params :keywordize-keys true)
                 ediv (js/document.createElement "div")
                 content (hiccups/html [:button.something value])]
             (set! ediv.innerHTML content)
             ediv)))

(defn dropdown-on-change
  []
  (fn [e]
    #?(:cljs (log/debug "something"))))

(defn get-value
  [params]
  #?(:cljs
     (let [{:keys [value]} (js->clj params :keywordize-keys true)]
       value)))

(defn link-renderer
  [params]
  #?(:cljs (let [{:keys [value uri]} (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html [:a {:href (str uri)} value])]
             (set! ediv.innerHTML content)
             ediv)))

(defn user-cell-action-on-click
  [r id]
  #?(:cljs
     (citrus.core/dispatch! r :user :load-user id)))

(defn active-bonus-action-renderer
  [params]
  #?(:cljs (let [{:keys [id r] :as value} (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html
                          [:div
                           [:button.btn.btn-info.btn-sm
                            {:disabled true}
                            "Active"]])]
             (set! ediv.innerHTML content)
             (let [select (.querySelector ediv "button")]
               (.addEventListener
                select "click"
                (fn [e] #?(:cljs
                           (log/debug "hahaha")))))
             ediv)))

(defn user-detail-action-renderer
  [params]
  #?(:cljs (let [{:keys [id r] :as value} (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html
                          [:div
                           [:button.btn.btn-sm.btn-primary
                            {:data-toggle "modal",
                             :data-target "#exampleModal"}
                            "View"]])]
             (set! ediv.innerHTML content)
             (let [select (.querySelector ediv "button")]
               (.addEventListener
                select "click"
                (fn [e] #?(:cljs
                           (user-cell-action-on-click r {:id id})
                           #_(log/debug "hahaha")))))
             ediv)))

(defn actions-renderer
  [params]
  #?(:cljs (let [{:keys [label uri]} (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html [:button {:type "button", :class "btn "} "View"])]
             (set! ediv.innerHTML content)
             (let [select (.querySelector ediv "button")]
               (.addEventListener
                select "click"
                (fn [e] #?(:cljs (log/debug "hahaha")))))
             ediv)))

(defn colored-number-renderer
  [params & {:keys [percentage?]
             :or {percentage? false}}]
  #?(:cljs (let [value (get-value params)
                 ediv (js/document.createElement "div")
                 class (if (> value 0) "green-value" "red-value")
                 el-str (str "span." class)
                 el-value (str (when (> value 0) "+")
                               value
                               (when percentage? "%"))
                 content (hiccups/html [el-str el-value])]
             (set! ediv.innerHTML content)
             ediv)))

(defn symbol-renderer
  [params]
  #?(:cljs (let [value (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html
                          [:div.currency-symbol value])]
             (set! ediv.innerHTML content)
             #_(let [select (.querySelector ediv "select")]
                 (.addEventListener select "change" (fn [e] #?(:cljs (log/debug "hahaha")))))
             ediv)))

(defn status-renderer
  [params]
  #?(:cljs (let [value (get-value params)
                 value-label (get
                              {0 "Pending"
                               1 "Pending"
                               2 "Approved"
                               10 "Processed"
                               -1 "Cancelled"
                               -2 "Declined"
                               -3 "On Hold"
                               -4 "Error"} value "Unknown")
                 ediv (js/document.createElement "div")
                 class (get {0 "label-info"
                             1 "label-info"
                             2 "label-primary"
                             10 "label-success"
                             -1 "label-default"
                             -2 "label-danger"
                             -3 "label-warning"
                             -4 "label-danger"}
                            value "label-default")
                 el-str (str "span." class)
                 content (hiccups/html [el-str value-label])]
             (set! ediv.innerHTML content)
             ediv)))

(defn type-renderer
  [params]
  #?(:cljs (let [value (get-value params)
                 ediv (js/document.createElement "div")
                 el-str (str "span.buy-sell-type." value)
                 content (hiccups/html
                          [el-str (name value)])]
             (set! ediv.innerHTML content)
             ediv)))

(def ag-grid-default-ui
  {:ag-grid-el nil
   :grid-opts nil})

(defn create-opts
  "Common usecase of opts is to pass events-chan to col-defs"
  ([col-defs row-data]
   (create-opts col-defs row-data {}))

  ([col-defs row-data {:keys [onRowClicked enableFilter? rowHeight] :as opts
                       :or {enableFilter? false}}]
   (let [col-defs* (if (fn? col-defs) (col-defs opts) col-defs)]
     {:columnDefs col-defs*
      :enableFilter enableFilter?
      :enableSorting true
      :rowSelection "single"
      :rowData row-data
      :rowHeight (or rowHeight 50)
      ;; :pagination true
      ; :paginationPageSize 10
      :onRowClicked (fn [e] (when onRowClicked (onRowClicked e)))
      :onGridReady
      (fn [params]
        (.. params -api sizeColumnsToFit))})))

(defn ag-grid-mixin
  [col-defs]
  {:will-update
   (fn [state]
     #?(:cljs
        (let [{:keys [data]} (-> state :rum/args first)
              {:keys [ag-grid-ui]} state]
          (swap! ag-grid-ui assoc :data data)
          state)
        :clj state))

   :did-mount
   (fn [state]
     #?(:cljs
        (let [el (rum/dom-node state)
              {:keys [data
                      onRowClicked
                      events-chan
                      opts] :as props} (-> state :rum/args first)
              *ui* (atom ag-grid-default-ui)]
          (go (when-not (lib-loaded?)
                (async/<! (load-lib)))
              (let [grid-opts (clj->js (create-opts col-defs data props))
                    ag-grid-el (js/agGrid.Grid. el grid-opts)]
                (swap! *ui* assoc
                       :ag-grid-el ag-grid-el
                       :grid-opts grid-opts
                       :data data)))
          (add-watch *ui* :data
                     (fn [_ _ _ {:keys [ag-grid-el grid-opts data]}]
                       (when (not= ag-grid-el nil)
                         (.setRowData (.-api grid-opts) (clj->js data)))))
          (assoc state :ag-grid-ui *ui*))
        :clj state))})

