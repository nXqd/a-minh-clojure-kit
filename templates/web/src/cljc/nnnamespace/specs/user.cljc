(ns nnnamespace.specs.user
  (:require
   [pear.spec.core :as ps]
   #?@(:clj [[clojure.spec.alpha :as s]])
   #?@(:cljs [[cljs.spec.alpha :as s]])))

(s/def ::username (s/and (ps/str-limit 5 16) string?))
(s/def ::password (s/and (ps/str-limit 8 256) string?))
(s/def ::hashed-password (s/and string? not-empty))
(s/def ::name (s/and string? not-empty))
(s/def ::email ::ps/email-type)
(s/def ::address-line-1 string?)
(s/def ::address-line-2 string?)
(s/def ::zip-code string?)
(s/def ::state string?)
(s/def ::country string?)
(s/def ::phone-number string?)
(s/def ::accepted? boolean?)
(s/def ::notes string?)
