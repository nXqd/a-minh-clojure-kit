(ns nnnamespace.libs.segmentio
  (:require
   [clojure.string :as cstr]
   [pear.spec.core :as ps]
   #?@(:clj [[clojure.spec.alpha :as s]])
   #?@(:cljs [[cljs.spec.alpha :as s]])))

(s/fdef script-snippet
  :args (s/cat :write-key string?)
  :ret string?)
(defn script-snippet
  [write-key]
  (str "<script type=\"text/javascript\">
  !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error(\"Segment snippet included twice.\");else{analytics.invoked=!0;analytics.methods=[\"trackSubmit\",\"trackClick\",\"trackLink\",\"trackForm\",\"pageview\",\"identify\",\"reset\",\"group\",\"track\",\"ready\",\"alias\",\"debug\",\"page\",\"once\",\"off\",\"on\"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t){var e=document.createElement(\"script\");e.type=\"text/javascript\";e.async=!0;e.src=(\"https:\"===document.location.protocol?\"https://\":\"http://\")+\"cdn.segment.com/analytics.js/v1/\"+t+\"/analytics.min.js\";var n=document.getElementsByTagName(\"script\")[0];n.parentNode.insertBefore(e,n)};analytics.SNIPPET_VERSION=\"4.0.0\";
              analytics.load(\"" write-key "\");
              analytics.page();
              }}();
  </script>"))

(defn- clj-key->js-key
  "Converts a clj key (:foo-bar) to a js key (foo_bar)."
  [kw]
  (-> kw
      name
      (cstr/replace #"-" "_")))

(defn- ->js-str
  "Recursively convert a map to replace all the dashes with underscores
  in the keys only. This is mean for converting to a clojure object which
  looks like a javscript object in form, so we can merge it with JS objects."
  [clj-map]
  (->> (for [[kw value] clj-map]
         [(-> kw
              clj-key->js-key
              keyword)
          (if (map? value)
            (->js-str value)
            value)])
       (into {})))

(s/def ::email ::ps/email-type)
(s/def ::user-data (s/keys :req-un [::id ::name ::email]))

(s/fdef identify
  :args (s/cat :user-data ::user-data))
#?(:cljs
   (defn identify
     [user-data]
     (js/analytics.identify (:id user-data)
                            (->js-str (dissoc user-data :id)))))

#?(:cljs
   (defn track-pageview
     [navigation-point subpage & properties]
     (js/analytics.page (name navigation-point)
                        (when subpage (name subpage))
                        (->js-str properties))))

#?(:cljs
   (defn track-event
     [event & properties]
     (js/analytics.track (name event)
                         (->js-str properties))))

