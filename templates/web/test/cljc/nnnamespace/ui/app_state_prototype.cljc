(ns nnnamespace.ui.app-state-prototype)

(def product-item
  {:image-url "https://ph-files.imgix.net/b495fcfb-6e31-42d7-b9f8-c36b899cbb02?auto=format&auto=compress&codec=mozjpeg&cs=strip&w=80&h=80&fit=crop"
   :title "Craft Prototype, from Invision"
   :sub-title "Design, prototype, and collaborate seamlessly"
   :upvote 170})

(defonce ^:const prototype-state
  {:data {:current-user {}}
   :routes {:current nil
            :uri ""
            :route-params {}
            :query-params {}}
   :ui {:current-lang :vi}
   :page {:project-details {}
          :project-list {:related-projects [product-item product-item product-item]}}})
