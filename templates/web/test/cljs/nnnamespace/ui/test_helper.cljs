(ns nnnamespace.ui.test-helper
  (:require
   [citrus.core :as citrus]))

(def empty-reconciler
  (citrus/reconciler {:state (atom {})
                      :controllers {:router (constantly nil)}}))

(defonce init-ctrl (citrus/broadcast-sync! empty-reconciler :init))

