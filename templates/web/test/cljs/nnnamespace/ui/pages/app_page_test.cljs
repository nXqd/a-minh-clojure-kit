(ns nnnamespace.ui.pages.app-page-test
  (:require
   [nnnamespace.ui.app :as sut]
   [nnnamespace.ui.test-helper :as helper])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard page-ui
  (sut/app-ui helper/empty-reconciler))
