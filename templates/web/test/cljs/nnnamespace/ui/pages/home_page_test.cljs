(ns nnnamespace.ui.pages.home-page-test
  (:require
   [nnnamespace.ui.test-helper :as helper]
   [nnnamespace.ui.pages.home-page :as sut]
   [rum.core :as rum])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard page-ui
  (sut/page-ui helper/empty-reconciler))
