(ns nnnamespace.ui.main-test
  (:require
   [devcards.core :refer [start-devcard-ui!]]
   [nnnamespace.ui.pages.home-page-test]
   [nnnamespace.ui.playground-test]))

(enable-console-print!)

(defn ^:export main []
  (start-devcard-ui!))
