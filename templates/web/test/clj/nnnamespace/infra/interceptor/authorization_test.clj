(ns nnnamespace.infra.interceptor.authorization-test
  (:require
   [nnnamespace.infra.pedestal.interceptor.authorization :as sut]
   [buddy.auth :as bauth]
   [buddy.auth.accessrules :as baccess]
   [clojure.test :refer :all]
   [buddy.auth.accessrules :as access]
   [clojure.spec.alpha :as s]))

(deftest responses-test
  (is sut/unauthorized)
  (is sut/forbidden))

(deftest authenticated-handler-test
  (testing "returns true if request is authenticated"
           (with-redefs [bauth/authenticated? (fn [& _] true)]
                        (is (= true (sut/authenticated-handler {})))))

  (testing "returns a buddy auth error if user is not authenticated"
           (with-redefs [bauth/authenticated? (fn [& _] false)
                         baccess/error (fn [& _] :buddy-error)]
                        (is (= :buddy-error (sut/authenticated-handler {}))))))

(deftest access-rules-test
  (is (not-empty sut/access-rules)))

(deftest authorize-request-test
  (let [authorize-request-fn #'sut/authorize-request]
    (testing "returns `[match res]` if a request is matched with access-rules"
             (with-redefs [access/match-access-rules (fn [& _] :match)
                           access/apply-matched-access-rule (fn [& _] :res)]
                          (is (= [:match :res] (authorize-request-fn {} {})))))
    (testing "returns `[nil true]` if a request is no matched"
             (with-redefs [access/match-access-rules (fn [& _] false)]
                          (is (= [nil true] (authorize-request-fn {} {})))))))
