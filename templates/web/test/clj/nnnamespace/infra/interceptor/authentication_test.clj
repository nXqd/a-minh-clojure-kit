(ns nnnamespace.infra.interceptor.authentication-test
  (:require
   [buddy.hashers :as hashers]
   [clojure.spec.alpha :as s]
   [clojure.test :refer :all]
   [nnnamespace.domain.user.core :as user]
   [nnnamespace.infra.pedestal.interceptor.authentication :as auth]))

(deftest login-test
  (testing "returns token in body with 200 status if user authenticated"
           (with-redefs [user/authenticated? (fn [& _] true)
                         auth/generate-token (fn [& _] "token")]
                        (let [login-fn #'auth/login]
                          (is (= {:headers {}, :status 200, :body {:token "token"}}
                                 (login-fn {}))))))
  (testing "returns unauthenticated response with 400 status if user is not authenticated"
           (with-redefs [user/authenticated? (fn [& _] false)]
                        (let [login-fn #'auth/login]
                          (is (= auth/unauthenticated (login-fn {})))))))

(deftest valid-user-creation?-test
  (testing "returns false if input is invalid"
           (with-redefs [s/valid? (fn [& _] false)]
                        (let [res (auth/valid-user-creation? {} {})]
                          (is (= false res)))))
  (testing "returns false if user with the same email is existed"
           (with-redefs [s/valid? (fn [& _] true)
                         user/by-email (fn [& _] {:user/email "email"})]
                        (let [res (auth/valid-user-creation? {} {})]
                          #_(is (= false res)))))
  (testing "returns true if user input is valid and user with the same email is not existed."
           (with-redefs [s/valid? (fn [& _] true)
                         user/by-email (fn [& _] nil)]
                        (let [res (auth/valid-user-creation? {} {})]
                          (is (= true res))))))

(deftest signup-valid-handler-test
  (testing "returns nil if user is not created successfully."
           (with-redefs [hashers/derive (fn [& _] "password")
                         user/create (fn [& _] nil)]
                        (let [res (auth/signup-valid-handler {} {} {} {})]
                          (is (= auth/unauthenticated2 res)))))
  (testing "returns token if user is created successfully."
           (with-redefs [hashers/derive (fn [& _] "password")
                         user/create (fn [& _] :user)
                         auth/generate-token (fn [& _] "token")]
                        (let [res (auth/signup-valid-handler {} {} {} {})]
                          (is (= {:headers {}, :status 200, :body {:token "token"}} res))))))

(deftest signup-test
  (testing "returns 400 if user and password is invalid"
           (with-redefs [auth/valid-user-creation? (fn [& _] false)]
                        (let [res (auth/signup {})]
                          (is (= auth/unauthenticated2 res)))))

  (testing "returns unauthenticated response with 400 status if user is not authenticated"
           (with-redefs [auth/valid-user-creation? (fn [& _] true)
                         auth/signup-valid-handler (fn [& _] {:headers {}
                                                              :status 200
                                                              :body {:token "token"}})]
                        (let [res (auth/signup {})]
                          (is (= {:headers {}
                                  :status 200
                                  :body {:token "token"}} res))))))

