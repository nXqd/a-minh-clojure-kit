(ns nnnamespace.test-helper.service-test
  (:require
   [cheshire.core :as json]
   [io.pedestal.test :refer :all]))

(defn add-token
  "Adds a valid auth token to a given URL"
  ([username url])
  ([service username url]))

(defn get-raw
  "Gets a resp"
  [service url]
  (response-for service :get url))

#_(defn get-json-header-token
    "Gets the url passing the token of the given user as a header"
    [username url]
    (response-for service :get url :headers {"x-obb-auth-token" (auth/token-for {:user username})}))

(defn- parse-json
  "Parses the resp json"
  [raw]
  (try
    (json/parse-string raw (fn [k] (keyword k)))
    (catch Exception e (str "Error parsing JSON: " raw))))

(defn get-json
  "Gets a json resp"
  ([service url]
   (let [resp (get-raw service url)]
     [(-> resp :body parse-json) (resp :status)]))
  ([service username url]
   (get-json (add-token service username url) url)))

(defn get-headers
  "Gets the resp headers"
  [service url]
  (-> (get-raw service url)
      :headers))

(defn post-json
  "Posts a json req"
  [service username url obj]
  (let [authed-url (add-token username url)
        data (json/generate-string obj)
        resp (response-for service :post url
                           :body data
                           :headers {"Content-Type" "application/json"})]
    [(-> resp :body) (:status resp)]))

(defn put-json
  "PUTs a json req"
  [service username url obj]
  (let [authed-url (add-token username url)
        data (json/generate-string obj)
        resp (response-for service :put authed-url
                           :body data
                           :headers {"Content-Type" "application/json"})]
    [(-> resp :body parse-json) (:status resp)]))
