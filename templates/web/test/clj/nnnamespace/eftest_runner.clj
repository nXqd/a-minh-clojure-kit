(ns nnnamespace.eftest-runner
  (:require
   [eftest.runner :refer [find-tests run-tests]]))

(defn main
  []
  (run-tests (find-tests "test"))
  (System/exit 0))
