#!/usr/bin/env bash
main() {
  gcloud compute copy-files --project "sliceview-realestate" --zone "us-west1-a" target/app.jar instance-1:~/app/app.jar
  # gcloud compute ssh nxqd@instance-1 --project "sliceview-realestate" --zone "us-west1-a" -- 'ping -c 1 google.com'
  # gcloud compute copy-files --project "sliceview-realestate" --zone "us-west1-a" resources instance-1:~/app/resources
  # gcloud compute --project "sliceview-realestate" ssh --zone "us-west1-a" "instance-1"
}

main
