# Project template
Run this command to change to new namespace
```
grep -lr --exclude-dir=".git" -e "nnnamespace" . | xargs sed -i "s/nnnamespace/somethingelse/g"
```

Change all folder's name in src/clj[c|s]/nnnamespace to the name you just changed above.

# TODO
- Setup blackbox to protect your config.clj
- Update your config.cljc

# Project features
- ENV: environment variable is set to development by default.
  ```
  development, staging, development
  ```
- i18n: Tongue from tonsky
- graphql
- web3 
- Database backend:
  SQL: yesql
  Datomic

### Backend
- Graphql is available at
```sh
curl localhost:8080/graphql -X POST -H "content-type: application/graphql" -d '{ droid { id } }'
```
