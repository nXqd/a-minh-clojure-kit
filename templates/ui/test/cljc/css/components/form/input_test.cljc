(ns css.components.form.input-test
  (:require
   [rum.core :as rum]
   [css.components.form-rum :as form]
   #?@(:cljs [[cljs.test :refer-macros [deftest is]]])
   #?@(:clj [[clojure.test :refer :all]])))

#?(:clj
   (deftest server-render-test
     (let [res (rum/render-html (form/form-ui {}))]
       (is (not (empty? res))))))

