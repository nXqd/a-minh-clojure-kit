(ns css.components.image-grid-rum-test
  (:require
   [rum.core :as rum]
   [css.components.image-grid :as grid]
   #?@(:cljs [[cljs.test :refer-macros [deftest is]]])
   #?@(:clj [[clojure.test :refer :all]])))

#?(:clj
   (deftest server-render-test
     (let [res (rum/render-html
                (grid/image-grid-ui {:images
                                     ["https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                                      "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                                      "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"]}))]
       (is (not (empty? res))))))
