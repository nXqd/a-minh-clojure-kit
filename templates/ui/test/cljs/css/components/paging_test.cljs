(ns css.components.paging-test
  (:require
   [css.components.paging :as cut]
   [cljs.test :refer-macros [deftest is are testing]]))

(deftest test-process-from-to
  (testing "returns the right range"
           (let [ret (cut/process-from-to 1 10 35)]
             (is (= ret [1 4])))))
