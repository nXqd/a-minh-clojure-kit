(ns ui.button-test
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard deftest]]))

;; om
(defui ButtonNormal
  Object
  (render [this]
          (sab/html [:button.button "Normal om next button"])))

(def button-normal (om/factory ButtonNormal))

(defcard om-normal-button
  (button-normal))

(defcard normal
  (sab/html
   [:div
    [:button.button "Normal button"]
    [:button.button {:disabled true}
     "Disabled button"]]))

(defcard variations
  (sab/html
   [:div
    [:button.button.button--primary "button primary"]
    [:button.button.button--small "Button small"]
    [:button.button.button--block "Block button"]]))

(defcard button-counter
  (sab/html
   [:button.button "Number button"
    [:span.counter "12"]]))

(defcard button-icon
  (sab/html
   [:div
    [:button.button
     [:i.fa.fa-plus]
     "Add something new"]
    [:p]
    [:button.button.button--icon
     [:i.fa.fa-plus]]]))

(defcard button-group
  (sab/html
   [:div.button-group
    [:button.button "button 1"]
    [:button.button "button 2"]
    [:button.button "button 3"]
    [:a.button "button 4"]]))
