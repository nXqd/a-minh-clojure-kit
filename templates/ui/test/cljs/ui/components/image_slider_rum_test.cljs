(ns ui.components.image-slider-rum-test
  (:require
   [css.components.image-slider-rum :refer
    [image-slider-ui one-item-slider-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard image-slider-card
  (image-slider-ui
   {:current-image-idx 1
    :images ["https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"]}))

(defcard image-slider-card-with-label
  (image-slider-ui
   {:current-image-idx 2
    :show-order? true
    :images [["https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg" "Some label here"]
             ["https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg" "Some label here 1"]
             "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"]}))

(defcard one-item-slider-multiple-card
  (one-item-slider-ui
   {:items          [[:img.image.image--contained
                      {:style {:padding "1rem"}
                       :src   "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"}]
                     [:img.image.image--contained
                      {:style {:padding "1rem"}
                       :src   "https://is1-3.housingcdn.com/012c1500/22981e1f1e0a344b3bf47995ef9ef3c3/v3/_m.jpg"}]
                     [:img.image.image--contained
                      {:style {:padding "1rem"}
                       :src   "https://images-housing.s3.amazonaws.com/new_projects/1205/slide_show/proccessed/21935/_m.jpg"}]
                     [:img.image.image--contained
                      {:style {:padding "1rem"}
                       :src   "https://is1-3.housingcdn.com/012c1500/22981e1f1e0a344b3bf47995ef9ef3c3/v3/_m.jpg"}]]
    :item-per-slide 2
    :step           2
    :styles         {:main     {:border-radius "2px"}
                     :controls {:space "1.5rem" :font-size "1.8rem"}}}))

