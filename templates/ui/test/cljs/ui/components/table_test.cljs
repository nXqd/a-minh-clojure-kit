(ns ui.components.table-test
  (:require
   [css.components.table :refer [table-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard table-normal (table-ui))
