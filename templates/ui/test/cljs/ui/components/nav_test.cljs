(ns ui.components.nav-test
  (:require
   [css.components.nav :as sut])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard sidebar-card (sut/sidebar))
