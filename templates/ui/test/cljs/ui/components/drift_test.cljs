(ns ui.components.drift-test
  (:require
   [css.components.drift :refer [image-zoom]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard image-zoom-card (image-zoom))

