(ns ui.components.accordion-rum-test
  (:require
   [taoensso.timbre :as log]
   [css.components.accordion-rum
    :refer [accordions-ui
            accordion-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard accordion-card (accordion-ui {:title   "Title"
                                       :content "Test"
                                       :on-click #(log/debug "click ne" %)}))

(defcard accordions-single-card
  (accordions-ui {:type         :single
                  :on-close-all #(log/debug "close all")
                  :accordions
                  [{:title    "Title 1"
                    :content  "Content 1"
                    :on-click #(log/debug "click" %)}
                   {:title   "Title 2"
                    :content "Content 2"}]}))

(defcard accordions-multiple-card
  (accordions-ui {:type :multiple
                  :index 0
                  :accordions
                  [{:title    "Title 1"
                    :content  "Content 1"
                    :on-click #(log/debug "click" %)}
                   {:title   "Title 2"
                    :content "Content 2"}]}))
