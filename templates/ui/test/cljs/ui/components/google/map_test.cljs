(ns ui.google.map-test
  (:require
   [css.google.map :refer [places-autocomplete-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard places-autocomplete-card (places-autocomplete-ui))
