(ns ui.google.custom-marker-test
  (:require
   [css.google.custom-marker :refer [custom-marker-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard custom-marker-card (custom-marker-ui))
