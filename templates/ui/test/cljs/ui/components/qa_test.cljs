(ns ui.components.qa-test
  (:require
   [taoensso.timbre :as timbre :refer-macros [debug info]]
   [css.components.qa :refer [qa-item-ui qa-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard qa-item-card (qa-item-ui {:db/id 1
                                   :question/answers [{:answer/content "USB Cord USB Charger - Rechargeable Battery is built in"
                                                       :answer/date "June 29, 2014"
                                                       :answer/user {:user/name "R. Tisinger "
                                                                     :user/titles [:constructor :developer] ;; relative
}}]
                                   :question/title  "What are the included items?"}))
(defcard qa-empty-card (qa-ui))
(defcard qa-full-card (qa-ui {:project/questions [{:db/id 1
                                                   :question/answers [{:answer/content "USB Cord USB Charger - Rechargeable Battery is built in"
                                                                       :answer/date "June 29, 2014"
                                                                       :answer/user {:user/name "R. Tisinger "
                                                                                     :user/titles [:constructor :developer] ;; relative
}}]
                                                   :question/title  "What are the included items?"}
                                                  {:db/id 2
                                                   :question/answers [{:answer/content "USB Cord USB Charger - Rechargeable Battery is built in"
                                                                       :answer/date "June 29, 2014"
                                                                       :answer/user {:user/name "R. Tisinger "
                                                                                     :user/titles [:constructor :developer] ;; relative
}}]
                                                   :question/title  "What are the included items?"}]}))
(defcard qa-no-value-state (qa-ui {:project/questions []
                                   :input "some value"}))
