(ns ui.components.tab-test
  (:require
   [taoensso.timbre :as log]
   [css.components.tab :refer [tab-ui test1-ui test2-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard tab-card (tab-ui {:init-content "Init content"
                           :tabs {:hello {:label "Hello"
                                          :selected? true
                                          :component test1-ui}
                                  :settings {:label "Settings"
                                             :component test2-ui}}}))
