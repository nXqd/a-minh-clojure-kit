(ns ui.components.paging-test
  (:require
   [taoensso.timbre :as log]
   [css.components.paging :refer
    [paging-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard paging-card (paging-ui {:current 1
                                 :count 55
                                 :on-change #(log/debug %)
                                 :styles {:button {:width "2rem"
                                                   :height "2rem"}}}))
