(ns ui.components.googlemap-rum-test
  (:require
   [css.components.googlemap-rum :refer [googlemap-ui test-ui
                                         googlemap-heatmap-ui
                                         googlemap-custom-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard googlemap-card
  (googlemap-ui
   {:styles         {:main {:height "40rem"}}
    :options        {"center"    {:lat 10.823099 :lng 106.629664}
                     "mapTypeId" "roadmap"
                     "zoom"      15}
    :circle-options {:radius      1500
                     :strokeColor "#000"
                     :fillOpacity 0.08}}))

#_(defcard test-card (test-ui))

#_(defcard googlemap-heatmap-card
    (googlemap-heatmap-ui))

(def options
  {"zoom"      3
   "center"    {:lat 0 :lng -180}
   "mapTypeId" "terrain"})

#_(defcard googlemap-flight-path
    (googlemap-custom-ui
     {:options options}))
