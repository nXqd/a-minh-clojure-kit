(ns ui.components.stripe-test
  (:require
   [css.components.stripe :refer [stripe-custom-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard stripe-custom-card (stripe-custom-ui))
