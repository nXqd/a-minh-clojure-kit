(ns ui.components.upload-test
  (:require
   [sablono.core :as sab :include-macros true]
   [css.components.upload :as upload])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard upload-card (upload/upload-ui {}))
(defcard upload-item-card (upload/upload-item-ui {}))
(defcard fineuploader-card (upload/fineuploader-ui))

