(ns ui.components.tooltip-test
  (:require
   [css.components.tooltips :refer [simple-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard tooltip-card
  (simple-ui {}))
