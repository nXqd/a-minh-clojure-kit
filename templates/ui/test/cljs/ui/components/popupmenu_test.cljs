(ns ui.components.popupmenu-test
  (:require
   [css.components.popupmenu :refer [popupmenu-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard popupmenu-card (popupmenu-ui))
