(ns ui.components.modal-test
  (:require
   [css.components.modal :refer [page-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard page-card (page-ui))
