(ns ui.components.placeholders-test
  (:require
   [css.components.placeholders :refer [placeholder-ui
                                        placeholder-property-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard placeholder-card (placeholder-ui {}))
(defcard placeholder-property-card (placeholder-property-ui {}))
