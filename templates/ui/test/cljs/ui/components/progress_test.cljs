(ns ui.components.progress-test
  (:require
   [css.components.progress :refer [progress]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard progress (progress {:percent "58%"
                             :colors []
                             :message "Something else"}))
