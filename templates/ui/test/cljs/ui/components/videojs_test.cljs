(ns ui.components.videojs-test
  (:require
   [cljs-css-modules.macro :refer-macros [defstyle]]
   [css.components.form.input :as input]
   [rum.core :as rum]
   [css.styles.colors :as colors])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defstyle header-styles
  [:.header-inner {:align-items "center"}]
  [:.header-logo {:padding 0
                  :margin 0
                  :display "inline-block"
                  :height "50px"}
   [:img {:display "block"
          :height "100%"}]]

  [:.header-link
   {:margin-right "10px"}])

(defn header-logo
  [logo-src]
  [:h1.header-logo
   {:class [(:header-logo header-styles)]}
   [:a {:href "/"}
    [:img {:src logo-src}]]])

(rum/defc header-ui < rum/static
  [{:keys [logo-src]}]
  [:#header
   {:class [(:header header-styles)]}
   [:.g--container
    [:div
     {:class [(:header-inner header-styles) :g-x :g-x--m]}
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      (header-logo logo-src)]
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8 :text--right]}
      (->> ["Tung Ng." "Đăng xuất"]
           (map (fn [x]
                  [:a
                   {:class [(:header-link header-styles)]
                    :href "#"}
                   x])))]]]])

;; remember to put videojs to your html
(defn video-player-didmount
  [states]
  (let []))

(defn user-answer-time
  [[username content not-disabled? time]]
  [:.user-answer-time
   {:class [:my--1-0]}
   [:div
    {:class [:d--flex :mb--0-5]}
    [:span username]

    [:span
     {:style {:margin-left "auto"}}
     (if (= time "now")
       [:strong "Đang chỉnh sửa."]
       [:span time])]]

   [:div
    (input/input-ui {:on-change (fn [e])
                     :type [:input :text]
                     :disabled (not not-disabled?)
                     :value content})]])

(defn from-to-block
  [from to]
  [:.from-to-block
   [:span "Trích đoạn từ "]
   (input/input-ui {:class "from"
                    :styles {:main {:width "70px"
                                    :display "inline-flex"}}
                    :value from})
   [:span " đến "]
   (input/input-ui {:class "to"
                    :styles {:main {:width "70px"
                                    :display "inline-flex"}}
                    :value to})])

(defn fixing-block
  [{:keys [from to editing? answers]}]
  (let [border (str "2px solid"
                    (if-not editing?
                      "#0288D1"
                      "#F44336"))]
    [:.fixing-block
     {:style {:border border
              :border-radius "4px"}
      :class [:p--1-0 :mb--1-0]}
     (from-to-block from to)
     (->> answers (map user-answer-time))]))

(rum/defc video-player-ui < rum/static
  {:did-mount video-player-didmount}
  []
  [:.audio
   {:class [:g--container]}

   [:div
    {:class [:mb--3-0 :text--center]}
    [:h2 "Chương trình dịch thuật cho customer service - Giọng nói ra chữ."]
    [:small "V1.5-11092017"]]

   [:div
    {:class [:g-x :g-x--m]}

    [:h6
     {:class [:mb--1-0]}
     "Hội thoại khách hàng #CU5432 - 11-19/09/2017"]

    [:audio
     {:class [:c :c-sm--16 :c-md--16 :c-lg--16
              :mb--1-0]
      :src "http://f9.stream.nixcdn.com/046aa1a039190af9b812f1b1c22ba209/59c0c81d/NhacCuaTui950/AnhMatBuon-PhamDinhThaiNgan-5181551.mp3?t=1505807170579"
      :controls true}
     "Your browser does not support the audio element."]

    [:.top-buttons
     {:class [:c :c-sm--16 :c-md--16 :c-lg--16
              :mb--2-0 :text--right]}
     [:button.button
      "Chọn file"]]

    [:.answers
     {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
     (->> [{:from 0
            :to 1.34
            :editing? true
            :answers
            [["Tuyen" "Nếu ngay từ chiều ngày 10/2, họ thông tin chính xác cho tôi về sự cố mất mạng thì sáng ngày 11/2 tôi đã không phải xin nghỉ làm để chờ nhân viên kỹ thuật. " false "5/09/2017"]
             ["Tung Ng." "Nếu ngay từ chiều ngày 10/3, họ thông tin chính xác cho tôi về sự cố mất mạng thì sáng ngày 11/2 tôi đã không phải xin nghỉ làm để chờ nhân viên kỹ thuật. " true "now"]]}

           {:from 1.34
            :to 2.5
            :answers
            [["Tuyen" "Tôi thấy FPT Telecom rất coi thường khách hàng, không có bất kỳ một thông tin nào cho khách hàng. Tôi đã phải gọi đi gọi lại tới 5 cuộc điện thoại, nhưng những gì nhận được là chờ kiểm tra" false "5/09/2017"]
             ["Tung Ng." "Tôi thấy FPT Telecom rất coi thường khách hàng, không có bất kỳ một thông tin nào cho khách hàng. Tôi đã phải gọi đi gọi lại tới 5 cuộc điện thoại, " true]]}

           {:from 2.5
            :to 3.86
            :answers
            [["Tuyen" "Anh Q. cũng cho biết, cơ quan làm việc cách nhà 15km, công việc lại rất bận, và phải xin nghỉ một buổi sáng vô ích vì cung cách làm việc vô trách nhiệm của FPT.\n\n" false "5/09/2017"]
             ["Tung Ng." "Anh Q. cũng cho biết thêm, cơ quan làm việc cách nhà 15km, công việc lại rất bận, và phải xin nghỉ một buổi sáng vô ích vì cung cách làm việc vô trách nhiệm của FPT.\n\n" true]]}]
          (map (fn [x]
                 (fixing-block x))))]

    [:.edit
     {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
     [:div
      {:class [:mb--1-0]}
      [:h6.mb--0-25 "Máy dịch"]
      [:textarea
       {:read-only true}
       "Nếu ngay từ chiều ngày 10/3, họ thông tin chính xác tôi sự cố mạng thì sáng 11/2 tôi đã không phải xin nghỉ làm để chờ nhân viên kỹ thuật. "]]

     [:div
      {:class [:d--flex]
       :style {:flex-direction "column"}}
      [:h6.mb--0-25 "Chỉnh sửa lại"]
      [:textarea "Nếu ngay từ chiều ngày 10/3, họ thông tin chính xác cho tôi về sự cố mất mạng thì sáng ngày 11/2 tôi đã không phải xin nghỉ làm để chờ nhân viên kỹ thuật. "]
      [:button.button
       {:class [:mt--0-5]
        :style {:margin-left "auto"}}
       "Cập nhật"]]]

    [:.buttons
     {:class [:c :c-sm--16 :c-md--16 :c-lg--16
              :text--right]}
     [:button.button
      "Chấp nhận"]]]])

(defcard video-player-card (video-player-ui))

(rum/defc page-ui < rum/static
  []
  [:.page-ui
   {:class [:g--container]}
   (header-ui {})
   [:div
    {:class [:g-x :g-x--m]}]
   (video-player-ui)])

(defcard page-card (page-ui))

