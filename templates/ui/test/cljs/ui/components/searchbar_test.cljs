(ns ui.components.searchbar-test
  (:require
   [css.components.searchbar :refer [search-bar]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard search-bar (search-bar))
