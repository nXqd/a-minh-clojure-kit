(ns ui.components.footer-test
  (:require
   [rum.core :as rum]
   [css.components.footer :as sut])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(rum/defc footer-ui
  []
  (sut/bootstrap-footer))

(defcard footer-card (footer-ui))
