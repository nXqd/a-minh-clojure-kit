(ns ui.components.menus-test
  (:require
   [css.components.menus :refer [menu-left]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard menu-left (menu-left))
