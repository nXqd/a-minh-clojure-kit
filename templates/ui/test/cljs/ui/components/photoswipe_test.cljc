(ns ui.components.photoswipe-test
  (:require
   [css.components.photoswipe :refer [photoswipe-ui]])
  #?(:cljs
     (:require-macros
      [devcards.core :as dc :refer [defcard]])))

#?(:cljs
   (defcard photoswipe-card
     (photoswipe-ui
      {:styles {:grid {:main {:width "100%"
                              :height "20rem"}}}
       :items
       [{:src "https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg"
         :msrc "https://farm2.staticflickr.com/1043/5186867718_06b2e9e551_b.jpg"
         :title "Caption 1"}
        {:src "https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg"
         :msrc "https://farm7.staticflickr.com/6175/6176698785_7dee72237e_b.jpg"
         :title "Caption 2"}]})))
