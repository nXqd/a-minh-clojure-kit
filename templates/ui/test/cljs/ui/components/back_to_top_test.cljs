(ns ui.components.back-to-top-test
  (:require
   [css.components.back-to-top :refer [back-to-top-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard back-to-top-card (back-to-top-ui))
