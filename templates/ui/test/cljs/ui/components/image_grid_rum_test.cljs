(ns ui.components.image-grid-rum-test
  (:require
   [css.components.image-grid :refer [image-grid-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard image-grid-card
  (image-grid-ui {:images
                  ["https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"
                   "https://is1-2.housingcdn.com/012c1500/7c6ff38d10bbea51d46bb1fa0edf4339/v2/_m.jpg"]
                  :on-click (fn [e order]
                              (js/console.log "order" order))}))
