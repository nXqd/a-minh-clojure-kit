(ns ui.components.cards-test
  (:require
   [css.components.cards :refer [card-ui cards-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard card (card-ui))
(defcard cards (cards-ui))
