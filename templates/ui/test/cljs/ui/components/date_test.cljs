(ns ui.components.date-test
  (:require
   [taoensso.timbre :as log]
   [css.components.date :refer [g-input-datepicker
                                g-date-picker-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard date-card (g-input-datepicker {:on-change (fn [value]
                                                     (log/debug "value" value))}))

(defcard g-date-picker-card (g-date-picker-ui {}))
