(ns ui.components.combobox-test
  (:require
   [taoensso.timbre :as log]
   [css.components.combobox :refer [combobox-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defn on-change
  [val]
  (log/debug "value" val))

(def options
  {"Another item 123" {:label "Another item 123"
                       :value :another-item}
   "Inbox" {:label "Inbox"
            :value :inbox}})

(defcard combo-card
  (combobox-ui {:placeholder "Default placeholder"
                :on-change on-change
                :options options}))
