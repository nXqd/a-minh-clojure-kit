(ns ui.components.graphs.line-chart-test
  (:require
   [css.components.graphs.line-chart :as sut])
  (:require-macros
   [devcards.core :as dc :refer [defcard defcard-rg]]))

(defcard linechart-card (sut/line-chart {}))
