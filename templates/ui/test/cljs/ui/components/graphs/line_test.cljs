(ns ui.components.graphs.line-test
  (:require-macros
   [devcards.core :as dc :refer [defcard defcard-rg]]))

#_(defcard-rg line* [line])
#_(defcard line-card (line-ui {:options {:title "Compnay Performance"
                                         :curveType "function"
                                         :legend {:position "bottom"}}
                               :data [["Year", "Sales", "Expenses"]
                                      ["2004",  1000,      400]
                                      ["2005",  1170,      460]
                                      ["2006",  660,       1120]
                                      ["2007",  1030,      540]]}))
