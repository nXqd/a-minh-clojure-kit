(ns ui.components.graphs.candlestick-test
  (:require
   [css.components.graphs.candlestick :as sut])
  (:require-macros

   [devcards.core :as dc :refer [defcard defcard-rg]]))

(defcard candlestick-card (sut/candlestick-ui {}))
