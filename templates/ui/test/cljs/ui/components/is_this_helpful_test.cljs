(ns ui.components.is-this-helpful-test
  (:require
   [taoensso.timbre :as log]
   [css.components.is-this-helpful :refer [is-this-helpful-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard is-this-helpful-card (is-this-helpful-ui {:message "Ok sir"
                                                   :on-yes #(log/debug "yes")
                                                   :on-no #(log/debug "no")}))
