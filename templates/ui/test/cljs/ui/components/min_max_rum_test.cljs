(ns ui.components.min-max-rum-test
  (:require
   [taoensso.timbre :as log]
   [css.components.min-max-rum :refer [min-max-ui min-max-static-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard min-max-card (min-max-ui {:placeholder "Budget"
                                   :values {:min {:label "200tr" :value 2}
                                            :max {:label "1 ti" :value 4}}
                                   :on-change #(log/debug %)
                                   :mins [{:label "200tr" :value 2}
                                          {:label "300tr" :value 3}
                                          {:label "500tr" :value 5}
                                          {:label "700tr" :value 7}
                                          {:label "900tr" :value 9}]
                                   :maxs [{:label "1 ti" :value 2}
                                          {:label "1.5 ti" :value 4}
                                          {:label "2 ti" :value 6}
                                          {:label "3 ti" :value 9}
                                          {:label "Max" :value :infinite}]}))

(defcard min-max-static-card (min-max-static-ui {:placeholder "Budget"
                                                 :values {:min {:label "200tr" :value 2}
                                                          :max {:label "1 ti" :value 4}}
                                                 :on-change #(log/debug %)
                                                 :styles {:mins {:padding-left "1rem"}
                                                          :maxs {:padding-right "1rem"}}
                                                 :mins [{:label "200tr" :value 2}
                                                        {:label "300tr" :value 3}
                                                        {:label "500tr" :value 5}
                                                        {:label "700tr" :value 7}
                                                        {:label "900tr" :value 9}]
                                                 :maxs [{:label "1 ti" :value 4}
                                                        {:label "1.5 ti" :value 4}
                                                        {:label "2 ti" :value 6}
                                                        {:label "3 ti" :value 9}
                                                        {:label "Max" :value :infinite}]}))
