(ns ui.components.popup-test
  (:require
   [css.components.popup :refer [popup-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard popup-card (popup-ui))
