(ns ui.components.step-test
  (:require
   [css.components.step :refer
    [step-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard step-card (step-ui))
