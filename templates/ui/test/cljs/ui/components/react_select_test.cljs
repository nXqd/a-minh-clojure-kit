(ns ui.components.react-select-test
  (:require
   [css.components.react-select :as sut])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard react-select-card (sut/react-select-ui {}))

