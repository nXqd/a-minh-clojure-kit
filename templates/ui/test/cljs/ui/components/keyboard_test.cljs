(ns ui.components.keyboard-test
  (:require
   [css.components.keyboard :refer [keyboard-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard keyboard-card (keyboard-ui))
