(ns ui.components.breadcrumb-rum-test
  (:require
   [css.components.breadcrumb-rum :refer
    [breadcrumb-ui breadcrumb-simple-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard bread-crumb-card (breadcrumb-ui
                           {:breadcrumbs
                            [{:label "ERPNext"
                              :link  "/test"}
                             {:label "Setup"
                              :link  "/test"}
                             {:label "Email Account"
                              :link  "/test"}]}))

(defcard breadcrumb-simple-card
  (breadcrumb-simple-ui {:breadcrumbs [{:link "#" :label "first"}
                                       {:link "#" :label "second"}
                                       {:link "#" :label "third"}]}))
