(ns ui.components.infinite-list-test
  (:require
   [rum.core :as rum]
   [css.components.infinite-list :refer [infinite-list] :as il])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(rum/defc country-card < rum/static {:key-fn (fn [props] props)}
  "Render the country list item."
  [props]
  [:div.country-card
   [:img.image.image--contained
    {:style {:width "200px"
             :padding "1rem"}
     :src   "https://is1-3.housingcdn.com/012c1500/22981e1f1e0a344b3bf47995ef9ef3c3/v3/_m.jpg"}]])

(def countries (range 150))

(rum/defcs container
  "Always pass scroll-target with height and overflowY"
  [state]
  [:div#scroll-target
   {:style {:overflowY "scroll"
            :height "15rem"}}
   (infinite-list
    (map-indexed #(country-card %1) countries)
    {:scroll-target il/scroll-target-t})])

(defcard infinite-card (container))
