(ns ui.components.clipboard-test
  (:require
   #_[css.components.clipboard :refer [playground-ui
                                       clipboard-button-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

#_(defcard clipboard-button-card (clipboard-button-ui {:label "Something"}))
#_(defcard playground-card (playground-ui))
