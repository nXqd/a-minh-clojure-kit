(ns ui.components.ag-grid-test
  (:require
   [rum.core :as rum]
   [css.components.ag-grid.stock-grid :as sg]
   [css.components.ag-grid :refer [grid-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(def data
  (atom
   [{:make {:label "test123123"}
     :model {:label "CLica5452342342j"
             :uri "Claci"}  "price" 35000}]))

(def new-data
  [{:make {:label "test"}
    :model {:label "CLica"
            :uri "Claci"}  "price" 35000}
   {:make {:label "test"}
    :model {:label "CLica"
            :uri "Claci"}  "price" 35000}])

(rum/defc grid-container-test < rum/reactive
  []
  [:div
   (grid-ui {:data (rum/react data)})
   [:button
    {:on-click #(reset! data new-data)}
    "Change data"]])

(defcard grid-card (grid-container-test))
(defcard stock-card (sg/grid-ui))
