(ns ui.components.infowindow-test
  (:require
   [css.components.infowindow :refer [infowindow-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard infowindow-card (infowindow-ui))
