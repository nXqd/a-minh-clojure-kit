(ns ui.components.slider-test
  (:require
   [taoensso.timbre :as log]
   [css.components.slider :refer [two-thumb-slider-ui]])
  (:require-macros
   [devcards.core :refer [defcard]]))

(defcard two-thumb-slider-card
  (two-thumb-slider-ui
   {:on-change (fn [val]
                 (log/debug "val" val))}))
