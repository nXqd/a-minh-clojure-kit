(ns ui.grid-test
  (:require
   [clojure.string :as cstr])
  (:require-macros
   [devcards.core :refer [defcard defcard-rg]]))

(def grid-cell-style {:background-color "#e7f6e0"
                      :padding "0.5rem"
                      :border "1px solid #c8eab9"})

(defn page []
  [:div
   [:h2 "Container"]
   [:p]
   [:div.container
    [:div.grid
     [:div.grid-cell.grid-lg-2.grid-md-4.grid-sm-8
      {:style grid-cell-style}
      ".grid-lg-2.grid-md-4.grid-sm-8"]
     [:div.grid-cell
      {:style grid-cell-style}
      [:div.demo "Resizinggg"]]]]

   [:h2 "Mobile"]
   [:p]
   [:div.container
    [:div.grid
     [:div.grid-cell.grid-sm-3
      {:style grid-cell-style}
      [:div.demo "1 of 3"]]
     [:div.grid-cell.grid-sm-13
      {:style grid-cell-style}
      [:div.demo "1 of 3"]]]]

   [:h2 "Equal"]
   [:p]
   [:div.container
    [:div.grid
     [:div.grid-cell
      {:style grid-cell-style}
      [:div.demo "1 of 3"]]
     [:div.grid-cell
      {:style grid-cell-style}
      [:div.demo "1 of 3"]]
     [:div.grid-cell
      {:style grid-cell-style}
      [:div.demo "1 of 3"]]]]

   [:h2 ".container.container--padded"]
   [:p]
   [:div.container.container--padded
    (let [class ["grid-lg-2" "grid-md-4" "grid-sm-8"]
          text (cstr/join "." class)]
      [:div.grid
       [:div.grid-cell
        {:class class
         :style grid-cell-style} text]

       [:div.grid-cell
        {:style grid-cell-style}
        [:div.demo "Resizinggg"]]])]

   [:h2 ".container.container--fluid"]
   [:p]
   [:div.container.container--fluid
    [:div.grid
     (let [class ["grid-lg-2" "grid-md-4" "grid-sm-8"]
           text (cstr/join "." class)]
       [:div.grid-cell.grid-lg-2.grid-md-4.grid-sm-8
        {:class class :style grid-cell-style}
        [:div text]])]]])

(defcard-rg page-card [page])
