(ns ui.grid-xy-test
  (:require
   [clojure.string :as cstr]
   sablono.interpreter
   [cljs-css-modules.macro :refer-macros [defstyle]]
   [rum.core :as rum])
  (:require-macros
   [devcards.core :refer [defcard defcard-rg]]))

(defstyle styles
  [:.cell-style {:background "#1779ba"
                 :line-height "50px"
                 :height "50px"
                 :color "white"
                 :text-align "center"
                 :margin-bottom "30px"}])

(rum/defc basics < rum/static
  []
  [:div
   [:div
    {:class [:g-x]}
    [:div
     {:class [:c (:cell-style styles)]} "Full width cell"]
    [:div
     {:class [:c (:cell-style styles)]} "Full width cell"]]
   [:div
    {:class [:g-x]}
    [:div
     {:class [:c :c-sm--8 (:cell-style styles)]} "Full width cell"]
    [:div
     {:class [:c :c-sm--8 (:cell-style styles)]} "Full width cell"]]
   [:div
    {:class [:g-x]}
    [:div
     {:class [:c :c-md--8 :c-lg--6 (:cell-style styles)]} "Full width cell"]
    [:div
     {:class [:c :c-md--8 :c-lg--10 (:cell-style styles)]} "Full width cell"]]])

(defcard basics-card (basics))

(rum/defc gutter < rum/static
  []
  [:div
   [:h2 "Gutters"]
   [:p "The defining feature of the XY grid is the ability to use margin AND padding grids in harmony.
   To define a grid type, simply set .g-x--m or .g-p-x on the grid."]
   [:h2 "Margin gutters"]
   [:div
    {:class [:g-x :g-x--m]}
    [:div
     {:class [:c :c-md--8 :c-lg--6 (:cell-style styles)]}
     [:.demo "16/8/6 cell"]]
    [:div
     {:class [:c :c-md--8 :c-lg--10 (:cell-style styles)]}
     [:.demo "16/8/10 cell"]]]
   [:h2 "Padding gutters"]
   [:div
    {:class [:g-x :g-x--p]}
    [:div
     {:class [:c :c-md--8 :c-lg--6 (:cell-style styles)]}
     [:.demo "16/8/6 cell"]]
    [:div.demo
     {:class [:c :c-md--8 :c-lg--10 (:cell-style styles)]}
     [:.demo "16/8/10 cell"]]]])

(defcard gutter-card (gutter))

(rum/defc grid-container < rum/static
  []
  [:div
   [:h2 "Grid container"]
   [:p "The grid defaults to the full width of its container. In order to contain the grid, use the .g-container class."]
   [:.g-container
    [:div
     {:class [:g-x :g-x--p]}
     [:div
      {:class [:c :c-md--8 :c-lg--6 (:cell-style styles)]}
      [:.demo "16/8/6 cell"]]
     [:div
      {:class [:c :c-md--8 :c-lg--10 (:cell-style styles)]}
      [:.demo "16/8/10 cell"]]]]])

(defcard grid-container-card (grid-container))

(rum/defc grid-y < rum/static
  []
  [:div
   [:h2 "Grid container"]
   [:p "Please note for vertical grids to work, the grid needs a height.
   You can also use grid frame to create a 100 vertical height grid (or 100% height if nested)."]
   [:div
    {:class [:g--container]}
    [:div
     {:class [:g-y]
      :style {:height "500px"}}
     [:div
      {:class [:c :c-md--8 :c-lg--6 (:cell-style styles)]}
      "16/8/6 cell"]
     [:div
      {:class [:c :c-md--8 :c-lg--10 (:cell-style styles)]}
      "16/8/10 cell"]]]])

(defcard grid-y-card (grid-y))
