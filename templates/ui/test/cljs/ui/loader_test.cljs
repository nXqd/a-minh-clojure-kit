(ns ui.loader-test
  (:require
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard deftest]]))

(defcard loader
  (sab/html
   [:div.loader]))
