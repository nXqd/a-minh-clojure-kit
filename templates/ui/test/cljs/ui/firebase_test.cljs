(ns ui.firebase-test
  (:require
   [taoensso.timbre :as log]
   [firebase-cljs.core :as f]
   [firebase-cljs.database :as fd]
   [firebase-cljs.database.query :as fdq]
   [firebase-cljs.database.reference :as fdr]
   [firebase-cljs.database.datasnapshot :as s])
  (:require-macros
   [devcards.core :refer [defcard deftest]]))

(def opts {:apiKey "AIzaSyBqnanJKgSSYYCJuWllQA30tIy1I_tOCDQ"
           :authDomain "sliceview-realestate.firebaseapp.com"
           :databaseURL "https://sliceview-realestate.firebaseio.com"
           :storageBucket "sliceview-realestate.appspot.com"})

;; initialize Firebase app
;; https://firebase.google.com/docs/reference/js/firebase#.initializeApp
;; https://degree9.github.io/firebase-cljs/firebase-cljs.core.html#var-init
(defonce app (f/init opts))

;; get database for app
;; https://firebase.google.com/docs/reference/js/firebase.database#database
;; https://degree9.github.io/firebase-cljs/firebase-cljs.core.html#var-get-db
(defonce database (f/get-db app))

(def root (fd/get-ref database))

(def project-details (fdr/get-child root "/project-details"))
(def project-details-test (fdr/get-child project-details "/test"))

(fdq/on project-details-test "value"
        (fn [state]
          (let [val (f/->cljs (s/val state))]
            (log/debug "state" val))))

(defcard nothing)
