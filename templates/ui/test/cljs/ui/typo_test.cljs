(ns ui.typo-test
  (:require
   [clojure.string :as cstr]
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard deftest]]))

(defn block-ui
  [{:keys [header content]}]
  [:section.grid
   {:style {:borderTop "1px solid rgb(223, 232, 238)"}}
   [:.grid
    {:style {:padding "2rem 0 1rem 0"
             :justifyContent "center"}}
    [:h3 header]]
   [:.grid
    {:style {:paddingBottom "2rem"}}
    content]])

(def review-tab-default-style
  {:bottom {:class ["grid-cell" "grid-8" "grid-sm-16"]}})

(defcard block-card
  (sab/html
   [:div
    (block-ui
     {:header "About"
      :content [:div "Successful people are those with successful habits. - Monique Holgye"]})

    (block-ui
     {:header "Reviews"
      :content (let [areas ["Thị trường hoạt động" ["Quận 1" "Quận 2"]]
                     experties ["Chuyên môn" ["Quản lý bất động sản" "Cho thuê bất động sản"]]
                     awards ["Giải thưởng" ["Nhân viên sales xuất sắc nhất năm Novaland 2015"]]
                     groups ["Tổ chức" ["Novaland" "Vinhome" "Hiệp hội bất động sản Việt Nam"]]
                     f (fn [[header items]]
                         (when-not (empty? items)
                           [:div.grid
                            {:class (get-in review-tab-default-style [:bottom :class])
                             :style {:flex-direction "column"
                                     :alignItems "flex-start"
                                     :margin "0 0 0.75rem 0"}}
                            [:div {:style {:font-weight "bold"}} header]
                            [:div (cstr/join " | " items)]]))]
                 (map f [areas experties awards groups]))})

    (block-ui
     {:header "Performance stats"
      :content
      [:.grid
       [:.grid
        [:.grid
         {:style {:padding "0.7rem"
                  :border-top "1px solid #eee"}}
         [:h4.grid-cell.grid-4 "1"]
         [:.grid-cell "Current properties for sale"]]

        [:.grid
         {:style {:padding "0.7rem"}}
         [:h4.grid-cell.grid-4 "9"]
         [:.grid-cell "Property sold in the last month"]]

        [:.grid
         {:style {:padding "0.7rem"}}
         [:h4.grid-cell.grid-4 "2.43$"]
         [:.grid-cell "Properties sold"]]]

       [:.grid
        {:style {:padding "0.7rem"
                 :border-top "1px solid #eee"}}
        [:h4.grid-cell.grid-4
         {:style {:color "#f16363"}} "1.23$"]
        [:.grid-cell "Highest price sold"]]
       [:.grid
        {:style {:padding "2rem 0"
                 :lineHeight 2
                 :textAlign "center"}}
        [:small "Data is based on information advertised in the public domain or supplied direct by real estate agents to homely.com.au and may not contain off-market, private sales or non-disclosed prices."]]]})]))

(defcard headers
  (sab/html
   [:div
    [:h1 "H1"]
    [:h2 "H2"]
    [:h3 "H3"]
    [:h4 "H4"]
    [:h5 "H5"]
    [:h6 "H6"]]))

(defcard code
  (sab/html
   [:div
    [:pre
     "h1 {font-family: $helvetica; font-size: golden-ratio(14px,  1);}"]]))

(defcard utilities
  (sab/html
   [:div
    [:div.text--left ".text--left"]
    [:div.text--right ".text--right"]
    [:div.text--center ".text--center"]]))
