(ns ui.list-test
  (:require
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard defcard-rg]]))

(defcard list
  (sab/html
   [:div
    [:ul
     [:li "list 1"]
     [:li "list 2"]
     [:li "list 3"]
     [:li "list 4"]
     [:li "list 5"]]

    [:ol
     [:li "list 1"]
     [:li "list 2"]
     [:li "list 3"]
     [:li "list 4"]
     [:li "list 5"]]]))

(defcard list-image
  (sab/html
   [:div
    [:h2 "Horizontal list"]
    [:p]
    [:ul.list
     (for [_ (range 3)]
       [:li.list-item
        [:img.list-image {:src "https://secure.assets.tumblr.com/images/default_avatar/cube_closed_128.png?_v=b3f6262c706b3aaa775c2e2cd668f4c7"
                          :style {:width "50px"}}]
        [:div.content
         [:div.title "Rachel"]
         [:div.description "Updated the status 10 hours ago."]]])]
    [:p]
    [:h2 "Vertical list"]
    [:p]
    [:ul.list.list--horizontal
     (for [_ (range 2)]
       [:li.list-item
        [:img.list-image {:src "https://secure.assets.tumblr.com/images/default_avatar/cube_closed_128.png?_v=b3f6262c706b3aaa775c2e2cd668f4c7"
                          :style {:width "50px"}}]
        [:div.content
         [:div.title "Rachel"]
         [:div.description "Updated the status 10 hours ago."]]])]]))

(defcard list-striped
  (sab/html
   [:div
    [:ul.list.list--striped
     (for [_ (range 3)]
       [:li.list-item
        [:i.fa.fa-map-marker]
        [:div.content
         [:div.title "Location"]]])]]))

(defcard list-icon
  (sab/html
   [:div
    [:ul.list
     (for [_ (range 3)]
       [:li.list-item
        [:i.fa.fa-map-marker]
        [:div.content
         [:div.title "Location"]]])]]))

(defcard-rg list-separator
  [:ul.list
   [:li.list-item.label
    "FIrst"]
   [:li.list-item.separator]
   [:li.list-item "FIrst"]
   [:li.list-item "FIrst"]])
