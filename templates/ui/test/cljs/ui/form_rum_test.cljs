(ns ui.form-rum-test
  (:require
   [css.components.form-rum :refer [form-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard defcard-rg deftest]]))

(def form-schema {})
(defcard form-card (form-ui {:schema form-schema}))
