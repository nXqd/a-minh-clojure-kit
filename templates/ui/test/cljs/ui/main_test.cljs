(ns ui.main-test
  (:require
   #_[ui.components.autocompleter]
   [css.components.graphs.funnel]
   [css.components.graphs.geo]
   [ui.algolia.core-test]
   [ui.button-test]
   [ui.components.accordion-rum-test]
   [ui.components.ag-grid-test]
   [ui.components.back-to-top-test]
   [ui.components.breadcrumb-rum-test]
   [ui.components.cards-test]
   [ui.components.chat-test]
   [ui.components.clipboard-test]
   [ui.components.date-test]
   [ui.components.drift-test]
   [ui.components.nav-test]
   [ui.components.editor-test]
   [ui.components.googlemap-rum-test]
   [ui.components.graphs.candlestick-test]
   [ui.components.graphs.line-chart-test]
   [ui.components.graphs.line-test]
   [ui.components.image-grid-rum-test]
   [ui.components.image-slider-rum-test]
   [ui.components.infinite-list-test]
   [ui.components.infowindow-test]
   [ui.components.is-this-helpful-test]
   [ui.components.keyboard-test]
   [ui.components.menus-test]
   [ui.components.min-max-rum-test]
   [ui.components.paging-test]
   [ui.components.photoswipe-test]
   [ui.components.placeholders-test]
   [ui.components.progress-test]
   [ui.components.qa-test]
   [ui.components.react-select-test]
   [ui.components.searchbar-test]
   [ui.components.select-test]
   [ui.components.slider-test]
   [ui.components.stripe-test]
   [ui.components.tab-test]
   [ui.components.table-test]
   [ui.components.tooltip-test]
   [ui.components.upload-test]
   [ui.components.videojs-test]
   [ui.form-rum-test]
   [ui.google.custom-marker-test]
   [ui.google.map-test]
   [ui.grid-test]
   [ui.grid-xy-test]
   [ui.icons-test]
   [ui.image-test]
   [ui.label-test]
   [ui.list-test]
   [ui.loader-test]
   [ui.playground-test]
   [ui.typo-test]

   [ui.templates.landing-test]

   [devcards.core :refer-macros [start-devcard-ui!]]))

(enable-console-print!)

(defn ^:export main []
  (start-devcard-ui!))

(comment
  (defn hightlight
    [xs]
    (clojure.string/replace "This does have a word in here"
                            (re-pattern "word")
                            :span)
    [:p (clojure.string/replace "This does have a word in here"
                                (re-pattern xs)
                                :span)])
  (log/debug
   #_(clojure.string/replace "This does have a word in here"
                             (re-pattern "word")
                             (fn [x] '"[:span x]"))
   (clojure.string/split  "This does have a word in here" #"word")
   (nth "This does have a word in here"  "word")
   #_(highlight "word")))
