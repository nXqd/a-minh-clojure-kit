(ns ui.templates.landing-test
  (:require
   [cljs-css-modules.macro :refer-macros [defstyle]]
   [css.components.tab :as tab]
   [rum.core :as rum]
   [goog.ui.TableSorter :as TableSorter])
  (:require-macros
   [devcards.core :refer [defcard defcard-rg]]))

(defstyle header-styles
  [:.header-inner {:align-items "center"}]
  [:.header-logo {:padding 0
                  :margin 0
                  :display "inline-block"
                  :height "50px"}
   [:img {:display "block"
          :height "100%"}]]

  [:.header-link
   {:margin-right "10px"}])

(defstyle styles
  [:.hero {:margin-top "5rem"}])

(defn header-logo
  [logo-src]
  [:h1.header-logo
   {:class [(:header-logo header-styles)]}
   [:a {:href "/"}
    [:img {:src logo-src}]]])

(rum/defc header-ui < rum/static
  [{:keys [logo-src]}]
  [:#header
   {:class [(:header header-styles)]}
   [:.g--container
    [:div
     {:class [(:header-inner header-styles) :g-x :g-x--m]}
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      (header-logo logo-src)]
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8 :text--right]}
      (->> ["Login" "Signup"]
           (map (fn [x]
                  [:a
                   {:class [(:header-link header-styles)]
                    :href "#"}
                   x])))]]]])

(defstyle top-hero-styles
  [:.main
   [:h6 {:margin-top "1rem"}]
   [:.button.signup {:text-transform "uppercase"
                     :margin-top "1rem"}]
   [:img {:display "block"
          :width "100%"}]])

(rum/defc top-hero < rum/static
  []
  [:.top-hero
   {:class
    [(:hero styles)
     (:main top-hero-styles)]}
   [:div
    {:class [:g--container]}
    [:div
     {:class [:g-x :g-x--m]}
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      [:h2 "Make money doing tasks."]
      [:h6 "Start earning from home today!"]
      [:button.signup
       {:class [:button]}
       "Signup"]]
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      [:img {:src "https://www.remotasks.com/static/img/homepage/app-screenshot.png"}]]]]])

(defstyle meet-tasker-styles
  [:.main {}])

(rum/defc tasker-item < rum/static
  [{:keys [user-name user-image-src text]}]
  [:.tasker
   {:class [(:tasker meet-tasker-styles) :c :c-sm--5 :c-lg--5 :c-md--5]}
   [:.card
    [:.card__divider user-name]
    [:.card__image
     [:img {:src "http://foundation.zurb.com/sites/docs/assets/img/generic/rectangle-1.jpg"}]]
    [:.card__section
     [:p text]]]])

(rum/defc meet-tasker-hero < rum/static
  []
  [:.meet-taskers
   {:class [(:hero styles)
            (:main meet-tasker-styles)]}
   [:div
    {:class [:g--container]}
    [:div
     {:class [:g-x :g-x--m]}
     (->> [{:user-image-src  ""
            :user-name "Nick S"
            :text "Not only I can watch over my daughter while working, but it showed me the value of self-motivation and hard work. Thank you, Remotasks, for this opportunity."}
           {:user-image-src  ""
            :user-name "Nick S"
            :text "Not only I can watch over my daughter while working, but it showed me the value of self-motivation and hard work. Thank you, Remotasks, for this opportunity."}
           {:user-image-src  ""
            :user-name "Nick S"
            :text "Not only I can watch over my daughter while working, but it showed me the value of self-motivation and hard work. Thank you, Remotasks, for this opportunity."}]
          (map-indexed
           (fn [i x]
             ^{:key i}
             [:div
              {:class [:c :c-sm--5 :c-lg--5 :c-md--5]}
              (tasker-item x)])))]]])

(defstyle works-to-be-done-styles
  [:.tab {:margin-top "4rem"}
   [:.card {:border "none"}]
   [:.goog-tab-bar {:background "transparent"}]
   [:.goog-tab-selected {:border "none"}]
   [:.goog-tab-bar-top {:border "none !important"}]])

(rum/defc test1-ui
  []
  [:div
   {:class [:text--center]}
   [:h6 "\" Drawing a bounding box around all the parking logs in the image.\""]
   [:img
    {:src "https://www.remotasks.com/static/img/homepage/example-1b.jpg"}]])

(rum/defc test2-ui
  []
  [:div "test 2 ui"])

(defn tab-header
  [x]
  [:.card
   {:style {:width "100px"}}
   [:.card__image
    [:img {:src "https://d30y9cdsu7xlg0.cloudfront.net/png/7467-200.png"}]]
   [:h6
    {:class [:text--center]}
    "Image recognition"]])

(rum/defc works-to-be-done-hero < rum/static
  []
  [:.works-to-be-done
   {:class [(:hero styles)]}
   [:div
    {:class [:g--container]}
    [:div
     {:class [:g-x :g-x--m]}
     [:div
      {:class [:c :text--center]}
      [:h2
       {:class [:mb--1-0]}
       "Easy to do tasks"]
      [:h6 "Tag images. Transcribe Audio. Moderate Content. We have many interesting tasks for you to do! English-fluency is the only requirement."]]
     [:div
      {:class [(:tab works-to-be-done-styles)]
       :style {:margin "0 auto"}}
      (tab/tab-ui {:init-content "Init content"
                   :tabs {:image-recognition {:label (tab-header {})
                                              :selected? true
                                              :component test1-ui}
                          :settings {:label (tab-header {})
                                     :component test2-ui}}})]]]])

(defn table-did-mount
  [states]
  (let [table (rum/ref-node states "table")
        comp (goog.ui.TableSorter.)]
    (.decorate comp table)
    ;; monthly-emi
    (.setSortFunction comp 0
                      (TableSorter/createReverseSort TableSorter/numericSort))
    (.setSortFunction comp 1
                      TableSorter/alphaSort))
  states)

(rum/defc table-ui < rum/static
  {:did-mount table-did-mount}
  []
  (let [data [["Jerry T" "6000" "98%"]]]
    [:table.sorted
     {:border "0", :cellpadding "3", :id "sortMe"
      :ref "table"}
     [:thead
      [:tr
       [:th
        "Name"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]
       [:th
        "Pay"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]
       [:th "Accuracy"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]]]
     [:tbody
      (->> data
           (map (fn [[nm pay accuracy]]
                  [:tr
                   {:key (str nm)}
                   [:td nm]
                   [:td pay]
                   [:td accuracy]])))]]))

(rum/defc get-started-hero < rum/static
  []
  [:.get-started
   {:class [(:hero styles)]}
   [:div
    {:class [:g--container]}
    [:div
     {:class [:g-x :g-x--m]}
     [:div
      {:class [:c :text--center :mb--3-0]}
      [:h2
       {:class [:mb--1-0]}
       "Get started"]
      [:h6 "Tag images. Transcribe Audio. Moderate Content. We have many interesting tasks for you to do! English-fluency is the only requirement."]]
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      (table-ui)]
     [:div
      {:class [:c :c-sm--8 :c-md--8 :c-lg--8]}
      [:img {:src "http://3.bp.blogspot.com/_CMbmKLtFhFY/TTYl-Qyq7ZI/AAAAAAAABgY/PQZkeEfLZQc/s1600/retro.happy+lady+with+money.jpg"}]]]]])

(rum/defc landing < rum/static
  []
  [:#landing
   (header-ui {:logo-src "https://www.remotasks.com/static/img/homepage/icon-paypal.svg"})
   (top-hero)
   (meet-tasker-hero)
   (works-to-be-done-hero)
   (get-started-hero)])

(defcard landing-card (landing))

