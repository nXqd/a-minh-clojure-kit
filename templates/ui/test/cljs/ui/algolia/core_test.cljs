(ns ui.algolia.core-test
  (:require
   [css.algolia.core :refer [core-ui]])
  (:require-macros
   [devcards.core :as dc :refer [defcard]]))

(defcard test-card (core-ui))

