(ns ui.icons-test
  (:require
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard deftest]]))

(defcard icons
  (sab/html
   [:div
    [:h2 "circle"]
    [:p]
    [:i.icon.icon--circle]]))
