(ns ui.image-test
  (:require
   [css.components.image :as image]
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard
                                 defcard-rg]]))

(defcard image-with-black-cover-card
  (sab/html
   [:div.project-featured.grid
    {:style {:position "relative"
             :width "300px"
             :height "250px"}}
    [:div.black-cover]
    [:div.img {:style {:width "100%"
                       :height "100%"
                       :background-size "cover"
                       :background-position "center"
                       :position "absolute"
                       :background-image "url(https://is1-2.housingcdn.com/4f2250e8/99cbea9efee4cf2bb92e6c60534b0807/v1/_logo.jpg)"}}]]))

(defcard-rg city*
  [:a.city
   {:style {:background-image "url(https://assets-0.housingcdn.com/website/images/home_page/popular_cities-ac3d78195b30c95700844578819526c1.jpg)"}}
   [:span "Mumbai"]])

(defcard-rg image-circle
  [:img.image.image--circle
   {:src "https://i.imgur.com/EGrpg28.gif"
    :width "50px"
    :height "50px"}])

(defcard image-with-black-cover-card
  (image/image-with-zoom {}))
