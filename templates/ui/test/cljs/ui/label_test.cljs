(ns ui.label-test
  (:require
   [sablono.core :as sab :include-macros true])
  (:require-macros
   [devcards.core :as dc :refer [defcard defcard-rg deftest]]))

(defn counter
  [num xs]
  (let [{:keys [style]} xs]
    [:span.counter
     {:style style}
     num]))

(defcard-rg counter*
  [counter 12])

(defcard-rg label-image*
  [:div.grid
   {:style {:flex-direction "column"}}

   [:a.label.label--image-left
    {:style {:color "white"
             :font-weight :bold
             :font-size "0.8rem"}}

    [:img {:style {:width "30px"
                   :height "30px"}
           :src "http://semantic-ui.com/images/avatar/small/elliot.jpg"}]

    [:span
     {:style {:background "brown"}}
     "Dung Nguyen"]]

   [:p]

   [:a.label.label--image-right
    {:style {:color "white"
             :font-weight :bold
             :font-size "0.8rem"}}

    [:span
     {:style {:background "brown"}}
     "Dung Nguyen"]

    [:img {:style {:width "30px"
                   :height "30px"}
           :src
           "http://semantic-ui.com/images/avatar/small/elliot.jpg"}]]])

(defcard label-icon
  (sab/html
   [:a.label.label--image-right
    {:style {:color "white"
             :font-weight :bold
             :font-size "0.8rem"}}

    [:i.fa.fa-square
     {:style {:font-size "1.3rem"}}]

    [:img {:style {:width "30px"
                   :height "30px"}
           :src
           "http://semantic-ui.com/images/avatar/small/elliot.jpg"}]]))
