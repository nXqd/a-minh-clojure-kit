(defproject css "MASTER"
  :dependencies [[org.clojure/clojure "1.10.0" :scope "provided"]]
  :cljfmt {:indents ~(clojure.edn/read-string (slurp ".cljfmt.edn"))}
  :profiles {:cljfmt {:plugins [[lein-cljfmt "0.3.0"]
                                [lein-parent "0.2.1"]]}
             :dev  {:source-paths ["test/cljs" "test/cljc"]
                    :dependencies [[clj-fuzzy "0.3.1"]
                                   [cljs-utils "0.1.0"]
                                   [cljsjs/clipboard "1.5.5-0"]
                                   [cljsjs/google-analytics "2015.04.13-0" :scope "provided"]
                                   [cljsjs/google-maps "3.18-1" :scope "provided"]
                                   [cljsjs/highcharts "4.2.2-2"]
                                   [cljsjs/photoswipe "4.1.1-0" :scope "provided"]
                                   [cljsjs/react-select "1.2.1-1"]
                                   [cljsjs/stripe "2.0-0"]
                                   [com.cognitect/transit-clj "0.8.300"]
                                   [com.cognitect/transit-cljs "0.8.239"]
                                   [degree9/firebase-cljs "1.3.0"]
                                   [cljs-css-modules "0.2.1"]
                                   [devcards "0.2.4"]
                                   [garden "1.3.6"]]
                    :plugins [[lein-cljsbuild "1.1.7" :exclusions [org.clojure/clojure]]
                              [lein-cljfmt "0.6.1" :exclusions [org.clojure/clojure org.clojure/tools.reader org.clojure/tools.cli]]
                              [lein-doo "0.1.7"]
                              [lein-figwheel "0.5.14"]
                              [lein-garden "0.2.6" :exclusions [org.clojure/clojure
                                                                org.apache.commons/commons-compress]]]}
             :provided {:dependencies [[org.clojure/clojurescript "1.9.946"]
                                       [hiccups "0.3.0"]
                                       [cljs-http "0.1.42"]
                                       [org.clojure/core.async "0.4.474"]
                                       [com.taoensso/timbre "4.10.0"]
                                       [rum "0.11.2"]
                                       [sablono "0.8.4"]
                                       [cljsjs/react "16.3.0-1"]
                                       [cljsjs/react-dom "16.3.0-1"]
                                       [cljsjs/react-dom-server "16.3.0-1"]]}
             :test {:dependencies [[org.clojure/test.check "0.9.0"]
                                   [prismatic/dommy "1.1.0" :scope "test"]]}}
  :jvm-opts ^:replace ["-Xms512m" "-Xmx512m" "-server"
                       "--add-modules" "java.xml.bind"]
  :clean-targets ^{:protect false} ["resources/public/cljs/" "target"]
  :doo {:build "browser-test"
        :paths {:karma "node_modules/karma/bin/karma"}}
  :source-paths ["src/clj" "src/cljs" "src/cljc"]
  :garden {:builds [{;; Optional name of the build:
                     :id           "screen"
                     :source-paths ["src/clj" "src/cljc"]
                     :stylesheet   css.styles/screen
                     :compiler     {:output-to     "resources/public/css/site.css"
                                    :vendors       [:moz :webkit]
                                    :auto-prefix   #{:transform
                                                     :appearance
                                                     :animation
                                                     :filter
                                                     :background-size
                                                     :user-select
                                                     :font-feature-settings}
                                    :pretty-print? true}}]}

  :cljsbuild {:builds [{:id           "devcards"
                        :source-paths ["src/cljs" "src/cljc" "test"]
                        :figwheel     {:devcards true}
                        :compiler     {:main                 "ui.main-test"
                                       :asset-path           "cljs/devcards_out"
                                       :parallel-build       true
                                       :output-to            "resources/public/cljs/css_devcards.js"
                                       :output-dir           "resources/public/cljs/devcards_out"
                                       :source-map-timestamp true}}
                       {:id           "browser-test"
                        :source-paths ["test/cljs" "src/cljs" "src/cljc" ]
                        :compiler     {:main                 css.browser-run-tests
                                       :optimizations        :none
                                       :recompile-dependents true
                                       :parallel-build       true
                                       :compiler-stats       true
                                       :asset-path           "cljs/test/karma/out"
                                       :output-to            "resources/public/cljs/test/karma/test.js"
                                       :output-dir           "resources/public/cljs/test/karma/out"}}]}

  :figwheel {:server-port 3500
             :css-dirs    ["resources/public/css"] }

  :externs ["resources/public/externs.js"])
