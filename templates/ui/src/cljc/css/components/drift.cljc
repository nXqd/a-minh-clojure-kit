(ns css.components.drift
  (:require
   [rum.core :as rum]
   [css.utils.core :as utils]
   #?@(:cljs [[cljs.core.async :as async]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go]])))

(def ^:const lib-uri "//imgix.github.io/drift/dist/Drift.js")

(defn lib-loaded?
  []
  #?(:cljs js/window.Drift))

(defn load-lib
  []
  (utils/load-script-async lib-uri))

(defn image-zoom-did-mount
  [states]
  #?(:cljs
     (let [pane-el (rum/ref-node states "pane-trigger")
           demo-el (rum/ref-node states "demo-trigger")]
       (go (when-not (lib-loaded?)
             (async/<! (load-lib)))
           (js/Drift. demo-el (clj->js {"paneContainer" pane-el
                                        "inlinePane" false
                                        :hoverBoundingBox false})))

       states)))

(rum/defc image-zoom < rum/static
  {:did-mount image-zoom-did-mount}
  []
  [:div
   [:div.demo-trigger
    [:img {:class "demo-trigger",
           :ref "demo-trigger"
           :src "https://demos.imgix.net/wristwatch.jpg?w=200&ch=DPR&dpr=2&border=1,ddd",
           :data-zoom "https://demos.imgix.net/wristwatch.jpg?w=1000&ch=DPR&dpr=2"}]]

   [:div.detail
    {:ref "pane-trigger"
     :style {:width "300px"
             :height "300px"}}]])
