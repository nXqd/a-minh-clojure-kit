(ns css.components.form-rum
  (:require
   [taoensso.timbre :as log]
   [css.components.form.input :as input]
   [css.components.editor :refer [editor-ui]]
   [rum.core :as rum]
   #?@(:cljs [[goog.dom :as gdom]
              [css.utils.utils :as outils :refer [stop-event]]])))


;; test
;; schema must have
;; value must have
;; placeholder optional


(def default-radios [{:value false :label "Radio 1"}
                     {:value false :label "Radio 2"}])

(def combobox-options
  {"Another item 123" {:label "Another item 123"
                       :value :another-item}
   "Inbox" {:label "Inbox"
            :value :inbox}})

(rum/defcs form-ui < rum/static
  (rum/local {:submit-once? false} :ui)
  (rum/local {:username     "123"
              :number ""
              :email        ""
              :birthday     ""
              :combobox     ""
              :checkbox     true
              :radio        true
              :radios       default-radios
              :select-field []} :data)

  [states props]
  (let [{:keys [data ui]} states
        {:keys [username
                email
                birthday
                checkbox
                combobox
                radio
                radios
                number
                select-field]} @data
        {:keys [submit-once?]} @ui
        schema {}
        valid? true]
    [:div.grid.form.form--validated
     {:style {:flex-direction "column"}}
     [:div.inputs.grid
      (input/input-ui {:on-change         (fn [e]
                                            #?(:cljs (let [value (.. e -target -value)]
                                                       (swap! data assoc :username value))))
                       :type              [:input :text]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :label             "Username"
                       :value             username
                       :schema            (:username schema)})

      (input/input-ui {:on-change         (fn [e]
                                            #?(:cljs (let [value (.. e -target -value)]
                                                       (swap! data assoc :number value))))
                       :type              [:input :number]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :label             "Number type"
                       :value             number
                       :schema            (:username schema)})

      (input/input-ui {:on-change         nil #_(om/update-state! this assoc :email
                                                                  (.. % -target -value))
                       :type              [:input :email]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             email
                       :label             "Email"
                       :styles            {:error {:top "3.1rem"}}
                       :message           "Please enter your email"
                       :schema            (:email schema)})

      (input/input-ui {:on-change
                       (fn [val]
                         #?(:cljs
                            (let [val' (js->clj val)]
                              #_(om/update-state! this assoc :select-field val'))))

                       :type    [:select]
                       :options {:options [{:label "2" :value "2"}
                                           {:label "3" :value "3"}
                                           {:label "1" :value "1"}]}

                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             (:select-field states)
                       :label             "Test"
                       :message           "Please enter your email"})

      (input/input-ui {:on-change
                       (fn [val]
                         #?(:cljs
                            (log/debug "value" val)))
                       :type              [:combobox]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :options combobox-options
                       :value             combobox
                       :placeholder       "Combobox placeholder"
                       :label             "Combobox input"
                       :message           "You forget to enter ..."})

      (input/input-ui {:on-change         (fn [val]
                                            #?(:cljs (log/debug "form val" val))
                                            #_(om/update-state! this assoc :birthday val))
                       :type              [:calendar]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             birthday
                       :label             "Birthday"
                       :message           "Please enter your birthday"})

      (input/input-ui {:on-change nil #_(om/update-state! this assoc :email
                                                          (.. % -target -value))

                       :type              [:textarea]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             email
                       :label             "Textarea"
                       :message           "Please enter your email"
                       :schema            (:email schema)})

      (input/input-ui {:on-change nil #_(om/update-state! this assoc :checkbox
                                                          (.. % -target -checked))

                       :type              [:input :checkbox]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             (:checkbox states)
                       :label             "Birthday"
                       :message           "Please enter your birthday"})

      (input/input-ui {:on-change         nil #_(om/update-state! this assoc :radios
                                                                  (assoc-in default-radios [0 :value] true))
                       :type              [:input :radio]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             (get-in radios [0 :value])
                       :label             (get-in radios [0 :label])})

      (input/input-ui {:on-change nil #_(om/update-state! this assoc :radios
                                                          (assoc-in default-radios [1 :value] true))

                       :type              [:input :radio]
                       :class             "grid-cell grid-md-8"
                       :form-submit-once? submit-once?
                       :value             (get-in radios [1 :value])
                       :label             (get-in radios [1 :label])})

      #_(editor-ui {:on-change #(log/debug "value" %)
                    :type      [:trix]
                    :class     "grid-cell grid-md-8"
                    :value     "Something here"
                    :label     "label"})

      #_(editor-ui {:on-change #(log/debug "value" %)
                    :type      [:trix]
                    :class     "grid-cell grid-md-8"
                    :value     "Another value here"
                    :label     "label"})]

     [:div.buttons.grid
      [:button.button.submit
       {:on-click (fn [e]
                    (if-not submit-once?
                      nil
                      #_(om/update-state! this assoc :submit-once? true))
                    (if valid? "" "")
                    #?(:cljs (stop-event e)))} "Submit"]]]))
