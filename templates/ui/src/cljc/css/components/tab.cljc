(ns css.components.tab
  (:require
   [rum.core :as rum]
   [taoensso.timbre :as log]
   #?@(:cljs [[goog.ui.TabBar]
              [goog.ui.Component.EventType]
              [goog.dom]
              [goog.events]])))

(rum/defc test1-ui
  []
  [:div "test 1 ui"])

(rum/defc test2-ui
  []
  [:div "test 2 ui"])

(defn debug-on-select
  [e]
  #?(:cljs
     (log/debug "target" (.-target e))))

(defn keys-contents-select
  [ui]
  (fn [e]
    #?(:cljs
       (try (let [data-key (-> e .-target
                               .getElement
                               (.getAttribute "data-key")
                               keyword)]
              (swap! ui assoc :current-key data-key))
         (catch js/Error _
           (log/debug "Missing data-key in html node"))))))

(defn tab-did-mount
  [states]
  #?(:cljs
     (let [props (-> states :rum/args first)
           {:keys [on-select tabs]} props
           current-key (ffirst tabs)
           {:keys [ui]} states
           tab (rum/ref-node states "tab")
           comp (goog.ui.TabBar.)
           on-select (or on-select debug-on-select)]
       (.decorate comp tab)
       (goog.events/listen comp goog.ui.Component.EventType.SELECT (keys-contents-select ui))
       (assoc states :current-key current-key))))

(defn tab-will-mount
  [states]
  (let [comp (:rum/react-component states)
        ui (atom {:current-key nil})]
    #?(:cljs
       (add-watch ui :component (fn [_ _ _ _]
                                  (rum/request-render comp))))
    (assoc states :ui ui)))

(defn tab-item
  [[data-key {:keys [selected? label]} :as x]]
  [:div
   {:data-key data-key
    :class [:goog-tab (when selected? :goog-tab-selected)]}
   label])

(defn tab-content
  [tabs current-key default-content]
  (let [tab-content (get-in tabs [current-key :component])]
    (if tab-content
      (tab-content)
      default-content)))

(rum/defcs tab-ui < rum/static
  {:will-mount tab-will-mount
   :did-mount tab-did-mount}
  [states props]
  (let [{:keys [tabs init-content]} props
        {:keys [ui]} states
        {:keys [current-key]} @ui
        tab-content (tab-content tabs current-key init-content)]
    [:div
     [:div
      {:ref "tab"
       :id "top"
       :class "goog-tab-bar goog-tab-bar-top"}
      (->> tabs (map tab-item))]
     [:.goog-tab-bar-clear]
     [:.google-tab-content
      {:ref "content"}
      tab-content]]))

