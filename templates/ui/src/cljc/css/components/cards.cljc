(ns css.components.cards
  (:require
   [rum.core :as rum]))

(rum/defc card-ui < rum/static
  []
  [:.card
   [:.card__divider
    "This is a header"]
   [:.card__image
    [:img {:src "http://foundation.zurb.com/sites/docs/assets/img/generic/rectangle-1.jpg"}]]
   [:.card__section
    [:h4 "This is a card."]
    [:p "It has an easy to override visual style, and is appropriately subdued."]]])

(rum/defc cards-ui < rum/static
  []
  [:div
   {:class [:g-x :g-x--p]}
   (for [i (range 6)]
     ^{:key i}
     [:div
      {:class [:c :c-sm--4 :c-lg--4 :c-md--4]}
      (card-ui)])])


