(ns css.components.placeholders
  (:require
   [rum.core :as rum]))

(def placeholder-ui-default-style
  {:height "20px"
   :background "#f4f4f4"})

(rum/defc placeholder-ui
  [props]
  [:div.placeholder.grid
   (let [{:keys [style]} props]
     {:style (merge placeholder-ui-default-style style)})])

(rum/defc placeholder-property-ui
  [props]
  [:div.placeholder-property.grid
   [:div.grid-cell.grid-md-8
    {:style {:padding-right "1rem"}}

    (placeholder-ui {:style {:height "230px"
                             :background "#f4f4f4"}})]
   [:ul.list.grid-cell.grid-md-8
    [:li.list-item.grid
     (placeholder-ui {:style {:width "200px"}})]
    [:li.list-item.grid
     (placeholder-ui {:style {:width "200px"}})]
    [:li.list-item.grid
     (placeholder-ui {:style {:width "150px"}})]
    [:li.list-item.grid
     (placeholder-ui {:style {:width "250px"}})]]])
