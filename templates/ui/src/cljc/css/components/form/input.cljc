(ns css.components.form.input
  (:require
   [css.components.editor :refer [editor-ui]]
   [css.components.date :as date]
   [css.components.combobox :as combobox]
   [rum.core :as rum]
   #?@(:cljs [[goog.dom :as gdom]
              [css.utils.utils :as outils :refer [stop-event]]])))

(def init-state
  {:touched? false
   :focus?   false})

(defn on-focus
  [e]
  #?(:cljs
     (let []
       (when on-focus (on-focus e))
       (stop-event e))))

(defn on-blur
  [e]
  #?(:cljs
     (let []
       (when on-blur (on-blur e))
       (stop-event e))))

(rum/defcs input-ui < rum/static
  (rum/local init-state :ui)
  [states props]
  (let [{:keys                 [schema
                                message
                                label
                                disabled
                                form-submit-once?
                                placeholder
                                show-value
                                value
                                type
                                class
                                styles
                                style
                                options
                                show-error-message? ;; sometimes we dont want to show error message, just do something
                                on-valid-error ;; function on validation error
                                on-change
                                on-focus
                                on-blur
                                on-mouse-down]}    props
        {:keys [focus? touched?]} states
        disabled                             (and disabled true)
        type                                 (or type [:input :text])
        ;; TODO
        schema                               {}
        value                                (if show-value
                                               (if-not focus? show-value value)
                                               value)
        ;; TOOD: refactor all the form to use (:main styles)
        style'                               (or style (:main styles) {})
        placeholder                          (or placeholder "")
        error-message                        (or message "Please fill out this field")
        ;; TODO
        valid?                               true

        ;; error message
        show-error?           (and (not valid?)
                                   (or form-submit-once? touched?))
        error-showable-inputs (not (= (first type) :select))

        show-error-message? (if-not (nil? show-error-message?)
                              show-error-message?
                              (and show-error? focus? error-showable-inputs))

        ;; run on valid error
        _ (when (and show-error?
                     (not (nil? on-valid-error)))
            (on-valid-error))

        checkbox? (or (= type [:input :checkbox])
                      (= type [:input :radio]))]
    (assert (vector? type) "type must be a vector [:input :text] [:input :password]")

    [:div.input-ui.grid
     {:class         class
      :style         (merge
                      style'
                      {:position      "relative"
                       :flexDirection "column"
                       :outline       "none"})
      :on-focus      on-focus
      :on-mouse-down (or on-mouse-down #())
      :on-blur       on-blur}
     [:div.form.grid
      [:label.grid
       {:style {:align-items "center"
                :position    "relative"}}

       (when-not checkbox?
         [:span.label-text label])

       (condp = (first type)
         :input [:input {:class       (cond-> []
                                              show-error?     (conj "error")
                                              checkbox?       (conj "checkbox")
                                              (not checkbox?) (conj "grid"))
                         :value       value
                         :disabled    disabled
                         :checked     (and checkbox? value)
                         :style       (:input styles)
                         :on-change   on-change
                         :type        (or (get type 1) "text")
                         :placeholder placeholder}]

         :textarea [:textarea.grid {:class       (when show-error? "error")
                                    :value       value
                                    :on-change   on-change
                                    :placeholder placeholder}]

         :trix (editor-ui {:value       value
                           :options     options
                           :placeholder placeholder
                           :on-change   on-change})

         :calendar #?(:cljs
                      [:div.calendar.grid
                       (date/g-input-datepicker
                        {:on-change on-change})])
         :combobox #?(:cljs
                      [:div.combobox.input.grid
                       (combobox/combobox-ui {:placeholder placeholder
                                              :options options
                                              :on-change on-change})])
         :select
         #?(:cljs
            (let [opts (clj->js (merge
                                 {:placeholder placeholder
                                  :multi       true
                                  :crazy       true
                                  :value       value
                                  :onChange    on-change} options))]
              [:div.select.grid
               {:class (when show-error? "error")}
               #_(js-select opts)])))

       (when checkbox?
         [:span.label-text.checkbox
          {:style {:cursor "pointer"}}
          label])

       ;; dont display error for checkbox type (radio, checkbox)
       (when (not checkbox?)
         (let [style (merge
                      {:line-height  2
                       :padding      "0 0.5rem"
                       :background   "#222"
                       :color        "white"
                       :z-index      1000
                       :border-top   "none"
                       :border-color "#b2b3b3"
                       :position     "absolute"
                       :align-items  "center"
                       :left         0}
                      (:error styles))]

           [:div.error.grid
            {:class (if show-error-message? "" "hidden")
             :style style}
            error-message]))]]]))
