(ns css.components.editor
  (:require
   [rum.core :as rum]))

(rum/defc editor-ui < rum/static
  [props]
  [:div {:class "label"} props])
