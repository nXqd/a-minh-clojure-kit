(ns css.components.infinite-list
  (:require
   [rum.core :as rum]
   [taoensso.timbre :as log]
   [css.mixins.events :as events]
   #?@(:cljs [[goog.dom :as gdom]
              [goog.style :as style]]))
  #?(:cljs (:import [goog.events EventHandler])))

(defn by-id [id]
  #?(:cljs (gdom/getElement id)))

(defn scroll-target-t
  "Return the scroll target of layout."
  []
  #?(:cljs (by-id "scroll-target")))

(defn page-height
  "Return the height of the page."
  []
  #?(:cljs js/document.documentElement.clientHeight))

(defn scroller
  "Return the scroller of `event`."
  [state event]
  (let [target (.-target event)]
    (or (aget target "scroller") target)))

(defn scroll-top
  "Return the top position of the `event` scroller."
  [state event]
  (.-scrollTop (scroller state event)))

(defn scroll-content-height
  "Return the height of the `event` scroller."
  [state event]
  (.-scrollHeight (scroller state event)))

(defn scroll-header-height
  "Return the header height of the `event` scroller."
  [state event]
  #?(:cljs (when-let [header (.-header (.-target event))]
             (.-height (style/getSize header)) 0)))

(defn scroll-visible-height
  "Return the content height of the `event` scroller."
  [state event]
  (- (page-height) (scroll-header-height state event)))

(defn content-height
  "Return the height of the content."
  [state]
  #?(:cljs (let [node (rum/dom-node state)]
             (.-height (style/getSize node)))))

(defn loading?
  "Returns true if the component is loading new data, otherwise false."
  [state]
  (-> state :loading? deref))

(defn list-items
  "Return the list items from `state`."
  [state]
  (-> state :rum/args first))

(defn options
  "Return the options from `state`."
  [state]
  (-> state :rum/args second))

(defn on-load-handler
  "Returns the :on-load handler."
  [state]
  (-> state options :on-load))

(defn threshold
  "Returns the :on-load handler."
  [state]
  (or (-> state options :threshold) 100))

(defn bottom-reached?
  "Returns true if the scroll position is near the bottom of the page."
  [state event]
  (neg? (- (scroll-content-height state event)
           (scroll-visible-height state event)
           (scroll-top state event)
           (threshold state))))

(defn load-next
  "Fetch the next page."
  [state]
  (when-let [handler (on-load-handler state)]
    (when-not (loading? state)
      (reset! (:loading? state) true)
      (handler))))

(defn on-scroll
  "The scroll event handler."
  [state event]
  (when (bottom-reached? state event)
    (log/debug "Infinite list bottom reached.")
    (load-next state)))

(defn on-resize
  "The resize event handler."
  [state event]
  (when (< (content-height state) (page-height))
    (load-next state)))

(defn scroll-target
  "Return the scroll target from `state`."
  [state]
  (let [scroll-target (-> state options :scroll-target)]
    (assert scroll-target ":scroll-target must not be nil.")
    #?(:cljs (cond
               (string? scroll-target)
               (or (some-> (js/document.getElementsByTagName scroll-target)
                           array-seq first)
                   (gdom/getElementByClass scroll-target))
               (ifn? scroll-target)
               (scroll-target)
               :else scroll-target))))

(defn attach-listeners
  "Attach scroll and resize listeners."
  [state]
  #?(:cljs (when-let [target (scroll-target state)]
             (events/listen state js/window :resize #(on-resize state %))
             (events/listen state target :resize #(on-resize state %))
             (events/listen state target :scroll #(on-scroll state %) {:passive true})
             state)))

(def infinite-list-mixin
  "The infinite list mixin."
  {:did-remount
   (fn [old-state new-state]
     (when (and (loading? new-state)
                (> (-> new-state list-items count)
                   (-> old-state list-items count)))
       (reset! (:loading? new-state) false))
     (when-not (= (options old-state) (options new-state))
       (events/detach old-state)
       (attach-listeners new-state))
     new-state)
   :did-mount
   (fn [state]
     (attach-listeners state))})

(rum/defcs infinite-list < rum/static (rum/local false :loading?) events/mixin infinite-list-mixin
  "Render an infinite list."
  [state list-items & [opts]]
  [:div.infinite-list
   {:class (some-> (:class opts) (str "-container"))}
   [:div.infinite-list__header
    {:class (some-> (:class opts) (str "-header"))}
    (:header opts)]
   [:div.infinite-list__content
    {:class (:class opts)}
    (for [[index list-item] (map-indexed vector list-items)]
      [:div.infinite-list__item
       {:class (some-> (:class opts) (str "-item"))
        :key (str "infinite-list__key-" index)}
       list-item])]
   [:div.infinite-list__footer
    {:class (:footer-class opts)}
    (:footer opts)]])
