(ns css.components.react-select
  (:require
   [rum.core :as rum]
   [css.utils.react-component :as ur]
   #?@(:cljs [[cljsjs.react-select]])))

(defn react-select-on-change
  [selected-opt]
  #?(:cljs (prn "selected" selected-opt)))

(def opts [{:value "a" :label "A"}])

(rum/defc react-select-ui < rum/static
  [props]
  #?(:cljs
     (ur/create-element js/Select {:value "some valuej"
                                   :onChange react-select-on-change
                                   :options opts})))
