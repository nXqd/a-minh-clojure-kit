(ns css.components.ag-grid
  (:require
   [css.components.ag-grid.utils :as agutils]
   [css.utils.core :as utils]
   [rum.core :as rum]
   [taoensso.timbre :as log]
   #?@(:cljs [[goog.dom :as gdom]
              [hiccups.runtime :as hiccupsrt]
              [css.utils.utils :as outils :refer [stop-event]]
              [cljs.core.async :as async]]))
  #?(:cljs
     (:require-macros
      [cljs.core.async.macros :refer [go]]
      [hiccups.core :as hiccups])))

(def col-defs
  [{:headerName "Make" :field "make" :cellRenderer agutils/drop-renderer}
   {:headerName "Model" :field "model" :cellRenderer agutils/link-renderer :filter "text"}
   {:headerName "Price" :field "price"}])

(rum/defc grid-ui < (agutils/ag-grid-mixin col-defs)
  [_]
  [:.ag-fresh
   {:style {:height "40rem"}}])