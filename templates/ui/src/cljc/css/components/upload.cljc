(ns css.components.upload
  (:require [rum.core :as rum]
            #?@(:cljs [[goog.net.jsloader :as jsloader]
                       [cljs.core.async :as async :refer [chan put!]]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go alt!]])))

;; fix 150px container
(def upload-item-default-styles
  {:main {:width "152px"
          :flexDirection "column"}})

(defn open-file-input
  [states]
  #?(:cljs
     (let [{:keys [cache ui]} states
           {:keys [file-input]} @cache
           {:keys [touched?]} @ui]
       (when-not touched? (swap! ui assoc :touched true))
       (when file-input (.click file-input)))))

(defn generate-preview
  [states e]
  #?(:cljs
     (let [{:keys [cache ui]} states
           {:keys [file-input]} @cache
           files (when file-input (.-files file-input))]
       (when files
         (let [item (.item files 0)
               rdr (js/FileReader.)]
           ;; this onload doesn't run in order 0 -> n
           (set! (.-onload rdr)
                 (fn [e]
                   (swap! ui assoc :image (.. e -target -result))))
           (.readAsDataURL rdr item))))))

(defn upload-item-did-mount
  [states]
  #?(:cljs
     (let [{:keys [cache]} states
           node (rum/dom-node states)
           file-input (rum/ref-node states "input")]
       (when file-input
         (swap! cache assoc :file-input file-input))))
  states)

(rum/defcs upload-item-ui < rum/static rum/reactive
  (rum/local {:file-input nil} :cache)
  (rum/local {:touched? false
              :image nil} :ui)
  {:did-mount upload-item-did-mount}
  [states props]
  (let [{:keys [src styles class]} props
        {:keys [ui]} states
        {:keys [image touched?]} @ui
        has-file? (not (nil? image))
        image (if-not touched?
                (or src "http://i.imgur.com/T4amQNy.png?1")
                (or image "http://i.imgur.com/T4amQNy.png?1"))]
    [:div.dropzone.grid
     {:class class
      :style (merge (:main upload-item-default-styles)
                    (:main styles))}
     [:div.preview.grid
      {:style {:cursor (when-not has-file? "pointer" "initial")
               :width "152px"
               :height "152px"
               :border "1px solid #ccc"
               :border-bottom "none"
               :border-top-left-radius "3px"
               :border-top-right-radius "3px"
               :align-items "center"}
       :on-click (fn [e]
                   #?(:cljs
                      (when-not has-file?
                        (open-file-input states))))}
      [:img.image.image--contained
       {:style {:width "150px"
                :height (if (= image src) "auto" "150px")}
        :src image}]]
     [:div.grid
      [:input.input
       {:on-change (fn [e]
                     (generate-preview states e))
        :ref "input"
        :class "hidden"
        :type "file"
        :accept "image/*"}]
      [:button.button.button--small.button--block
       {:style {:border-top-left-radius "0"
                :border-top-right-radius "0"}
        :on-click (fn [e]
                    #?(:cljs
                       (if has-file?
                         (swap! ui assoc :image nil)
                         (open-file-input states))))}
       (if has-file? "Remove file" "Upload file")]]]))

(def uplaod-default-styles
  {:zone {:display "flex"
          :cursor "pointer"
          :justifyContent "center"
          :alignItems "center"
          :height "5rem"
          :border "1px solid black"}})

(defn upload-generate-preview
  [states]
  #?(:cljs
     (let [{:keys [cache ui]} states
           {:keys [file-input]} @cache
           files (when file-input (.-files file-input))]
       ;; files is an object
       (when files
         (let [c (async/chan)]
           (go
             (let [images_ (atom [])
                   length (.-length files)]
               (doall
                (for [i (range length)]
                  (let [item (.item files i)
                        rdr (js/FileReader.)]
                     ;; this onload doesn't run in order 0 -> n
                    (set! (.-onload rdr)
                          (fn [e]
                            (swap! images_ conj (.. e -target -result))
                            (when (= (count @images_) length)
                              (async/put! c @images_) (async/close! c))))
                    (.readAsDataURL rdr item))))))
           (async/take! c (fn [x]
                            (swap! ui assoc :images x))))))))

(defn- upload-did-mount
  [states]
  #?(:cljs
     (let [{:keys [cache]} states
           node (rum/dom-node states)
           file-input (.querySelector node ".input")]
       (when file-input
         (swap! cache assoc :file-input file-input))))
  states)

(rum/defcs upload-ui < rum/static rum/reactive
  (rum/local {:file-input nil} :cache)
  (rum/local {:images []} :ui)
  {:did-mount upload-did-mount}
  [states props]
  (let [{:keys [styles type]} props
        {:keys [cache ui]} states
        {:keys [images]} @ui]
    [:div.dropzone
     [:div.inner
      {:on-click (fn [e]
                   (open-file-input states))
       :style (merge (:zone upload-item-default-styles)
                     (:zone styles))}
      "Click here to upload your pictures"]
     [:input.input
      {:on-change (fn [e]
                    (upload-generate-preview states))
       :type "file"
       :accept "image/*"
       :multiple true}]
     (let [item-fn (fn [i x] [:img {:style {:width "5rem"
                                            :height "5rem"}
                                    :key i
                                    :src x}])]
       [:div.preview
        (map-indexed item-fn images)])]))

(def fineuploader-default-options
  {:validation {:allowedExtensions ["jpeg" "png"]
                :acceptFiles "image/*"}})

(defn- fineuploader-did-mount
  [states]
  #?(:cljs
     (let [props (-> states :rum/args first)
           {:keys [options]} props
           el (rum/dom-node states)
           options (merge
                    fineuploader-default-options
                    {:element el
                     :template "qq-template-manual-trigger"}
                    options)]
       (js/qq.FineUploader. (clj->js  options))))
  states)

(rum/defcs fineuploader-ui < rum/static rum/reactive
  {:did-mount fineuploader-did-mount}
  [states]
  [:div])
