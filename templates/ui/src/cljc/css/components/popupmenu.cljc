(ns css.components.popupmenu
  (:require [rum.core :as rum])
  #?(:cljs
     (:import
      [goog.ui PopupMenu MenuItem MenuSeparator])))

(defn popupmenu-did-mount
  [states]
  #?(:cljs
     (let [div (rum/dom-node states)
           button (rum/ref-node states "button")
           pu (goog.ui.PopupMenu.)
           _ (.setToggleMode pu true)]
       (do (.addItem pu (goog.ui.MenuItem. "one"))
         (.addItem pu (goog.ui.MenuItem. "one"))
         (.addItem pu (goog.ui.MenuItem. "one"))
         (.addItem pu (goog.ui.MenuItem. "one"))
         (.addItem pu (goog.ui.MenuItem. "one"))
         (.render pu div)
         (.attach pu button
                  goog.positioning.Corner.TOP_LEFT
                  goog.positioning.Corner.BOTTOM_LEFT))))
  states)

(rum/defc popupmenu-ui < rum/static
  {:did-mount popupmenu-did-mount}
  []
  [:div
   [:button {:ref

             "button"}]])
