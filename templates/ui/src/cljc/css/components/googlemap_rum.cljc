(ns css.components.googlemap-rum
  (:require
   [css.google.core :as gcore]
   [rum.core :as rum]
   #?@(:cljs [[cljs.core.async :as async]
              [css.utils.utils :as outils :refer [stop-event]]
              [goog.dom :as gdom]
              [hiccups.runtime :as hiccupsrt]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go]]
            [hiccups.core :as hiccups])))

(def googlemap-default-state {:data  {:options        {}
                                      :circle-options {}
                                      :markers        {}}
                              :cache {}})

(def googlemap-atom googlemap-default-state)

(defn get-props
  [states k]
  (-> states :rum/args first k))

(defn make-marker
  [{:keys [lat lng icon map]}]
  #?(:cljs
     (let [position (google.maps.LatLng. lat,lng)]
       (google.maps.Marker. (clj->js {:map      map
                                      :icon     icon
                                      :position position})))))
(defn make-circle
  [x]
  #?(:cljs
     (when x
       (google.maps.Circle. (clj->js x)))))

(defn make-gmap
  [canvas options]
  #?(:cljs
     (when (and canvas (map? options))
       (google.maps.Map. canvas (clj->js options)))))

(def options
  #?(:cljs
     {"zoom"      3
      "center"    {:lat 0 :lng -100}
      "mapTypeId" "terrain"}))

;; example
(defn info-window-content
  [{:keys [name car-time] :as x}]
  [:div.info-window-hover.grid
   {:style "width: 10rem"}
   [:div.left.grid-cell.grid-md-8
    {:style "background: #007AFF; flex-direction: column; justify-content: center"}
    [:i.fa.fa-motorcycle
     {:style "color: white; font-size: 1rem"}]
    [:div.time
     {:style "font-weight: bold; color: white"}
     (str car-time " mins")]]
   [:div.right.grid-cell.grid-md-8
    {:style "padding-left: 0.5rem"}

    [:div.name
     {:style "font-weight: bold"} name]]])

(def default-options
  {"mapTypeControl"     false
   "mapTypeId"          "roadmap"
   "scrollwheel"        false
   :zoomControlOptions :top-right
   "zoom"               12})

(def light-style
  [{"featureType" "administrative", "elementType" "labels.text.fill", "stylers" [{"color" "#444444"}]},{"featureType" "landscape", "elementType" "all", "stylers" [{"color" "#f2f2f2"}]},{"featureType" "poi", "elementType" "all", "stylers" [{"visibility" "off"}]},{"featureType" "road", "elementType" "all", "stylers" [{"saturation" -100},{"lightness" 45}]},{"featureType" "road.highway", "elementType" "all", "stylers" [{"visibility" "simplified"}]},{"featureType" "road.arterial", "elementType" "labels.icon", "stylers" [{"visibility" "off"}]},{"featureType" "transit", "elementType" "all", "stylers" [{"visibility" "off"}]},{"featureType" "water", "elementType" "all", "stylers" [{"color" "#46bcec"},{"visibility" "on"}]}])

(defn add-markers
  [gmap markers]
  #?(:cljs
     (when (and gmap (not (empty? markers)))
       (doall (map (fn [{:keys [lat lng icon info-content] :as x}]
                     (let [marker (when (and lat lng)
                                    (make-marker {:map gmap :lat lat :lng lng :icon icon}))]
                       (when marker
                         (let [infowindow (when info-content
                                            #?(:cljs
                                               (js/google.maps.InfoWindow.
                                                (clj->js {:content (hiccups/html info-content)
                                                          :maxWidth 200}))))]
                           (when infowindow
                             (.addListener marker "mouseover" #(.open infowindow gmap marker))
                             (.addListener marker "mouseout" #(.close infowindow gmap marker)))
                           {:infowindow infowindow :marker marker})))) markers)))))

(defn remove-markers
  [markers]
  #?(:cljs
     (when-not (empty? markers)
       (doall (map (fn [{:keys [infowindow marker] :as x}]
                     (when marker
                       (.setMap marker nil)
                       (js/google.maps.event.clearInstanceListeners marker))
                     (when infowindow
                       (js/google.maps.event.clearInstanceListeners infowindow)
                       (.close infowindow)))
                   markers)))))

(defn make-polygons
  [gmap coords]
  #?(:cljs
     (let [a       (into [] (map (fn [x]
                                   {:lat (second x) :lng (first x)}) coords))
           polygon (js/google.maps.Polygon.
                    (clj->js
                     {:paths         a,
                      :strokeColor   "#444",
                      :strokeOpacity 0.8,
                      :strokeWeight  1,
                      :fillColor     "#444",
                      :fillOpacity   0.2}))]
       (.setMap polygon gmap))))

#_(defn equal-options
    [options next-options]
    (and
     (= (dissoc options "center") (dissoc next-options "center"))
     (.equals (get options "center") (get next-options "center"))))

(defn make-circle-1
  [map options circle-options]
  (make-circle (merge circle-options
                      {:map    map
                       :center (get options "center")})))

(defn googlemap-did-mount
  [states]
  (let [props (-> states :rum/args first)
        {:keys [options polygons circle-options markers]} props
        cache_         (atom {})]
    #?(:cljs
       (go (when-not (gcore/gmap-loaded?)
             (async/<! (gcore/load-gmap)))
           (let [gmap (make-gmap (rum/dom-node states) options)]
             (swap! cache_ assoc :map gmap)
             (when circle-options
               (let [circle' (make-circle-1 gmap options circle-options)]
                 (swap! cache_ assoc :circle circle')))
             (when-not (empty? markers)
               (let [markers' (add-markers gmap markers)]
                 (swap! cache_ assoc :markers markers')))
             (when-not (empty? polygons)
               (make-polygons gmap polygons)))))
    (assoc states :cache cache_)))

(rum/defcs googlemap-ui < rum/static
  {:did-mount googlemap-did-mount}
  [states props]
  (let [{:keys [styles]} props]
    [:div.google-map
     {:style (merge {:height "500px"} (:main styles))}]))

(def test-init-state
  {:markers  [{:lat 10.7792957, :lng 106.7047745, :icon "https://storage.googleapis.com/support-kms-prod/SNP_2752068_en_v0", :info-content [:ul.list.info-window-hover.grid {:style "font-family: Arial; font-size: 0.8rem; min-width:7rem;"} [:li.list-item.grid {:style "font-weight: bold; padding: 0.5rem 0;justify-content:center;text-align:center;"} "Vietnam Visa"] [:li.list-item.grid {:style "line-height: 2.5"} [:ul.list.list--horizontal.grid {:style "border-top:1px solid #eee;font-size:0.7rem;font-weight:bold"} [:li.list-item {:style "justify-content:center"} "0.5 km"] [:li.list-item {:style "justify-content:center"} "3 mins"]]]]}]
   :markers2 [{:lat 10.8, :lng 106.8, :icon "https://storage.googleapis.com/support-kms-prod/SNP_2752068_en_v0", :info-content [:ul.list.info-window-hover.grid {:style "font-family: Arial; font-size: 0.8rem; min-width:7rem;"} [:li.list-item.grid {:style "font-weight: bold; padding: 0.5rem 0;justify-content:center;text-align:center;"} "Vietnam Visa"] [:li.list-item.grid {:style "line-height: 2.5"} [:ul.list.list--horizontal.grid {:style "border-top:1px solid #eee;font-size:0.7rem;font-weight:bold"} [:li.list-item {:style "justify-content:center"} "0.5 km"] [:li.list-item {:style "justify-content:center"} "3 mins"]]]]}]
   :zoom     15
   :lat      10.823099
   :long     106.629664
   :polygons
   [[106.65787506103533,10.770506858825684],[106.65881347656244,10.766732215881348],[106.65963745117188,10.763960838317985],[106.65895843505888,10.763578414916935],[106.65943145751953,10.761593818664608],[106.65946960449224,10.761445045471248],[106.66031646728527,10.75831413269043],[106.659194946289,10.758072853088493],[106.65568542480474,10.757411003112736],[106.6535415649414,10.756998062133732],[106.65238189697266,10.75677585601818],[106.65078735351574,10.756509780883846],[106.65136718749994,10.755861282348576],[106.65132904052763,10.755586624145565],[106.65129852294928,10.755249023437443],[106.65113067626982,10.75357627868658],[106.65070343017584,10.753525733947868],[106.65036010742193,10.753561019897518],[106.64997863769537,10.753601074218807],[106.64927673339838,10.753673553466797],[106.64283752441406,10.754342079162598],[106.64208221435547,10.754420280456543],[106.64154815673834,10.754470825195256],[106.6382904052735,10.754801750183105],[106.63712310791016,10.754856109619197],[106.63705444335949,10.75552082061779],[106.63504791259771,10.75782299041748],[106.63438415527355,10.758603096008244],[106.63426208496094,10.758730888366813],[106.63417053222685,10.75881576538086],[106.63388061523438,10.759067535400447],[106.63433074951178,10.75950813293457],[106.63460540771501,10.76006221771246],[106.63484954833984,10.760905265808105],[106.63497924804682,10.761434555053825],[106.63505554199224,10.76203823089611],[106.63505554199224,10.762749671936035],[106.635238647461,10.764607429504451],[106.635238647461,10.765547752380371],[106.63520050048845,10.766755104064941],[106.63508605957037,10.768202781677246],[106.63506317138678,10.769459724426383],[106.63529205322271,10.769477844238338],[106.63594818115251,10.769353866577148],[106.63668823242182,10.769168853759822],[106.63743591308605,10.76892280578619],[106.63726043701166,10.769695281982422],[106.63764953613276,10.769737243652287],[106.637992858887,10.769843101501522],[106.6383590698245,10.770162582397518],[106.64093017578125,10.772207260131836],[106.64120483398443,10.772390365600586],[106.64138793945318,10.772392272949332],[106.64163208007824,10.772404670715332],[106.6417999267581,10.772392272949332],[106.6419296264649,10.772613525390625],[106.64238739013683,10.773139953613395],[106.64282226562517,10.773566246032715],[106.64315032958996,10.774043083190918],[106.64345550537115,10.774518966674805],[106.64355468749994,10.774769783020133],[106.64377593994158,10.775131225585938],[106.64408874511747,10.775232315063533],[106.64435577392595,10.775248527526912],[106.64472198486345,10.775229454040641],[106.64556884765648,10.775135040283317],[106.64670562744163,10.775056838989315],[106.64734649658214,10.775053977966252],[106.64757537841814,10.774758338928223],[106.64849090576183,10.77347278594965],[106.65164947509771,10.769029617309513],[106.65196228027361,10.768609046935978],[106.65234375000017,10.77131271362316],[106.6527404785158,10.77365970611578],[106.65316772460943,10.775780677795467],[106.65560913085955,10.778453826904297],[106.65566253662115,10.778267860412654],[106.65615081787104,10.776465415954647],[106.65680694580084,10.774301528930721],[106.6573486328125,10.771920204162598],[106.65787506103533,10.770506858825684]]})

(rum/defcs test-ui < rum/static
  (rum/local test-init-state :data)
  [states]
  #?(:cljs
     (let [{:keys [data]}                                    states
           {:keys [markers2 markers zoom lat long polygons]} @data]
       [:div.test
        [:button.button
         {:on-click (fn [e] (swap! data assoc :markers markers2
                                   :zoom (- zoom 1)
                                   :long (+ 0.001 long)))}
         "Change"]
        (googlemap-ui
         {:styles         {:main {:height "40rem"}}
          :markers        markers
          :options
          {:styles     light-style
           "center"    (js/google.maps.LatLng. lat, long)
           "mapTypeId" "roadmap"
           "zoom"      zoom}
          :polygons       polygons
          :circle-options {:radius      1500
                           :strokeColor "#000"
                           :fillOpacity 0.08}})])))

(defn transform-options
  "Transform some of clojure data to options."
  [{:keys [center] :as options}]
  #?(:cljs
     (assoc options :zoomControlOptions
            {:top-right {:position js/google.maps.ControlPosition.TOP_RIGHT}})))

(defn google-map-custom-did-mount
  [states]
  #?(:cljs
     (go (when-not (gcore/gmap-loaded?)
           (async/<! (gcore/load-gmap)))
         (let [canvas (rum/dom-node states)
               flight-plan [{:lat 37.772 :lng -122.214}
                            {:lat 21.291 :lng -157.821}
                            {:lat -18.142 :lng 178.431}
                            {:lat -27.467 :lng 153.027}]
               flight-path-options (clj->js {:path           flight-plan
                                             "geodesic"      true
                                             "strokeColor"   "#FF0000"
                                             "strokeOpacity" 1.0
                                             "strokeWeight"  2})
               flight-path (js/google.maps.Polyline. flight-path-options)
               options (-> states :rum/args first :options)
               options (transform-options options)
               gmap (js/google.maps.Map. canvas (clj->js options))]
           (.setMap flight-path gmap))))
  states)

(rum/defcs googlemap-custom-ui < rum/static
  {:did-mount google-map-custom-did-mount}
  [states props]
  [:div.google-map
   {:style {:height "500px"}}])

(def center-ui3 {:lat 37.775 :lng -122.434})
(def heatmap-data [[37.782551 -122.445368] [37.782745 -122.444586]])

(defn googlemap-heatmap-did-mount
  [states]
  #?(:cljs
     (go (when-not (gcore/gmap-loaded?)
           (async/<! (gcore/load-gmap)))
         (let [element (rum/dom-node states)
               gmap (js/google.maps.Map.
                     element
                     (clj->js {:zoom 13 :center center-ui3 :mapTypeId "satellite"}))]
           (js/google.maps.visualization.HeatmapLayer.
            (clj->js {:data (clj->js [(js/google.maps.LatLng. 37.782551  -122.445368)
                                      (js/google.maps.LatLng. 37.782745  -122.444586)]) :map gmap})))))
  states)

(rum/defcs googlemap-heatmap-ui < rum/static
  {:did-mount googlemap-heatmap-did-mount}
  [states]
  [:div.googlemap.googlemap-heatmap {:style {:height "500px"}}])
