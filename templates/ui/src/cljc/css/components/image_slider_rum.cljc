(ns css.components.image-slider-rum
  (:require
   [rum.core :as rum]
   [clojure.string :as cstr]
   #?@(:cljs [[cljs-utils.core :refer [stop-event]]
              [cljs.core.async :as async :refer [chan put!]]
              [goog.style :as gstyle]])
   #?@(:clj [[clojure.core.async :as async :refer [chan put! go-loop]]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go go-loop alt!]])))

(def ^:dynamic *margin* 20)

(defn get-offset
  [element-width image-width]
  (/ (- element-width image-width) 2))

(defn initial-position [offset margin]
  #?(:cljs
     (Math/abs (- offset *margin*))))

(defn slide-next-move
  [image-width margin]
  (+ image-width (* 2 margin)))

(defn position
  [image-width offset margin image-idx]
  (+ (initial-position offset margin)
     (* (dec image-idx)
        (slide-next-move image-width margin))))

(defn image-slider-update-sizes
  [states]
  (fn [e]
    #?(:cljs
       (let [element                     (rum/dom-node states)
             ui                          (:ui states)
             {:keys [current-image-idx]} @ui
             element-width               (-> element gstyle/getSize .-width)
             image                       (.querySelector element ".image.list-item:first-child .image")
             image-width                 (-> image gstyle/getSize .-width)
             image-event                 (fn [e]
                                           (let [image-width (-> image gstyle/getSize .-width)]
                                             (swap! ui assoc :image-width image-width)))
             offset                      (get-offset element-width image-width)
             translate-x                 (position image-width offset *margin* current-image-idx)]
         (.addEventListener image "load" image-event)
         (swap! ui assoc :element-width element-width)))))

(defn image-slider-will-mount
  [states]
  (let [component         (:rum/react-component states)
        current-image-idx (-> states :rum/args :current-image-idx)
        current-image-idx (if current-image-idx
                            (- 1 current-image-idx) 1)
        ui                {:current-image-idx current-image-idx}
        ui-atom           (atom ui)]
    (add-watch ui-atom :component
               (fn [_ _ _ _]
                 (rum/request-render component)))
    (assoc states :ui ui-atom)))

(defn image-slider-did-mount
  [states]
  #?(:cljs (let [update-size-event (image-slider-update-sizes states)]
             (update-size-event)
             (.addEventListener js/window "resize" update-size-event)
             (assoc states :update-size-event update-size-event))
     :clj states))


;; TODO: add didremount


#_(componentWillUpdate
   [this next-props next-state]
   (let [props       (om/props this)
         states      (om/get-rendered-state this)
         current-idx (:current-image-idx props)
         next-idx    (:current-image-idx next-props)]

     (when-not (= current-idx next-idx)
       (om/update-state! this assoc :current-image-idx (- 1 next-idx)))))

(rum/defcs image-slider-ui < rum/static
  {:will-mount image-slider-will-mount
   :did-mount  image-slider-did-mount}
  [states props]
  (let [{:keys [ui]}          states
        {:keys [element-width
                current-image-idx
                image-width]} @ui
        {:keys [styles
                images
                show-order?]} props
        show-order?           (boolean show-order?)
        image-count           (count images)
        offset                (get-offset element-width image-width)
        translate-x           (position image-width offset *margin* current-image-idx)]
    [:div.image-slider.grid
     {:style (merge (:main styles)
                    {:flex-direction "column"})}
     [:div.inner.grid
      [:div.nav-prev
       (let [disabled? (= current-image-idx 1)]
         {:on-click
          (fn [e]
            #?(:cljs
               (when-not disabled?
                 (swap! ui update :current-image-idx inc))))
          :class (when disabled? "disabled")})

       [:i.fa.fa-chevron-left]]

      [:div.nav-next
       (let [disabled? (= current-image-idx (- 2 image-count))]
         {:on-click
          (fn [e]
            #?(:cljs (do
                       (stop-event e)
                       (when-not disabled?
                         (swap! ui update :current-image-idx dec)))))
          :class (when disabled? "disabled")})
       [:i.fa.fa-chevron-right]]

      [:ul.images.list.list--horizontal
       {:style
        {:transform (str  "translateX(" translate-x "px)")}}
       (map-indexed
        (fn [i x]
          (let [src   (if (vector? x)
                        (first x) x)
                label (when (vector? x) (second x))
                label (when label
                        (if show-order?
                          (str label " (" (inc i) "/" image-count ")")
                          label))]
            [:li.image.list-item
             {:key   i
              :class (when (= (- 1 i) current-image-idx) "active")}
             [:img.image.image--contained
              {:src src}]
             [:label.label label]]))
        images)]]]))

(defn one-image-slider-will-mount
  [states]
  (let [{:keys [cache]}              states
        {:keys [image-count styles]} (-> states :rum/args first)
        main-styles                  (:main styles)
        width-num                    (:width-num main-styles)
        padding-num                  (:padding-num main-styles)
        images-width                 (- width-num (* (or (second padding-num) 0) 2))
        image-count                  (or image-count 1)
        main-styles'                 (cond-> (dissoc main-styles :width-num :padding-num)
                                             width-num   (assoc :width (str width-num "rem"))
                                             padding-num (assoc :padding (str (cstr/join " " (map #(str % "rem") padding-num)))))]

    (reset! cache {:main-styles  main-styles'
                   :images-width images-width
                   :image-count  image-count
                   :item-width   (/ images-width image-count)})))

(rum/defcs one-image-slider-ui < rum/static
  (rum/local :cache {:item-width nil})
  (rum/local :ui {:current-image-idx 0})
  [states props]
  (let [{:keys [cache ui]}                 states
        cache                              @cache
        {:keys [current-image-idx]}        @ui
        {:keys [images image-step styles]} props
        image-step                         (or image-step 1)]
    [:div.image-slider--one.grid
     {:style (merge {:position "relative"
                     :overflow "hidden"}
                    (:main-styles cache))}

     (let [share-styles (merge {:top         "50%"
                                :color       "white"
                                :text-shadow "0 0 5px rgba(0,0,0,.6)"
                                :transform   "translateY(-50%)"
                                :z-index     1
                                :font-size   "1rem"
                                :position    "absolute"}
                               (dissoc (:controls styles) :space))
           space        (or (get-in styles [:controls :space]) "1rem")]
       [(let [disabled? (or (zero? current-image-idx))]
          [:i.fa.fa-chevron-left
           {:on-click (fn [e]
                        #?(:cljs
                           (do (stop-event e)
                             (let [step (max 0 (- current-image-idx image-step))]
                               (when (not disabled?)
                                 (swap! ui assoc :current-image-idx step))))))
            :style    (merge share-styles {:left space})}])

        (let [disabled? (= (dec (count images)) current-image-idx)]
          [:i.fa.fa-chevron-right
           {:on-click (fn [e]
                        #?(:cljs
                           (do (stop-event e)
                             (let [step (min (- (count images) 1)
                                             (+ current-image-idx image-step))]
                               (when-not disabled?
                                 (swap! ui assoc :current-image-idx step))))))
            :style    (merge share-styles {:right space})}])])

     (let [translate-x  (- (* (:item-width cache) current-image-idx))
           images-width (str (:images-width cache) "rem")
           item-width   (str (:item-width cache) "rem")]
       [:div.images-wrapper
        {:style (merge {:overflow "hidden"}
                       {:width images-width})}
        [:ul.images.list.list--horizontal
         {:style
          {:transform (str "translateX(" translate-x "rem)")}}
         (map-indexed (fn [i x]
                        [:li.image.grid.list-item
                         {:key   i
                          :style (merge (:item styles)
                                        {:width item-width})}
                         x]) images)]])]))

(defn one-item-slider-did-mount
  [states]
  #?(:cljs
     (let [{:keys [cache]} states
           {:keys [item-per-slide]} (-> states :rum/args first)
           el (rum/dom-node states)
           bounding (.getBoundingClientRect el)
           item-width (/ (.-width bounding) item-per-slide)]
       (swap! cache assoc :item-width item-width)))
  states)

(rum/defcs one-item-slider-ui < rum/static
  (rum/local {:item-width 0} :cache)
  (rum/local {:current-image-idx 0} :ui)
  {:did-mount one-item-slider-did-mount}
  [states props]
  (let [{:keys [ui cache]}                         states
        cache                                      @cache
        {:keys [current-image-idx]}                @ui
        {:keys [items item-per-slide step styles]} props
        step                                       (or step 1)]
    [:div.image-slider--one.grid
     {:style {:position "relative"
              :overflow "hidden"}}
     (let [share-styles (merge {:top         "50%"
                                :color       "white"
                                :text-shadow "0 0 5px rgba(0,0,0,.6)"
                                :transform   "translateY(-50%)"
                                :z-index     1
                                :font-size   "1rem"
                                :position    "absolute"}
                               (dissoc (:controls styles) :space))
           space        (or (get-in styles [:controls :space]) "1rem")]
       [:div
        (let [disabled? (or (zero? current-image-idx))]
          [:i.fa.fa-chevron-left
           {:on-click (fn [e]
                        #?(:cljs
                           (do (stop-event e)
                             (let [step (max 0 (- current-image-idx step))]
                               (when-not disabled?
                                 (swap! ui assoc :current-image-idx step))))))
            :style    (merge share-styles {:left space})}])

        (let [disabled? (= (- (count items) item-per-slide) current-image-idx)]
          [:i.fa.fa-chevron-right
           {:on-click (fn [e]
                        #?(:cljs
                           (do (stop-event e)
                             (let [step (min (- (count items) 1)
                                             (+ current-image-idx step))]
                               (when-not disabled?
                                 (swap! ui assoc :current-image-idx step))))))
            :style    (merge share-styles {:right space})}])])

     (let [item-width  (:item-width cache)
           translate-x (- (* item-width current-image-idx))]
       [:div.inner.grid
        {:style {:overflow "hidden"}}
        [:ul.items.list.list--horizontal
         {:style {:transform (str "translateX(" translate-x "px)")}}
         (map-indexed (fn [i x]
                        [:li.image.grid.list-item
                         {:key   i
                          :style (merge (:item styles)
                                        {:width (str item-width "px")})} x])
                      items)]])]))
