(ns css.components.nav)

#_(def tabs {:home home-ui
             :purchase purchase-ui})

#_(def xs
    {:home "Home"
     :purchase "Purchase"
     :history "History"
     :referrals "Referrals"
     :bounty "Bounty"
     :settings "Settings"})

#_(defn sidebar-on-click
    [r next-tab]
    (citrus.core/dispatch! r :route/investor-page :change-nav :purchase))

#_(rum/defc investor-sidebar < rum/static rum/reactive
    [r]
    (let [{:keys [current-tab]} (rum/react (citrus/subscription r [:route/investor-page :ui]))]
      [:nav.sidebar.ico-sidebar
       [:ul#sidebar
        (->> xs (map-indexed (fn [idx [k v]]
                               [:li.sidebar-item
                                {:key idx
                                 :on-click #(sidebar-on-click r k)}
                                [:i.fa.fa-home.mr--3-0]
                                [:a
                                 {:class (when (= current-tab k) "active")}
                                 (str v)]])))]]))

#_(defmethod control :change-nav [_ [next-tab] state]
    (let [state (assoc-in state [:ui :current-tab] next-tab)]
      {:state state}))

(defn sidebar []
  [:nav.sidebar.ico-sidebar
   [:ul#sidebar
    [:li.sidebar-item
     [:i.fa.fa-home.mr--3-0]
     [:a.active {:on-click #()} "Home"]]
    [:li.sidebar-item
     [:i.fa.fa-shopping-cart.mr--3-0]
     [:a {:on-click #()} "Purchase"]]
    [:li.sidebar-item
     [:i.fa.fa-history.mr--3-0]
     [:a {:on-click #()} "History"]]
    [:li.sidebar-item
     [:i.fa.fa-users.mr--3-0]
     [:a {:on-click #()} "Referrals"]]
    [:li.sidebar-item
     [:i.fa.fa-connectdevelop.mr--3-0]
     [:a {:on-click #()} "Bounty"]]
    [:li.sidebar-item
     [:i.fa.fa-cog.mr--3-0]
     [:a {:on-click #()} "Settings"]]]])

