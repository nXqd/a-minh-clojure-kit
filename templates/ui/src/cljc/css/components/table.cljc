(ns css.components.table
  (:require
   [rum.core :as rum]
   #?@(:cljs [[goog.ui.TableSorter :as TableSorter]])))

(defn table-did-mount
  [states]
  #?(:cljs
     (let [table (rum/ref-node states "table")
           comp (goog.ui.TableSorter.)]
       (.decorate comp table)
       ;; monthly-emi
       (.setSortFunction comp 0
                         (TableSorter/createReverseSort TableSorter/numericSort))
       (.setSortFunction comp 1
                         TableSorter/alphaSort)))
  states)

(rum/defc table-ui < rum/static
  {:did-mount table-did-mount}
  []
  (let []
    [:table.sorted
     {:border "0", :cellpadding "3", :id "sortMe"
      :ref "table"}
     [:thead
      [:tr
       [:th
        "Number"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]
       [:th "Month"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]
       [:th "Days (non-leap year)"
        [:div.arrows
         [:i.fa.fa-arrow-down]
         [:i.fa.fa-arrow-up]]]]]
     [:tbody
      [:tr
       [:td "1"]
       [:td "January"]
       [:td "31"]]
      [:tr
       [:td "2"]
       [:td "Februrary"]
       [:td "28"]]
      [:tr
       [:td "3"]
       [:td "March"]
       [:td "31"]]
      [:tr
       [:td "4"]
       [:td "April"]
       [:td "30"]]
      [:tr
       [:td "5"]
       [:td "May"]
       [:td "31"]]
      [:tr
       [:td "6"]
       [:td "June"]
       [:td "30"]]
      [:tr
       [:td "7"]
       [:td "July"]
       [:td "31"]]
      [:tr
       [:td "8"]
       [:td "August"]
       [:td "31"]]
      [:tr
       [:td "9"]
       [:td "September"]
       [:td "30"]]
      [:tr
       [:td "10"]
       [:td "October"]
       [:td "31"]]
      [:tr
       [:td "11"]
       [:td "November"]
       [:td "30"]]
      [:tr
       [:td "12"]
       [:td "December"]
       [:td "31"]]]]))
