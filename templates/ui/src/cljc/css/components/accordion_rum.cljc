(ns css.components.accordion-rum
  (:require
   #?@(:cljs
       [[cljs-utils.core :refer [stop-event]]])
   [rum.core :as rum]))

(defn accordion-ui-will-mount
  [states]
  (let [active? (or (-> states :rum/args first :active?) false)
        active-atom (atom active?)
        component (:rum/react-component states)]

    (add-watch active-atom :component
               (fn [_ _ _ _] (rum/request-render component)))

    (assoc states :active? active-atom)))

(rum/defcs accordion-ui < rum/static
  {:will-mount accordion-ui-will-mount
   :key-fn (fn [props] (:key props))}

  [states props]
  (let [{:keys [title
                content
                on-click
                on-click-index
                styles]} props
        {:keys [active?]} states]
    [:div.accordion.grid
     {:class (when @active? "active")}
     [:div.title.grid
      {:style {:align-items "center"}
       :on-click
       ;; order matters here
       (fn [e]
         (reset! active? (not @active?))
         (when on-click (on-click {:active? @active?}))
         (when on-click-index (on-click-index {:active? @active?}))
         #?(:cljs (stop-event e)))}
      (if @active?
        [:i.fa.fa-chevron-up]
        [:i.fa.fa-chevron-down])
      title]
     [:div.content.grid
      {:class (when-not @active? "hidden")
       :style (:content styles)}
      content]]))

(rum/defcs accordions-ui < rum/static
  (rum/local 0 :current-index)
  [states props]
  (let [{:keys [accordions on-close-all type styles index]} props
        {:keys [current-index]}                             states
        index (if (number? index) index -1)]
    [:div.accordions.list.grid
     {:style (:main styles)}
     (->> accordions
          (map-indexed
           #(accordion-ui
             (cond-> %2
                     true
                     (assoc :styles (:accordion styles)
                            :active? (= %1 index)
                            :key %1
                            :type type)

                     (= type :single)
                     (assoc :on-click-index
                            (fn [x]
                              (let [current-index' (when-not (:active? x) %1)]
                                (when (and on-close-all (nil? current-index'))
                                  (on-close-all))
                                (reset! current-index current-index'))))))))]))
