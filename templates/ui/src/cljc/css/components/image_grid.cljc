(ns css.components.image-grid
  (:require
   [rum.core :as rum]))

(def default-styles
  {:main      {:width  "30rem"
               :height "20rem"}
   :more-text {:position    "absolute"
               :z-index     2
               :color       "white"
               :font-weight "bold"
               :font-size   "1.5rem"
               :text-align  "center"
               :text-shadow "1px 1px 1px rgba(0,0,0,.4)"
               :width       "100%"
               :top         "50%"
               :transform   "translateY(-50%)"}
   :image     {:border                "5px solid white"
               :cursor                "pointer"
               :background-size       "cover"
               :background-position-x "50%"
               :background-position-y "50%"}})

(defn image-item
  [x]
  (let [{:keys [url
                order
                style
                class
                on-click]} x
        on-click         (or on-click #())]
    [:.image.grid-cell
     {:itemScope true
      :itemProp "associatedMedia"
      :itemType "http://schema.org/ImageObject"
      :class    class
      :on-click (fn [e] (on-click e order))
      :style    (merge
                 (:image default-styles)
                 (assoc
                  (or style {})
                  :background-image (str "url(" url ")")))}]))

(rum/defc image-grid-ui < rum/static
  [props]
  (let [{:keys [styles
                class
                on-click
                max-image
                images]} props
        on-click         (or on-click #())
        max-image        (or max-image 3)
        images-count     (count images)
        more-images      (if (> images-count max-image)
                           (- images-count max-image) 0)]
    (assert (>= (count images)))
    [:div.image-grid.grid
     {:itemScope true
      :itemType "http://schema.org/ImageGallery"
      :class class
      :style (or (:main styles)
                 (:main default-styles))}

     (image-item
      {:url      (get images 0)
       :order    0
       :on-click on-click
       :class    "grid-md-11"})
     [:div.grid-cell.grid-md-5
      {:style {:position "relative"}}

      (image-item
       {:url      (get images 1)
        :order    1
        :on-click on-click
        :style    {:position "absolute"
                   :top      0
                   :width    "100%"
                   :height   "50%"}})

      [:div.last-image.grid
       {:style
        {:position "absolute"
         :width    "100%"
         :height   "50%"
         :bottom   0}}

       (when (not= more-images 0)
         [:div.black.grid
          {:on-click (fn [e] (on-click e 2))
           :style
           {:border           "5px solid white"
            :cursor           "pointer"
            :position         "absolute"
            :z-index          1
            :width            "100%"
            :height           "100%"
            :background-color "rgba(0,0,0,0.3)"}}
          [:span.text
           {:style (:more-text default-styles)}
           (str "+ " more-images)]])

       (image-item
        {:url      (get images 2)
         :order    2
         :on-click (fn [e] (on-click e 2))
         :style    {:position "absolute"
                    :cursor   "pointer"
                    :width    "100%"
                    :height   "100%"
                    :bottom   0}})]]]))
