(ns css.components.popup
  (:require [rum.core :as rum])
  #?(:cljs
     (:import
      goog.ui.Popup
      goog.ui.PopupBase
      goog.positioning.AnchoredPosition
      goog.positioning.AnchoredViewportPosition
      goog.positioning.Corner)))

(defn on-click
  [states]
  #?(:cljs
     (fn [e]
       (let [{:keys [ui]} states
             {:keys [popup anchor]} @ui]
         (.setVisible popup false)
         (.setPosition popup
                       (goog.positioning.AnchoredViewportPosition.
                        anchor
                        goog.positioning.Corner.BOTTOM_RIGHT))
         (.setVisible popup true)))))

(defn popup-did-mount
  [states]
  #?(:cljs
     (let [anchor-el (rum/ref-node states "anchor")
           popup-el (rum/ref-node states "popup")
           popup (goog.ui.Popup. popup-el)
           {:keys [ui]} states]
       (reset! ui {:popup popup :anchor anchor-el})
       (.setHideOnEscape popup true)
       (.setAutoHide popup true)
       states)
     :clj states))

(rum/defcs popup-ui < rum/static
  (rum/local {:popup nil
              :anchor nil} :ui)
  {:did-mount popup-did-mount}
  [states]
  [:div
   [:button
    {:ref "anchor"
     :on-click (on-click states)}
    "Button"]
   [:div
    {:style {:position "absolute"
             :visibility "hidden"}
     :ref "popup"}
    "Popup"]])
