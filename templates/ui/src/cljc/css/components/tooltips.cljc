(ns css.components.tooltips
  (:require
   [rum.core :as rum]
   #?@(:cljs [[goog.ui.Tooltip :as Tooltip]])))

(defn simple-did-mount [states]
  #?(:cljs (let [el-node (rum/ref-node states "element")
                 tip (goog.ui.Tooltip. el-node "ssome message")]))
  states)

(rum/defcs simple-ui < rum/static
  {:did-mount simple-did-mount}
  [props states]
  (let [{:keys [el text]} props]
    [:div.tooltips
     [:div.el
      {:ref "element"}
      [:button



       "some string"]]]))


