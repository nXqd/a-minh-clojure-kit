(ns css.components.breadcrumb-rum
  (:require
   [rum.core :as rum]))

(defn default-breadcrumb-ui
  [x]
  [:li.list-item
   {:key (:label x)}
   [:a.link
    {:style {:text-decoration "none"}
     :href  (:link x)}
    (:label x)]])

(rum/defc breadcrumb-ui
  [props]
  (let [{:keys [breadcrumbs styles breadcrumb-ui]} props
        breadcrumb-ui (or breadcrumb-ui default-breadcrumb-ui)]
    [:ul.ui-breadcrumb.list.list--horizontal
     (map breadcrumb-ui breadcrumbs)]))

(defn breadcrumb-simple
  [{:keys [label link]} index total]
  (when (and link label)
    [:li.list-item
     {:style {:flex-grow 0}
      :key   label}
     [:a.link
      {:style {:text-decoration "none"}
       :href  link} label]
     (when-not (= index (dec total))
       [:span
        {:style {:color     "#999"
                 :font-size "1.5rem"
                 :padding   "0 0.5rem"}}
        " › "])]))

(rum/defc breadcrumb-simple-ui
  [props]
  (let [{:keys [breadcrumbs styles]} props
        breadcrumb-ui (or breadcrumb-ui default-breadcrumb-ui)]
    [:ul.ui-breadcrumb.list.list--horizontal
     (map-indexed #(breadcrumb-simple %2 %1 (count breadcrumbs)) breadcrumbs)]))
