(ns css.components.combobox
  (:require
   [rum.core :as rum]
   #?@(:cljs
       [goog.events
        goog.dispose]))
  #?(:cljs
     (:import
      [goog.ui ComboBox ComboBoxItem MenuSeparator])))

(defn make-combobox
  [node {:keys [placeholder options]}]
  #?(:cljs
     (let [cb (goog.ui.ComboBox.)]
       (do
         (.setDefaultText cb placeholder)
         (.setUseDropdownArrow cb true)
         ;; add items
         (doall
          (map #(.addItem cb (goog.ui.ComboBoxItem. %)) options))
         (.render cb node))
       cb)))

(defn combobox-on-change
  [on-change labels-values]
  (fn [e]
    #?(:cljs
       (let [combo-value (.. e -target getValue)
             value (get labels-values combo-value)]
         (on-change value)))))

(defn combobox-did-mount
  [states]
  (let [props (-> states :rum/args first)
        {:keys [placeholder options on-change]} props
        node (rum/ref-node states "ref-combo")
        cb (make-combobox node {:placeholder placeholder
                                :options (keys options)})
        ui (atom {:combobox cb})]
    #?(:cljs
       (when on-change
         (goog.events.listen cb "change"
                             (combobox-on-change on-change options))))
    (assoc states :ui ui)))

(defn combobox-will-unmount
  [states]
  #?(:cljs
     (let [{:keys [ui]} states
           {:keys [combobox]} @ui]
       (goog.dispose combobox)))
  states)

(rum/defcs combobox-ui < rum/static
  {:did-mount combobox-did-mount
   :will-unmount combobox-will-unmount}
  [states props]
  [:.combo.grid {:ref "ref-combo"}])
