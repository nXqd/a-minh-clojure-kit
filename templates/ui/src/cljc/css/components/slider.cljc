(ns css.components.slider
  (:require
   [rum.core :as rum]
   #?@(:cljs
       [goog.events
        goog.dispose]))
  #?(:cljs
     (:import
      [goog.ui Component TwoThumbSlider])))

(defn make-two-thumb-slider
  [el opts]
  #?(:cljs
     (let [{:keys [step value extent]} opts
           step (or step 1)
           s (goog.ui.TwoThumbSlider.)]
       (do
         (.setStep s step)
         (.decorate s el)
         (when value (.setValue s value))
         (when extent (.setExtent s extent)))
       s)))

(defn two-thumb-slider-did-mount
  [states]
  (let [props (-> states :rum/args first)
        {:keys [on-change]} props
        node (rum/ref-node states "ref-two-thumb-slider")
        two-thumb-slider (make-two-thumb-slider node {:step 10})]
    #?(:cljs
       (when on-change
         (.addEventListener two-thumb-slider
                            goog.ui.Component.EventType.CHANGE
                            (fn []
                              (let [value (.getValue two-thumb-slider)
                                    extent (.getExtent two-thumb-slider)
                                    val {:start value
                                         :end (+ value extent)}]
                                (on-change val)))))))
  states)

(def two-thumb-slider-default-styles
  {:main {:height "1rem"}
   :thumb {:width "1rem"
           :height "1rem"}})

(rum/defcs two-thumb-slider-ui < rum/static
  {:did-mount two-thumb-slider-did-mount}
  [states props]
  (let [{:keys [styles]} props]
    [:div.two-thumb-slider
     {:style (merge
              (:main two-thumb-slider-default-styles)
              (:main styles))
      :ref "ref-two-thumb-slider"}
     [:div
      {:style
       {:position "absolute"
        :width "100%"
        :top "9px"
        :height "2px"
        :background "rebeccapurple"
        :overflow "hidden"}}]
     [:div.goog-twothumbslider-value-thumb
      {:style (:thumb two-thumb-slider-default-styles)}]
     [:div.goog-twothumbslider-extent-thumb
      {:style (:thumb two-thumb-slider-default-styles)}]]))
