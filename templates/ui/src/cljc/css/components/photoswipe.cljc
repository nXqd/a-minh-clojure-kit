(ns css.components.photoswipe
  (:require
   [rum.core :as rum]
   [css.components.image-grid :as image-grid]
   [css.utils.core :as utils]
   #?@(:cljs [[cljs-utils.core :refer [stop-event]]
              [cljs.core.async :as async :refer [chan put!]]
              [cljsjs.photoswipe-ui-default]
              [cljsjs.photoswipe]
              [goog.style :as gstyle]])
   #?@(:clj [[clojure.core.async :as async :refer [chan put! go-loop]]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go go-loop alt!]])))

;; photoswipe
(def photoswipe-default-options
  {:history false
   :focus false
   :showAnimationDuration 0
   :hideAnimationDuration 0})

(def ^:const lib-uri1 "https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe.min.js")
(def ^:const lib-uri2 "https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.1/photoswipe-ui-default.min.js")

(defn libs-loaded?
  []
  #?(:cljs (and js/window.PhotoSwipe js/window.PhotoSwipeUI_Default)))

(defn load-lib1
  []
  (utils/load-script-async lib-uri1))

(defn load-lib2
  []
  (utils/load-script-async lib-uri2))

(defn open-photoswipe
  [el items options]
  #?(:cljs
     (let [{:keys [index]} options
           options (assoc options :index (or index 0))
           options (merge photoswipe-default-options options)
           options (clj->js options)]
       (go (when-not (libs-loaded?)
             (async/<! (load-lib1))
             (async/<! (load-lib2)))
           (let [photoswipe (js/PhotoSwipe. el js/PhotoSwipeUI_Default items options)]
             (.init photoswipe))))))

(defn not-sizes-included?
  [image]
  (not (and (:w image) (:h image))))

(defn image-sizes
  [src cb]
  #?(:cljs
     (let [img (js/Image.)]
       (set! (.-onload img)
             (fn []
               (cb {:w img.width :h img.height})))
       (set! (.-src img) src))))

(defn- add-images-sizes
  [images cb]
  #?(:cljs
     (let [c (async/chan 1)
           images-count (count images)
           ret (atom [])]
       (doall
        (map (fn [{:keys [src w h] :as x}]
               (if (not-sizes-included? x)
                 (image-sizes src (fn [sizes]
                                    (async/put! c (merge x sizes))))
                 (async/put! c x)))
             images))

       (go-loop [count 0]
         (let [val (async/<! c)
               count (inc count)]
           (swap! ret conj val)
           (if-not (= count images-count)
             (recur count)
             (cb @ret)))))))

(defn grid-images-from
  [items]
  (vec (map (fn [x] (or (:msrc x) (:src x))) items)))

(defn update-cached-images
  [ui items]
  (let [grid-images (grid-images-from items)]
    (swap! ui assoc :grid-images grid-images)
    (add-images-sizes items (fn [xs]
                              (swap! ui assoc :items-with-sizes xs)))))

(defn photoswipe-did-remount
  [old-state state]
  (let [old-props (-> old-state :rum/args first)
        props (-> state :rum/args first)
        {:keys [ui]} state
        {:keys [items]} props]
    (when-not (= old-props props)
      (update-cached-images ui items))
    state))

(defn photoswipe-will-mount
  [states]
  #?(:cljs (let [props (-> states :rum/args first)
                 {:keys [items]} props
                 component (:rum/react-component states)
                 ui (atom {:photoswipe nil})]
             (add-watch ui :component (fn [_ _ _ _]
                                        (rum/request-render component)))
             (update-cached-images ui items)
             (assoc states :ui ui))
     :clj states))

(defn photoswipe-did-mount
  [states]
  #?(:cljs (let [{:keys [ui]} states el (rum/ref-node states "photoswipe")] (swap! ui assoc :photoswipe el) states)
     :clj states))

(rum/defcs photoswipe-ui < {:will-mount photoswipe-will-mount
                            :did-mount photoswipe-did-mount
                            :did-remount photoswipe-did-remount}
  [states props]
  (let [{:keys [styles]} props
        {:keys [ui]} states
        {:keys [grid-images items-with-sizes photoswipe]} @ui]
    [:div.photoswipe.grid
     (image-grid/image-grid-ui
      {:images grid-images
       :styles (:grid styles)
       :on-click (fn [e order]
                   #?(:cljs
                      (when-not (empty? items-with-sizes)
                        (let [items (clj->js items-with-sizes)]
                          (open-photoswipe photoswipe items {:index order})))))})
     [:div {:class "pswp", :tabIndex "-1",
            :role "dialog", :aria-hidden "true"
            :ref "photoswipe"}
      [:div {:class "pswp__bg"}]
      [:div {:class "pswp__scroll-wrap"}
       [:div {:class "pswp__container"}
        [:div {:class "pswp__item"}]
        [:div {:class "pswp__item"}]
        [:div {:class "pswp__item"}]]
       [:div {:class "pswp__ui pswp__ui--hidden"}
        [:div {:class "pswp__top-bar"}
         [:div {:class "pswp__counter"}]
         [:button {:class "pswp__button pswp__button--close", :title "Close (Esc)"}]
         [:button {:class "pswp__button pswp__button--share", :title "Share"}]
         [:button {:class "pswp__button pswp__button--fs", :title "Toggle fullscreen"}]
         [:button {:class "pswp__button pswp__button--zoom", :title "Zoom in/out"}]
         [:div {:class "pswp__preloader"}
          [:div {:class "pswp__preloader__icn"}
           [:div {:class "pswp__preloader__cut"}
            [:div {:class "pswp__preloader__donut"}]]]]]
        [:div {:class "pswp__share-modal pswp__share-modal--hidden pswp__single-tap"}
         [:div {:class "pswp__share-tooltip"}]]
        [:button {:class "pswp__button pswp__button--arrow--left", :title "Previous (arrow left)"}]
        [:button {:class "pswp__button pswp__button--arrow--right", :title "Next (arrow right)"}]
        [:div {:class "pswp__caption"}
         [:div {:class "pswp__caption__center"}]]]]]]))
