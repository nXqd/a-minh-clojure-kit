(ns css.components.graphs.line-chart
  (:require
   [rum.core :as rum]))

(def series-data
  [{:name "Installation"
    :data [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]}
   {:name "Manufacturing"
    :data [24916, 24064, 29742, 29851, 32490, 30282, 38121, 40434]}
   {:name "Sales & Distribution"
    :data [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]}
   {:name "Project Development"
    :data [nil, nil, 7988, 12169, 15112, 22452, 34400, 34227]}
   {:name "Other"
    :data [12908, 5948, 8105, 11248, 8989, 11816, 18274, 18111]}])

(defn line-did-mount
  [states]
  #?(:cljs
     (let [element (rum/dom-node states)
           chart-config {:title       {:text "Title"}
                         :chart       {:zoomType "x"}
                         :subtitle    {:text "Subtitle"}
                         :xAxis       {:type  "datetime"
                                       :title {:text "ttttttttttt"}}
                         :yAxis       {:title     {:text  "KWh"
                                                   :align "high"}
                                       :plotLines [{:value 0
                                                    :width 1
                                                    :color "#808080"}]}
                         :tooltip     {:valueSuffix "KWh"}
                         :legend      {:enable false}
                         :series      series-data}]
       (js/Highcharts.chart  element (clj->js chart-config))
       states)
     :clj states))

(rum/defcs line-chart < rum/static rum/reactive
  {:did-mount line-did-mount}
  [_ props]
  [:div.line-chart
   {:style {:min-width "310px" :max-width "800px"
            :height "400px" :margin "0 auto"}}])
