(ns css.components.graphs.candlestick
  (:require
   [rum.core :as rum]
   [taoensso.timbre :as log]))

(defn chart-config
  [data]
  {:rangeSelector {:selected 1}
   :title {:text "AAPL Stock Price"}
   :series [{:type "candlestick"
             :name "AAPL Stock Price"
             :data data
             :dataGrouping {:units [["week" [1]]
                                    ["month" [1 2 3 4 6]]]}}]})

(defn candlestick-did-mount
  [states]
  #?(:cljs
     (let [el (rum/dom-node states)]
       (.getJSON js/$ "https://www.highcharts.com/samples/data/jsonp.php?a=e&filename=aapl-ohlc.json&callback=?"
                 (fn [data]
                   (js/Highcharts.stockChart el (clj->js (chart-config data))))))))

(rum/defcs candlestick-ui < rum/static
  {:did-mount candlestick-did-mount}
  [states props]
  [:div {:style {:min-width "310px" :max-width "800px"
                 :min-height "400px" :margin "0 auto"}}])

