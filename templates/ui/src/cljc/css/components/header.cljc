(ns css.components.header)

(defn bootstrap-header
  []
  [:nav.navbar.navbar-expand-lg.navbar-light.bg-light
   [:a.navbar-brand {:href "#"} "Navbar"]
   [:button.navbar-toggler
    {:aria-label "Toggle navigation",
     :aria-expanded "false",
     :aria-controls "navbarSupportedContent",
     :data-target "#navbarSupportedContent",
     :data-toggle "collapse",
     :type "button"}
    [:span.navbar-toggler-icon]]
   [:div#navbarSupportedContent.collapse.navbar-collapse
    [:ul.navbar-nav.mr-auto
     [:li.nav-item.active
      [:a.nav-link {:href "#"} "Home " [:span.sr-only "(current)"]]]
     [:li.nav-item [:a.nav-link {:href "#"} "Link"]]
     [:li.nav-item.dropdown
      [:a#navbarDropdown.nav-link.dropdown-toggle
       {:aria-expanded "false",
        :aria-haspopup "true",
        :data-toggle "dropdown",
        :role "button",
        :href "#"}
       "Dropdown"]
      [:div.dropdown-menu
       {:aria-labelledby "navbarDropdown"}
       [:a.dropdown-item {:href "#"} "Action"]
       [:a.dropdown-item {:href "#"} "Another action"]
       [:div.dropdown-divider]
       [:a.dropdown-item {:href "#"} "Something else here"]]]
     [:li.nav-item [:a.nav-link.disabled {:href "#"} "Disabled"]]]
    [:form.form-inline.my-2.my-lg-0
     [:input.form-control.mr-sm-2
      {:aria-label "Search", :placeholder "Search", :type "search"}]
     [:button.btn.btn-outline-success.my-2.my-sm-0
      {:type "submit"}
      "Search"]]]])

(defn header-notification-state-no-notifications []
  [:div.state-empty
   "Caught up with everything!"])

(defn header-notification
  [state]
  [:div.header-account
   {:tabIndex 0
    :style {:outline "none"}
    :on-blur #()}
   [:i.fa.fa-bell
    {:on-click #()}]
   [:div.notification-menu
    {:class (if (:active @state) "" "hidden")
     :style {:width "400px"}}
    [:ul.list.list--horizontal.header
     [:li.list-item
      "Notifications"]]
    [:div.main
     [header-notification-state-no-notifications]]
    [:div.previous
     [:a
      "Previously read"]]]])

(defn account
  [state]
  [:div.header-account
   {:tabIndex 0
    :style {:outline "none"}
    :on-blur #()}
   [:img.image.image--circle
    {:on-click #()
     :src "https://i.imgur.com/EGrpg28.gif"
     :width "35px"
     :height "35px"}]
   [:ul.list.account-menu
    {:class (if (:active @state) "" "hidden")
     :style {:width "180px"}}
    [:li.list-item.label
     "Signed in as nXqd"]
    [:li.list-item.separator]
    [:li.list-item
     "Settings"]
    [:li.list-item
     "Logout"]]])

(defn header-horizontal []
  [:div.app-header.header.header--horizontal
   {:style {:background-color "#3887be"}}

   [:div.container.container--fluid
    [:div.container
     [:div.grid
      ;; logo
      [:div.grid-cell.grid-lg-4.grid-md-4.grid-sm-4
       {:style
        {:background "transparent url('https://www.mapbox.com/base/latest/base@2x.png') no-repeat 0 0"
         :-webkit-background-size "100px 350px"
         :background-size "100px 350px"
         :background-position "0px -60px"}}]
      ;; list
      (let [navs ["Products" "Data" "Gallery" "Industries" "Pricing" "Help" "Developers" "Blog"]]
        [:ul.list.list--horizontal
         (map-indexed
          (fn [i x]
            ^{:key i}
            [:li.list-item
             [:a
              {:href "#"} x]]) navs)])]]]])

