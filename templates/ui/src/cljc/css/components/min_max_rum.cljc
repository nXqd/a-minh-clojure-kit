(ns css.components.min-max-rum
  (:require
   [rum.core :as rum]
   [clojure.string :as cstr]
   #?@(:cljs [[cljs-utils.core :refer [stop-event]]
              [css.utils.keys :refer [is-key? escape-key]]
              [cljs.core.async :as async :refer [chan put!]]])))

(defn process-label
  [placeholder min max]
  (let [label-min (:label min)
        label-max (:label max)]
    (if (and (empty? label-min)
             (empty? label-max))
      placeholder
      (let [label-min' (if (empty? label-min) "Min" label-min)
            label-max' (if (empty? label-max) "Max" label-max)]
        (cstr/join " - " [label-min' label-max'])))))

;; you can control is this component active? from outside : active? and ... from props
;; or it will control itself internally

(defn min-max-did-mount
  [states]
  #?(:cljs
     (let [{:keys [ui]} states
           props (-> states :rum/args first)
           min-input (rum/with-ref states "min-input")
           max-input (rum/with-ref states "max-input")
           values (or (:values props) {})]
       #_(.focus min-input)
       (swap! ui assoc :values values
              :min-input min-input
              :max-input max-input)))
  states)

(rum/defcs min-max-ui < rum/static
  (rum/local {:values {:min nil
                       :max nil}
              :active? false
              :at-min? true} :ui)
  {:did-mount min-max-did-mount}
  [states props]
  (let [{:keys [placeholder
                on-change
                on-blur
                on-focus
                mins
                maxs
                styles]} props
        {:keys [ui]} states
        {:keys [values at-min?]} @ui
        {:keys [min max]} values
        label (process-label placeholder min max)
        active? (if-not (nil? (:active? props)) (:active? props) (:active? states))]
    [:div.min-max.grid
     {:tabIndex 0
      :on-focus (or on-focus #())
      :on-blur (or on-blur #())}
     [:div.label.grid
      {:style (when styles (:label styles))
       :on-click (fn [e]
                   #?(:cljs (swap! ui assoc :active? true)))}
      label]

     [:div.inner.grid
      {:tabIndex 0
       :on-key-down (fn [e]
                      #?(:cljs
                         (do
                           (stop-event e)
                           (when (is-key? e escape-key)
                             (swap! ui assoc :at-min? true
                                    :active? false)))))
       :class (if active? "" "hidden")}
      [:ul.top.list.list--horizontal.grid
       [:li.list-item.grid-cell.grid-md-8
        [:input.min {:ref :min-input
                     :on-focus (fn [e]
                                 (swap! ui assoc :at-min? true
                                        :active? true))
                     :placeholder "Min"
                     :value (:label min)
                     :type "text"}]]
       [:li.list-item.grid-cell.grid-md-8
        [:input.max {:ref :max-input
                     :on-focus (fn [e]
                                 #?(:cljs
                                    (swap! ui assoc
                                           :at-min? false
                                           :active? true)))
                     :on-blur (fn [e]
                                #?(:cljs
                                   (swap! ui assoc :active? false)))

                     :placeholder "Max"
                     :value (:label max)
                     :type "text"}]]]
      (let [values (if at-min? mins maxs)]
        [:ul.values.grid.list
         {:on-click (fn [e] #?(:cljs (stop-event e)))
          :class    (if at-min? "" "max")}
         (let [on-click-fn (fn [x]
                             (fn [e]
                               #?(:cljs
                                  (do (if at-min?
                                        (do (swap! ui assoc-in [:values :min] x)
                                          (swap! ui assoc :at-min? false))

                                        (do (swap! ui assoc-in [:values :max] x)
                                          (swap! ui assoc :active? false
                                                 :at-min? true)))

                                    (when on-change
                                      (on-change values))

                                    (stop-event e)))))

               item-ui (fn [x]
                         [:li.list-item.grid
                          {:key (:label x)
                           :on-click (on-click-fn x)}
                          (:label x)])]
           (map item-ui  values))])]]))

(rum/defcs min-max-static-ui < rum/static
  (rum/local {:values {:min 0 :max 0}} :ui)
  [states props]
  (let [{:keys [on-change
                mins
                maxs
                styles]} props
        {:keys [ui]} states
        values (or (:values props) (:values @ui))
        {:keys [min max]} values
        on-change-fn (fn [x k]
                       (fn [e]
                         #?(:cljs
                            (do
                              (swap! ui assoc-in [:values k] x)
                              (when on-change
                                (on-change (:values @ui)))
                              (stop-event e)))))]
    [:div.min-max-static.grid
     [:ul.top.list.list--horizontal.grid
      {:style {:justify-content "space-between"}}

      [:li.list-item.grid-cell.grid-md-7
       [:input.grid.min {:ref :min-input
                         :placeholder "Min"
                         :value (:label min)
                         :type "text"}]
       [:ul.list.grid
        [:li.list-item.grid
         [:ul.mins.list
          {:style (:mins styles)}
          (map (fn [x]
                 [:li.list-item.grid.value
                  {:key (:label x)
                   :class (when (= x (:min values)) "active")
                   :on-click (on-change-fn x :min)}
                  (:label x)]) mins)]]]]

      [:li.list-item.grid-cell.grid-md-7
       [:input.max {:ref :max-input
                    :placeholder "Max"
                    :value (:label max)
                    :type "text"}]
       [:ul.list.grid
        [:li.list-item.grid
         {:style {:justify-content "flex-end"}}
         [:ul.maxs.list
          {:style (:maxs styles)}
          (map (fn [x]
                 [:li.list-item.grid.value
                  {:key (:label x)
                   :class (when (= x (:max values)) "active")
                   :on-click (on-change-fn x :max)}
                  (:label x)]) maxs)]]]]]]))
