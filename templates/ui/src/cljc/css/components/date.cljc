(ns css.components.date
  (:require
   [rum.core :as rum]
   #?@(:cljs
       [goog.events]))
  ;; #?(:cljs (:require ))
  #?(:cljs
     (:import
      [goog.i18n
       DateTimeSymbols
       DateTimePatterns
       DateTimeFormat
       DateTimeParse]
      [goog.ui InputDatePicker]
      [goog.date Date Interval])))

(def date-picker
  #?(:cljs
     (do
       (set! js/goog.i18n.DateTimeSymbols js/goog.i18n.DateTimeSymbols_vi)
       (set! js/goog.i18n.DateTimePatterns js/goog.i18n.DateTimePatterns_vi) (goog.ui.DatePicker.))))

(defn- date-picker-did-mount
  [state]
  #?(:cljs
     (let [widget-node (rum/ref-node state "widget")
           label-node (rum/ref-node state "label")
           dp date-picker]
       (.render dp widget-node)
       (goog.dom.setTextContent label-node
                                (.toIsoString (.getDate dp) true))))
  state)

(rum/defcs g-date-picker-ui < rum/static
  {:did-mount date-picker-did-mount}
  [states props]
  [:div
   [:div {:ref "widget"}]
   [:div {:ref "label"}]])

(defn input-date-picker
  "Build a google.ui.InputDatePicker with a specific format"
  [f]
  #?(:cljs
     (InputDatePicker. (DateTimeFormat. f) (DateTimeParse. f))))

(defn g-input-datepicker-did-mount
  [state]
  #?(:cljs
     (let [props (-> state :rum/args first)
           {:keys [on-change]} props
           node (rum/dom-node state)
           idp (input-date-picker "dd/MM/yyyy")]
       (.decorate idp node)
       (goog.events.listen idp goog.ui.DatePicker.Events.CHANGE
                           on-change)))
  state)

(rum/defcs g-input-datepicker < rum/static
  {:did-mount g-input-datepicker-did-mount}
  [states props]
  [:input.date-picker
   {:type "text"}])
