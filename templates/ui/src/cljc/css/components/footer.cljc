(ns css.components.footer)

(defn bootstrap-footer
  []
  [:footer
   [:.container
    [:p "© 2018 thoughtbot, inc."]
    [:p "The design of a robot and thoughtbot are registered trademarks of thoughtbot, inc."]]])
