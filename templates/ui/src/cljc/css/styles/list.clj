(ns css.styles.list
  (:refer-clojure :exclude [list])
  (:require [garden.selectors :as s]))

(def list-item
  [:.list-item {:position "relative"
                :list-style "none"
                :display "flex"
                :flex "1 0 auto"
                :flex-wrap "wrap"
                :flex-direction "row"
                :vertical-align "middle"
                :align-items "center"
                :margin 0
                :box-shadow "none"
                :border-radius "0"
                :border "none"}
   ;; label
   [:&.label {:cursor "default"}]

   ;; label
   [:&.separator {:height "1px"
                  :padding "0"
                  :margin "8px 1px"
                  :background-color "#e5e5e5"}]

   ;; list-image
   [:>
    ;; list-icon
    [:i {:display "inline-flex"
         :flex "0 1 auto"
         :font-size "1rem"
         :margin-right "0.5rem"
         :color "black"}]

    ;; list image
    [:img.list-image {:display "block"
                      :flex "0 1 auto"
                      :border-radius "50%"
                      :margin-right "1rem"}]]

   ;; content
   [:div.content {:flex "0 1 auto"}
    [:div.title {:font-weight 700
                 :line-height 1.8
                 :font-size "0.9rem"}]
    [:div.description {:font-size "0.8rem"}]]])

(def list-striped
  [:&.list--striped
   [(garden.selectors/& (garden.selectors/nth-child :odd))
    {:background-color "#ccc"}]])

(def list
  [[:.list {:display "flex"
            :justify-content "space-between"
            :flex-direction "column"
            :align-items "stretch"
            :background "0 0"
            :box-shadow "none"}
    [:li {:list-style "none"}]
    [:&.list--horizontal {:flex-direction "row"
                          :justify-content "initial"}
     [:.list-item {:padding 0}]]
    [list-striped]
    [list-item]]])
