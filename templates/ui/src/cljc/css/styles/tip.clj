(ns css.styles.tip
  (:require
   [garden.def :refer [defkeyframes]]))

(defkeyframes pulestate
  ["0%" {:transform "scale(0.9, 0.9)"
         :opacity 0.5}]

  ["50%" {:opacity 1}]

  ["100%" {:transform "scale(1.1, 1.1)"
           :opacity 0.0}])

(def tip [:.tip {:position "relative"}
          [:&.hide {:display "none"}]
          pulestate
          [:span
           ^:prefix {:animation [[pulestate "1s" :infinite]]}
           {:background-color "#cc0400"
            :border-radius "4px"
            :cursor "pointer"
            :display "block"
            :height "8px"
            :position "absolute"
            :width "8px"}]
          [:.content {:background-color "#fff"
                      :border-radius "2px"
                      :border "1px solid #b2b3b3"
                      :bottom "5px"
                      :box-shadow "0 2px 10px rgba(32, 41, 49, 0.15)"
                      :opacity 0
                      :padding "7px"
                      :position "absolute"
                      :right "-105px"
                      :text-align "center"
                      :transition "all, 0.2s, ease-in-out"
                      :width "210px"
                      :z-index 10}]
          [:&.show {:opacity 1}]])
