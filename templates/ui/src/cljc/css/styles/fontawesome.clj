(ns css.styles.fontawesome)

(def default-style [:i.fa {:color "#aaa"
                           :cursor "pointer"
                           :transition "color 0.2s ease"}
                    [:&:hover {:color "#000"}]])

(def fontawesome [default-style])
