(ns css.styles.grid-xy
  (:require
   [clojure.spec.alpha :as s]
   [garden.selectors :as gs]
   [garden.stylesheet :refer [at-media]]))

(defonce medium-screen-size "40em")
(defonce large-screen-size "64em")

(s/def ::grid-type #{"x" "y"})
(s/def ::gutter-type #{"margin" "padding"})
(s/def ::num-col #{12 16})
(s/def ::size-prefix #{"sm" "md" "lg"})

;; grid
;; gutter
;; g-c ;; grid-container
;; c c-sm--4 c-lg--4
;; utils including margin ...

(s/fdef gen-cell
  :args (s/cat :size-prefix ::size-prefix
               :post-fix pos-int?
               :percent double?
               :offset (s/? double?)
               :grid-type (s/? ::grid-type)))
(defn- gen-cell
  "Generates cell with prefix and postfix and percent.
  For example: .c-sm--8 {:width 50%} or .c-sm--8 {:width calc(100% - offset)}
  Optional args:
  offset: is metrics with unit. 10rem instead of 10"
  [size-prefix postfix percent {:keys [offset grid-type]}]
  (let [clazz (if (= grid-type "y") :height :width)]
    [(keyword (str ".c-" size-prefix "--" postfix))
     {clazz
      (let [percent-str (str percent "%")]
        (if-not offset
          percent-str
          (str "calc(" percent-str " - " offset ")")))}]))

(defn percent
  [x y]
  (double (* 100 (/ x y))))

(s/fdef gen-cells
  :args (s/cat :num-col ::num-col
               :prefix ::size-prefix
               :offset (s/? double?)
               :grid-type (s/? ::grid-type)))
(defn- gen-cells
  [num-col size-prefix & {:keys [offset grid-type]
                          :or {grid-type "x"}}]
  (let [sizes (range 1 (inc num-col))]
    (->> sizes (map #(gen-cell size-prefix % (percent % num-col) {:offset offset
                                                                  :grid-type grid-type})))))

(defn grid-xy-cells
  [num-col grid-type]
  `(~@(gen-cells num-col "sm" :grid-type grid-type)
    ~(at-media {:screen true
                :min-width medium-screen-size}
               [(vec (gen-cells num-col "md" :grid-type grid-type))])
    ~(at-media {:screen true
                :min-width large-screen-size}
               [(vec (gen-cells num-col "lg" :grid-type grid-type))])))

(s/fdef cell-attrs
  :args (s/cat :grid-type ::grid-type))
(defn cell-attrs
  "Default cell attributes for grid x and y."
  [grid-type]
  (cond-> {:flex "0 0 auto"
           :min-height "0px"
           :min-width "0px"}
          (= grid-type "x") (merge {:width "100%"})
          (= grid-type "y") (merge {:width "auto"})))

(def x-auto-cell {:flex "1 1 auto"
                  :width "auto"})

(def x-shrink-cell {:flex "0 0 auto"
                    :width "auto"})

(def y-auto-cell {:flex "1 1 0px"
                  :height "auto"})

(def y-shrink-cell {:height "auto"})

(defn grid-auto-cell
  [grid-type]
  (if (= grid-type "x")
    x-auto-cell
    y-auto-cell))

(defn grid-shrink-cell
  [grid-type]
  (if (= grid-type "y")
    x-shrink-cell
    y-shrink-cell))

(s/fdef base-xy-cell
  :args (s/cat :grid-type (s/? ::grid-type)))
(defn base-xy-cell
  "Grid auto and shrink cell utils."
  [& {:keys [grid-type]}]
  (let [auto-cell (grid-auto-cell grid-type)
        shrink-cell (grid-shrink-cell grid-type)]
    [[:.c (cell-attrs grid-type)
      [:&.c--auto auto-cell]
      [:&.c--shrink shrink-cell]]
     (at-media {:screen true
                :min-width medium-screen-size}
               [[:.c-md--auto auto-cell]
                [:.c-md--shrink shrink-cell]])
     (at-media {:screen true
                :min-width large-screen-size}
               [[:.c-lg--auto auto-cell]
                [:.c-lg--shrink shrink-cell]])]))

(s/fdef grid-xy
  :args (s/cat :num-col ::num-col
               :grid-type ::grid-type))
(defn grid-xy
  [num-col & {:keys [grid-type]}]
  (let [grid-class (if (= grid-type "y") :.g-y :.g-x)
        flex-flow-attr (if (= grid-type "y")
                         "column wrap"
                         "row wrap")
        grid-cell-utils (base-xy-cell :grid-type grid-type)]
    [grid-class
     {:display "flex"
      :flex-flow flex-flow-attr}
     `[:>
       ~grid-cell-utils
       ~@(grid-xy-cells num-col grid-type)]]))

(def factor 6.4)

(def n 4)

(defn small-x
  [num-col]
  (gen-cells num-col "sm"))

(defn medium-x
  [num-col]
  (at-media {:screen true
             :min-width medium-screen-size}
            [(vec (gen-cells num-col "md"))]))

(defn large-x
  [num-col]
  (at-media {:screen true
             :min-width large-screen-size}
            [(vec (gen-cells num-col "lg"))]))

(defmulti grid-gutter-attrs (fn [grid-type gutter-type _] [grid-type gutter-type]))
(defmethod grid-gutter-attrs ["x" "margin"]
  [_ _ {:keys [half-offset]}])

(s/fdef grid-xy-gutter
  :args (s/cat :num-col ::num-col
               :size-prefix ::size-prefix
               :v double?
               :gutter-type ::gutter-type))
(defn grid-xy-gutter
  "Generates grid x gutter"
  [num-col size-prefix v gutter-type]
  (let [half-offset (str v "rem")
        grid-class (if (= gutter-type "margin")
                     :.g-x--m
                     :.g-x--p)

        grid-attrs (if (= gutter-type "margin")
                     {:margin-left (str "-" half-offset)
                      :margin-right (str "-" half-offset)}
                     {:padding-left (str "-" half-offset)
                      :padding-right (str "-" half-offset)})

        cell-attrs (cond-> {:width (str "calc(100% - " (* 2 v) "rem)")}

                           (= gutter-type "margin")
                           (merge {:margin-left half-offset
                                   :margin-right half-offset})

                           (= gutter-type "padding")
                           (merge {:padding-left half-offset
                                   :padding-right half-offset}))]
    `[~grid-class ~grid-attrs
      [:>
       [:.c ~cell-attrs]
       ~@(gen-cells num-col size-prefix :offset (str (* 2 v) "rem"))]]))

(s/fdef grid-margin-x*
  :args (s/cat :num-col ::num-col
               :size-prefix ::size-prefix
               :typ (s/? #{"padding" "margin"})))
(defmulti grid-x-gutter* (fn [_ size-prefix & _] size-prefix))
(defmethod grid-x-gutter* "sm"
  [num-col size-prefix & {:keys [typ]}]
  (let [v (double (/ 4 factor))]
    (grid-xy-gutter num-col size-prefix v typ)))

(defmethod grid-x-gutter* "md"
  [num-col size-prefix & {:keys [typ]}]
  (let [v (double (/ 6 factor))]
    (at-media {:screen true
               :min-width medium-screen-size}
              [(grid-xy-gutter num-col size-prefix v typ)])))

(defmethod grid-x-gutter* "lg"
  [num-col size-prefix & {:keys [typ]}]
  (let [v (double (/ 6 factor))]
    (at-media {:screen true
               :min-width large-screen-size}
              [(grid-xy-gutter num-col size-prefix v typ)])))

(defn grid-x-gutter-all
  [num-col]
  [[]
   (grid-x-gutter* num-col "sm" :typ "margin")
   (grid-x-gutter* num-col "md" :typ "margin")
   (grid-x-gutter* num-col "lg" :typ "margin")

   (grid-x-gutter* num-col "sm" :typ "padding")
   (grid-x-gutter* num-col "md" :typ "padding")
   (grid-x-gutter* num-col "lg" :typ "padding")])

;; grid-y
(defn grid-container
  []
  [:.g--container
   {:max-width "75rem"
    :margin "0 auto"}])

(defn grid
  "Main grid that contains other grid styles"
  [& {:keys [num-col]
      :or {num-col 16}}]
  [(grid-container)
   (grid-xy num-col :grid-type "x")
   (grid-xy num-col :grid-type "y")
   (grid-x-gutter-all num-col)])

(defn demo
  []
  [[:.c
    [(garden.selectors/& (garden.selectors/nth-child :odd))
     {:background-color "#37a0e6"}]]
   [:.demo {:background-color "#37a0e6"}]
   [:.g-x--p [:> [:.c {:background "red"}]]]])

(defn main
  []
  [(grid)
   #_(demo)                                                   ;; demo only
])
