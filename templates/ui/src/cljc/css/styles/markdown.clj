(ns css.styles.markdown)

(def markdown
  [:.markdown {:line-height "2"}
   [:p {:margin-bottom "20px"}]
   [:h1 :h2 :h3 :h4 {:font-weight "400"
                     :font-size "16px"
                     :color "#3d464d"
                     :margin-bottom "10px"}]
   [:li {:padding-left "15px"}
    [:&:before {:color "#aaa"
                :content "•"
                :display "block"
                :position "relative"
                :max-width "0px"
                :max-height "0px"
                :left "-15px"
                :top "-6px"
                :font-size "20px"}]]])
