(ns css.styles.components.date)

(def g-input-datepicker
  [[:.goog-date-picker :.goog-date-picker " " :th :.goog-date-picker " " :td {:font "normal"}]
   [:.goog-date-picker
    {:-moz-user-focus "normal"
     :-moz-user-select "none"
     :background "white"
     :position "absolute"
     :border "1px solid #ccc"
     :border-radius "2px"
     :float "left"
     :padding "2px"
     :color "#000"
     :cursor "default"}]
   [:.goog-date-picker
    [:td {:text-align "center" :vertical-align "middle" :padding "1px 3px"}
     [:div {:float "left"}]]
    [:th {:text-align "center"
          :font-size "90%"}]
    [:button {:padding "0px" :margin "1px 0" :border "0"
              :cursor "pointer"
              :font-weight "bold"
              :background "transparent"}]]

   [:.goog-date-picker-menu
    {:position "absolute"
     :background "#efefef"
     :border "1px solid #ccc"
     :-moz-user-focus "normal"
     :z-index "1"
     :outline "none"}
    [:ul {:list-style "none" :margin "0px" :padding "0px"}
     [:li {:cursor "default"}]]]

   [:.goog-date-picker-menu-selected {:background "#bbb"}]

   [:.goog-date-picker-date {:background "#fff"}]
   [:.goog-date-picker-week
    :.goog-date-picker-wday
    {:padding "1px 3px" :border "0" :border-color "#eee" :border-style "solid"}]
   [:.goog-date-picker-week {:border-right-width "1px"}]
   [:.goog-date-picker-wday {:border-bottom-width "1px"}]
   [:.goog-date-picker-head " " :td {:text-align "center"}]
   [:td :.goog-date-picker-today-cont {:text-align "center"}]
   [:td :.goog-date-picker-none-cont {:text-align "center"}]
   [:.goog-date-picker-month {:min-width "11ex" :white-space "nowrap"}]
   [:.goog-date-picker-year {:min-width "6ex" :white-space "nowrap"}]
   [:.goog-date-picker-monthyear {:white-space "nowrap"}]
   [:.goog-date-picker " " :table {:border-collapse "collapse"}]
   [:.goog-date-picker-other-month {:color "#888"}]
   [:.goog-date-picker-wkend-start :.goog-date-picker-wkend-end {:background "#eee"}]
   [:td.goog-date-picker-selected {:background "#ccc"}]
   [:.goog-date-picker-today
    {:background "#9ab" :font-weight "bold" :border-color "#246 #9bd #9bd #246" :color "#fff"}]])

(def date
  [g-input-datepicker])
