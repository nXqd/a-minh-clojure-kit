(ns css.styles.components.modal)

(def body
  [:body.with-modal {:overflow "hidden"}])

(def black-cover
  [:div.black-cover
   {:position "absolute"
    :z-index 1
    :top 0
    :bottom 0
    :left 0
    :right 0
    :background "rgba(0,0,0,0.8)"}])

(def modal*
  [:.modal-container
   {:position "fixed"
    :top "0"
    :left "0"
    :width "100%"
    :height "100%"
    :white-space "nowrap"
    :overflow "auto"
    :outline "0"
    :z-index 2000
    :background "rgba(0,0,0,.93)"}

   [:.modal {:position "relative"
             :margin "5rem auto"}

    [:.modal-close
     {:position "absolute"
      :z-index 5
      :color "white"
      :left "100%"
      :font-size "2rem"}]

    [:.modal-inner {:height "100%"
                    :width "100%"
                    :overflow "hidden"
                    :position "absolute"
                    :top 0}]]])

(def modal [body
            black-cover
            modal*])
