(ns css.styles.components.tooltip)

(def simple
  [:.goog-tooltip
   {:background "rgba(0,0,0,.8)"
    :padding "4px 8px"
    :border-radius "2px"
    :font-size "0.73rem"
    :color "white"}])

(def tooltip
  [simple])

