(ns css.styles.components.editor)

(def trix
  [:.editor-component
   [:&.editor--no-toolbar [:trix-toolbar {:display "none"}]]
   [:trix-editor {:display "flex"
                  :padding "10px 20px"
                  :width "100%"}]
   [:trix-toolbar {:display "flex"
                   :width "100%"}]])

(def editor
  [trix])
