(ns css.styles.components.header
  (:require
   [css.styles.menu :as menu]))

(def account-menu
  [:.account-menu
   {:position "absolute"
    :top "100%"
    :left "0"
    :z-index "100"
    :width "160px"
    :margin-top "2px"
    :padding-top "5px"
    :padding-bottom "5px"}
   [menu/shadow]])

(def notification-menu-state-empty
  [:.state-empty
   {:width "100%"
    :height "100%"
    :display "flex"
    :align-items "center"
    :justify-content "center"}])

(def notification-menu
  [:.notification-menu
   {:position "absolute"
    :top "100%"
    :left "0"
    :z-index "100"
    :width "160px"
    :margin-top "2px"
    :padding-top "5px"
    :padding-bottom "5px"}
   [menu/shadow]
   [:>

    [:.header {:line-height 3}
     [:li:first-child
      {:justify-content "center"}]]

    [:.main {:height "210px"}
     [:>
      [notification-menu-state-empty]]]

    [:.previous
     {:line-height 2
      :text-align "center"}]]])

(def account
  [:div.header-account
   {:position "relative"}
   [account-menu]])

(def header-horizontal
  [:div.app-header.header.header--horizontal
   {:background-color "#3887be"}

   [:div.container.container--fluid
    {:padding "1rem 0"}
    [:div.container
     [:div.grid
      ;; logo
      [:div.grid-cell.grid-lg-4.grid-md-4.grid-sm-4
       {:display "inline-block"
        :vertical-align "top"
        :height "40px"
        :width "100px"
        :text-indent "-999em"
        :overflow "hidden"}]
      ;; list
      [:ul.list.list--horizontal
       {:style {:margin-left "auto"}}
       [:li.list-item [:a
                       {:color "white"
                        :padding "0.4rem"
                        :text-decoration "none"
                        :border-radius "3px"
                        :font-weight "bold"}
                       [:&:hover {:background "rgba(255,255,255,.05)"}]]]]]]]])

(def header [header-horizontal
             account
             notification-menu])
