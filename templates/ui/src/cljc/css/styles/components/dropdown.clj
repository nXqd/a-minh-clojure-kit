(ns css.styles.components.dropdown
  (:require
   [css.styles.form.utils :refer [state-active]]))

(def dropdown
  [:div.dropdown {:position "relative"
                  :cursor "pointer"
                  :word-wrap "break-word"
                  :line-height "1em"
                  :white-space "normal"
                  :outline "0"
                  :min-width "14em"
                  :min-height "2.3rem"
                  :background "#fff"
                  :display "inline-block"
                  :color "rgba(0,0,0,.87)"
                  :box-shadow "none"
                  :border "1px solid rgba(34,36,38,.15)"
                  :border-radius "3px"
                  :transition "box-shadow .1s ease,width .1s ease"}
   [:&:hover
    [:> [:i {:color "#000"}]]]
   [:&.active
    [state-active]
    [:>
     [:input {:cursor "auto"}]
     [:.menu
      [:> [:.list-item:hover {:background "#eee"}]]]]]

   [:>
    [:i {:cursor "pointer"
         :position "absolute"
         :top "0.5rem"
         :right "0.5rem"
         :width "auto"
         :opacity ".8"
         :transition "opacity .1s ease"}]
    [:input
     {:background "none!important"
      :border "none!important"
      :box-shadow "none!important"
      :cursor "pointer"
      :top "0"
      :left "0"
      :width "100%"
      :outline "0"
      :-webkit-tap-highlight-color "rgba(255,255,255,0)"
      :padding "0.5rem"
      :position "absolute"
      :z-index 1}]
    [:div.text {:cursor "text"
                :position "relative"
                :z-index 3
                :color "rgba(179,179,179,.7)"}]
    [:.menu {:margin "0 -1px"
             :margin-top "2rem"
             :overflow-x "hidden"
             :overflow-y "auto"
             :-webkit-backface-visibility "hidden"
             :backface-visibility "hidden"
             :-webkit-overflow-scrolling "touch"
             :width "100%"
             :background "white"
             :z-index 2
             :position "absolute"
             :border "1px solid rgba(34,36,38,.15)"
             :border-top 0
             :border-radius "0 0 4px 4px"
             :box-shadow "0 2px 3px 0 rgba(34,36,38,.15)"
             :box-sizing "content-box"
             :max-height "10rem"}
     [:.list-item {:line-height 2
                   :padding "0 0.7rem"}
      [:.title {:font-weight "normal"}]]]]])
