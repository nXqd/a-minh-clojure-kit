(ns css.styles.components.menus)

(def menu-left
  [:.menu.menu--left
   {:margin-bottom "15px"
    :list-style "none"
    :background-color "#fff"
    :border "1px solid #d8d8d8"
    :border-radius "3px"}
   [:>
    [:.list-item
     {:position "relative"
      :display "block"
      :padding "8px 10px"
      :text-shadow "0 1px 0 #fff"
      :border-bottom "1px solid #eee"}
     [:&.menu-header
      {:display "block"
       :padding "8px 10px"
       :margin-top "0"
       :margin-bottom "0"
       :font-size "13px"
       :font-weight "bold"
       :line-height "20px"
       :color "#555"
       :background-color "#f7f7f7"
       :border-bottom "1px solid #eee"}]
     [:&.menu--active
      {:font-weight "bold"
       :color "#222"
       :cursor "default"
       :background-color "#fff"}
      [:&:before
       {:position "absolute"
        :top "0"
        :left "0"
        :bottom "0"
        :width "2px"
        :content "''"
        :background-color "#d26911"}]]]]])

(def menus [menu-left])
