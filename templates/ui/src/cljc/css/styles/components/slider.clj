(ns css.styles.components.slider)

(def two-thumb-slider
  [[:.goog-twothumbslider-vertical
    :.goog-twothumbslider-horizontal {:background-color "white"
                                      :position "relative"
                                      :overflow "hidden"}]

   [:.goog-twothumbslider-value-thumb
    :.goog-twothumbslider-extent-thumb {:position "absolute"
                                        :overflow "hidden"
                                        :width "1rem"
                                        :height "1rem"
                                        :border-radius "50%"
                                        :background "white"
                                        :border "2px solid rebeccapurple"}]])

(def slider
  [two-thumb-slider])
