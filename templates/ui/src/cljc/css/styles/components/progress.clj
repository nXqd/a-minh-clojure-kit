(ns css.styles.components.progress
  (:require
   [garden.def :refer [defkeyframes]]))

(defkeyframes animation-progress-active
  ["0%" {:opacity 0.3
         :width 0}]
  ["100%" {:opacity 0
           :width "100%"}])

(def progress
  [[animation-progress-active]
   [:div.progress
    [:>
     [:div.inner
      [:&.inner--animated
       [:&:after
        ^:prefix {:animation [[animation-progress-active "2s"
                               :linear :infinite]]}
        {:content "''"
         :opacity 0
         :position "absolute"
         :top 0
         :left 0
         :right 0
         :bottom 0
         :background "#FFF"
         :border-radius "0.2rem"}]]]]]])
