(ns css.styles.components.step)

;; TODO : fix after not rgba

(def after-arrow
  [:&:after
   {:display "block"
    :position "absolute"
    :z-index "2"
    :content "''"
    :top "50%"
    :right "0"
    :border-style "solid"
    :border-top-color "rgba(34, 36, 38, 0.14902)"
    :border-top-style "solid"
    :border-top-width "0px"
    :border-right-width "1px"
    :border-bottom-width "1px"
    :border-left-width "0px"
    :border-color "rgba(34,36,38,.15)"
    :background-color "#fff"
    :width "1.14285714em"
    :height "1.14285714em"
    :border-width "0 1px 1px 0"
    :transition "background-color .1s ease,opacity .1s ease,color .1s ease,box-shadow .1s ease"
    :transform "translateY(-50%) translateX(50%) rotate(-45deg)"}])

(def step
  [:div.step {:cursor "pointer"
              :position "relative"
              :display "flex"
              :flex "1 0 auto"
              :flex-wrap "wrap"
              :flex-direction "row"
              :vertical-align "middle"
              :align-items "center"
              :justify-content "center"
              :margin "0"
              :padding "1.14285714em 2em"
              :background "#fff"
              :color "rgba(0,0,0,.87)"
              :box-shadow "none"
              :border-radius "0"
              :border "none"
              :border-right "1px solid rgba(34,36,38,.15)"
              :transition "background-color .1s ease,opacity .1s ease,color .1s ease,box-shadow .1s ease"}
   [after-arrow]

   [:&:first-child {:border-radius "3px 0 0 3px"}]

   ;; remove arrow on the last child
   [:&:last-child {:border "none"}
    [:&:after {:display "none"}]]

   ;; state-active
   [:&.active
    [:div.content
     [:div.title {:color "#4183c4"}]]]

   ;; state-disabled
   [:&.disabled {:color "#aaa"}
    [:i {:color "#aaa"}]]
   [:i {:display "block"
        :flex "0 1 auto"
        :font-size "2rem"
        :margin-right "1rem"
        :color "black"}]
   [:div.content {:flex "0 1 auto"}
    [:div.title {:font-weight 700
                 :font-size "1rem"}]
    [:div.description {:font-size "0.8rem"}]]])

(def steps
  [:div.steps {:display "inline-flex"
               :flex-direction "row"
               :align-items "stretch"
               :margin "1em 0"
               :background "0 0"
               :box-shadow "none"
               :line-height "1.14285714em"
               :border-radius ".28571429rem"
               :border "1px solid rgba(34,36,38,.15)"}
   [step]])
