(ns css.styles.components.paging)

(def paging
  [:.paging
   [:.button
    {:padding 0
     :font-weight "normal"
     :width "2.2rem"
     :height "2.2rem"
     :justify-content "center"
     :align-items "center"}
    [:&.active {:font-weight "bold"}]
    [:i {:margin-right 0}]]])
