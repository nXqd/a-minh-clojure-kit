(ns css.styles.components.combobox)

(def goog-combobox
  [:.goog-combobox
   {:display "block"
    :position "relative"
    :width "100%"}
   [:.goog-combobox-button
    {:display "none"}]
   [:.goog-menuitem
    {:padding "6px 8px"
     :cursor "pointer"}]
   [:.goog-menuitem-highlight
    {:background "#eee"}]
   [:.goog-menu
    {:background "#fff"
     :border "1px solid #ccc"
     :border-top "none"
     :outline "none"
     :padding "4px 0"
     :width "100%"
     :position "absolute"
     :z-index 20000}]])

(def combobox
  [goog-combobox])
