(ns css.styles.components.chat)

(def chat
  [:.chat
   [:.left {:margin-right "0.5rem"}]
   [:div.right
    [:>
     [:h4.name
      {:font-size "0.8rem"
       :line-height 1.5}]
     [:.misc
      {:margin-top "0.5rem"
       :font-size "0.7rem"}]]]])

(def chats*
  [:.chats
   [chat]])

(def chats [chats*])
