(ns css.styles.components.min-max)

(def min-max
  [:.min-max {:position "relative"}
   [:.label {:background "white"
             :cursor "pointer"
             :line-height 2}]
   [:.inner
    {:position "absolute"
     :top "3rem"
     :left 0
     :z-index 1}
    [:.top
     {:background "#eee"}
     [:li.list-item
      {:padding "1rem"}]]
    [:.values
     {:background "white"}
     [:li.list-item
      {:padding "0.5rem 1rem"
       :justify-content "flex-start"
       :cursor "pointer"}
      [:&:hover {:background "#eee"}]]
     [:&.max
      [:.list-item {:justify-content "flex-end"}]]]]])

(def min-max-static
  [:.min-max-static
   [:.maxs
    [:.list-item {:justify-content "flex-end"}]]
   [:.list-item.value {:cursor "pointer"
                       :color "#555"
                       :line-height 2.2}
    (let [active-style {:color "black"
                        :font-weight "bold"}]
      [[:&.active active-style]
       [:&:hover active-style]])]])

(def min-maxes
  [min-max
   min-max-static])
