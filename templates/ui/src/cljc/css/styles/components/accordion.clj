(ns css.styles.components.accordion)

(def accordion*
  [:div.accordion
   {:border-bottom "1px solid #eee"}
   [:>
    [:div.title
     {:cursor "pointer"
      :line-height 3}
     [:i {:padding "0 1rem"}]
     [:&:hover {:background "#f4f4f4"}
      [:i {:color "black"}]]]
    [:div.content
     {:padding "1rem"
      :max-height "600px"
      :overflow "hidden"}]]])

(def accordions
  [:div.accordions
   {:border-radius "3px"
    :border "1px solid #eee"}])

(def accordion
  [accordion*
   accordions])
