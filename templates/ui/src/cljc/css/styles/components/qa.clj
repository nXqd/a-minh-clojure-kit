(ns css.styles.components.qa)

(def qa
  [:.qa-component
   [:.qas
    [:.qa-item
     {:margin-bottom "0.7rem"}
     [:&:last-child {:margin-bottom 0}]]]])
