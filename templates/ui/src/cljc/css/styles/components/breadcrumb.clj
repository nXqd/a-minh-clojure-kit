(ns css.styles.components.breadcrumb)

(def breadcrumb
  [:ul.breadcrumb
   {:justify-content "initial"}
   [:li.list-item
    {:flex "initial"}
    [:&:last-child [:&:after {:content "''"}]]
    [:&:after {:padding "0 1rem"
               :display "flex"
               :content "'\\f054'"
               :text-rendering "auto"
               :font "normal normal normal 14px/1 FontAwesome"}]]])
