(ns css.styles.components.table)

(def table*
  [:table
   {:border-collapse "collapse"
    :width "100%"
    :max-width "100%"}
   [:thead
    [:.arrows {:display "inline-flex"
               :padding-left "0.5rem"}]
    [:th
     [:i.fa {:visibility "hidden"
             :transition "initial"}]
     [:&.goog-tablesorter-sorted
      [:.fa-arrow-up {:visibility "inherit"}]]
     [:&.goog-tablesorter-sorted-reverse
      [:.fa-arrow-down {:visibility "inherit"}]]]]
   [:tbody
    [:tr
     [:td [:div
           {:display "flex"
            :flex-direction "column"
            :justify-content "center"
            :height "5.5rem"}]]]]
   [:&.sorted [:thead [:th {:cursor "pointer"}]]]])

(def table-react
  [:.table-react
   [:.fixedDataTableCellGroupLayout_cellGroup
    [:>.public_fixedDataTableCell_main
     {:background-image "none"}]]
   [:.public_fixedDataTableCell_cellContent
    {:padding 0}]
   [:.fixedDataTableCellLayout_wrap1
    {:width "100%"
     :height "100%"}]
   [:.public_fixedDataTable_header
    :.public_fixedDataTableCell_main
    {:background-image "none"}]])

(def table-normal
  [:.table.list
   {:border "1px solid rgba(34,36,38,.1)"}
   [:>
    [:.headers {:cursor "auto"
                :background "#F9FAFB"
                :text-align "inherit"
                :color "rgba(0,0,0,.87)"
                :padding ".9rem .7rem"
                :vertical-align "inherit"
                :font-style "none"
                :fo#nt-weight "700"
                :text-transform "none"
                :border-bottom "1px solid #eee"
                :border-left "none"}]
    [:.filters
     [:> [:.list-item {:padding "0.5rem"
                       :border-bottom "1px solid #eee"}]]]

    [:.data {:border-bottom "1px solid rgba(34,36,38,.1)"}
     [:.list-item {:padding "0.7rem 0"}]
     [:&:last-child {:border-bottom "none"}]]]])

(def table [table*
            table-react
            table-normal])
