(ns css.styles.components.image-sliders)

;; make sure this match with the one in image-slider
(def ^:dynamic *margin* 20)

(def image-slider
  [:.image-slider
   {:background "rgba(0,0,0,0.9)"}
   [:>
    [:.inner
     {:position "relative"
      :height "100%"}
     [:.nav-next
      :.nav-prev {:position "absolute"
                  :width "15%"
                  :display "flex"
                  :height "100%"
                  :z-index 2
                  :cursor "pointer"
                  :align-items "center"
                  :justify-content "center"}
      [:i {:color "white"
           :opacity 0.5
           :font-size "1.5rem"}]
      [:&:hover [:i {:opacity 1}]]
      [:&.disabled:hover [:i {:opacity 0.5
                              :cursor "default"}]]]
     [:.nav-next {:right 0}]]]
   [:.images
    {:transition "transform 0.3s ease-in-out"}
    [:.image
     ;; make sure this margin value match the one in cljs file
     {:margin (str "0 " *margin* "px")
      :flex-direction "column"}
     [:>
      [:img {:opacity 0.5
             :height "400px"
             :width "auto"
             :margin 0
             :filter "brightness(40%)"
             :transition "all 0.2s ease-in"}]
      [:.label {:height "1.8rem"
                :color "white"
                :font-weight "bold"
                :display "none"}]]
     [:&.active
      [:>
       [:img {:filter "initial"
              :opacity 1}]
       [:.label {:align-items "center"
                 :display "inline-flex"}]]]]]])

(def image-sliders [image-slider])
