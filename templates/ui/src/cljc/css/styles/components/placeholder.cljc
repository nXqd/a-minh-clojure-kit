(ns css.styles.components.placeholder
  #?(:clj
     (:require
      [garden.def :refer [defkeyframes]])))

#?(:clj
   (defkeyframes animation-progress-active
     ["0%" {:opacity 0.3
            :width 0}]
     ["100%" {:opacity 0
              :width "100%"}]))

(def animated)

(def placeholder
  [animated])
