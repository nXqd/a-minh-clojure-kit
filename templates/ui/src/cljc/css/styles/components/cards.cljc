(ns css.styles.components.cards)

(def card-attrs
  {:background "#fefefe"
   :border "1px solid #e6e6e6"
   :border-radius 0
   :display "flex"
   :flex-direction "column"
   :flex-grow "1"
   :margin-bottom "1rem"
   :overflow "hidden"
   :color "#0a0a0a"})

(def card
  [:.card card-attrs
   [:&:last-child
    {:margin-bottom 0}]
   [:.card__divider
    {:flex "0 1 auto"
     :display "flex"
     :padding "1rem"
     :background "#e6e6e6"}
    [:&:last-child
     {:margin-bottom 0}]]
   [:.card__section
    :.card__inner
    {:flex "1 0 auto"
     :padding "1rem"}
    [:&:last-child
     {:margin-bottom 0}]]
   [:.card__image
    {:min-height "1px"}]])

(def main [card])
