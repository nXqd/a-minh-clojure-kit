(ns css.styles.google.map)

(def remove-google-logo-on-autocomplete-place
  [:.pac-container:after
   {:background-image "none !important"
    :height "0px"}])

(def custom-marker-style
  [:.gmap-custom-marker
   {:border-style "solid",
    :line-height "1rem",
    :min-width "38px",
    :color "#fff",
    :text-align "center",
    :font-size "13px",
    :transition ".3s all cubic-bezier(.39,.575,.565,1)",
    :width "auto",
    :cursor "pointer",
    :border-width "1px",
    :z-index "1000",
    :padding "2px 6px",
    :position "absolute",
    :border-radius "1000px",
    :visibility "visible",
    :height "21px"}
   [:&.primary-marker
    {:color "#fff", :background-color "#20c063", :border-color "#228959"}]
   [:&:before
    {:background-color "inherit"
     :border-color "inherit"
     :display "inline-block"
     :margin 0
     :content "''"
     :width "4px"
     ::height "4px"
     :position "absolute"
     :top "18px"
     :left "calc(50% - 2px)"
     :border-width "0 1px 1px 0"
     :border-style "solid"
     :transform "rotate(45deg)"}]])

(def gmap
  [remove-google-logo-on-autocomplete-place
   custom-marker-style])
