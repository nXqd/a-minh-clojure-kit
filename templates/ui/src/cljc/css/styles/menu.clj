(ns css.styles.menu)

(def shadow
  [:& {:background-color "#fff !important"
       :background-clip "padding-box !important"
       :border "1px solid rgba(0,0,0,0.15)"
       :box-shadow "0 3px 12px rgba(0,0,0,0.15) !important"}])

(def menu
  [:.menu {:position "absolute"
           :top "100%"
           :left "0"
           :z-index "100"
           :margin-top "2px"
           :padding-top "5px"
           :padding-bottom "5px"}
   [shadow]])
