(ns css.styles.icons)

;; Prioritize using svg icon

(def circle
  [:.icon.icon--circle
   {:width "10px"
    :height "10px"
    :border-radius "50%"
    :background "black"
    :display "inline-block"}])

(def svg-icon
  [:.svg-icon
   {:width "20px"
    :height "20px"
    :display "inline-flex"}])

(defn up
  [& {:keys [style classes]
      :or {style {}
           classes {:svg []}}}]
  [:svg {:style style
         :class (conj (:svg classes) "svg-icon")
         :viewBox "0 0 20 20"}
   [:path {:d "M13.889,11.611c-0.17,0.17-0.443,0.17-0.612,0l-3.189-3.187l-3.363,3.36c-0.171,0.171-0.441,0.171-0.612,0c-0.172-0.169-0.172-0.443,0-0.611l3.667-3.669c0.17-0.17,0.445-0.172,0.614,0l3.496,3.493C14.058,11.167,14.061,11.443,13.889,11.611 M18.25,10c0,4.558-3.693,8.25-8.25,8.25c-4.557,0-8.25-3.692-8.25-8.25c0-4.557,3.693-8.25,8.25-8.25C14.557,1.75,18.25,5.443,18.25,10 M17.383,10c0-4.07-3.312-7.382-7.383-7.382S2.618,5.93,2.618,10S5.93,17.381,10,17.381S17.383,14.07,17.383,10"}]])

(defn search
  [& {:keys [styles classes]
      :or {styles {:svg {} :path {}}
           classes {:svg []}}}]
  [:svg {:style (:svg styles)
         :class "svg-icon svg-icon--search"
         :viewBox "0 0 20 20"}
   [:path {:d "M9.383 10.347c-.987.78-2.233 1.244-3.588 1.244C2.595 11.59 0 8.997 0 5.796 0 2.595 2.595 0 5.795 0c3.2 0 5.796 2.595 5.796 5.795 0 1.355-.464 2.6-1.243 3.588L15 14.036l-.964.964-4.653-4.653zm-3.588-.12c2.448 0 4.432-1.984 4.432-4.432 0-2.447-1.984-4.43-4.432-4.43-2.447 0-4.43 1.983-4.43 4.43 0 2.448 1.983 4.432 4.43 4.432z",
           :fill "#BBB", :fill-rule "evenodd"}]])

(defn three-dots
  [& {:keys [styles classes]
      :or {styles {:svg {} :path {}}
           classes {:svg []}}}]
  [:svg {:style (:svg styles)
         :class "svg-icon svg-icon--search"
         :viewBox "0 0 20 20"}
   [:path {:d "M2 4c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zm8 0c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2zm8 0c1.105 0 2-.895 2-2s-.895-2-2-2-2 .895-2 2 .895 2 2 2z"
           :fill "#BBB", :fill-rule "evenodd"}]])

(def icons
  [svg-icon
   circle])

