(ns css.styles.utils.core
  (:require
   [css.styles.colors :as colors]
   [clojure.string :as cstr]))

(def clearfix-mixin
  [[:&:before {:display "table"
               :content "''"}]
   [:&:after {:display "table"
              :content "''"
              :clear "both"}]])

(def clearfix
  [:.clearfix clearfix-mixin])

(def invisible-mixin
  {:visibility "hidden"})

(def invisible
  [:.invisible invisible-mixin])

(def hidden
  [:.hidden {:display "none !important"}])

(def stick
  [:.stick {:position "fixed"
            :top 0
            :left 0
            :right 0
            :z-index 1000
            :background "white"}])

;; display
(def display
  [[:.d--block {:display "block"}]
   [:.d--flex {:display "flex"}]
   [:.d--inline-flex {:display "inline-flex"}]
   [:.d--inline {:display "inline"}]
   [:.d--inline-block {:display "inline-block"}]])

(def position
  [[:.position-relative {:position "relative !important"}]
   [:.position-absolute {:position "absolute!important"}]])

(def flex
  [[:.flex-row {:flex-direction "row !important"}]
   [:.flex-row--reverse {:flex-direction "row-reverse !important"}]
   [:.flex-column {:flex-direction "column !important"}]
   [:.flex-column-reverse {:flex-direction "column-reverse !important"}]
   [:.justify-content--center {:justify-content "center !important"}]
   [:.align-items--center {:align-items "center !important"}]
   [:.align-items-center {:align-items "center !important"}]
   [:.align-items--end {:align-items "flex-end !important"}]
   [:.align-items-end {:align-items "flex-end !important"}]
   [:.align-items--start {:align-items "flex-start !important"}]
   [:.align-items-start {:align-items "flex-start !important"}]])

;; spacing
#?(:clj
   (defn- gen-space-key
     "Generate space keyword."
     [p side]
     (let [p-val (condp = p
                   "m" "margin"
                   "p" "padding")
           s-val (condp = side
                   "t" "top"
                   "r" "right"
                   "b" "bottom"
                   "l" "left"
                   nil)]
       (-> (if s-val
             (str p-val "-" s-val)
             p-val)
           keyword))))

#?(:clj
   (defn- gen-css
     "Generate css
      p is property, can be padding (p) or margin (m)
      s is side, can be top (t), right (r), b (bottom), l (left), x (right,left) or y (top,bottom)
      sz is size in rem"
     [p s sz]
     (let [class (-> (str "." p s "--" (cstr/replace sz #"\." "-"))
                     keyword)
           rem-val (str (* 1 sz) "rem")
           val (condp = s
                 "x" (str "0 " rem-val)
                 "y" (str rem-val " 0")
                 rem-val)
           val (str val " !important")
           css-space-key (gen-space-key p s)]
       {class {css-space-key val}})))

#?(:clj
   (defn- gen-spacings
     []
     (let [properties ["m" "p"]
           ;; empty "" is for the case p--1
           sides ["t" "r" "b" "l" "x" "y" ""]
           sizes (range 0 4.25 0.25)]
       (->> properties
            (map
             (fn [p]
               (->> sides
                    (map (fn [s]
                           (->> sizes
                                (map (fn [sz]
                                       (gen-css p s sz)))))))))

            flatten
            (map seq)
            (map first)
            (into [])))))

#?(:clj
   (def spacings (gen-spacings)))

;; Borders
(def borders
  [[:.border--rounded {:border-radius "2px"}]
   [:.border-tl-none {:border-top-left-radius "0 !important"}]
   [:.border-tr-none {:border-top-right-radius "0 !important"}]
   [:.border-br-none {:border-bottom-right-radius "0 !important"}]
   [:.border-bl-none {:border-bottom-left-radius "0 !important"}]])

(def border-style
  (str "1px solid " colors/border-color))

#?(:clj
   (def utils
     [borders
      clearfix
      spacings
      display
      position
      flex
      hidden
      invisible
      stick]))

;; TODO: add border utils

;; TODO: add vh utils

