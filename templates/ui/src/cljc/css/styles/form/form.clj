(ns css.styles.form.form
  (:require
   [css.styles.loader :refer [loader-animation]]))

(def form-input-error
  {:border "1px solid red"})

(def form-with-inputs
  [:.form
   [:.input-ui
    {:padding "1rem 0"}
    [:label {:align-items "center"}
     [:.label-text {:padding-bottom "0.5rem"}]]

    ;; input
    [:input
     [:&.error form-input-error]]

    ;; calendar
    [:.calendar
     [:&.error
      [:.datepicker__input-container :input form-input-error]]]

    ;; checkbox
    [:.checkbox {:width "initial"}]
    [:.label-text.checkbox {:padding-bottom 0}]

    ;; select
    [:.select
     [:&.error
      [:.Select-control form-input-error]]
     [:.Select {:width "100%"}]]]])

(def form-loading
  [:.form.form--loading
   {:position "relative"
    :cursor "default"
    :point-events "none"
    :text-shadow "none!important"
    :color "transparent!important"
    :-webkit-transition "all 0s linear"
    :transition "all 0s linear"
    :z-index "100"}
   [:&:before {:position "absolute"
               :content "''"
               :top "0"
               :left "0"
               :background "rgba(255,255,255,.6)"
               :width "100%"
               :height "100%"
               :z-index "100"}]
   [:&:after
    ^:prefix {:animation [[loader-animation "0.6s" :linear :infinite]]}
    {:position "absolute"
     :content "''"
     :top "50%"
     :left "50%"
     :margin "-1.5em 0 0 -1.5em"
     :width "3em"
     :height "3em"
     :border-radius "500rem"
     :border-color "#767676 rgba(0,0,0,.1) rgba(0,0,0,.1)"
     :border-style "solid"
     :border-width ".2em"
     :box-shadow "0 0 0 1px transparent"
     :visibility "visible"
     :z-index "101"}]])

(def form [form-with-inputs
           form-loading])
