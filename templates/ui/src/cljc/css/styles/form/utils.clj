(ns css.styles.form.utils)

(def state-active
  {:outline "none"
   :border-color "#85b7d9"
   :background "#fff"
   :color "rgba (0,0,0,.8)"
   :box-shadow "none"})

(def focus
  [:&:focus state-active])

;; header being used in
;; textarea, checkbox, radio
(def input-header
  [:.header {:font-weight "bold"
             :line-height 2}])
