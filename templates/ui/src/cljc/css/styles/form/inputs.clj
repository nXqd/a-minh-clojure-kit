(ns css.styles.form.inputs
  (:require
   [css.styles.form.utils :refer [focus]]
   [garden.selectors :as s]))

;; slider
;; label: 5 10 15 20 25 30
;; 100/5 > 20 > 19.3%
;; last should be 0%
(def slider
  [:div.slider
   [:>
    [:.labels
     [:.list-item:last-child {:width "auto !important"}]]]])

;; switch
(def switch
  [:div.switch {:position "relative"}
   [:>
    [:label {:width "55px"
             :height "23px"
             :position "absolute"
             :background-color "#999"
             :top 0
             :left 0
             :border-radius "50px"}
     [:&:after {:content "''"
                :width "21px"
                :height "21px"
                :border-radius "50%"
                :position "absolute"
                :top "1px"
                :left "1px"
                :transition "all 0.1s"
                :background-color "white"}]]
    [:input {:visibility "hidden"}
     [:&:checked [:+
                  [:label
                   {:background-color "green"}
                   [:&:after {:left "33px"}]]]]]]])

(defn type= [v]
  (s/attr :type := v))

(def input-types #{"email" "number" "search" "text" "tel" "url" "password"})

(def inputs*
  (for [i input-types]
    `(s/input (type= ~i))))

(def input-disabled
  [:&:disabled {:pointer-events "none"
                :opacity 0.5}])

(def input-header
  [:.header {:font-size "0.85rem"
             :font-weight "bold"
             :line-height 3}])

(def inputs
  [(s/input (type= "email"))
   (s/input (type= "number"))
   (s/input (type= "search"))
   (s/input (type= "tel"))
   (s/input (type= "text"))
   (s/input (type= "url"))
   (s/input (type= "password"))
   :select {:background-color "#fff"
            :border-color "#b2b3b3"
            :border-radius "2px"
            :border-style "solid"
            :border-width "1px"
            :box-shadow "inset 0 1px 2px rgba(137, 140, 144, 0.2), 0 0 0 white"
            :box-sizing "border-box"
            :color "#40484f"
            :display "inline-block"
            :font-weight "normal"
            :line-height 1.5
            :margin "0"
            :padding ".4em .6em"
            :transition "border-color .15s ease-in-out,box-shadow .15s ease-in-out,background-color .15s ease-in-out"
            :vertical-align "middle"
            :width "100%"}
   [focus]
   [input-disabled]])

(def input-validate
  [:.input-validate {:position "relative"
                     :display "flex"
                     :align-items "center"}
   [:.tip {:position "absolute"
           :top "50%"
           :transform "translate(-50%, -50%)"}]])
