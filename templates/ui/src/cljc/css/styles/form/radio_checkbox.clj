(ns css.styles.form.radio-checkbox
  (:require
   [css.styles.form.utils :refer [input-header]]))

(def radio-checkbox-share
  [:div.field--radio
   :div.field--checkbox
   [:>
    [:label {:display "flex"
             :align-items "center"
             :position "relative"
             :cursor "pointer"
             :line-height 2}
     [:div.title {:padding-left "0.5rem"}]]]])

(def checkbox
  [:div.field--checkbox
   [:>
    [:label
     [>
      [:.character
       {:line-height 1}
       [:&:before {:display "inline-flex"
                   :width "17px"
                   :height "17px"
                   :content "''"
                   :background "#fff"
                   :border-radius "0.2rem"
                   :-webkit-transition "border .1s ease,opacity .1s ease,-webkit-transform .1s ease,box-shadow .1s ease"
                   :transition "border .1s ease,opacity .1s ease,transform .1s ease,box-shadow .1s ease"
                   :border "1px solid #d4d4d5"}]

       [:&:after
        {:border "none"
         :content "''!important"
         :position "absolute"
         :left "0"
         :width "17px"
         :height "17px"
         :border-radius "0.2rem"
         :transform "scale(.6)"
         :background-color "rgba(0,0,0,.87)"
         :text-align "center"
         :opacity "0"
         :color "rgba(0,0,0,.87)"
         :transition "border .1s ease,opacity .1s ease,transform .1s ease,box-shadow .1s ease"
         :font-family "FontAwesome"}]] [:&.active
                                        [:.character

                                         [:&:before {:border "2px solid #333"}]
                                         [:&:after {:content "'\\f00c'"
                                                    :opacity 1}]]]

      [:input
       {:cursor "pointer"
        :position "absolute"
        :top "0"
        :left "0"
        :opacity "0!important"
        :outline "0"
        :z-index "3"
        :width "17px"
        :height "17px"}]]]]])

(def radio
  [:div.field--radio
   [:>
    [:label
     [>
      [:.character
       {:line-height 1}
       [:&:before {:display "inline-flex"
                   :width "17px"
                   :height "17px"
                   :content "''"
                   :background "#fff"
                   :border-radius "50%"
                   :-webkit-transition "border .1s ease,opacity .1s ease,-webkit-transform .1s ease,box-shadow .1s ease"
                   :transition "border .1s ease,opacity .1s ease,transform .1s ease,box-shadow .1s ease"
                   :border "1px solid #d4d4d5"}]

       [:&:after
        {:border "none"
         :content "''!important"
         :position "absolute"
         :left "0"
         :width "17px"
         :height "17px"
         :border-radius "50%"
         :transform "scale(.6)"
         :background-color "rgba(0,0,0,.87)"
         :text-align "center"
         :opacity "0"
         :color "rgba(0,0,0,.87)"
         :transition "border .1s ease,opacity .1s ease,transform .1s ease,box-shadow .1s ease"
         :font-family "Checkbox"}]] [:&.active
                                     [:.character

                                      [:&:before {:border "2px solid #333"}]
                                      [:&:after {:content "'\\e800'"
                                                 :opacity 1}]]]

      [:input
       {:cursor "pointer"
        :position "absolute"
        :top "0"
        :left "0"
        :opacity "0!important"
        :outline "0"
        :z-index "3"
        :width "17px"
        :height "17px"}]]]]])

(def radio-group
  [:div.radio-group
   [radio-checkbox-share]
   [radio]
   [:>
    [input-header]]])

(def checkbox-group
  [:div.checkbox-group
   [radio-checkbox-share]
   [checkbox]
   [:>
    [input-header]]])
