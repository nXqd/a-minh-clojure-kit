(ns css.styles.form.textarea
  (:require
   [css.styles.form.utils :refer [focus
                                  input-header]]))

(def textarea
  [:textarea {:width "100%"
              :margin "0"
              :-webkit-appearance "none"
              :padding "0.6rem !important"
              :background "#fff"
              :border-color "#b2b3b3"
              :border-radius "2px"
              :border-style "solid"
              :border-width "1px"
              :box-shadow "inset 0 1px 2px rgba(137, 140, 144, 0.2), 0 0 0 white"
              :box-sizing "border-box"
              :outline "0"
              :color "rgba (0,0,0,.87)"
              :transition "border-color .15s ease-in-out,box-shadow .15s ease-in-out,background-color .15s ease-in-out"
              :resize "vertical"
              :min-height "8rem"
              :max-height "24rem"} [focus]])
