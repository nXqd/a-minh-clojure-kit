(ns css.styles.button
  (:require
   [css.styles.label :refer [counter]]))

;; button (selected
;; state : hover, active, focus
(def button-base
  [:.button {:-webkit-appearance "none"
             :text-decoration "none"
             :background-color "#eee"
             :background-image "linear-gradient(#fcfcfc, #eee)"
             :border "1px solid #d5d5d5"
             :border-radius "3px"
             :color "#333"
             :cursor "pointer"
             :display "inline-flex"
             :align-items "center"
             :font-weight "bold"
             :line-height 1.8
             :padding "6px 12px"
             :position "relative"
             :user-select "none"
             :vertical-align "middle"
             :white-space "nowrap"}
   [:&:hover :&:active {:text-decoration "none"
                        :background-color "#ddd"
                        :background-image "linear-gradient(#eee, #ddd)"
                        :border-color "#ccc"}]
   [:&:active :&.selected {:background-color "#dcdcdc"
                           :background-image "none"
                           :border-color "#b5b5b5"
                           :box-shadow "inset 0 2px 4px rgba(0, 0, 0, 0.15)"}]
   [:&:focus {:text-decoration "none"
              :border-color "#51a7e8"
              :outline "none"
              :box-shadow "0 0 5px rgba(81, 167, 232, 0.5)"}]
   [:&:focus:hover :&.selected:focus {:border-color "#51a7e8"}] [:&.selected:hover {:background-color "#dcdcdc"}]
   [:&:disabled :&.disabled
    [:& :&:hover
     {:color "rgba(102, 102, 102, 0.5)"
      :cursor "default"
      :background-color "rgba(229, 229, 229, 0.5)"
      :background-image "none"
      :border-color "rgba(197, 197, 197, 0.5)"
      :box-shadow "none"}]]

   [:&.secondary {:border "1px solid #1fc055"
                  :background "#1fc055"
                  :color "#fff"}
    [:&:hover :&:active {:background "#23cc5c"}]]

   ;; icon
   [:i {:font-size "1rem"
        :color "inherit"
        :margin-right "1rem"}]])

(def button-block
  [:.button.button--block {:display "block"
                           :width "100%"
                           :text-align "center"}])

(def button-counter
  [:.button [counter]])

(def button-primary
  [:.button.button--primary
   {:color "#fff"
    :text-shadow "0 -1px 0 rgba(0,0,0,0.15)"
    :background-color "#60b044"
    :background-image "linear-gradient(#8add6d, #60b044)"
    :border-color "darken(#60b044, 2%)"}
   [:&:hover {:color "#fff"
              :background-color "#60b044"
              :background-image "linear-gradient(#8add6d, #60b044)"
              :border-color "#4a993e"}]
   [:&:active :&.selected {:text-shadow "0 1px 0 rgba(0, 0, 0, 0.15)"
                           :background-color "#60b044"
                           :background-image "none"
                           :border-color "#4a993e"}]
   [:.selected:hover {:background-color "#60b044"}]
   [:&:disabled :&.disabled
    [:& :&:hover {:color "#fefefe"
                  :background-color "#add39f"
                  :background-image "linear-gradient(#c3ecb4, #add39f)"
                  :border-color "#b9dcac #b9dcac #a7c89b"}]]
   [:counter {}]])

(def button-small
  [:.button.button--small {:font-size "0.75rem"
                           :padding "2px 10px"}])

(def button-group
  [:div.button-group
   [:.button {:margin 0
              :border-radius "0"
              :border-right "0"}
    [:&:first-child {:border-top-left-radius "2px"
                     :border-bottom-left-radius "2px"}]
    [:&:last-child {:border-top-right-radius "2px"
                    :border-bottom-right-radius "2px"
                    :border "1px solid #cdcdcd"}]]])

(def button-icon
  [:.button.button--icon
   [:>
    [:i {:margin-right 0}]]])

(def buttons [button-counter
              button-small
              button-block
              button-group
              button-primary
              button-icon
              button-base])
