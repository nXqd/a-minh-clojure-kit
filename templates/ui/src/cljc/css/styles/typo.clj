(ns css.styles.typo
  (:refer-clojure :exclude [list]))

;; NOTICE: dont change to apple or any special font, it often fucks thing up
(def fonts {:normal "\"SF Pro Text\", \"SF Pro Icons\", \"Helvetica Neue\", \"Helvetica\", \"Arial\", sans-serif;"
            :mono "Consolas, Menlo, Courier New, monospace"})

(def body-font-size "106.25%")

(def reset-font-size
  [:input
   :textarea
   :keygen
   :select
   :button {:font-size body-font-size}])

(def text-utils
  [[:.text--left {:text-align "left"}]
   [:.text--right {:text-align "right"}]
   [:.text--center {:text-align "center"}]
   [:.text--overflow {:overflow "hidden"
                      :word-break "break-word"
                      :text-overflow "ellipsis"}]
   [:.text--truncate {:max-width "100%"
                      :overflow "hidden !important"
                      :text-overflow "ellipsis !important"
                      :white-space "nowrap !important"
                      :word-wrap "normal !important"}]])

(def headers
  [[:h1 :h2 :h3 :h4 :h5 :h6
    :.h1 :.h2 :.h3 :.h4 :.h5 :.h6
    {:font-weight 500
     :margin "0"
     :line-height "1.1"}]
   [:h1 :.h1 {:font-size "2.5rem"}]
   [:h2 :.h2 {:font-size "2rem"}]
   [:h3 :.h3 {:font-size "1.75rem"}]
   [:h4 :.h4 {:font-size "1.5rem"}]
   [:h5 :.h5 {:font-size "1.25rem"}]
   [:h6 :.h6 {:font-size "1rem"}]])

(def body
  [[:body
    {:font-size "17px",
     :line-height "1.52947",
     :font-weight "400",
     :letter-spacing "-0.021em",
     :font-family "\"SF Pro Text\" , \"SF Pro Icons\" , \"Helvetica Neue\" , \"Helvetica\" , \"Arial\" , sans-serif",
     :background-color "white",
     :color "#333333",
     :font-style "normal"}]

   [[:pre {:margin-top "0"
           :margin-bottom "0"
           :font-size "0.7rem"
           :font-family (:mono fonts)}]
    [:code :tt {:font-family (:mono fonts)
                :font-size body-font-size}]]
   [:body
    :input
    :textarea
    :select
    :button
    {:font-synthesis "none",
     :-moz-font-feature-settings "'kern'",
     :-webkit-font-smoothing "antialiased",
     :-moz-osx-font-smoothing "grayscale",
     :direction "ltr",
     :text-align "left"}]])

(def misc
  [[:small :.small-font {:font-size "14px"}]])

(def typo
  [reset-font-size
   text-utils
   headers
   body
   misc])
