(ns css.styles.loader
  (:require
   [garden.def :refer [defkeyframes]]))

(defkeyframes loader-animation
  ["from" {:transform "rotate(0deg)"}]
  ["to" {:transform "rotate(360deg)"}])

(def loader
  [[loader-animation]
   [:.loader {:width "2.2585em"
              :height "2.2585em"
              :font-size "1em"
              :position "absolute"
              :top "50%"
              :left "50%"
              :margin "0"
              :text-align "center"
              :z-index "1000"
              :transform "translateX(-50%) translateY(-50%)"}
    [:&:before
     :&:after {:width "2.2585em"
               :height "2.2585em"
               :margin "0 0 0 -1.12925em"}]
    [:&:before {:position "absolute"
                :content "''"
                :top "0"
                :left "50%"
                :border-radius "500rem"
                :border ".2em solid rgba(0,0,0,.1)"}]
    [:&:after
     ^:prefix {:animation [[loader-animation "0.6s" :linear :infinite]]}
     {:position "absolute"
      :content "''"
      :top "0"
      :left "50%"
      :border-radius "500rem"
      :border-color "#767676 transparent transparent"
      :border-style "solid"
      :border-width ".2em"
      :box-shadow "0 0 0 1px transparent"}]]])
