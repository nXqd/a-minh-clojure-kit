(ns css.styles.alert)

(def alert
  [:.alert {:display "flex"
            :position "relative"
            :line-height 2.5
            :padding "0 1rem"
            :align-items "center"
            :margin-bottom "1rem"
            :border-radius "2px"
            :box-shadow "0 2px 5px rgba (0,0,0,.1)"}
   [:&:last-child {:margin-bottom 0}]
   [:&.alert--info {:color "#246"
                    :background-color "#e2eef9"
                    :border "solid 1px #bac6d3"}]
   [:&.alert--error {:color "#911"
                     :background-color "#fcdede"
                     :border "solid 1px #d2b2b2"}]
   [:.message {:margin 0}]
   [:.fa-times {:font-size "1rem"
                :margin-left "auto"
                :color "#246"}]])
