(ns css.styles.grid
  (:require
   [garden.stylesheet :refer [at-media]]))

(def ^:dynamic default-num-col 16)
;; golden grid
(defonce golden-line-height 24)
(defonce golden-rem default-num-col)
(defonce golden-column (/ 100 18))

;; 16 columns
(defn generate-class [prefix num-col]
  {:pre [(string? prefix)]}
  (let [grid-with-prefix (if-not (empty? prefix)
                           (str ".grid-" prefix "-")
                           ".grid-")
        clazz-hidden (keyword (str grid-with-prefix "hidden"))]
    [[clazz-hidden {:display "none !important"}]
     (for [i (range num-col)]
       (let [fraction (* (double (/ 100 num-col)) (inc i))
             clazz (keyword (str ".grid-cell" grid-with-prefix (inc i)))]
         [clazz {:flex (str "0 0 " fraction "% !important")
                 :max-width (str fraction "%")}]))]))

(def grid-utils
  [[:.grid-top {:align-self "flex-start"}]
   [:.grid-bottom {:align-self "flex-end"}]
   [:.grid-center {:align-self "center"}]])

(def container
  [:.container {:width "100vw"}
   [:&.container--padded {:padding "0 0.9375rem"}]
   [:&.container--fluid {:max-width "100% !important"}]])

;; extra small
(defn default [num-col]
  [(generate-class "" num-col)])

(defn small [num-col]
  (at-media {:screen true :max-width "767px"}
            [[:.container {:max-width "34rem"}]
             (generate-class "sm" num-col)]))

(defn medium [num-col]
  (at-media {:screen true :min-width "991px"}
            [#_[:.container {:max-width "45rem"}]
             (generate-class "md" num-col)]))

(defn large [num-col]
  (at-media {:screen true :min-width "992px"}
            [#_[:.container {:max-width "68rem"}]
             (generate-class "lg" num-col)]))

(defn grid
  ([] (grid default-num-col))
  ([num-col]
   [[(default num-col)]
    [(small num-col)]
    [(medium num-col)]
    [(large num-col)]
    [grid-utils]
    [container]
    [:.grid {:display "flex"
             :width "100%"
             :flex-wrap "wrap"

             ;; long text should wrap to be contained
             :white-space "initial"
             :word-wrap "break-word"
             :padding 0}

     [:.grid-cell {:align-items "center"
                   :display "flex"
                   :flex 1
                   :white-space "initial"
                   :word-wrap "break-word"}]]]))
