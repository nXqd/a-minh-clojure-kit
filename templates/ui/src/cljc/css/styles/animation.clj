(ns css.styles.animation
  (:require
   [garden.def :refer [defkeyframes]]
   [css.styles.tip :refer [pulestate]]))

(defkeyframes zoom-in-animation
  [:from {:transform "scale5d(0.5, 0.5, 0.5)"
          :opacity 0}]
  ["20%" {:opacity 0.3}]
  ["30%" {:opacity 0.5}]
  ["50%" {:opacity 0.8}]
  ["70%" {:opacity 1}])

(def zoom-in
  [:.animated.animated--zoom-in
   ^:prefix {:animation [[zoom-in-animation "1s" :infinite]]}])

(def animation
  [zoom-in-animation
   zoom-in
   pulestate])
