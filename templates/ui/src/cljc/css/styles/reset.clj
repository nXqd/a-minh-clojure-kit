(ns css.styles.reset)

;; Copy from Apple
;; https://developer.apple.com/assets/styles/global.dist.css
(def reset
  [[:html {:-ms-text-size-adjust "100%", :-webkit-text-size-adjust "100%"}]
   [:body {:margin "0", :padding "0"}]
   [:ul
    :ol
    :li
    :dl
    :dt
    :dd
    :h1
    :h2
    :h3
    :h4
    :h5
    :h6
    :hgroup
    :p
    :blockquote
    :figure
    :form
    :fieldset
    :input
    :legend
    :pre
    :abbr
    :button
    {:margin "0", :padding "0"}]
   [:pre :code :address :caption :th :figcaption {:font-size "1em", :font-weight "normal", :font-style "normal"}]
   [:fieldset :iframe :img {:border "0"}]
   [:caption :th {:text-align "left"}]
   [:table {:border-collapse "collapse", :border-spacing "0"}]
   [:article :aside :footer :header :nav :main :section :summary :details :hgroup :figure :figcaption {:display "block"}]
   [:audio :canvas :video :progress {:display "inline-block", :vertical-align "baseline"}]
   [:button
    {:line-height "inherit",
     :box-sizing "content-box",
     :color "inherit",
     :vertical-align "inherit",
     :overflow "visible",
     :background "none",
     :cursor "pointer",
     :font "inherit",
     :border "0",
     :-webkit-box-sizing "content-box"}
    [:&:disabled {:cursor "default"}]]
   [":focus" {:outline "3px solid rgba(131,192,253,0.5)", :outline-offset "1px"}]
   [":focus[data-focus-method=\"mouse\"]:not(input):not(textarea):not(select)"
    ":focus[data-focus-method=\"touch\"]:not(input):not(textarea):not(select)"
    {:outline "none"}]
   ["::-moz-focus-inner" {:border "0", :padding "0"}]])
