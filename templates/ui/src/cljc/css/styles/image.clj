(ns css.styles.image)

(def image-base
  [:img.image
   :img
   {:display "inline-flex"
    :max-width "100%"
    :height "auto"
    :user-select "none"}])

(def image-circle
  [:img.image.image--circle
   {:border-radius "50%"}])

;; image being put inside a container (div, list, flexbox )
;; should have some default css
(def image-contained
  [:img.image.image--contained
   {:display "flex"
    :width "100%"
    :height "auto"}])

(def image-city
  [:a.city
   {:display "inline-flex"
    :background-color "#2D0073"
    :width "320px"
    :height "160px"
    :line-height "160px"
    :text-align "center"
    :color "#fff"
    :font-size "20px"
    :font-weight "700"
    :margin "10px"
    :position "relative"
    :background-size "cover"
    :cursor "pointer"
    :justify-content "center"}
   [:> [:span {:color "white"}]]
   [:&:hover {:text-decoration "none"}
    [:&:before
     {:background-color "rgba(0,0,0,.2)"}]]

   [:&:before {:position "absolute"
               :content "' '"
               :top "0"
               :left "0"
               :right "0"
               :bottom "0"
               :background-color "transparent"
               :z-index "2"}]])

(def image [image-base
            image-contained
            image-circle
            image-city])
