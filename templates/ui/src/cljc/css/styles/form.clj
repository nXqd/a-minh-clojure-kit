(ns css.styles.form
  (:require
   [css.styles.form.inputs :refer
    [inputs
     switch
     slider
     input-validate]]
   [css.styles.form.form :as f]
   [css.styles.form.textarea :refer [textarea]]
   [css.styles.form.radio-checkbox :refer [radio-group
                                           checkbox-group]]
   [garden.selectors :as s]))

;; TODO
;; input focus
;; form group

(def form [f/form
           radio-group
           checkbox-group
           textarea
           inputs
           switch
           slider
           input-validate])
