(ns css.styles.colors)

(def grey-range
  ["#fafafa"
   "#f5f5f5"
   "#eeeeee"
   "#e0e0e0"
   "#bdbdbd"
   "#9e9e9e"
   "#757575"
   "#616161"
   "#424242"
   "#212121"])

(defn grey
  [i]
  (get grey-range i))

(def greys
  (->>
   grey-range
   (map-indexed #(let [k (- %1 5)]
                   [{(str ".bg-color--grey--" k) {:color %2}}
                    {(str ".color--grey--" k) {:color %2}}]))
   flatten
   (into {})))

(def color-white "#fff")
(def color-white-class [:.color--white {:color color-white}])

;; Application
;; You should define your own application colors
(def border-color (get grey-range 3))
(def border [:.color--border {:color border-color}])

(def color-primary "#E2592C")
(def color-primary-class [[:.color--primary {:color color-primary}]
                          [:.bg-color--primary {:background-color color-primary}]])

#?(:clj
   (def colors
     [greys
      color-primary-class
      color-white-class



      border]))


