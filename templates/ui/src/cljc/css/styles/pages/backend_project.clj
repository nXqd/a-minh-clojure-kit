(ns css.styles.pages.backend-project)

(def backend-project
  [:.backend-page
   [:h1.header.grid {:font-size "1.3rem"
                     :border-bottom "1px solid #eee"
                     :line-height 3
                     :padding-left "3rem"}]
   [:div.main.grid
    {:align-items "flex-start"}

    [:.left
     {:padding-top "1rem"
      :border-right "1px solid #eee"
      :padding-left "3rem"
      :align-items "flex-start"}

     [:li.list-item {:font-weight "bold"
                     :line-height 2.5}]]

    [:.right
     [:li.list-item {:padding "1rem"
                     :border-bottom "1px solid #eee"}

      [:div.title.grid {:font-weight "bold"
                        :padding-bottom "0.5rem"
                        :font-size "1rem"}]
      [:div.sub.grid {:color "#777"}]]]]])
