(ns css.styles.pages.backend-layout)

(def backend-page
  [:.backend-page
   [:.header {:font-size "1.3rem"
              :border-bottom "1px solid #eee"
              :line-height 3
              :padding-left "3rem"}
    [:h1 {:font-size "1.3rem"
          :border-bottom "1px solid #eee"
          :line-height 3
          :padding-left "3rem"}]
    [:.buttons {:padding-right "1rem"
                :margin-left "auto"}]]

   [:div.main.grid
    {:align-items "flex-start"}

    [:.left
     {:padding-top "1rem"
      :border-right "1px solid #eee"
      :padding-left "3rem"
      :align-items "flex-start"}
     [:>
      [:li.list-item {:font-weight "bold"
                      :line-height 2.5}
       [:&.active:after {:display "inline-flex"
                         :font-family "FontAwesome"
                         :font-size "0.7rem"
                         :margin-left "auto"
                         :padding-right "1rem"
                         :content "'\\f054'"}]]]]

    [:.right
     [:>
      [:li.list-item {:padding "1rem"
                      :border-bottom "1px solid #eee"}

       [:div.title.grid {:font-weight "bold"
                         :padding-bottom "0.5rem"
                         :font-size "1rem"}]
       [:div.sub.grid {:color "#777"}]
       [:&:hover {:background "#f7f7f7"}]]]]]])

(def backend-layout [backend-page])
