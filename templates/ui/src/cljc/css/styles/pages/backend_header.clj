(ns css.styles.pages.backend-header
  (:require
   [css.styles.menu :as menu]))

(def notification-menu-state-empty
  [:.state-empty
   {:width "100%"
    :height "100%"
    :display "flex"
    :align-items "center"
    :justify-content "center"}])

(def notification-menu
  [:.notification-menu
   {:position "absolute"
    :top "100%"
    :left "0"
    :z-index "100"
    :width "160px"
    :margin-top "2px"
    :padding-top "5px"
    :padding-bottom "5px"}
   [menu/shadow]
   [:> [:.header {:line-height 3}
        [:li:first-child
         {:justify-content "center"}]]

    [:.main {:height "210px"}
     [:> [notification-menu-state-empty]]]

    [:.previous
     {:line-height 2
      :text-align "center"}]]])

(def account
  [:div.header-account
   {:position "relative"}
   [:.account-menu
    {:position "absolute"
     :top "100%"
     :left "0"
     :z-index "100"
     :width "160px"}
    [menu/shadow]
    [:.list-item {:padding "0.4rem 0.7rem"
                  :cursor "pointer"}
     [:&:hover {:background "#f7f8fa"}]
     [:&.separator {:padding 0
                    :margin "0.2rem 0"}]]]])

(def menu
  [:.menu
   [menu/shadow]
   [:.list-item {:padding "0.4rem 0.7rem"
                 :cursor "pointer"}
    [:&:hover {:background "#f7f8fa"}]
    [:&.separator {:padding 0
                   :margin "0.2rem 0"}]]])

(def header
  [:div.backend-header.header.header--horizontal
   [menu]
   [account]
   [notification-menu]

   [:div.container.container--fluid
    [:div.grid
     ;; logo
     [:div.logo.logo--text
      {:font-weight "bold"
       :text-transform "uppercase"
       :font-size "0.8rem"}]
     ;; list
     [:ul.navs.list.list--horizontal
      {:margin-left "auto"}
      [:li.list-item [:a
                      {:padding "0 1rem"
                       :text-decoration "none"
                       :border-radius "2px"
                       :font-weight "bold"}
                      [:&:hover {:background "rgba(255,255,255,.05)"}]]]]]]])

(def backend-headers [header])
