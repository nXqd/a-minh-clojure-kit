(ns css.styles.pages.housing-developer)

(def similar-properties
  [:.similar-properties
   [:.similar-property
    {:padding "0.5rem"
     :border "1px solid transparent"}
    [:&:hover {:background "#f7f7f7"
               :border "1px solid #e6e6e6"}]]])

(def housing-developer
  [:div.housing-developer
   [similar-properties]
   {:background-color "#f4f4f4"}
   [:div.main.container
    [:div.inner.grid
     [:div.left.list.grid-cell.grid-md-12
      [:> [:.block {:background "white"
                    :margin-bottom "1rem"}
           [:> [:h2 {:padding "1rem"
                     :border-bottom "1px solid #eee"
                     :font-weight 500}]
            [:.inner {:padding "1rem"}
             [:> [:.block {:margin-bottom "1.5rem"}
                  [:h4 {:text-transform "uppercase"
                        :line-height 2
                        :font-size "0.75rem"}]
                  [:&:last-child {:margin-bottom 0}]]]]]
           [:&:last-child {:margin-bottom 0}]]]]]]])
