(ns css.styles.pages.housing-home)

(def housing-home
  [:.housing-home
   [:>
    [:.top
     [:> [:.navs
          {:line-height 2
           :padding "1rem 0"}
          [:.list-item {:color "white"
                        :text-transform "uppercase"
                        :font-weight "bold"
                        :justify-content "center"}
           [:span {:padding "0 0.5rem"}]
           [:&.active
            [:span {:border-bottom "2px solid white"}]]]]]]]])
