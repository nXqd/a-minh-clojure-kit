(ns css.styles.label)

(def counter*
  [:span.counter {:font-size "0.7rem"
                  :padding "0.2rem 0.4rem"
                  :color "#333"
                  :background-color "#eee"
                  :font-weight "bold"
                  :line-height 1}])

(def counter-circle
  [:span.counter.counter--circle {:border-radius "50%"}])

(def counter-square
  [:span.counter.counter--square {:border-radius "2px"}])

(def counter [counter* counter-circle counter-square])

(def label-image
  [[:.label.label--image-right
    [:span {:border-radius "4px 0 0 4px"}]
    [:img {:border-radius "0 4px 4px 0"}]]

   [:.label.label--image-left
    [:span {:border-radius "0 4px 4px 0"}]
    [:img {:border-radius "4px 0 0 4px"}]]])

(def label*
  [:.label {:display "inline-flex"
            :align-items "stretch"}
   ;; image
   [:span
    :i {:display "flex"
        :align-items "center"
        :padding "0 0.4rem"}]])

(def label [counter
            label*
            label-image])
