(ns css.playground
  (:require
   [taoensso.timbre :as log]
   [rum.core :as rum]
   [css.utils.core :as utils]
   #?@(:cljs [[goog.dom :as gdom]
              [hiccups.runtime :as hiccupsrt]
              [css.utils.utils :as outils :refer [stop-event]]
              [cljs.core.async :as async]]))
  #?(:cljs
     (:require-macros
      [cljs.core.async.macros :refer [go]]
      [hiccups.core :as hiccups])))

(defn dropdown-on-change
  []
  (fn [e]
    #?(:cljs (log/debug "something"))))

(defn dropdown
  []
  [:select
   [:option "1"]
   [:option "2"]])

(defn- get-value
  "Get value from ag grid cell"
  [params]
  #?(:cljs
     (let [{:keys [value]} (js->clj params :keywordize-keys true)]
       value)))

(defn link-renderer
  [params]
  #?(:cljs (let [{:keys [label uri]} (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html [:a {:href (str uri)} label])]
             (set! ediv.innerHTML content)
             ediv)))

(defn drop-renderer
  [params]
  #?(:cljs (let [value (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html (dropdown))]
             (set! ediv.innerHTML content)
             (let [select (.querySelector ediv "select")]
               (.addEventListener select "change" (fn [e] #?(:cljs (log/debug "hahaha")))))
             ediv)))

(def col-defs
  [{:headerName "Make" :field "make"
    :cellRenderer drop-renderer}
   {:headerName "Model" :field "model"
    :cellRenderer link-renderer
    :filter "text"}
   {:headerName "Price" :field "price"}])

(defn green-red-cell-renderer
  [params]
  #?(:cljs (let [value (get-value params)
                 ediv (js/document.createElement "div")
                 color (if (> value 0) "green" "red")
                 color-style (str "color: " color)
                 content (hiccups/html [:span {:style color-style} (str value " %")])]
             (set! ediv.innerHTML content)
             ediv)))

(defn price-graph-image-renderer
  [params]
  #?(:cljs (let [url (get-value params)
                 ediv (js/document.createElement "div")
                 content (hiccups/html [:img {:src url}])]
             (set! ediv.innerHTML content)
             ediv)))

(def col-defs2
  [{:headerName "Name"
    :field "name"
    :filter "text"}

   {:headerName "Market cap"
    :field "market-cap"}

   {:headerName "Price"
    :field "price"}

   {:headerName "Volume 24h"
    :field "volume-24h"}

   {:headerName "Calculating Supply"
    :field "calculating-supply"}

   {:headerName "Change"
    :field "change-percent"
    :cellRenderer green-red-cell-renderer}

   {:headerName "Price Graph (7d)"
    :field "price-graph-image"
    :cellRenderer price-graph-image-renderer}])

(def row-data2
  [{:name "EOS"
    :image ""
    :platform "Etherum"
    :market-cap 9999999
    :price 300
    :volume-24h 120000
    :calculating-supply 6444444
    :change-percent 5.42
    :price-graph-image "https://files.coinmarketcap.com/generated/sparklines/1765.png"}
   {:name "OmiseGO"
    :image ""
    :platform "Etherum"
    :market-cap 8888888
    :price 300
    :volume-24h 120000
    :calculating-supply 6444444
    :change-percent 5.42
    :price-graph-image "https://files.coinmarketcap.com/generated/sparklines/1765.png"}
   {:name "EOS2"
    :image ""
    :platform "Etherum"
    :market-cap 1000000000000
    :price 300
    :volume-24h 120000
    :calculating-supply 6444444
    :change-percent -5.42
    :price-graph-image "https://files.coinmarketcap.com/generated/sparklines/1765.png"}])

(def row-data
  [{:make {:label "test"}
    :model {:label "CLica"
            :uri "Claci"}  "price" 35000}])

(def opts
  {:columnDefs col-defs2
   :rowData row-data2
   :enableColResize true
   :enableSorting true
   :enableFilter true
   :getRowHeight (fn [params] 80)})

(def ^:const lib-uri "//www.ag-grid.com/dist/ag-grid.js?ignore=notused38")

(defn lib-loaded?
  []
  #?(:cljs js/window.agGrid))

(defn load-lib
  []
  #?(:cljs (utils/load-script-async lib-uri)))

(defn did-mount
  [states]
  #?(:cljs (let [el (rum/dom-node states)]
             (go (when-not (lib-loaded?)
                   (async/<! (load-lib)))
                 (js/agGrid.Grid. el (clj->js opts)))))
  states)

(rum/defc grid-ui <
  {:did-mount did-mount}
  []
  [:div#something.ag-fresh
   {:style {:height "100rem"}}])

