(ns css.mixins.events
  #?(:cljs (:import [goog.events EventHandler])))

(defn detach
  "Detach all event listeners."
  [state]
  (some-> state ::event-handler .removeAll))

(defn listen
  "Register an event `handler` for events of `type` on `target`."
  [state target type handler & [opts]]
  #?(:cljs (.listen (::event-handler state) target (name type) handler (clj->js opts))))

(def mixin
  "The event handler mixin."
  {:will-mount
   (fn [state]
     (assoc state ::event-handler #?(:clj nil :cljs (EventHandler.))))
   :will-unmount
   (fn [state]
     (detach state)
     (dissoc state ::event-handler))})
