(ns css.analytics.google-analytics
  #?(:cljs
     (:require
      [cljsjs.google-analytics])))

#?(:cljs
   (defn loaded?
     []
     (aget js/window "ga")))

#?(:cljs
   (defn event!
     [category action]
     (when (loaded?)
       (js/ga "send" "event" category action))))

#?(:cljs
   (defn require-plugin
     "Require ga plugin
      (require :ec) enhanced commerce, dont use the old :ecommerce"
     [k]
     {:pre [(keyword? k)]}
     (when (loaded?)
       (js/ga "require" (name k)))))

(comment
  #?(:cljs
     (do (require-plugin :ecommerce)
       (js/ga "ecommerce:addTransaction"
              (clj->js {:id "12345"}))
       (js/ga "ecommerce:addItem"
              (clj->js {:id "12345"}))
       (js/ga "ecommerce:send")
       #_(event! "category" "action"))))
