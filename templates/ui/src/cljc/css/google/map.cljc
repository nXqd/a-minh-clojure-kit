(ns css.google.map
  (:require
   [rum.core :as rum]
   #?@(:cljs [[goog.net.jsloader :as jsloader]])))

(def ^:const google-api "AIzaSyAoOs0rl6ANkoRWn4udnpIbPODzdinVWtI")

(defn places-loaded?
  []
  #?(:cljs js/goog.maps.places))

(def default-options
  #?(:cljs
     (clj->js {:types ["(cities)"]
               :componentRestrictions {:country "vn"}})))

;; if not loaded then showing loading
(defn places-autocomplete-did-mount
  [states]
  (let [input (rum/ref-node states "input")]
    #?(:cljs
       (google.maps.places.Autocomplete. input default-options)))
  states)

(rum/defcs places-autocomplete-ui < rum/static rum/reactive
  {:did-mount places-autocomplete-did-mount}
  [props]
  (let []
    [:div.googlemap
     [:input.input
      {:type "text"
       :ref "input"}]]))
