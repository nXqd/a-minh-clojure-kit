(ns css.google.core
  (:require
   [css.utils.core :as utils]
   #?@(:cljs [[hiccups.runtime :as hiccupsrt]]))
  #?(:cljs (:require-macros
            [hiccups.core :as hiccups])))

(def ^:const lib-uri "//maps.googleapis.com/maps/api/js?key=AIzaSyAoOs0rl6ANkoRWn4udnpIbPODzdinVWtI&libraries=places")

(defn gmap-loaded?
  []
  #?(:cljs
     (try
       js/window.google.maps.Map
       (catch js/Error e
         false))))

(defn load-gmap
  []
  (utils/load-script-async lib-uri))

(defn custom-marker-content
  [content]
  #?(:cljs
     (let [div (js/document.createElement "div")]
       (aset div "innerHTML" (hiccups/html content))
       (.-firstChild div))))

(defn custom-marker
  [gmap latlng marker-template
   & {:keys [on-mouse-over on-mouse-out]}]
  #?(:cljs
     (let [overlay (js/google.maps.OverlayView.)]
       (do (set! (.-position overlay) latlng)

         (set! (.-getposition overlay)
               (fn [] (.-position overlay)))

         (set! (.-onAdd overlay)
               (fn []
                 (let [marker-content (custom-marker-content marker-template)
                       panes (.getPanes overlay)]
                   (.appendChild (.-overlayMouseTarget panes) marker-content)
                   (set! (.-contentEl overlay) marker-content))))

         (set! (.-draw overlay)
               (fn []
                 (let [content-el (.-contentEl overlay)
                       projection (.getProjection overlay)
                       point (.fromLatLngToDivPixel projection (.-position overlay))
                       width (.-offsetWidth content-el)
                       height (.-offsetHeight content-el)
                       left (- (.-x point) (/ width 2))
                       top (- (.-y point) height 5)]
                   (set! (-> content-el .-style .-left) (str left "px"))
                   (set! (-> content-el .-style .-top) (str top "px"))

                     ; add events
                   (when on-mouse-over
                     (js/google.maps.event.addDomListener
                      content-el "mouseover"
                      (fn [] (on-mouse-over))))

                   (when on-mouse-out
                     (js/google.maps.event.addDomListener
                      content-el "mouseout"
                      (fn [] (on-mouse-out)))))))

         (set! (.-remove overlay)
               (fn []
                 (when-let [content-el (.-contentEl overlay)]
                   (.removeChild (.-parentNode content-el) content-el))))

         (.setMap overlay gmap))
       overlay)))

(defn remove-marker
  [marker]
  #?(:cljs
     (when marker
       (.setMap marker nil)
       (js/google.maps.event.clearInstanceListeners marker))))

(defn create-custom-marker-gmap
  [gmap [long lat] infowindow infowindow-content marker-content]
  #?(:cljs
     (let [latlng (js/google.maps.LatLng. lat long)
           on-mouse-over (fn []
                           (.setContent infowindow infowindow-content)
                           (.setPosition infowindow latlng)
                           (.open infowindow gmap))

           on-mouse-out (fn []
                          (.close infowindow gmap))]

       (custom-marker gmap latlng marker-content
                      :on-mouse-over on-mouse-over
                      :on-mouse-out on-mouse-out))))