(ns css.google.custom-marker
  (:require
   [css.google.core :as gcore]
   [rum.core :as rum]
   #?@(:cljs [[hiccups.runtime :as hiccupsrt]
              [cljs-http.client :as http]
              [cljs.core.async :as async]])
   #?@(:clj [[clojure.core.async :as async :refer [chan put!]]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go]]
            [hiccups.core :as hiccups])))

(def page-default {:data {}
                   :cache nil})
(def page-atom (atom page-default))

(defn fetch-geojson-data
  []
  #?(:cljs
     (go (let [response (async/<! (http/get "/provinces.json"))
               response (js->clj response :keywordize-keys true)
               data (get-in response [:body])]
           (swap! page-atom assoc :data data)))))

;; just an example, create your own in production
(def marker-template
  [:a.gmap-custom-marker.primary-marker
   [:span "some value"]])

;; just an example
;; for performance sake, only create one infowindow
(def ^:dynamic *infowindow* (atom nil))
(defn infowindow
  []
  #?(:cljs
     (when-not @*infowindow*
       (let [infowindow
             (let [infowindow-content [:div {:style "width: 20rem; margin-bottom: 20px"}
                                       "something here my friend"]]
               (js/google.maps.InfoWindow.
                (clj->js {:content (hiccups/html infowindow-content)
                          :maxWidth 200})))]
         (reset! *infowindow* infowindow)))))

;; https://developers.google.com/maps/documentation/javascript/customoverlays#initialize
(defn create-custom-marker
  [gmap]
  #?(:cljs
     (let [latlng (js/google.maps.LatLng. 16.1497 106.215)
           on-mouse-over (fn []
                           (infowindow)
                           (.setPosition @*infowindow*  latlng)
                           (.open @*infowindow* gmap))

           on-mouse-out (fn []
                          (.close @*infowindow* gmap))]
       (gcore/custom-marker gmap latlng marker-template
                            :on-mouse-over on-mouse-over
                            :on-mouse-out on-mouse-out))))

(defn gmap-search-will-mount
  [states]
  #?(:cljs
     (do (fetch-geojson-data)
       (add-watch page-atom :component
                  (fn [_ _ _ new-value]
                    (let [{:keys [cache data]} new-value
                          {:keys [gmap gmap-data]} cache]
                      (when (and gmap (not (empty? data)))
                        (when-not gmap-data
                          (let [{:keys [geo center]} data
                                gmap-data (js/google.maps.Data.)]
                            (.setStyle gmap-data (clj->js {:fillOpacity 0
                                                           :strokeColor "green"}))
                            (.setCenter gmap (clj->js center))
                            (.addGeoJson gmap-data (clj->js geo))
                            (.setMap gmap-data gmap)))))))
       states)))

(def center {:lat 16.1497 :lng 106.215})

(defn gmap-search-did-mount
  [states]
  #?(:cljs
     (go (when-not (gcore/gmap-loaded?)
           (async/<! (gcore/load-gmap)))
         (let [element (rum/dom-node states)
               gmap (js/google.maps.Map. element
                                         (clj->js {:zoom 5 :center center}))
               marker (create-custom-marker gmap)]
           (swap! page-atom assoc-in [:cache :gmap] gmap))))
  states)

(rum/defcs custom-marker-ui < rum/static rum/reactive
  {:will-mount gmap-search-will-mount
   :did-mount  gmap-search-did-mount}
  [states]
  #?(:clj [:div.map]
     :cljs [:.google-map {:style {:height 700 :weight 400}}]))
