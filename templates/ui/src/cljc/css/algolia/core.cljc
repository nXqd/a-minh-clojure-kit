(ns css.algolia.core
  (:require
   [rum.core :as rum]
   [css.utils.core :as utils]
   #?@(:cljs [[cljs.core.async :as async]]))
  #?(:cljs (:require-macros
            [cljs.core.async.macros :refer [go]])))

(def ^:const search-lib-uri "https://cdn.jsdelivr.net/algoliasearch/3/algoliasearchLite.min.js")
(def ^:const search-lib-helper-uri "https://cdn.jsdelivr.net/algoliasearch.helper/2/algoliasearch.helper.min.js")

(defn algolia-search-loaded?
  []
  #?(:cljs js/window.algoliasearch))

(defn algolia-search-helper-loaded?
  []
  #?(:cljs js/window.algoliasearchHelper))

(def algolia-search-loaded-memo algolia-search-loaded?)
(def algolia-search-helper-loaded-memo algolia-search-helper-loaded?)

(defn load-algoliasearch
  []
  (utils/load-script-async search-lib-helper-uri))

(defn load-algoliasearch-helper
  []
  (utils/load-script-async search-lib-uri))

(defn normal-search
  []
  #?(:cljs
     (go (when-not (algolia-search-loaded-memo)
           (async/<! (load-algoliasearch)))
         (let [client (js/algoliasearch "Y8UWCZS0VZ" "bc9139f22f20e389d82556b476a2dec4")
               index (.initIndex client "getstarted_actors")]
           (.search index "oh" (fn [err content]
                                 (prn "content" content)))))))

;;https://www.algolia.com/doc/guides/geo-search/geo-search-overview/
(def ^:const default-search-opts
  {:hitsPerPage 60})

(defn geo-search
  []
  #?(:cljs
     (go (when-not (and (algolia-search-loaded-memo)
                        (algolia-search-helper-loaded-memo))
           (async/<! (load-algoliasearch-helper))
           (async/<! (load-algoliasearch)))
         (let [client (js/algoliasearch "Y8UWCZS0VZ" "bc9139f22f20e389d82556b476a2dec4")
               helper (js/algoliasearchHelper client "geo" (clj->js default-search-opts))]
           (.on helper "result" (fn [content states]
                                  (js/console.log "content" content)))
           (.search (.setQueryParameter helper "insideBoundingBox" "60, 16, 40, -4"))))))

#_(geo-search)

(rum/defc core-ui < rum/static rum/reactive
  {:will-mount (fn [states]
                 #_(normal-search)
                 states)}
  []
  [:div])
