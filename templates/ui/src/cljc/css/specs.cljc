(ns css.specs
  (:require
   [css.bootstrap.components.button :as bb]
   #?@(:clj [[clojure.spec.alpha :as s]]
       :cljs [[cljs.spec.alpha :as s]])))

(s/def ::class vector?)

;; button fn
(s/def :button/type #{:primary :secondary :success :danger :warning :info :link})
(s/def :button/size #{:large :small})
(s/def :button/state #{:disabled :active})
(s/def :button/opts (s/keys :opt-un [:button/type :button/state :button/size
                                     ::class]))
(s/fdef bb/button :args (s/cat :opts :button/opts))

(comment
  (require '[clojure.spec.test.alpha :as st])
  (st/instrument)

  (sut/button {:type :warning
               :size :small
               :state :disable
               :class [:extra-class]}))


