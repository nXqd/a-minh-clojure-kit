(ns css.bootstrap.components.utilities)

;; most of these are classes

(def text-center :.text-center)
(def text-left :.text-left)
(def text-right :.text-right)

(def items-center :.justify-items-center)
(def items-left :.justify-items-start)
(def items-right :.justify-items-end)

(def items-center-vertical :.align-items-center)
(def items-left-vertical :.align-items-start)
(def items-right-vertical :.align-items-end)

(def self-center-vertical :.align-self-center)
(def self-left-vertical :.align-self-start)
(def self-right-vertical :.align-self-end)

(def clearfix :.cleafix)

