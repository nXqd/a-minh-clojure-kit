(ns css.bootstrap.components.button
  (:require
   #?@(:clj [[clojure.spec.alpha :as s]]
       :cljs [[cljs.spec.alpha :as s]])
   [rum.core :as rum]))

(defn button
  [{:keys [type class state size]
    :or {class []}}]
  (let [type-class (str "btn-" (name type))
        state-class (when state (name state))
        size-class (when size (str "btn-"
                                   (condp = size
                                     :small "sm"
                                     :large "lg")))
        class (->> (conj class type-class state-class size-class)
                   (filter #(not (nil? %))))]
    [:button.btn
     {:type "button"
      :class class}]))

(comment
  (require '[clojure.spec.test.alpha :as st])
  (st/instrument)

  (button {:type :warning
           :size :small
           :state :disabled
           :class [:extra-class]}))
