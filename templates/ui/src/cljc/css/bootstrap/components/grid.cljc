(ns css.bootstrap.components.grid
  (:require
   [css.specs :as cs]
   [clojure.string :as cstr]
   #?@(:clj [[clojure.spec.alpha :as s]]
       :cljs [[cljs.spec.alpha :as s]])
   [rum.core :as rum]))

;; doc https://getbootstrap.com/docs/4.0/layout/grid/

;; standard grid props [:type :class]
(s/fdef opts-children
  :args (s/cat :children coll?)
  :ret vector?)
(defn- opts-children
  [children]
  (let [opts (first children)]
    (if (map? opts)
      [opts (rest children)]
      [{} children])))

;; layout
(s/def :layout/type #{"fluid"})
(s/def ::layout-props (s/keys :opt-un [::cs/class :layout/type]))

(s/fdef layout
  :args (s/cat :children coll?)
  :ret vector?)
(defn layout
  [& children]
  (let [[opts children] (opts-children children)
        {:keys [type]} opts
        main-class (if (= type "fluid")
                     :.container-fluid
                     :.container)
        opts (dissoc opts :type)]
    [main-class opts children]))

;; row
(s/def :row/gutter? boolean?)
(s/def :row/opts (s/keys :opt-un [:row/gutter? ::cs/class]))
(defn row
  [& children]
  (let [[opts children] (opts-children children)
        {:keys [gutter?]} opts
        opts (select-keys opts [:class])
        row-el (if gutter? :.row.no-gutters :.row)]
    [row-el children]))

;; col
(def col-types #{:sm :md :lg :xl})
(def col-widths (set (range 1 13)))
(s/def :col/width col-widths)
(s/def :col/opts (s/keys :opt-un [:col/width ::cs/class]))

(s/fdef gen-sizes
  :args (s/cat :width :col/width))
(defn gen-sizes
  [prefix width opts]
  (if (nil? width)
    :.col
    (let [cols-maps (merge
                     (zipmap col-types (repeat (count col-types) width))              ;; {:sm 4 :md 4}
                     (select-keys opts col-types))]
      (->> cols-maps                                       ;; {:sm 4 :md 5}
           (map (fn [[typ size]]
                  (str "." prefix "-" (name typ) "-" size)))
           (cstr/join "")))))

(defn gen-col
  [width opts]
  (gen-sizes "col" width opts))

(defn gen-offset
  [width opts]
  (gen-sizes "offset" width opts))

(gen-offset 4 {})

(defn col
  [& children]
  (let [[opts children] (opts-children children)
        {:keys [width offset]} opts
        col (gen-col width opts)
        offset (gen-offset offset opts)
        opts (select-keys opts [:class])
        classes (str col offset)]
    [classes opts children]))

(comment
  (require '[rum.core :as rum])

  (layout {:type "fluid"})

  (col {:width 3
        :offset 3
        :offset-sm 4
        :sm 3
        :md 5
        :xl 6
        :lg 4})

  (layout
   (row
    (col {:width 2}))
   (row {:gutter? false}
        (col {:width 1
              :sm 3
              :md 4
              :lg 5
              :xl 5}))))
