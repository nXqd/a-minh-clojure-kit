(ns css.utils.react-component)

(defn create-element
  [rn-comp opts & children]
  #?(:cljs
     (apply js/React.createElement rn-comp (clj->js opts) children)))
