(ns css.utils.utils)

;; stop event
(defn stop-event
  [e]
  #?(:cljs
     (doto e (.preventDefault) (.stopPropagation))))

