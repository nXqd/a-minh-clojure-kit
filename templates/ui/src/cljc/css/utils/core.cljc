(ns css.utils.core
  (:require
   [clojure.string :as string]
   #?@(:cljs
       [[goog.net.jsloader :as jsloader]
        [goog.html.legacyconversions :as conv]
        [cljs.core.async :as async]])))

(defn load-script-async
  [uri]
  #?(:cljs
     (let [ch (async/promise-chan)
           trusted-uri (->> uri
                            str
                            conv/trustedResourceUrlFromString)]
       (.addCallback (jsloader/safeLoad trusted-uri)
                     #(async/put! ch :lib-loaded)) ch)))
