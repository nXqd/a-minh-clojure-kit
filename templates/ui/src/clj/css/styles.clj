(ns css.styles
  (:refer-clojure :exclude [list])
  (:require
   [garden.def :refer [defstyles]]

   [css.styles.components.accordion :refer [accordion]]
   [css.styles.components.breadcrumb :refer [breadcrumb]]
   [css.styles.components.cards :as cards]
   [css.styles.components.chat :refer [chats]]
   [css.styles.components.combobox :refer [combobox]]
   [css.styles.components.date :refer [date]]
   [css.styles.components.dropdown :refer [dropdown]]
   [css.styles.components.editor :refer [editor]]
   [css.styles.components.fineuploader :refer [fineuploader]]
   [css.styles.components.header :refer [header]]
   [css.styles.components.image-sliders :refer [image-sliders]]
   [css.styles.components.menus :refer [menus]]
   [css.styles.components.min-max :refer [min-maxes]]
   [css.styles.components.modal :refer [modal]]
   [css.styles.components.paging :refer [paging]]
   [css.styles.components.placeholder :refer [placeholder]]
   [css.styles.components.progress :refer [progress]]
   [css.styles.components.qa :refer [qa]]
   [css.styles.components.slider :refer [slider]]
   [css.styles.components.step :refer [steps]]
   [css.styles.components.tab :refer [tab]]
   [css.styles.components.table :refer [table]]
   [css.styles.components.tooltip :refer [tooltip]]

   [css.styles.animation :refer [animation]]
   [css.styles.button :refer [buttons]]
   [css.styles.colors :refer [colors]]
   [css.styles.fontawesome :refer [fontawesome]]
   [css.styles.form :refer [form]]
   [css.styles.google.map :refer [gmap]]
   [css.styles.grid :refer [grid]]
   [css.styles.grid-xy :as grid-xy]
   [css.styles.icons :refer [icons]]
   [css.styles.image :refer [image]]
   [css.styles.label :refer [label]]
   [css.styles.list :as css-list]
   [css.styles.loader :refer [loader]]
   [css.styles.reset :refer [reset]]
   [css.styles.typo :refer [typo]]
   [css.styles.utils.core :as utils]

   [css.styles.pages.backend-header :refer [backend-headers]]
   [css.styles.pages.backend-layout :refer [backend-layout]]
   [css.styles.pages.housing-header :refer [housing-headers]]
   [css.styles.pages.housing-home :refer [housing-home]]))

(def framework [(grid)
                (grid-xy/main)
                animation
                buttons
                cards/main
                colors
                css-list/list
                fontawesome
                form
                icons
                image
                label
                loader
                reset
                tooltip
                typo
                utils/utils])

(def pages [backend-headers
            backend-layout

            housing-home
            housing-headers])

(def components [accordion
                 breadcrumb
                 chats
                 combobox
                 date
                 dropdown
                 editor
                 fineuploader
                 gmap
                 header
                 image-sliders
                 menus
                 min-maxes
                 modal
                 paging
                 progress
                 qa
                 slider
                 steps
                 tab
                 table])

(def styles [framework components pages])

(defstyles ^:prefix screen styles)
