(ns css.utils.keys)

(def escape-key 27)

;; this event is react event so there is no (.-charCode e)
(defn is-key?
  [e key-code]
  (= key-code (.-which e)))
