(ns css.utils.parser
  (:require [om.next :as om]))

(def endpoint "http://localhost:1337/localhost:8000/api")

;; read
(defmulti read om/dispatch)

(defmethod read :default
  [{:keys [state]} k _]
  (let [st @state] ;; CACHING!!!
    (if (contains? st k)
      {:value (get st k)}
      {:remote true})))

;; state {:project/page {:project/page [{:project/name "Project 1", :project/developer {:db/id 17592186045424, :developer/name "Phu My Hung 1"}}]}}
(defmethod read :project/page
  [{:keys [state]} k _]
  (let [st @state]
    (if (contains? st k)
      {:value (-> st :project/page)}
      {:remote true})))

;; mutate
(defmulti mutate om/dispatch)

(defmethod mutate :default
  [_ _ _] {:remote true})

(defmethod mutate 'projects/create-temp
  [{:keys [state]} _ new-project]
  {:value []
   :action (fn [] (swap!  assoc :projects/temp new-todo))})
