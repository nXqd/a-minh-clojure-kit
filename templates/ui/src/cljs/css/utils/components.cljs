(ns css.utils.components)

;; shared protocol
(defprotocol IStyle
  (default-styles [this]))

(defn get-default-styles
  [x]
  (if (implements? IStyle x)
    (default-styles x)
    ;; advanced compile
    (when (goog/isFunction x)
      (let [y (js/Object.create (.-prototype x))]
        (when (implements? IStyle y)
          (default-styles y))))))

(defprotocol IAuthorize
  (authorize [this current-user opts]))

(defn authorize?
  [x current-user opts]
  (if (implements? IAuthorize x)
    (authorize x current-user opts)
    ;; advanced compile
    (when (goog/isFunction x)
      (let [y (js/Object.create (.-prototype x))]
        (when (implements? IAuthorize y)
          (authorize y current-user opts))))))

(defprotocol IForm
  (default-state [this])
  (schema [this])
  (submit [this valid?])
  (validate [this]))

(defprotocol IChannel
  (process-events [this e]))

(defprotocol IVersion
  (version [this]))
