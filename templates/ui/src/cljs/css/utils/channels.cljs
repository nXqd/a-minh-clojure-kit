(ns css.utils.channels
  (:require
   [cljs.core.async :as async :refer [chan close!]]))

;; change to defonce in production
;; channels
(def navigation-ch (chan))
(def errors-ch (chan))
(def components-ch (chan))

;; mult
(def navigation-mult (async/mult navigation-ch))
(def components-mult (async/mult components-ch))
