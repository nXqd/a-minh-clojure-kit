(ns css.utils.utils-om
  (:require
   [cognitect.transit :as t]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true])
  (:import [goog.net XhrIo]))

;; should be removed later
(defn atom-read [{:keys [state] :as env} key params]
  (let [st @state]
    (if-let [[_ value] (find st key)]
      {:value value}
      {:value :not-found})))

(defn atom-remote [{:keys [state] :as env} key params]
  (let [st @state]
    (if-let [[_ value] (find st key)]
      {:value value}
      {:remote true})))

;; stop event
(defn stop-event
  [e]
  (doto e (.preventDefault) (.stopPropagation)))

;; try parse
(defn try-parse-int
  [x]
  (let [val (js/parseInt x)]
    (if (js/isNaN val) x val)))

;; connector
(defn transit-post [url]
  (fn [{:keys [remote]} cb]
    (.send XhrIo url
           (fn [e]
             (this-as this
                      (cb (t/read (t/reader :json) (.getResponseText this)))))
           "POST" (t/write (t/writer :json) remote)
           #js {"Content-Type" "application/transit+json"})))

(comment
  (def sel [{:todos/list [:db/id :todo/title :todo/completed :todo/created]}])

  (t/write (t/writer :json) sel))
