(ns css.components.infowindow
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui InfoWindow
  static IStyle
  (default-styles
    [this]
    {:padding "0.5rem"
     :position "absolute"
     :zIndex 5
     :background "white"
     :border-radius "2px"})

  Object
  (render
    [this]
    (sab/html
     (let [{:keys [style
                   rect
                   content]} (om/props this)
           style (cond-> (merge (default-styles this)
                                (or style {}))
                         (not (nil? rect)) (assoc :top (str (.-top rect) "px"))
                         (and
                          (not (nil? rect))
                          (<= (.-left rect) (.-right rect))) (assoc :left
                                                                    (str (- (.-left rect)
                                                                            (.-width rect)) "px"))

                         (and
                          (not (nil? rect))
                          (> (.-left rect) (.-right rect))) (assoc :right
                                                                   (str (+ (.-right rect)
                                                                           (.-width rect)) "px")))]

       [:div.info-window
        {:class  (when (nil? rect) "hidden")
         :style style}
        content]))))

(def infowindow-ui (om/factory InfoWindow))
