(ns css.components.searchbar
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui SearchBar
  Object
  (render
    [this]
    (sab/html
     [:ul.list.list--horizontal
      [:li.list-item
       [:input {:type "text"
                :placeholder "Location"
                :value "123231"}]]
      [:li.list-item
       [:input {:type "number"
                :placeholder "Enter number here"
                :value "123231"}]]
      [:li.list-item
       [:input {:type "number"
                :style {:border-radius "3px 0px 0px 3px"}
                :placeholder "Enter number here"
                :value "123231"}]]

      [:li.list-item
       [:button.button
        {:style {:border-radius "0px 3px 3px 0px"}}

        "Search"]]])))

(def search-bar (om/factory SearchBar))
