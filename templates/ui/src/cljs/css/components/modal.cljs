(ns css.components.modal
  (:require
   [goog.dom :as gdom]
   [sablono.core :as sab :include-macros true]
   [om.next :as om :refer-macros [defui]]))

;; usage make-modal {:current-modal (nil|:login-modal)
;;                 :close-fn #()
;;                 :modals {:login-modal login-ui}}
(defn make-modal [x]
  (let [{:keys [current-modal
                styles
                close-fn
                modals]} x
        modal-ui (get modals current-modal)]
    (when current-modal
      [:div.modal-container
       {:style {:display "flex"
                :align-items "center"}}
       [:div.modal
        {:style (:modal styles)}
        [:i.modal-close.fa.fa-times
         {:style (:modal-close styles)
          :on-click close-fn}]
        modal-ui]])))

(defn modal-inner [x]
  (let [{:keys [close-fn]} x]
    [:div.modal-inner
     {:on-click #()}
     "let's put some text here"]))

(defui Page
  Object
  (initLocalState
   [this]
   {:current-modal nil})
  (render
    [this]
    (sab/html
     (let [{:keys []} (om/props this)
           {:keys [current-modal]} (om/get-state this)]

       [:div

        (make-modal {:current-modal current-modal
                     :close-fn #(om/update-state! this assoc :current-modal nil)
                     :modals {:test-modal modal-inner}})

        [:button.button
         {:on-click #(om/update-state! this assoc :current-modal :test-modal)}
         "CLick here"]]))))

(def page-ui (om/factory Page))
