(ns css.components.tooltip
  (:require
   [css.utils.components :refer [IStyle default-styles]]
   [cljs.test :refer-macros [is async]]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(def default-content-style
  {:background "#333"
   :color "white"
   :zIndex 1000000
   :border-radius "3px"
   :font-size "0.72rem"
   :padding "3px 4px"})

(def default-triangle-style
  {:position "absolute"
   :bottom "-18px"
   :left "20px"
   :width 0
   :height 0
   :content "''"
   :border-color "#333 transparent transparent transparent"
   :border-width "5px"
   :border-style "solid"})

(defui Tooltip
  Object
  (initLocalState
   [this]
   {:ui {:hovered false}})
  (render
    [this]
    (sab/html
     (let [{:keys [styles position sticky placeholder content]} (om/props this)
           {:keys [ui]} (om/get-state this)
           {:keys [hovered]} ui
           sticky (or sticky false)
           position (or position :top)]
       [:span.tooltip
        {:style {:position "relative"
                 :display "inline-block"
                 :margin-left "200px"
                 :margin-top "200px"}
         :on-mouse-enter #(om/update-state! this assoc-in [:ui :hovered] true)
         :on-mouse-leave #(when-not sticky
                            (om/update-state! this assoc-in [:ui :hovered] false))}
        [:span.content
         {:class (when-not hovered "hidden")
          :style (merge (:content styles)
                        {:position "absolute"})}
         content
         (when (= position :top)
           [:span.triangle
            {:style
             (merge default-triangle-style (:triangle styles))}])]
        [:span.placeholder
         {:style {:cursor "pointer"}}
         placeholder]]))))

(def tooltip-ui (om/factory Tooltip))
