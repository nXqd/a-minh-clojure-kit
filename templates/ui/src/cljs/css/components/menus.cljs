(ns css.components.menus
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui MenuLeft
  Object
  (render
    [this]
    (sab/html
     [:nav.menu.menu--left.list.grid
      {:style
       {:margin-bottom "15px"
        :list-style "none"
        :background-color "#fff"
        :border "1px solid #d8d8d8"
        :border-radius "3px"}}
      [:span.menu-header.list-item
       {:style
        {:display "block"
         :padding "8px 10px"
         :margin-top "0"
         :margin-bottom "0"
         :font-size "13px"
         :font-weight "bold"
         :line-height "20px"
         :color "#555"
         :background-color "#f7f7f7"
         :border-bottom "1px solid #eee"}}
       "Header"]
      [:a.list-item
       {:style {:position "relative"
                :display "block"
                :padding "8px 10px"
                :text-shadow "0 1px 0 #fff"
                :border-bottom "1px solid #eee"}}

       "Menu 1"]
      [:a.menu--active.list-item
       "Menu active"]])))

(def menu-left (om/factory MenuLeft))
