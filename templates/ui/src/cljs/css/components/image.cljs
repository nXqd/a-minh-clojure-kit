(ns css.components.image
  (:require
   [rum.core :as rum]))

(rum/defcs image-with-zoom < rum/static
  {:did-mount (fn [state]
                (let [demo-trigger   (js/document.querySelector ".demo-trigger")
                      pane-container (js/document.querySelector ".detail")]
                  (js/Drift. demo-trigger (clj->js {"paneContainer"    pane-container
                                                    "inlinePane"       900
                                                    "hoverBoundingBox" true}))
                  state))}
  [states props]
  [:div
   [:section {:class "content"}
    [:article {:class "demo-area"}
     [:img {:class "demo-trigger", :src "https://is1-3.housingcdn.com/91aba65c/8cb8d5c99afc356c809e61cd381a4d1c/v2/fs.jpg", :data-zoom "https://is1-3.housingcdn.com/91aba65c/8cb8d5c99afc356c809e61cd381a4d1c/v2/fs.jpg"}]
     [:div {:class "detail"}
      [:section
       [:h3 "Men&#39;s Watch - Drift Demo"]
       [:p "Specifications:"]
       [:ul
        [:li "Hover over image"]
        [:li "35 mm stainless steel case"]
        [:li "Stainless link bracelet with buckle closure"]
        [:li "Water resistant to 100m"]]
       [:h4 "$XX.XX "
        [:button "Add to Cart"]]]]]]])
