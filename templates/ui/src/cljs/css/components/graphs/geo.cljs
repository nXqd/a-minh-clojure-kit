(ns css.components.graphs.geo)

(defn home-render []
  [:div {:style {:min-width "310px" :max-width "800px"
                 :min-height "400px" :margin "0 auto"}}])

(def data [{:code "DE.SH" :value 728}
           {:code "DE.BE" :value 710}
           {:code "DE.MV" :value 963}
           {:code "DE.HB" :value 541}
           {:code "DE.HH" :value 622}
           {:code "DE.RP" :value 866}
           {:code "DE.SL" :value 398}
           {:code "DE.BY" :value 785}
           {:code "DE.SN" :value 223}
           {:code "DE.ST" :value 605}
           {:code "DE." :value 361}
           {:code "DE.NW" :value 237}
           {:code "DE.BW" :value 157}
           {:code "DE.HE" :value 134}
           {:code "DE.NI" :value 136}
           {:code "DE.TH" :value 704}])

(defn chart-config [geojson]
  {:chart {:type "pyramid"
           :margin-right 100}

   :title {:text "GeoJSON in HighMaps"}

   :map-navigation {:enabled true
                    :button-options
                    {:vertical-align "bottom"}}
   :series [{:data data
             :map-data geojson
             :join-by ["code_hasc" "code"]
             :name "Random data"
             :states {:hover {:color "#BADA55"}}
             :data-labels {:enabled true
                           :format "{point.properties.postal}"}}]})

