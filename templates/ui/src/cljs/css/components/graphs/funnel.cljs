(ns css.components.graphs.funnel)

(defn home-render []
  [:div {:style {:min-width "310px" :max-width "800px"
                 :height "400px" :margin "0 auto"}}])

(def chart-config
  {:chart {:type "pyramid"
           :margin-right 100
           :events {:load #()}}

   :title {:text "Sales funnel"
           :x -50}

   :plotOptions {:series
                 {:data-labels {:enabled true
                                :format "<b>{point.name}</b> ({point.y:,.0f})"
                                :soft-connector true}

                  :neck-width "30%"
                  :neck-height "25%"}}

   :tooltip {:enabled true}
   :legend {:enabled false}
   :series [{:name "Unique users"
             :data [["Website visits" 15654]
                    ["Downloads" 4064]
                    ["Requested price list" 1987]
                    ["Invoice sent" 976]
                    ["Finalized" 846]]}]})

