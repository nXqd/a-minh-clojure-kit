(ns css.components.back-to-top
  (:require
   [css.scroll :refer [scroll-to-top]]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui BackToTop
  Object
  (render
    [this]
    (sab/html
     (let [{:keys [style class time]} (om/props this)]
       [:div.back-to-top.grid
        {:class class
         :style (merge {:cursor "pointer"
                        :background "white"
                        :padding "0.5rem 0"
                        :justify-content "center"}
                       style)
         :on-click #(scroll-to-top)}
        "Back to top"]))))

(def back-to-top-ui (om/factory BackToTop))
