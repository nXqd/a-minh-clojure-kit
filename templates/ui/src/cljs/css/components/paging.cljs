(ns css.components.paging
  (:require
   [taoensso.timbre :as timbre :refer-macros [debug info]]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defn process-from-to
  [current per-page count]
  (let [offset      (int (/ per-page 4))
        right-count (max 1
                         (js/Math.ceil (/ count per-page)))]
    (if (< current (+ offset (int (/ per-page 2))))
      [1 right-count]
      [(max (- current (* 2 offset)) 0)
       (min (+ current (* 2 offset))
            (max 1 right-count))])))

;; refactor
(defui Paging
  Object
  (render
    [this]
    (let [{:keys [current
                  on-change
                  per-page
                  count
                  styles]} (om/props this)
          on-change        (if (fn? on-change) on-change #())
          per-page         (or per-page 10)
          [from to]        (process-from-to current per-page count)]
      (sab/html
       [:div.paging.button-group
        (let [below-zero? (<= (dec current) 0)]
          [:button.button
           {:class    (when below-zero? "disabled")
            :on-click (fn [e]
                        (when-not below-zero?
                          (on-change (- current 1))))
            :style    (:button styles)}
           [:i.fa.fa-chevron-left]])
        (for [i (range from (inc to) 1)]
          (let [active? (= i current)]
            [:button.button
             {:key      i
              :on-click #(when-not active?
                           (on-change i))
              :style    (:button styles)
              :class    (when active? "active")}
             (str i)]))
        (let [no-more? (>= (* current per-page) count)]
          [:button.button
           {:class    (when no-more? "disabled")
            :on-click (fn [e]
                        (when-not no-more?
                          (on-change (+ current 1))))
            :style    (:button styles)}
           [:i.fa.fa-chevron-right]])]))))

(def paging-ui (om/factory Paging))
