(ns css.components.step
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui Step
  Object
  (render
    [this]
    (sab/html
     [:div.ui.steps
      [:div.step
       [:i.fa.fa-truck]
       [:div.content
        [:div.title "Shipping"]
        [:div.description "Choose your shipping options"]]]
      [:div.step.active
       [:i.fa.fa-credit-card]
       [:div.content
        [:div.title "Billing"]
        [:div.description "Enter billing information"]]]
      [:div.step.disabled
       [:i.fa.fa-info]
       [:div.content
        [:div.title "Confirm Order"]]]])))

(def step-ui (om/factory Step))
