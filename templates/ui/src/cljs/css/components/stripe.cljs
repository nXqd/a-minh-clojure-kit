(ns css.components.stripe
  (:require
   [taoensso.timbre :as log]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui StripeCustom
  Object
  (initLocalState
   [this]
   {:ui {:card {:number "4242424242424242"
                :cvc "123"
                :exp_month "12"
                :exp_year "2017"}}})

  (componentDidMount
   [this]
   (.setPublishableKey js/Stripe "pk_0BCiy9S8LMWOk9i9lbjNgaUXlslrN"))

  (handleChange
   [this e]
   (let [states (om/get-state this)
         cur-card (get-in states [:ui :card])
         name (.. e -target -name)
         val (.. e -target -value)
         card (assoc cur-card (keyword name) val)]
     (om/update-state! this assoc-in [:ui :card] card)))

  (submit
   [this e]
   (.preventDefault e)
   (let [card (get-in (om/get-state this) [:ui :card])]
     (.createToken js/Stripe (clj->js card)
                   (fn [status resp]
                     (log/debug "status" status)))))

  (render
    [this]
    (sab/html
     (let [{:keys []} (om/props this)
           {:keys [ui]} (om/get-state this)
           {:keys [card]} ui]
       [:div.strip-component
        [:form
         {:action "/charge"
          :method "POST"
          :on-submit #(.submit js/this %)}
         [:input {:on-change #(.handleChange js/this %)
                  :value (:number card)
                  :type "text", :size "20", :name "number"}]
         [:input {:on-change #(.handleChange js/this %)
                  :value (:cvc card)
                  :type "text", :size "4", :name "cvc"}]
         [:input {:on-change #(.handleChange js/this %)
                  :value (:exp_month card)
                  :type "text", :size "2", :name "exp_month"}]
         [:input {:on-change #(.handleChange js/this %)
                  :value (:exp_year card)
                  :type "text", :size "4", :name "exp_year"}]
         [:button
          {:type "submit"} "Pay"]]]))))

(def stripe-custom-ui (om/factory StripeCustom))
