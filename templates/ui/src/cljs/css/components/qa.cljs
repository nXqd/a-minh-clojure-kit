(ns css.components.qa
  (:require
   [css.utils.components :refer [IStyle default-styles]]
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui QAItem
  IStyle
  (default-styles
    [this]
    {:left {:flex "initial"
            :alignItems "flex-start"
            :width "100px"}})
  Object
  (top-answer
   [this]
   (let [{:keys [question/answers]} (om/props this)]
     (first answers)))

  (render
    [this]
    (sab/html
     (let [{:keys [question/title
                   question/answers]} (om/props this)]
       [:div.qa-item.grid
        [:div.question.grid
         {:style {:margin-bottom "1rem"}}
         [:div.left.grid-cell
          {:style (:left (default-styles this))}
          "Questions: "]
         [:div.right.grid-cell
          [:a {:href "#"} title]]]
        [:div.answer.grid
         [:div.left.grid-cell
          {:style (:left (default-styles this))}
          "Answer: "]
         (when-let [{:keys [answer/content
                            answer/date
                            answer/user] :as answer} (.top-answer js/this)]
           [:div.right.grid-cell
            {:style {:flex-direction "column"}}
            [:p.grid content]
            [:p.meta.grid
             {:style {:color "#767676"}}
             (str "By " (:user/name user) " on " date)]])]]))))

(def qa-item-ui (om/factory QAItem {:keyfn :db/id}))

;; candidate backend qa/featured qa/full
;; TODO: change the empty state to realestate
;;       check if the project literally doesn't have any question then ...
(defui QA
  IStyle
  (default-styles
    [this]
    {:header {:margin-bottom "1rem"}})
  Object
  (search-no-value-state
   [this]
   [:div.no-value-state.grid
    [:div.main.grid
     {:style {:margin "1.5rem 0"
              :justify-content "center"
              :align-items "center"}}
     [:span
      {:style {:font-size "1rem"
               :margin-right "1rem"}}
      "Don't see what you looking for?"]
     [:button.button "Ask the Community"]]
    [:p.bottom.grid
     {:style {:justify-content "center"}}
     "We were unable to find any existing answers to your question. Please submit your question to our community of customers by selecting the 'Ask' button above. "]])
  (empty-state
   [this]
   [:div.empty-state.grid
    {:style {:line-height 1.9
             :margin "1.5rem"}}
    [:p.grid
     "Typical questions asked about products:"]
    [:ul.list
     (let [items ["- Is the item durable?"
                  "- Is this item easy to use?"
                  "- What are the dimensions of this item?"]
           item-ui (fn [i x]
                     [:li.list-item
                      {:key i
                       :style {:color "#777"}} x])]
       (map-indexed item-ui items))]])
  (render
    [this]
    (sab/html
     (let [{:keys [styles input project/questions]} (om/props this)]
       [:div.qa-component.grid
        {:style (:main styles)}
        [:h2.grid
         {:style (merge (:header (default-styles this))
                        (:header styles))}
         "Customer Questions & Answers"]
        [:div.form.grid
         {:style {:position "relative"}}
         [:i.fa.fa-search
          {:style {:position "absolute"
                   :top "50%"
                   :transform "translateY(-50%)"
                   :left "5px"}}]
         [:input.input.grid
          {:type "search"
           :value input
           :style {:padding "8px 8px 8px 25px"
                   :border "1px solid #ccc"
                   :border-radius "2px"}
           :placeholder "What can we help you? Search for topic or question..."}]]
        (if (empty? questions)
          (if-not input
            (.empty-state js/this)
            (.search-no-value-state js/this))
          [:div.qas.grid
           {:style {:margin-top "1.5rem"}}
           (map qa-item-ui questions)])]))))

(def qa-ui (om/factory QA))
