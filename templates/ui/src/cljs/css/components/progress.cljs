(ns css.components.progress
  (:require
   [cljs.test :refer-macros [is async]]
   [om.next :as om :refer-macros [defui]]
   [goog.dom :as gdom]
   [sablono.core :as sab :include-macros true]))

(defui Progress
  Object
  (render
    [this]
    (sab/html
     (let [{:keys [message percent colors]} (om/props this)]
       (when percent
         [:div.progress
          {:style {:display "flex"
                   :max-width "100%"
                   :border "none"
                   :box-shadow "none"
                   :background "rgba(0,0,0,.1)"
                   :padding 0
                   :border-radius "0.2rem"}}
          [:div.inner.inner--animated
           {:style {:width percent
                    :display "block"
                    :position "relative"
                    :min-width "2em"
                    :background "#888"
                    :color "white"
                    :line-height 2
                    :border-radius ".2rem"
                    :transition "width .1s ease,background-color .1s ease"}}
           (or message percent)]])))))

(def progress (om/factory Progress))
