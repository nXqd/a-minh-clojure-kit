(ns css.components.is-this-helpful
  (:require
   [om.next :as om :refer-macros [defui]]
   [sablono.core :as sab :include-macros true]))

(defui IsThisHelpful
  Object
  (render
    [this]
    (sab/html
     (let [{:keys [message styles class on-yes on-no]} (om/props this)
           on-yes (or on-yes #())
           on-no (or on-no #())
           message (or message "Was this helpful to you ?")]
       [:div.is-this-helpful
        {:class class
         :style (merge {:cursor "pointer"
                        :background "white"
                        :padding "0.5rem 0"
                        :justifyContent "center"}
                       (:main styles))}
        [:span.text
         {:style {:margin-right "1rem"}}
         message]
        [:span.buttons
         [:button.button.button--small
          {:on-click on-yes
           :style {:margin-right "1rem"}} "Yes"]
         [:button.button.button--small
          {:on-click on-no}
          "No"]]]))))

(def is-this-helpful-ui (om/factory IsThisHelpful))
