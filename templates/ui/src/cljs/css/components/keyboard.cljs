(ns css.components.keyboard
  (:require
   [cljs.test :refer-macros [is async]]
   [om.next :as om :refer-macros [defui]]
   [goog.dom :as gdom]
   [goog.events :as events]
   [sablono.core :as sab :include-macros true])
  (:import [goog.ui KeyboardShortcutHandler]))

(defui Keyboard
  Object
  (componentDidMount
   [this]
   (let [handler (doto
                  (KeyboardShortcutHandler. js/document)
                   (.registerShortcut 'A' 'a'))
         triggered-fn (fn [e]
                        (js/alert (str "Shortcut triggered" (.identifier e))))]
     (.log js/console handler)
     (events/listen handler
                    js/goog.ui.KeyboardShortcutHandler.EventType.SHORTCUT_TRIGGERED
                    triggered-fn)))

  (render
    [this]
    (sab/html
     [:div.test])))

(def keyboard-ui (om/factory Keyboard))
