(ns css.animation
  (:require
   [goog.events :as events]
   [goog.fx.dom :as fx-dom]))

;; more animation
;; https://github.com/dimsuz/trium/blob/ac57eed973028b24f70526eaca1d6ac1414b3b28/src/trium/anim-utils.cljs

;; check types in http://google.github.io/closure-library/api/namespace_goog_fx_dom.html
;; (dom-animate fx-dom/ 400 #())
(defn fade [x]
  (let [{:keys [type time callback]} x
        time                         (or time 1000)]
    (assert type)
    (fn [node]
      (let [anim (type. node time)]
        (when callback
          (events/listen anim js/goog.fx.Animation.EventType.END callback))
        (.play anim)))))

#_(defui ElementTest
    Object
    (componentDidMount
     [this]
     (let [node (dom/node this)]
       ((fade {:type     fx-dom/FadeOutAndHide
               :time     1000
               :callback #(log/debug "done")}) (dom/node this))))

    (render
      [this]
      (sab/html
       (let [{:keys []} (om/props this)]
         [:div.test "something"]))))

#_(def element-test-ui (om/factory ElementTest))

;; toggle
;; block componentWillUpdate
;; store animation in local-state, animate it then set the value to nil >> another rerender.
#_(defui Test2
    Object
    (initLocalState
     [this]
     {:animate-value "Something"
      :value         "Something"})
    (componentDidUpdate
     [this prev-props prev-state]
     (let [{:keys [value animate-value] :as state} (om/get-state this)]
       (when-not (= value animate-value)
         (let [fadeout-pred (fn [prev cur]
                              (and (seq prev)
                                   (empty? cur)))
               fadeout?     (fadeout-pred (:value prev-state) (:value state))
               fade-fn      (fn [type]
                              (fade {:type     type
                                     :time     1000
                                     :callback #(om/update-state! this assoc :animate-value value)}))
               node         (dom/node this "fade")
               fade         (if fadeout?
                              (fade-fn fx-dom/FadeOut)
                              (fade-fn fx-dom/FadeIn))]
           (fade node)))))
    (render
      [this]
      (sab/html
       (let [{:keys [value animate-value]} (om/get-state this)]
         [:div.test
          [:div.value
           {:style {:min-height "15px"}
            :ref   "fade"}
           animate-value]
          [:button.button
           {:on-click
            #(let [val (if (empty? value) "something" "")]
               (om/update-state! this assoc :value val))}
           "Toggle Fade"]]))))

#_(def test-2 (om/factory Test2))
