(ns css.scroll
  (:require
   [om.next :as om :refer-macros [defui]]
   [goog.dom :as gdom]
   [goog.style :as gstyle]
   [sablono.core :as sab :include-macros true])
  (:import [goog.fx.dom Scroll]))

(defn scroll-to-top
  ([] (scroll-to-top 500))
  ([time]
   (let [body (-> js/window .-parent .-document .-body)]
     (.play
      (Scroll. body
               (array 0 (.-scrollTop body))
               (array 0 0)
               time)))))

(defn scroll-to-selector!
  ([x] (when (map? x)
         (scroll-to-selector! js/document.body x)))
  ([el x]
   (when (gdom/isElement el)
     (let [{:keys [offset selector time]} x
           offset (or offset 0)
           time (or time 500)
           node (.querySelector el selector)
           body (gdom/getDocumentScrollElement)]
       (.play
        (Scroll. body
                 (array 0 (.-scrollTop body))
                 (array 0 (+ offset
                             (gstyle/getPageOffsetTop node))) time))))))

(defui ScrollingEl
  Object
  (render
    [this]
    (sab/html
     (let [{:keys []} (om/props this)]
       [:div.test.grid
        {:style {:height "400rem"
                 :flex-direction "column"
                 :justify-content "flex-end"}}
        "Scrolling down for actions boyz"
        [:div.component
         {:id "component"
          :ref "component"
          :style {:height "300px"}}
         "Component"]
        [:button.button
         {:on-click
          #(scroll-to-selector! (dom/node this) {:offset -100
                                                 :time 300
                                                 :selector "#component"})
          #_(scroll-to-selector! {:offset -100
                                  :time 300
                                  :selector "#component"})}
         "Scrolling to #component"]

        [:div.component1
         {:id "component1"
          :style {:height "300px"}}
         "Component"]]))))

(def scrolling-el-ui (om/factory ScrollingEl))
#_(defcard scrolling-el-card (scrolling-el-ui))
