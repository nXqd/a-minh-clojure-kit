(require '[figwheel-sidecar.repl :as r]
         '[figwheel-sidecar.repl-api :as ra])

(ra/start-figwheel!
 {:figwheel-options {:server-port 3500
                     :css-dirs ["resources/public/css"]}
  :build-ids ["devcards"]
  :all-builds
  [{:id "devcards"
    :figwheel {:devcards true}
    :source-paths ["src/cljs"]
    :compiler {:main 'devcards.main
               :asset-path "js/compiled/devcards_out"
               :output-to  "resources/public/js/compiled/css_devcards.js"
               :output-dir "resources/public/js/compiled/devcards_out"
               :parallel-build true
               :compiler-stats true
               :verbose true}}]})

(ra/cljs-repl)
