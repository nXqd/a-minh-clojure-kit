(ns pear.string
  (:require
   [clojure.string :as cstr]))

#?(:clj
   (defn strip-accents
     "Strip all accents (diacritical marks) from s.
     Et ça sera sa moitié => Et ca sera sa moitie"
     ^String [^String s]
     {:pre [(string? s)]
      :post [(string? %)]}
     (-> s
         (java.text.Normalizer/normalize java.text.Normalizer$Form/NFD)
         (.replaceAll "\\p{InCombiningDiacriticalMarks}+" ""))))

#?(:clj
   (defn translate
     "Translate all characters in s according to the mappings found in tmap.
     Any characters found in the set delete-chars will be pruned prior to
     consulting tmap.
     Any characters mapping to nil in tmap will also be deleted.
     (translate \"abba\" {\"a\" \"b\"}) => bbbb
     (translate \"abba\" {\"a\" \"b\", \"b\" \"a\"}) => baab
     (translate \"foo\" {\"a\" \"b\"}) =>  foo
     (translate \"gabba\" {\"a\" \"b\"} #{\"b\"}) => gbb
     (translate \"gabba\" {\\a nil} #{\\b}) => g"
     ([s tmap]
      (translate s tmap #{}))
     ([s tmap delete-chars]
      (->> s
           (remove delete-chars)
           (map (fn [c] (let [replacement (get tmap c :not-found)]
                          (cond
                            (= replacement :not-found) c
                            (nil? replacement) nil
                            :else replacement))))
           (remove nil?)
           (apply str)))))

(defn- compact-spaces
  [s]
  (cstr/join " " (cstr/split (cstr/trim s) #"\s+")))

#?(:clj
   (defn slug
     "Transform s so it's suitable for use in URLs.
     The following transformations are applied:
     * Diacritical marks are removed from all characters.
     * Any character which isn't alphanumeric or in #{_-.~} is removed.
     * Lower case
     * Whitespace is collapsed and replaced by a single dash."
     [^String s]
     {:pre [(string? s)]
      :post [(string? %)]}
     (-> s
         (cstr/replace #"-" " ")
         compact-spaces
         (cstr/replace #"\s+" "-")
         strip-accents
         (translate {"ł" "l"})
         (cstr/replace #"[^A-Za-z0-9_.~-]" "")
         (cstr/replace #"-+" "-")
         cstr/lower-case)))

(comment
 (slug "nguyen xuan quan dung")
 )
