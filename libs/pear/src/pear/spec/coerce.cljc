(ns pear.spec.coerce
  (:refer-clojure :exclude [def])
  (:require
   [clojure.spec.alpha :as s]
   [clojure.walk :as walk]
   [clojure.string :as str]
   #?(:clj
   [clojure.instant]))
  #?(:clj
     (:import (java.util UUID)
              (java.net URI))))

;; spec inspec
(defn- accept-keyword [x]
  (if (qualified-keyword? x) x))

(defn- accept-symbol [x]
  (if (qualified-symbol? x) x))

(defn- accept-symbol-call [spec]
  (if (and (seq? spec)
           (symbol? (first spec)))
    spec))

(defn safe-form
  "Return the spec form or nil."
  [spec]
  (if (contains? (s/registry) spec)
    (s/form spec)))

(defn form->spec
  "Return the spec, or first spec when input is a s/and."
  [and-spec]
  (if (and (seq? and-spec)
           (= (first and-spec) `s/and))
    (second and-spec)
    and-spec))

(defn spec->root-sym
  "Determine the main spec symbol from a spec form."
  [spec]
  (let [f (or (safe-form spec)
              (accept-symbol spec)
              (accept-symbol-call spec))]
    (let [spec-def (form->spec f)]
      (if (qualified-keyword? spec-def)
        (recur spec-def)
        spec-def))))

(defn parent-spec
  "Look up for the parent coercer using the spec hierarchy."
  [k]
  (or (-> (s/get-spec k) accept-keyword)
      (-> (form->spec (safe-form k)) accept-keyword)))

(s/fdef parent-spec
        :args (s/cat :k qualified-keyword?)
        :ret (s/nilable ::coerce-fn))

(defn registry-lookup
  "Look for the key in registry, if not found try key spec parent recursively."
  [registry k]
  (if-let [c (get registry k)]
    c
    (when-let [parent (-> (parent-spec k) accept-keyword)]
      (recur registry parent))))

(s/fdef registry-lookup
        :args (s/cat :registry map? :k qualified-keyword?)
        :ret any?)

;; spec-coerce
(declare coerce)

(defonce ^:private registry-ref (atom {}))

(defn parse-long [x]
  (if (string? x)
    (try
      #?(:clj  (Long/parseLong x)
         :cljs (if (= "NaN" x)
                 js/NaN
                 (let [v (js/parseInt x)]
                   (if (js/isNaN v) x v))))
      (catch #?(:clj Exception :cljs :default) _
        x))
    x))

(defn parse-double [x]
  (if (string? x)
    (try
      #?(:clj  (case x
                 "##-Inf" ##-Inf
                 "##Inf" ##Inf
                 "##NaN" ##NaN
                 "NaN" ##NaN
                 "Infinity" ##Inf
                 "-Infinity" ##-Inf
                 (Double/parseDouble x))
         :cljs (if (= "NaN" x)
                 js/NaN
                 (let [v (js/parseFloat x)]
                   (if (js/isNaN v) x v))))
      (catch #?(:clj Exception :cljs :default) _
        x))
    x))

(defn parse-uuid [x]
  (if (string? x)
    (try
      #?(:clj  (UUID/fromString x)
         :cljs (uuid x))
      (catch #?(:clj Exception :cljs :default) _
        x))
    x))

(defn parse-inst [x]
  (if (string? x)
    (try
      #?(:clj  (clojure.instant/read-instant-timestamp x)
         :cljs (js/Date. x))
      (catch #?(:clj Exception :cljs :default) _
        x))
    x))

(defn parse-boolean [x]
  (case x
    "true" true
    "false" false
    x))

(defn parse-keyword [x]
  (if (string? x)
    (if (str/starts-with? x ":")
      (keyword (subs x 1))
      (keyword x))
    x))

(defn parse-symbol [x]
  (if (string? x)
    (symbol x)
    x))

(defn parse-ident [x]
  (if (string? x)
    (if (str/starts-with? x ":")
      (parse-keyword x)
      (symbol x))
    x))

(defn parse-nil [x]
  (if (and (string? x)
           (#{"nil" "null"} (str/trim x)))
    nil
    x))

(defn parse-or [[_ & pairs]]
  (fn [x]
    (reduce
     (fn [x [_ pred]]
       (let [coerced (coerce pred x)]
         (if (= x coerced)
           x
           (reduced coerced))))
     x
     (partition 2 pairs))))

(defn parse-coll-of [[_ pred & _]]
  (fn [x]
    (if (sequential? x)
      (into (empty x) (map (partial coerce pred)) x)
      x)))

(defn parse-map-of [[_ kpred vpred & _]]
  (fn [x]
    (if (associative? x)
      (into {} (map (fn [[k v]]
                      [(coerce kpred k)
                       (coerce vpred v)]))
            x)
      x)))

#?(:clj
   (defn parse-decimal [x]
     (if (string? x)
       (if (str/ends-with? x "M")
         (bigdec (subs x 0 (dec (count x))))
         (bigdec x))
       x)))

#?(:clj
   (defn parse-uri [x]
     (if (string? x)
       (URI. x)
       x)))

(defmulti sym->coercer
          (fn [x]
            (if (sequential? x)
              (first x)
              x)))

(defmethod sym->coercer `number? [_] parse-double)
(defmethod sym->coercer `integer? [_] parse-long)
(defmethod sym->coercer `int? [_] parse-long)
(defmethod sym->coercer `pos-int? [_] parse-long)
(defmethod sym->coercer `neg-int? [_] parse-long)
(defmethod sym->coercer `nat-int? [_] parse-long)
(defmethod sym->coercer `even? [_] parse-long)
(defmethod sym->coercer `odd? [_] parse-long)
(defmethod sym->coercer `float? [_] parse-double)
(defmethod sym->coercer `double? [_] parse-double)
(defmethod sym->coercer `boolean? [_] parse-boolean)
(defmethod sym->coercer `ident? [_] parse-ident)
(defmethod sym->coercer `simple-ident? [_] parse-ident)
(defmethod sym->coercer `qualified-ident? [_] parse-ident)
(defmethod sym->coercer `keyword? [_] parse-keyword)
(defmethod sym->coercer `simple-keyword? [_] parse-keyword)
(defmethod sym->coercer `qualified-keyword? [_] parse-keyword)
(defmethod sym->coercer `symbol? [_] parse-symbol)
(defmethod sym->coercer `simple-symbol? [_] parse-symbol)
(defmethod sym->coercer `qualified-symbol? [_] parse-symbol)
(defmethod sym->coercer `uuid? [_] parse-uuid)
(defmethod sym->coercer `inst? [_] parse-inst)
(defmethod sym->coercer `nil? [_] parse-nil)
(defmethod sym->coercer `false? [_] parse-boolean)
(defmethod sym->coercer `true? [_] parse-boolean)
(defmethod sym->coercer `zero? [_] parse-long)
(defmethod sym->coercer `s/or [form] (parse-or form))
(defmethod sym->coercer `s/coll-of [form] (parse-coll-of form))
(defmethod sym->coercer `s/map-of [form] (parse-map-of form))

#?(:clj (defmethod sym->coercer `uri? [_] parse-uri))
#?(:clj (defmethod sym->coercer `decimal? [_] parse-decimal))

(defmethod sym->coercer :default [_] identity)

(defn- keys-parser
  [[_ & {:keys [req-un opt-un]}]]
  (let [keys-mapping (into {} (map #(vector (keyword (name %)) %) (concat req-un opt-un)))]
    (fn [x]
      (with-meta
       (reduce-kv (fn [m k v]
                    (assoc m k (coerce (or (keys-mapping k) k) v)))
                  {}
                  x)
       (meta x)))))

(defmethod sym->coercer `s/keys
  [form]
  (keys-parser form))

(defn infer-coercion [k]
  "Infer a coercer function from a given spec."
  (-> (spec->root-sym k)
      (sym->coercer)))

(defn coerce-fn [k]
  "Get the coercing function from a given key. First it tries to lookup the coercion
  on the registry, otherwise try to infer from the specs. In case nothing is found, identity function is returned."
  (or (when (qualified-keyword? k)
        (registry-lookup @registry-ref k))
      (infer-coercion k)))

(defn coerce [k x]
  "Coerce a value x using coercer k. This function will first try to use
  a coercer from the registry, otherwise it will try to infer a coercer from
  the spec with the same name. Coercion will only be tried if x is a string.
  Returns original value in case a coercer can't be found."
  (if-let [coerce-fn (coerce-fn k)]
    (coerce-fn x)
    x))

(defn ^:skip-wiki def-impl [k coerce-fn]
  (assert (and (ident? k) (namespace k)) "k must be namespaced keyword")
  (swap! registry-ref assoc k coerce-fn)
  k)

(s/fdef def-impl
        :args (s/cat :k qualified-keyword?
                     :coercion ifn?)
        :ret any?)

(defmacro def
  "Given a namespace-qualified keyword, and a coerce function, makes an entry in the
  registry mapping k to the coerce function."
  [k coercion]
  `(def-impl '~k ~coercion))

(s/fdef def
        :args (s/cat :k qualified-keyword?
                     :coercion any?)
        :ret qualified-keyword?)

(defn coerce-structure
  "Recursively coerce map values on a structure."
  ([x] (coerce-structure x {}))
  ([x {::keys [overrides]}]
   (walk/prewalk (fn [x]
                   (if (map? x)
                     (with-meta (into {} (map (fn [[k v]]
                                                (let [coercion (get overrides k k)]
                                                  [k (coerce coercion v)]))) x)
                                (meta x))
                     x))
                 x)))

(comment
 ;; Examples
 ; Define a spec as usual
 (s/def ::number int?)

 ; Call the coerce method passing the spec and the value to be coerced
 (coerce ::number "42") ; => 42

 ; Like spec generators, when using `and` it will use the first item as the inference source
 (s/def ::odd-number (s/and int? odd?))
 (coerce ::odd-number "5") ; => 5

 ; When inferring the coercion, it tries to resolve the upmost spec in the definition
 (s/def ::extended (s/and ::odd-number #(> % 10)))
 (coerce ::extended "11") ; => 11

 ; If you wanna play around or use a specific coercion, you can pass the predicate symbol directly
 (coerce `int? "40") ; => 40

 ; Parsers are written to be safe to call, when unable to coerce they will return the original value
 (coerce `int? "40.2") ; => "40.2"
 (coerce `inst? "date") ; => "date"

 ; To leverage map keys and coerce a composed structure, use coerce-structure
 (coerce-structure {::number      "42"
                    ::not-defined "bla"
                    :sub          {::odd-number "45"}})
 ; => {::number      42
 ;     ::not-defined "bla"
 ;     :sub          {::odd-number 45}}

 ; coerce-structure supports overrides, so you can set a custom coercer for a specific context, and can be also a point
 ; to set coercer for unqualified keys
 (coerce-structure {::number      "42"
                    ::not-defined "bla"
                    :unqualified  "12"
                    :sub          {::odd-number "45"}}
                   {::overrides {::not-defined `keyword?
                                 :unqualified  ::number}})
 ; => {::number      42
 ;     ::not-defined :bla
 ;     :unqualified  12
 ;     :sub          {::odd-number 45}}

 ; If you want to set a custom coercer for a given spec, use the spec-coerce registry
 (defrecord SomeClass [x])
 (s/def ::my-custom-attr #(instance? SomeClass %))
 (def ::my-custom-attr #(map->SomeClass {:x %}))

 ; Custom registered keywords always takes precedence over inference
 (coerce ::my-custom-attr "Z") ; => #user.SomeClass{:x "Z"}

 ; Numbers
 (coerce `number? "42")                                   ; => 42.0
 (coerce `integer? "42")                                  ; => 42
 (coerce `int? "42")                                      ; => 42
 (coerce `pos-int? "42")                                  ; => 42
 (coerce `neg-int? "-42")                                 ; => -42
 (coerce `nat-int? "10")                                  ; => 10
 (coerce `even? "10")                                     ; => 10
 (coerce `odd? "9")                                       ; => 9
 (coerce `float? "42.42")                                 ; => 42.42
 (coerce `double? "42.42")                                ; => 42.42
 (coerce `zero? "0")                                      ; => 0

 ; Numbers on CLJS
 (coerce `int? "NaN")                                     ; => js/NaN
 (coerce `double? "NaN")                                  ; => js/NaN

 ; Booleans
 (coerce `boolean? "true")                                ; => true
 (coerce `boolean? "false")                               ; => false
 (coerce `true? "true")                                   ; => true
 (coerce `false? "false")                                 ; => false

 ; Idents
 (coerce `ident? ":foo/bar")                              ; => :foo/bar
 (coerce `ident? "foo/bar")                               ; => 'foo/bar
 (coerce `simple-ident? ":foo")                           ; => :foo
 (coerce `qualified-ident? ":foo/baz")                    ; => :foo/baz
 (coerce `keyword? "keyword")                             ; => :keyword
 (coerce `keyword? ":keyword")                            ; => :keyword
 (coerce `simple-keyword? ":simple-keyword")              ; => :simple-keyword
 (coerce `qualified-keyword? ":qualified/keyword")        ; => :qualified/keyword
 (coerce `symbol? "sym")                                  ; => 'sym
 (coerce `simple-symbol? "simple-sym")                    ; => 'simple-sym
 (coerce `qualified-symbol? "qualified/sym")              ; => 'qualified/sym

 ; Collections
 (coerce `(s/coll-of int?) ["5" "11" "42"])               ; => [5 11 42]
 (coerce `(s/coll-of int?) ["5" "11.3" "42"])             ; => [5 "11.3" 42]
 (coerce `(s/map-of keyword? int?) {"foo" "42" "bar" "31"})
 ; => {:foo 42 :bar 31}

 ; Branching
 ; tests are realized in order
 (coerce `(s/or :int int? :bool boolean?) "40")           ; 40
 (coerce `(s/or :int int? :bool boolean?) "true")         ; true
 ; returns original value when no options can handle
 (coerce `(s/or :int int? :bool boolean?) "nil")          ; "nil"

 ; Others
 (coerce `uuid? "d6e73cc5-95bc-496a-951c-87f11af0d839")   ; => #uuid "d6e73cc5-95bc-496a-951c-87f11af0d839"
 (coerce `inst? "2017-07-21")                             ; => #inst "2017-07-21T00:00:00.000000000-00:00"
 (coerce `nil? "nil")                                     ; => nil
 (coerce `nil? "null")                                    ; => nil

 ;; Clojure only:
 (coerce `uri? "http://site.com") ; => (URI. "http://site.com")
 (coerce `decimal? "42.42") ; => 42.42M
 (coerce `decimal? "42.42M") ; => 42.42M
 )
