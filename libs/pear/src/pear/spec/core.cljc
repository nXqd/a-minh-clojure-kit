(ns pear.spec.core
  (:require
   #?@(:clj [[clojure.spec.alpha :as s]
             [clojure.spec.gen.alpha :as gen]]
       :cljs [[clojure.test.check.generators]
              [cljs.reader :refer [read-string]]
              [cljs.spec.alpha :as s]
              [cljs.spec.gen.alpha :as gen]])))

;; all specs here should support conformer and gen

(s/def ::not-nil #(not (nil? %)))

;; conformer
(defn x-string?
  [x]
  (str x))

(defn x-integer?
  [x]
  #?(:clj
     (cond
       (integer? x) x
       (string? x) (try
                     (Long/parseLong x)
                     (catch Exception e
                       ::s/invalid))
       :else ::s/invalid)))

(defn x-double?
  [x]
  #?(:clj
     (cond
       (double? x) x
       (int? x) (double x)
       (string? x) (try
                     (Double/parseDouble x)
                     (catch Exception e
                       ::s/invalid))
       :else ::s/invalid)))

(defn x-boolean?
  [x]
  #?(:clj
     (cond
       (boolean? x) x
       (string? x) (cond
                     (= "true" x) true
                     (= "false" x) false
                     :else ::s/invalid)
       :else ::s/invalid)))

(defn x-keyword?
  [x]
  #?(:clj
     (cond
       (keyword? x) x
       (string? x) (keyword x)
       :else ::s/invalid)))

;; gen
(defn x-int-gen
  []
  (gen/fmap #(str %) (gen/int)))

(defn x-integer-gen
  []
  (gen/fmap #(str %) (gen/large-integer)))

(defn x-double-gen
  []
  (gen/fmap #(str %) (gen/double)))

(defn x-boolean-gen
  []
  (gen/fmap #(str %) (gen/boolean)))

(defn x-keyword-gen
  []
  (gen/fmap #(name %) (gen/keyword)))

(defn x-not-empty-string
  []
  (gen/fmap #(str %)
            (gen/such-that
             #(not= % "")
             (gen/string-alphanumeric))))

(s/def ::x-integer
  (s/with-gen (s/conformer x-integer? (fn [_ v] (str v))) x-integer-gen))
(s/def ::x-double
  (s/with-gen (s/conformer x-double? (fn [_ v] (str v))) x-double-gen))
(s/def ::x-boolean
  (s/with-gen (s/conformer x-boolean? (fn [_ v] (str v))) x-boolean-gen))
(s/def ::x-not-empty-string
  (s/with-gen (s/conformer x-string? (fn [_ v] (str v))) x-not-empty-string))

(defn uuid-str?
  "Checks string uuid"
  [s]
  (uuid? (read-string (str "#uuid \"" s "\""))))

;; email
(def mail-hosts ["gmail.com" "yahoo.com" "hotmail.com" "sendmail.com" "mail.com" "email.com"
                 "aol.com" "123x.com"])

(defn gen-email
  []
  (gen/fmap (fn [[name domain-name]]
              (str name "@" domain-name))
            (gen/tuple (gen/string-alphanumeric) (gen/elements mail-hosts))))

(def email-regex #"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,63}$")

(defn email?
  [^String s]
  (boolean (re-matches email-regex s)))

(s/def ::email-type
  (s/with-gen
    (s/and string? email?)
    gen-email))

;; domain
(def domain-extensions ["com" "net" "info" "shop" "io" "ly" "us" "co.uk" "co.au" "org"
                        "cm" "ca"])
(defn gen-domain []
  (gen/fmap
   (fn [[domain extension]] (str domain "." extension))
   (gen/tuple (gen/string-alphanumeric) (gen/elements domain-extensions))))

(def domain-regex #"([a-zA-Z0-9][a-zA-Z0-9\-]{0,61}[a-zA-Z0-9]?\.)+[a-zA-Z]{2,6}")

(defn domain?
  [^String s]
  (boolean (re-matches domain-regex s)))

(s/def ::domain-type
  (s/spec (s/and string? domain?)
          :gen gen-domain))

(defn str-limit
  ([s] (str-limit s 0 255))
  ([s upper] (str-limit s 0 upper))
  ([s lower upper] (and string?
                        (<= lower (count s) upper))))

(s/def ::num-non-negative (s/and number? #(< 0 %)))
(s/def ::password-type (s/and string? #(>= 255 (count %) 6)))
