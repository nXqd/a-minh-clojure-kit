(ns pear.spec.utils
  (:require
   #?@(:clj [[clojure.spec.alpha :as s]]
       :cljs [[cljs.spec.alpha :as s]])))

(defn dev-mode
  "Turn on clojure spec checking in development mode or not."
  ([] (dev-mode true))
  ([b]
   (s/check-asserts b)))

(comment
 (dev-mode true))