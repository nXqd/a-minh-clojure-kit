(ns pear.csv.core
  (:require
   [clojure.java.io :as io]
   [clojure.data.csv :as csv]
   [clojure.spec.alpha :as s])
  (:import (org.apache.commons.io.input BOMInputStream)))

(defn csv-data->maps
  [csv-data]
  (let [headers
        (->> (filter not-empty (first csv-data))                      ;; First row is the header, filter empty header
             (map keyword) ;; Drop if you want string keys instead
)]
    (map zipmap
         (->> headers repeat)
         (rest csv-data))))

(s/fdef read-csv
        :ret (s/coll-of vector?))
(defn read-csv
  "Read csv file to clojure ."
  [resource-path]
  (with-open [reader (-> (io/resource resource-path)
                         io/input-stream
                         BOMInputStream.
                         io/reader)]
    (let [data (csv/read-csv reader)]
      (->> (csv-data->maps data)
           (into [])))))

(defn write-csv
  "Write clojure data to csv"
  [path row-data]
  (let [columns (keys (first row-data))
        headers (map name columns)
        rows (mapv #(mapv % columns) row-data)]
    (with-open [file (io/writer path)]
      (csv/write-csv file (cons headers rows)))))

(comment
  (read-csv "test.csv"))
