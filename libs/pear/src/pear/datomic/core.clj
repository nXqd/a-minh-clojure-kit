(ns pear.datomic.core
  (:require
   [clojure.spec.alpha :as s]
   [datomic.api :as d]
   [pear.spec.core :as ps]))

;; https://github.com/Datomic/day-of-datomic/blob/master/tutorial/schema_queries.clj#L17:1
;; database internal helper functions
(s/def ::dbid int?)

;; find the idents of all schema elements in the schema
(defn schema-idents
  [db]
  (->> (d/q '[:find [?ident ...]
              :where [_ :db/ident ?ident]]
            db)
       sort))

;; find just the attributes
(defn schema-attrs
  [db]
  (sort (d/q '[:find [?ident ...]
               :where
               [?e :db/ident ?ident]
               [_ :db.install/attribute ?e]]
             db)))

;; find just the data functions
(defn schema-data-fns
  [db]
  (sort (d/q '[:find [?ident ...]
               :where
               [?e :db/ident ?ident]
               [_ :db.install/function ?e]]
             db)))

;; internal datomic
(defn tx->create-dbid
  "Get first db/id from transaction."
  [tx]
  (->> tx :tempids vals first))

(defn tx->update-dbid
  ":e in :eav"
  [tx]
  (->> tx :tx-data first :e))

(defn tx->upsert-dbid
  [entity tx]
  (or (:db/id entity)
      (tx->create-dbid tx)))

;; utilities
(defn find-entity-dbids
  "Return set of vector"
  [db attr val]
  (d/q '[:find ?e
         :in $ ?attr ?val
         :where [?e ?attr ?val]]
       db attr val))

(s/fdef find-dbids
        :args (s/cat :db ::ps/not-nil :attr keyword? :val ::ps/not-nil))
(defn find-dbids
  [db attr val]
  (let [entities (find-entity-dbids db attr val)]
    (->> entities (map first))))

(s/fdef find-dbid
        :args (s/cat :db ::ps/not-nil :attr keyword? :val ::ps/not-nil))
(defn find-dbid
  [db attr val]
  (->> (find-dbids db attr val) first))

(s/fdef existed?
        :args (s/cat :db ::ps/not-nil :attr keyword? :val ::ps/not-nil))
(defn existed?
  [db attr val]
  (->> (find-dbid db attr val) boolean))

(s/fdef transact
        :args (s/cat :conn ::ps/not-nil :entity map?)
        :ret ::dbid)
(defn transact
  "Create single transaction and returns db/id or true if updated.
   In case you want to double check results without 'real' insert, use d/transact instead"
  [conn entity]
  (let [tx-res @(d/transact conn [entity])]
    (->> tx-res :tempids vals first)))

(s/fdef find-all-pull
        :args (s/cat :conn ::ps/not-nil :attr keyword?))
(defn find-all-pull
  [db attr]
  (->> (d/q '[:find (pull ?e [*])
              :in $ ?attr
              :where [?e ?attr]]
            db attr)
       flatten))

(defn find-entities-by-pull
  [db attr val]
  (->> (d/q '[:find (pull ?e [*])
              :in $ ?attr ?val
              :where [?e ?attr ?val]]
            db attr val)
       flatten))

(s/fdef find-entity-by-pull
        :args (s/cat :db ::ps/not-nil :attr qualified-keyword? :val ::ps/not-nil)
        :ret map?)
(defn find-entity-by-pull
  [db attr val]
  (->> (find-entities-by-pull db attr val) first))

(s/fdef find-or-create-by
        :args (s/cat :conn ::ps/not-nil :entity map? :val ::ps/not-nil)
        :ret ::dbid)
(defn find-or-create-by
  "Find or create entity by attr"
  [conn entity attr]
  (let [val (get entity attr)]
    (if-let [dbid (find-dbid (d/db conn) attr val)]
      dbid
      (transact conn entity))))

(defn find-and-update-by
  [conn entity attr]
  (let [val (get entity attr)]
    (if-let [dbid (find-dbid (d/db conn) attr val)]
      dbid
      (transact conn entity))))

(defn upsert-by
  "Update or create by attr.
   - if this entity has db/id then just transact
     if not then find-or-create-by"
  [conn {:keys [db/id] :as entity} attr]
  (if-not (nil? id)
    (transact conn entity)
    (find-or-create-by conn entity attr)))

(defn retract-by-dbid
  [conn dbid]
  @(d/transact conn [[:db.fn/retractEntity dbid]]))

(comment
 ;; usage in domain's project
  (defn find-users-by-username
    [db username]
    (find-entities-by-pull db :user/full-name username))

  (defn find-companies-by-name
    [db company-name]
    (find-entities-by-pull db :company/name company-name))

  (defn find-or-create-company
    [conn entity]
    (find-or-create-by conn entity :company/name))

  (defn find-or-create-user
    [conn entity]
    (find-or-create-by conn entity :user/full-name))


 ;; internal tools
 (def tx
   {:db-before :db-before,
    :db-after :db/after,
    :tx-data [[13194139534324 50 #inst "2018-05-15T10:47:00.578-00:00" 13194139534324 true]], :tempids {:tempid1 13194139534324}})

 (tx->create-dbid tx)
 (tx->update-dbid tx))
