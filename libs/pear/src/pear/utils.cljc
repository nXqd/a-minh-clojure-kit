(ns pear.utils
  (:require
   [clojure.spec.alpha :as s]
   [clojure.string :as cstr]
   [clojure.walk :as walk]))

(defn transform-keys
  "Recursively transforms all map keys in coll with t."
  [t coll]
  (let [f (fn [[k v]] [(t k) v])]
    (walk/postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) coll)))

(defn ->kebab-case
  "converts to keywordize kebab case."
  [s & {:keys [keywordize]
        :or {keywordize false}}]
  (cond-> (cstr/replace s #"_" "-")
          keywordize keyword))

(defn ->snake-case
  "converts to keywordize kebab case."
  [x & {:keys [keywordize]
        :or {keywordize false}}]
  (let [x (if (keyword? x) (name x) x)
        keywordize (if (keyword? x) true keywordize)]
    (cond-> (cstr/replace x #"-" "_")
            keywordize keyword)))

(defn ->strip-ns
  "converts to keywordize kebab case."
  [s & {:keys [keywordize]
        :or {keywordize false}}]
  (cond-> (name s)
          keywordize keyword))

;; sugar
(s/fdef ->kebab-case-coll
        :args (s/cat :m map?)
        :ret map?)
(defn ->kebab-case-coll
  [m & {:keys [keywordize]}]
  (transform-keys
   #(->kebab-case % :keywordize keywordize) m))

(s/fdef ->snake-case-coll
        :args (s/cat :m map?)
        :ret map?)
(defn ->snake-case-coll
  [m & {:keys [keywordize]}]
  (transform-keys
   #(->snake-case % :keywordize keywordize) m))

(defn ->strip-ns-coll
  [m & {:keys [keywordize]}]
  (transform-keys
   #(->strip-ns % :keywordize keywordize) m))

(comment
 (->kebab-case-coll {"a_b" "c"})
 (->snake-case-coll {"a-b" "c"})
 (->snake-case-coll {:a-b "c"})
 (->snake-case-coll {:a-b "c"} :keywordize true)
 (->strip-ns-coll {:a/b "c"} :keywordize true))