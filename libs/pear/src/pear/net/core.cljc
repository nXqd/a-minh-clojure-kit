(ns pear.net.core
  (:refer-clojure :exclude [replace read-string])
  (:require
   [clojure.string :refer [blank? join replace split upper-case]]
   #?(:clj [clojure.edn :refer [read-string]])
   #?(:cljs [cljs.reader :refer [read-string]]))
  #?(:clj (:import [java.net URLEncoder URLDecoder])))

(def ^:const port-number
  {:amqp 5672
   :http 80
   :https 443
   :mysql 3306
   :postgresql 5432
   :rabbitmq 5672
   :zookeeper 2181})

(def ^:const url-regex #"([^:]+)://(([^:]+):([^@]+)@)?(([^:/]+)(:([0-9]+))?((/[^?#]*)(\?([^#]*))?)?)(\#(.*))?")

(defn compact-map
  "Removes all map entries where the value of the entry is empty."
  [m]
  (reduce
    (fn [m k]
      (let [v (get m k)]
        (if (or (nil? v)
                (and (or (map? v)
                         (sequential? v))
                     (empty? v)))
          (dissoc m k) m)))
    m (keys m)))

(defn url-encode
  "Returns `s` as an URL encoded string."
  [s & [encoding]]
  (when s
    #?(:clj (-> (URLEncoder/encode (str s) (or encoding "UTF-8"))
                (replace "%7E" "~")
                (replace "*" "%2A")
                (replace "+" "%20"))
       :cljs (-> (js/encodeURIComponent (str s))
                 (replace "*" "%2A")))))

(defn url-decode
  "Returns `s` as an URL decoded string."
  [s & [encoding]]
  (when s
    #?(:clj (URLDecoder/decode s (or encoding "UTF-8"))
       :cljs (js/decodeURIComponent s))))

(defn parse-query-params
  "Parse the query parameter string `s` and return a map."
  [s]
  (if s
    (->> (split (str s) #"&")
         (map #(split %1 #"="))
         (filter #(= 2 (count %1)))
         (mapcat #(vector (keyword (url-decode (first %1))) (url-decode (second %1))))
         (apply hash-map))))

(defn format-query-params
  "Format the map `m` into a query parameter string."
  [m]
  (let [params (->> (sort-by first (seq m))
                    (remove #(blank? (str (second %1))))
                    (map #(vector (url-encode (name (first %1)))
                                  (url-encode (second %1))))
                    (map #(join "=" %1))
                    (join "&"))]
    (if-not (blank? params)
      params)))

(defn- apply-unit [number unit]
  (if (string? unit)
    (case (upper-case unit)
      (case unit
        "M" (* number 1000000)
        "B" (* number 1000000000)))
    number))

(defn- parse-number [s parse-fn]
  (if-let [matches (re-matches #"\s*([-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?)(M|B)?.*" (str s))]
    #?(:clj
       (try (let [number (parse-fn (nth matches 1))
                  unit (nth matches 3)]
              (apply-unit number unit))
         (catch NumberFormatException _ nil))
       :cljs
       (let [number (parse-fn (nth matches 1))
             unit (nth matches 3)]
         (if-not (js/isNaN number)
           (apply-unit number unit))))))

(defn parse-integer
  "Parse `s` as a integer number."
  [s]
  (parse-number s #(#?(:clj Integer/parseInt :cljs js/parseInt) %1)))

(defn parse-url
  "Parse the url `s` and return a Ring compatible map."
  [s]
  (if-let [matches (re-matches url-regex (str s))]
    (let [scheme (keyword (nth matches 1))]
      (compact-map
       {:scheme scheme
        :username (nth matches 3)
        :password (nth matches 4)
        :server-name (nth matches 6)
        :server-port (or (parse-integer (nth matches 8)) (port-number scheme))
        :uri (nth matches 10)
        :query-params (parse-query-params  (nth matches 12))
        :query-string (nth matches 12)
        :fragment (nth matches 14)}))))

(defn format-url
  "Format the Ring map as an url."
  [m]
  (if (not (empty? m))
    (let [query-params (:query-params m)]
      (str (if (:scheme m)
             (str (name (:scheme m)) "://"))
           (let [{:keys [username password]} m]
             (when username
               (str username (when password (str ":" password)) "@")))
           (:server-name m)
           (if-let [port (:server-port m)]
             (if-not (= port (port-number (:scheme m)))
               (str ":" port)))
           (if (and (nil? (:uri m))
                    (not (empty? query-params)))
             "/" (:uri m))
           (if-not (empty? query-params)
             (str "?" (format-query-params query-params)))
           (if-not (blank? (:fragment m))
             (str "#" (:fragment m)))))))
