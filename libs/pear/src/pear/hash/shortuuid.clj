(ns pear.hash.shortuuid
  (:require
   [clojure.spec.alpha :as s])
  (:import
   [pear.hash ShortUuid]))

(s/fdef generate
        :ret string?)
(defn generate
  "Generates an up to n character long base62 secure random string."
  []
  (ShortUuid/randomNanoId))

(comment
 (generate))
