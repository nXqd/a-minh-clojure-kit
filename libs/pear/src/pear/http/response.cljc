(ns pear.http.response)

;; TODO: make cljc works too
#?(:clj
   (do (defmacro defstatus
         "Define helper functions to generate and check HTTP status codes."
         [status name]
         (let [status# status name# name]
           `(do
              (defn ~name#
                ~(str "Returns a Ring response with the HTTP " status#
                      " status code.")
                [& [~'body ~'headers]]
                {:status ~status :body ~'body :headers (or ~'headers {})})
              (defn ~(symbol (str name# "?"))
                ~(str "Returns true if the request has the HTTP " status#
                      " status code, otherwise false.")
                [~'request]
                (= ~status# (:status ~'request))))))

     ;; INFORMATIONAL

     (defstatus 100 continue)
     (defstatus 101 switching-protocols)
     (defstatus 102 processing)
     (defstatus 103 checkpoint)

     ;; SUCESS

     (defstatus 200 ok)
     (defstatus 201 created)
     (defstatus 202 accepted)
     (defstatus 203 non-authoritative-information)
     (defstatus 204 no-content)
     (defstatus 205 reset-content)
     (defstatus 206 partial-content)
     (defstatus 207 multi-status)
     (defstatus 226 im-used)

     ;; REDIRECTION

     (defstatus 300 multiple-choices)
     (defstatus 301 moved-permanently)
     (defstatus 302 found)
     (defstatus 303 see-other)
     (defstatus 304 not-modified)
     (defstatus 305 use-proxy)
     (defstatus 306 switch-proxy)
     (defstatus 307 temporary-redirect)
     (defstatus 308 resume-incomplete)

     ;; CLIENT ERROR

     (defstatus 400 bad-request)
     (defstatus 401 unauthorized)
     (defstatus 402 payment-required)
     (defstatus 403 forbidden)
     (defstatus 404 not-found)
     (defstatus 405 method-not-allowed)
     (defstatus 406 not-acceptable)
     (defstatus 407 proxy-authentication-required)
     (defstatus 408 request-timeout)
     (defstatus 409 conflict)
     (defstatus 410 gone)
     (defstatus 411 length-required)
     (defstatus 412 precondition-failed)
     (defstatus 413 request-entity-too-large)
     (defstatus 414 request-uri-too-long)
     (defstatus 415 unsupported-media-type)
     (defstatus 416 requested-range-not-satisfiable)
     (defstatus 417 expectation-failed)
     (defstatus 418 im-a-teapot)
     (defstatus 422 unprocessable-entity)
     (defstatus 423 locked)
     (defstatus 424 failed-dependency)
     (defstatus 425 unoredered-collection)
     (defstatus 426 upgrade-required)
     (defstatus 444 no-response)
     (defstatus 449 retry-with)
     (defstatus 450 blocked-by-windows-parental-controls)
     (defstatus 499 client-closed-request)

     ;; SERVER ERROR

     (defstatus 500 internal-server-error)
     (defstatus 501 not-implemented)
     (defstatus 502 bad-gateway)
     (defstatus 503 service-unavailable)
     (defstatus 504 gateway-timeout)
     (defstatus 505 http-version-not-supported)
     (defstatus 506 variant-also-negotiates)
     (defstatus 507 insufficient-storage)
     (defstatus 509 bandwidth-limit-exceeded)
     (defstatus 510 not-extended)))
