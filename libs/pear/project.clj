(defproject pear "MASTER"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[expound "0.5.0" :scope "provided"]
                 [org.clojure/clojure "1.10.0" :scope "provided"]
                 [org.clojure/data.csv "0.1.4" :scope "dev"]
                 [org.clojure/clojurescript "1.10.439" :exclusions [com.google.guava/guava] :scope "provided"]
                 [ch.qos.logback/logback-classic "1.1.7" :exclusions [org.slf4j/slf4j-api]]
                 [commons-io "2.6" :scope "provided"]
                 [com.datomic/datomic-free "0.9.5697" :scope "dev"]
                 [org.clojure/test.check "0.9.0"]]
  :java-source-paths ["src/pear"]
  :pedantic? :abort
  :plugins [[lein-doo "0.1.6" :scope "provided"]
            [lein-cljsbuild "1.1.2" :scope "provided"]]
  :cljfmt {:indents ~(clojure.edn/read-string (slurp ".cljfmt.edn"))}
  :cljsbuild {:builds
              {:test
               {:source-paths ["src" "test"]
                :compiler {:output-to "target/main.js"
                           :output-dir "target"
                           :main pear.test-runner
                           :optimizations :simple}}}}
  :doo {:paths {:rhino "lein run -m org.mozilla.javascript.tools.shell.Main"}}
  :aliases
  {"test-cljs" ["doo" "rhino" "test" "once"]
   "test-clj"  ["trampoline" "run" "-m" "pear.eftest-runner/main"]
   "test-all"  ["do" ["test-clj"] ["test-cljs"]]}
  :resource-paths ["config", "resources"]
  :profiles {:test {:dependencies [[org.mozilla/rhino "1.7.7"]]}
             :dev {:source-paths ["dev"]
                   :dependencies [[eftest "0.1.4"]]
                   :plugins [[lein-cljfmt "0.5.7" :exclusions [org.clojure/google-closure-library-third-party
                                                               org.clojure/google-closure-library
                                                               com.google.javascript/closure-compiler
                                                               org.clojure/clojurescript]]]
                   :repl-options {:init-ns user}}})
