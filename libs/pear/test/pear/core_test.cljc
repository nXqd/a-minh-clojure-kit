(ns pear.core-test
  #?(:clj (:import [clojure.lang ArityException]))
  (:require #?(:clj  [clojure.test :refer :all]
               :cljs [cljs.test :refer-macros [deftest is testing]])
            [pear.core :as p]))

(deftest test-find-first
  (is (= (p/find-first even? [7 3 3 2 8]) 2))
  (is (nil? (p/find-first even? [7 3 3 7 3]))))

(deftest test-dissoc-in
  (is (= (p/dissoc-in {:a {:b {:c 1 :d 2}}} [:a :b :c])
         {:a {:b {:d 2}}}))
  (is (= (p/dissoc-in {:a {:b {:c 1}}} [:a :b :c])
         {}))
  (is (= (p/dissoc-in {:a {:b {:c 1} :d 2}} [:a :b :c])
         {:a {:d 2}}))
  (is (= (p/dissoc-in {:a 1} [])
         {:a 1})))

(deftest test-assoc-some
  (is (= (p/assoc-some {:a 1} :b 2) {:a 1 :b 2}))
  (is (= (p/assoc-some {:a 1} :b nil) {:a 1}))
  (is (= (p/assoc-some {:a 1} :b 2 :c nil :d 3) {:a 1 :b 2 :d 3})))

(deftest test-map-entry
  (is (= (key (p/map-entry :a 1)) :a))
  (is (= (val (p/map-entry :a 1)) 1))
  (is (= (first  (p/map-entry :a 1)) :a))
  (is (= (second (p/map-entry :a 1)) 1))
  (is (= (type (p/map-entry :a 1))
         (type (first {:a 1})))))

(deftest test-map-kv
  (is (= (p/map-kv (fn [k v] [(name k) (inc v)]) {:a 1 :b 2})
         {"a" 2 "b" 3}))
  (is (= (p/map-kv (fn [k v] [(name k) (inc v)]) (sorted-map :a 1 :b 2))
         {"a" 2 "b" 3}))
  (is (= (p/map-kv (fn [k v] (p/map-entry (name k) (inc v))) {:a 1 :b 2})
         {"a" 2 "b" 3})))

(deftest test-map-keys
  (is (= (p/map-keys name {:a 1 :b 2})
         {"a" 1 "b" 2}))
  (is (= (p/map-keys name (sorted-map :a 1 :b 2))
         (sorted-map "a" 1 "b" 2))))

(deftest test-map-vals
  (is (= (p/map-vals inc {:a 1 :b 2})
         {:a 2 :b 3}))
  (is (= (p/map-vals inc (sorted-map :a 1 :b 2))
         (sorted-map :a 2 :b 3))))

(deftest test-filter-kv
  (is (= (p/filter-kv (fn [k v] (and (keyword? k) (number? v))) {"a" 1 :b 2 :c "d"})
         {:b 2}))
  (is (= (p/filter-kv (fn [k v] (= v 2)) (sorted-map "a" 1 "b" 2))
         (sorted-map "b" 2))))

(deftest test-filter-keys
  (is (= (p/filter-keys keyword? {"a" 1 :b 2})
         {:b 2}))
  (is (= (p/filter-keys #(re-find #"^b" %) (sorted-map "a" 1 "b" 2))
         (sorted-map "b" 2))))

(deftest test-filter-vals
  (is (= (p/filter-vals even? {:a 1 :b 2})
         {:b 2}))
  (is (= (p/filter-vals even? (sorted-map :a 1 :b 2))
         (sorted-map :b 2))))

(deftest test-remove-kv
  (is (= (p/remove-kv (fn [k v] (and (keyword? k) (number? v))) {"a" 1 :b 2 :c "d"})
         {"a" 1 :c "d"}))
  (is (= (p/remove-kv (fn [k v] (= v 2)) (sorted-map "a" 1 "b" 2))
         (sorted-map "a" 1))))

(deftest test-remove-keys
  (is (= (p/remove-keys keyword? {"a" 1 :b 2})
         {"a" 1}))
  (is (= (p/remove-keys #(re-find #"^b" %) (sorted-map "a" 1 "b" 2))
         {"a" 1})))

(deftest test-remove-vals
  (is (= (p/remove-vals even? {:a 1 :b 2})
         {:a 1}))
  (is (= (p/remove-keys #(re-find #"^b" %) (sorted-map "a" 1 "b" 2))
         {"a" 1})))

(deftest test-queue
  (testing "empty"
           #?(:clj  (is (instance? clojure.lang.PersistentQueue (p/queue)))
              :cljs (is (instance? cljs.core.PersistentQueue (p/queue))))
           (is (empty? (p/queue))))
  (testing "not empty"
           #?(:clj  (is (instance? clojure.lang.PersistentQueue (p/queue [1 2 3])))
              :cljs (is (instance? cljs.core.PersistentQueue (p/queue [1 2 3]))))
           (is (= (first (p/queue [1 2 3])) 1))))

(deftest test-queue?
  #?(:clj  (is (p/queue? clojure.lang.PersistentQueue/EMPTY))
     :cljs (is (p/queue? cljs.core.PersistentQueue.EMPTY)))
  (is (not (p/queue? []))))

(deftest test-least
  (is (= (p/least) nil))
  (is (= (p/least "a") "a"))
  (is (= (p/least "a" "b") "a"))
  (is (= (p/least 3 2 5 -1 0 2) -1)))

(deftest test-greatest
  (is (= (p/greatest) nil))
  (is (= (p/greatest "a") "a"))
  (is (= (p/greatest "a" "b") "b"))
  (is (= (p/greatest 3 2 5 -1 0 2) 5)))

(deftest test-mapply
  (letfn [(foo [& {:keys [bar]}] bar)]
    (is (= (p/mapply foo {}) nil))
    (is (= (p/mapply foo {:baz 1}) nil))
    (is (= (p/mapply foo {:bar 1}) 1)))
  (letfn [(foo [bar & {:keys [baz]}] [bar baz])]
    (is (= (p/mapply foo 0 {}) [0 nil]))
    (is (= (p/mapply foo 0 {:baz 1}) [0 1]))
    (is (= (p/mapply foo 0 {:spam 1}) [0 nil]))
    (is (= (p/mapply foo 0 nil) [0 nil]))
    #?@(:clj  [(is (thrown? ArityException (p/mapply foo {})))
               (is (thrown? IllegalArgumentException (p/mapply foo 0)))]
        :cljs [(is (thrown? js/Error (p/mapply foo 0)))])))

(deftest test-interleave-all
  (is (= (p/interleave-all []) []))
  (is (= (p/interleave-all [1 2 3]) [1 2 3]))
  (is (= (p/interleave-all [1 2 3] [4 5 6]) [1 4 2 5 3 6]))
  (is (= (p/interleave-all [1 2 3] [4 5 6] [7 8 9]) [1 4 7 2 5 8 3 6 9]))
  (is (= (p/interleave-all [1 2] [3]) [1 3 2]))
  (is (= (p/interleave-all [1 2 3] [4 5]) [1 4 2 5 3]))
  (is (= (p/interleave-all [1] [2 3] [4 5 6]) [1 2 4 3 5 6])))

(deftest test-distinct-by
  (testing "sequences"
           (is (= (p/distinct-by count ["a" "ab" "c" "cd" "def"])
                  ["a" "ab" "def"]))
           (is (= (p/distinct-by count [])
                  []))
           (is (= (p/distinct-by first ["foo" "faa" "boom" "bar"])
                  ["foo" "boom"])))

  (testing "transucers"
           (is (= (into [] (p/distinct-by count) ["a" "ab" "c" "cd" "def"])
                  ["a" "ab" "def"]))
           (is (= (into [] (p/distinct-by count) [])
                  []))
           (is (= (into [] (p/distinct-by first) ["foo" "faa" "boom" "bar"])
                  ["foo" "boom"]))))

(deftest test-dedupe-by
  (testing "sequences"
           (is (= (p/dedupe-by count ["a" "b" "bc" "bcd" "cd"])
                  ["a" "bc" "bcd" "cd"]))
           (is (= (p/dedupe-by count [])
                  []))
           (is (= (p/dedupe-by first ["foo" "faa" "boom" "bar"])
                  ["foo" "boom"])))

  (testing "transucers"
           (is (= (into [] (p/dedupe-by count) ["a" "b" "bc" "bcd" "cd"])
                  ["a" "bc" "bcd" "cd"]))
           (is (= (into [] (p/dedupe-by count) [])
                  []))
           (is (= (into [] (p/dedupe-by first) ["foo" "faa" "boom" "bar"])
                  ["foo" "boom"]))))

(deftest test-take-upto
  (testing "sequences"
           (is (= (p/take-upto zero? [1 2 3 0 4 5 6]) [1 2 3 0]))
           (is (= (p/take-upto zero? [0 1 2 3 4 5 6]) [0]))
           (is (= (p/take-upto zero? [1 2 3 4 5 6 7]) [1 2 3 4 5 6 7])))

  (testing "tranducers"
           (is (= (into [] (p/take-upto zero?) [1 2 3 0 4 5 6]) [1 2 3 0]))
           (is (= (into [] (p/take-upto zero?) [0 1 2 3 4 5 6]) [0]))
           (is (= (into [] (p/take-upto zero?) [1 2 3 4 5 6 7]) [1 2 3 4 5 6 7]))
           (is (= (transduce (p/take-upto zero?)
                             (completing (fn [_ x] (reduced x)))
                             nil
                             [0 1 2])
                  0))))

(deftest test-drop-upto
  (testing "sequences"
           (is (= (p/drop-upto zero? [1 2 3 0 4 5 6]) [4 5 6]))
           (is (= (p/drop-upto zero? [0 1 2 3 4 5 6]) [1 2 3 4 5 6]))
           (is (= (p/drop-upto zero? [1 2 3 4 5 6 7]) [])))

  (testing "transducers"
           (is (= (into [] (p/drop-upto zero?) [1 2 3 0 4 5 6]) [4 5 6]))
           (is (= (into [] (p/drop-upto zero?) [0 1 2 3 4 5 6]) [1 2 3 4 5 6]))
           (is (= (into [] (p/drop-upto zero?) [1 2 3 4 5 6 7]) []))))

(deftest test-indexed
  (testing "sequences"
           (is (= (p/indexed [:a :b :c :d])
                  [[0 :a] [1 :b] [2 :c] [3 :d]]))
           (is (= (p/indexed [])
                  [])))

  (testing "transducers"
           (is (= (into [] (p/indexed) [:a :b :c :d])
                  [[0 :a] [1 :b] [2 :c] [3 :d]]))
           (is (= (into [] (p/indexed) [])
                  []))))

(deftest test-abs
  (is (= (p/abs -3) 3))
  (is (= (p/abs 2) 2))
  (is (= (p/abs -2.1) 2.1))
  (is (= (p/abs 1.8) 1.8))
  #?@(:clj [(is (= (p/abs -1/3) 1/3))
            (is (= (p/abs 1/2) 1/2))
            (is (= (p/abs 3N) 3N))
            (is (= (p/abs -4N) 4N))]))

(deftest test-deref-swap!
  (let [a (atom 0)]
    (is (= (p/deref-swap! a inc) 0))
    (is (= @a 1))
    (is (= (p/deref-swap! a inc) 1))
    (is (= @a 2))))

(deftest test-deref-reset!
  (let [a (atom 0)]
    (is (= (p/deref-reset! a 3) 0))
    (is (= @a 3))
    (is (= (p/deref-reset! a 1) 3))
    (is (= @a 1))))

(deftest test-ex-message
  (is (= (p/ex-message (ex-info "foo" {})) "foo"))
  (is (= (p/ex-message (new #?(:clj Exception :cljs js/Error) "bar")) "bar")))

(deftest test-ex-cause
  (let [cause (new #?(:clj Exception :cljs js/Error) "foo")]
    (is (= (p/ex-cause (ex-info "foo" {} cause)) cause))
    #?(:clj (is (= (p/ex-cause (Exception. "foo" cause)) cause)))))

(deftest test-uuid?
  (let [x #uuid "d1a4adfa-d9cf-4aa5-9f05-a15365d1bfa6"]
    (is (p/uuid? x))
    (is (not (p/uuid? 2)))
    (is (not (p/uuid? (str x))))
    (is (not (p/uuid? nil)))))

(deftest test-uuid
  (let [x (p/uuid "d1a4adfa-d9cf-4aa5-9f05-a15365d1bfa6")]
    (is (instance? #?(:clj java.util.UUID :cljs cljs.core.UUID) x))
    (is (= x #uuid "d1a4adfa-d9cf-4aa5-9f05-a15365d1bfa6"))))

(deftest test-random-uuid
  (let [x (p/random-uuid)
        y (p/random-uuid)]
    (is (instance? #?(:clj java.util.UUID :cljs cljs.core.UUID) x))
    (is (instance? #?(:clj java.util.UUID :cljs cljs.core.UUID) y))
    (is (not= x y))))

(deftest test-deep-merge
  (let [x {:a {:c :d}}
        y {:a {:e :f}}]
    (is (= {:a {:c :d :e :f}} (p/deep-merge x y)))))
