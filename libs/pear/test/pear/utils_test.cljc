(ns pear.utils-test
  #?(:clj (:import [clojure.lang ArityException]))
  (:require #?(:clj  [clojure.test :refer :all]
               :cljs [cljs.test :refer-macros [deftest is testing]])
            [pear.utils :as sut]))

(deftest transform-keys-test
  (are [x f expected]
       (= (sut/transform-keys f x) expected)
    {"a" "b"} keyword {:a "b"}
    {"a_b" "b"} sut/->kebab-case {"a-b" "b"}
    {"a_b" "b"} (comp keyword sut/->kebab-case) {:a-b "b"}))

