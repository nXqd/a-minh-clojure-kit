(ns pear.http.response-test
  (:require
   [pear.http.response :as response]
   #?(:clj [clojure.test :refer :all]
      :cljs [cljs.test :refer-macros [are deftest is]])))

#?(:clj
   (deftest http-response-test
     (let [resp {:status 201}
           created-resp (response/created {:a :b})]
       (is (= created-resp {:headers {}, :status 201, :body {:a :b}}))
       (is (response/created? resp)))))
