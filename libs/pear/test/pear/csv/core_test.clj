(ns pear.csv.core-test
  (:require
   [pear.csv.core :as sut]
   [clojure.test :refer :all]))

(deftest read-csv-test
  (let [data (sut/read-csv "test.csv")]
    (is (= 3 (count data)))))
