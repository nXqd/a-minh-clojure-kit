(ns pear.eftest-runner
  (:require
   [eftest.runner :refer [find-tests run-tests]]))

(defn ^:export main
  []
  (run-tests (find-tests "test"))
  (System/exit 0))
