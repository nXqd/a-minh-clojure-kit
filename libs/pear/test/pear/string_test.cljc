(ns pear.string-test
  (:require
   [pear.string :refer :all]
   #?@(:clj [[clojure.test :refer :all]]
       :cljs [[cljs.test :refer-macros [deftest is are]]])))

#?(:clj
   (deftest slug-test
     (are [original-string slugged]
          (is (= (slug original-string) slugged))
       "Hưng Phúc ---- Happy Residence-" "hung-phuc-happy-residence")))
