(ns pear.spec.core-test
  (:require
   [pear.spec.core :as sut]
   #?@(:clj [[clojure.spec.alpha :as s]
             [clojure.test :refer :all]]
       :cljs [[cljs.spec.alpha :as s]
              [cljs.test :refer-macros [are is deftest]]])))

(deftest shared-x-type-spec
  (is (s/exercise ::sut/x-boolean))
  (is (s/exercise ::sut/x-integer))
  (is (s/exercise ::sut/x-integer))
  (is (s/exercise ::sut/x-not-empty-string)))

(deftest custom-spec-test
  (is (s/exercise ::sut/domain-type))
  (is (s/exercise ::sut/email-type)))

(deftest uuid-str?-test
  (is (sut/uuid-str? "a097e68f-3ed8-53c0-ae5c-b76e32859677")))
