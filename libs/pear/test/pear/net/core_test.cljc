(ns pear.net.core-test
  (:require
   [pear.net.core :as c]
   #?(:clj [clojure.test :refer :all]
      :cljs [cljs.test :refer-macros [are is deftest]])))

(deftest test-compact-map
  (are [x expected]
       (= expected (c/compact-map x))
    nil nil
    {} {}
    {:x nil} {}
    {:x []} {}
    {:x {}} {}
    {:x ["x"]} {:x ["x"]}
    {:x {:a 1}} {:x {:a 1}}))

(deftest test-url-encode
  (are [s expected]
       (= expected (c/url-encode s))
    nil nil
    1 "1"
    "" ""
    "a" "a"
    " " "%20"
    "*" "%2A"
    "~" "~"))

(deftest test-parse-query-params
  (are [s expected]
       (= expected (c/parse-query-params s))
    nil nil
    "" {}
    "a=1" {:a "1"}
    "a=1&b=2&c=%2A" {:a "1" :b "2" :c "*"}))

(deftest test-url-decode
  (are [s expected]
       (= expected (c/url-decode s))
    nil nil
    "" ""
    "a" "a"
    "1" "1"
    "%20" " "
    "%2A" "*"
    "~" "~"))

(deftest test-format-url
  (is (nil? (c/format-url nil)))
  (are [s]
       (= (dissoc (c/parse-url s) :query-string)
          (dissoc (c/parse-url (c/format-url (c/parse-url s))) :query-string))
    "http://example.com/"
    "https://example.com/"
    "http://bob:secret@example.com/"
    "https://bob:secret@example.com/"
    "https://bob:secret@example.com/?a=1&b=2"
    "https://bob:secret@example.com/?a=1&b=2&c=%2A"
    "https://bob:secret@example.com/?a=1&b=2&c=%2A#_=_"))
