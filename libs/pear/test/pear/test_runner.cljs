(ns pear.test-runner
  (:require
   [doo.runner :refer-macros [doo-all-tests]]
   [pear.core-test]
   [pear.http.response-test]
   [pear.net.core-test]
   [pear.spec.core-test]))

(doo-all-tests)
